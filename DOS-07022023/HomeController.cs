﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Models;
using System.Data.OleDb;
using System.Data;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }
        public ActionResult ReadExcel(string ExcelPath)
        {
            InitializeOLEDBConnection(ExcelPath);
            //DataTable a = ReadFile();
            return View();
        }
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        public void InitializeOLEDBConnection(string filePath)
        {
            string connString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + filePath + "; Extended Properties=\"Excel 8.0 HDR=Yes;IMEX=1\"";
            OleDbConnection cnn;
            cnn = new OleDbConnection(connString);
        }
    }
}
