﻿$(document).ready(function () {


    var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];


    $("#TeamID").change(function () {

        if ($(this).val() != "") {
            var getCenter = $(this).find("option:selected").text();


            if (getCenter == "Manchester Team 9") {
                $("#Center").val("Beaconsfield");
            }
            else if (getCenter == "Manchester Team 7") {
                $("#Center").val("Pimlico");
            }
            else {
                //var getCenter = $(this).find("option:selected").text().split(" ");

                $("#Center").val(getCenter.split(" ")[0]);
            }
            
        }
        else {
            $("#Center").val("");
        }
    });


    $("#MatterTypeID").change(function () {

        if ($(this).val() != "") {

            if ($(this).find("option:selected").text() == "SLR" || $(this).find("option:selected").text() == "Re-mortgage") {

                var getMattertype = $(this).find("option:selected").text();
                $("#TransactionType").val(getMattertype);
            }
            else {
                $("#TransactionType").val("Transactional");
            }
        }
        else {
            $("#TransactionType").val("");
        }
    });


    $("#ExpectedCompletionDate").change(function () {

        var getExpectedCompletionDate = new Date($(this).val());
        var generateDateFomrat = months[getExpectedCompletionDate.getMonth()] + '-' + getExpectedCompletionDate.getFullYear().toString().substr(-2);
        $("#CompMonth").val(generateDateFomrat);

    });

    $("#PackChecked").keyup(function () {
        var getPackChecked = $(this).val();
        var generateDateFomrat = getPackChecked.substr(0, 2);
        $("#CheckedBy").val(generateDateFomrat);

    });

});