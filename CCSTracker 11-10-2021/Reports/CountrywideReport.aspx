﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CountrywideReport.aspx.cs" Inherits="CountryWide.Reports.CountrywideReport" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body style="margin: 0px; padding: 0px;">
    <form id="form1" runat="server">
        <div>
            <asp:ScriptManager ID="ScriptManager1" runat="server">
                <Scripts>
                    
                </Scripts>
            </asp:ScriptManager>
            <rsweb:ReportViewer ID="CountrywideReportViewer" runat="server"></rsweb:ReportViewer>
        </div>
    </form>
</body>
</html>
