﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DASReportRun.Models
{
    public class DASSource
    {
        public string Source_Office{get;set;}
        public string Source_Short{get;set;}
        public DateTime Source_Date{get;set;}
        public string Source_Transaction { get; set; }
        public string Source_Category { get; set; }
        public string Source_Ref { get; set; }
        public string Source_Address { get; set; }
        public double Source_Amount { get; set; }
        public string Source_Tenant { get; set; }
        public int Source_Claims { get; set; }
        public string Source_Period { get; set; }
        public int Source_Rent { get; set; }
        public int Source_RowID { get; set; }

  
       

    }
}