﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DOSUI.Models
{
    public class MenuItems
    {
        public int MenuId { get; set; }
        public string MenuName { get; set; }
    }
}