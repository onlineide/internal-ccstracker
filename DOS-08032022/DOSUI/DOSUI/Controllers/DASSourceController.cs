﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using Excel = Microsoft.Office.Interop.Excel;
using DASReportRun.Models;
using System.Configuration;
using System.Data;
using System.Data.OleDb;

namespace DASReportRun.Controllers
{
    public class DASSourceController : Controller
    {
        // GET: DAS_Source_

        //public ActionResult Base()
        //{
        //    return View();
        //}

        [HttpPost]
        ////public ActionResult Import(HttpPostedFileBase excelFile)
        ////{
        ////    if (Session["model"] == null)
        ////    {
        ////        if (excelFile == null || excelFile.ContentLength == 0)
        ////        {
        ////            ViewBag.error = "Please select a excel file";
        ////            return View("Index");
        ////        }
        ////        else
        ////        {
        ////            if (excelFile.FileName.EndsWith("xls") || excelFile.FileName.EndsWith("xlsx"))
        ////            {

        ////                string filePath = string.Empty;

        ////                string path = Server.MapPath("~/Content/");

        ////                if (!Directory.Exists(path))

        ////                {

        ////                    Directory.CreateDirectory(path);

        ////                }

        ////                //get file path

        ////                filePath = path + Path.GetFileName(excelFile.FileName);

        ////                //get file extenstion

        ////                string extension = Path.GetExtension(excelFile.FileName);

        ////                //save file on "Uploads" folder of project

        ////                excelFile.SaveAs(filePath);



        ////                string conString = string.Empty;

        ////                //check file extension

        ////                switch (extension)

        ////                {

        ////                    case ".xls": //Excel 97-03.

        ////                       conString = ConfigurationManager.ConnectionStrings["Excel03ConString"].ConnectionString;
        ////                       // conString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + filePath + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";


        ////                        break;

        ////                    case ".xlsx": //Excel 07 and above.
        ////                        //conString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filePath + ";Extended Properties=\"Excel 8.0;HDR=Yes\"";
        ////                        conString = ConfigurationManager.ConnectionStrings["Excel07ConString"].ConnectionString;

        ////                        break;

        ////                }



        ////                //create datatable object

        ////                DataTable dt = new DataTable();

        ////                conString = string.Format(conString, filePath);



        ////                //Use OldDb to read excel

        ////                using (OleDbConnection connExcel = new OleDbConnection(conString))

        ////                {

        ////                    using (OleDbCommand cmdExcel = new OleDbCommand())

        ////                    {

        ////                        using (OleDbDataAdapter odaExcel = new OleDbDataAdapter())

        ////                        {

        ////                            cmdExcel.Connection = connExcel;



        ////                            //Get the name of First Sheet.

        ////                            connExcel.Open();

        ////                            DataTable dtExcelSchema;

        ////                            dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);

        ////                            string sheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();

        ////                            connExcel.Close();



        ////                            //Read Data from First Sheet.

        ////                            connExcel.Open();

        ////                            cmdExcel.CommandText = "SELECT * From [" + sheetName + "]";

        ////                            odaExcel.SelectCommand = cmdExcel;

        ////                            odaExcel.Fill(dt);

        ////                            connExcel.Close();

        ////                        }

        ////                    }

        ////                }


        ////                List<DASSource> DASSourceList = new List<DASSource>();
        ////                for (int i = 2; i < dt.Rows.Count; i++)
        ////                {
        ////                    DASSource d = new DASSource();
        ////                    d.Source_Office = dt.Rows[i][0].ToString();
        ////                    d.Source_Short = dt.Rows[i][1].ToString();
        ////                    d.Source_Date = DateTime.Parse(dt.Rows[i][3].ToString());
        ////                    d.Source_Transaction = dt.Rows[i][4].ToString();
        ////                    d.Source_Category = dt.Rows[i][5].ToString();
        ////                    d.Source_Ref = dt.Rows[i][6].ToString();
        ////                    d.Source_Address = dt.Rows[i][7].ToString();
        ////                    d.Source_Amount = Convert.ToDouble(dt.Rows[i][8].ToString());
        ////                    d.Source_Tenant = dt.Rows[i][9].ToString();
        ////                    d.Source_Claims = Convert.ToInt32(dt.Rows[i][10].ToString());
        ////                    d.Source_Period = dt.Rows[i][11].ToString();
        ////                    d.Source_Rent = Convert.ToInt32(dt.Rows[i][12].ToString());

        ////                    d.Source_RowID = i - 1;
        ////                    DASSourceList.Add(d);
        ////                }
        ////                ViewBag.DASSourceList = DASSourceList;
        ////                Session["model"] = DASSourceList;
        ////                return View("Index", DASSourceList);
        ////                // return View("Success");
        ////                //return View(listsource);
        ////            }


        ////            else
        ////            {
        ////                ViewBag.error = "File Type is incorrect<br>";

        ////                return View("Index");
        ////            }

        ////        }
        ////    }
        ////    else
        ////    {
        ////        return View("Index", Session["model"]);
        ////    }
        ////}
        public ActionResult Import(HttpPostedFileBase excelFile)
        {
            if (Session["model"] == null)
            {
                if (excelFile == null || excelFile.ContentLength == 0)
                {
                    ViewBag.error = "Please select a excel file";
                    return View("Index");
                }
                else
                {
                    if (excelFile.FileName.EndsWith("xls") || excelFile.FileName.EndsWith("xlsx"))
                    {

                        string path1 = Server.MapPath("~/Content/");
                        string path = path1 + Path.GetFileName(excelFile.FileName);
                        //string path = excelFile.FileName;

                        if (System.IO.File.Exists(path))
                        {
                            System.IO.File.Delete(path);
                        }
                        
                        
                            excelFile.SaveAs(path);
                        
                        using (var stream = System.IO.File.Open(path, FileMode.Open, FileAccess.Write, FileShare.ReadWrite))
                        {
                            Excel.Application application = new Excel.Application();
                            Excel.Workbook workbook = application.Workbooks.Open(path);
                            Excel.Worksheet worksheet = workbook.ActiveSheet;
                            Excel.Range range = worksheet.UsedRange;
                            List<DASSource> DASSourceList = new List<DASSource>();
                            for (int row = 2; row < range.Rows.Count; row++)
                            {
                                DASSource d = new DASSource();
                                d.Source_Office = ((Excel.Range)range.Cells[row, 1]).Text;
                                d.Source_Short = ((Excel.Range)range.Cells[row, 2]).Text;
                                d.Source_Date = DateTime.Parse(((Excel.Range)range.Cells[row, 4]).Text);
                                d.Source_Transaction = ((Excel.Range)range.Cells[row, 5]).Text;
                                d.Source_Category = ((Excel.Range)range.Cells[row, 6]).Text;
                                d.Source_Ref = ((Excel.Range)range.Cells[row, 7]).Text;
                                d.Source_Address = ((Excel.Range)range.Cells[row, 8]).Text;
                                d.Source_Amount = Convert.ToDouble(((Excel.Range)range.Cells[row, 9]).Text);
                                d.Source_Tenant = ((Excel.Range)range.Cells[row, 10]).Text;
                                d.Source_Claims = Convert.ToInt32(((Excel.Range)range.Cells[row, 11]).Text);
                                d.Source_Period = ((Excel.Range)range.Cells[row, 12]).Text;
                                d.Source_Rent = Convert.ToInt32(((Excel.Range)range.Cells[row, 13]).Text);

                                d.Source_RowID = row - 1;
                                DASSourceList.Add(d);
                            }
                            ViewBag.DASSourceList = DASSourceList;
                            Session["model"] = DASSourceList;
                            return View("Index", DASSourceList);
                            // return View("Success");
                            //return View(listsource);
                        }
                    }

                    else
                    {
                        ViewBag.error = "File Type is incorrect<br>";

                        return View("Index");
                    }
                }
            }
            else
            {
                return View("Index", Session["model"]);
            }
        }
        public ActionResult Index()
        {
            //return View();
            //return null;
            //List<DAS_Source_> listsource = new List<DAS_Source_>();
            //listsource = null;
            //return View(listsource);

            var model = (List<DASSource>)Session["model"];
            
            return View(model);

        }

        public JsonResult SaveSource(DASSource model)
        {

            var modelList = (List<DASSource>)Session["model"];
            if(model.Source_RowID>0)
            {
                var index = modelList.FindIndex(x => x.Source_RowID == model.Source_RowID);
                modelList[index] = model;
            }
            else
            {

                var item = modelList.LastOrDefault();
                model.Source_RowID = item.Source_RowID + 1;
                modelList.Add(model);
            }
            Session["model"] = modelList;
            return Json(new { Source_RowID = model.Source_RowID, Source_Office = model.Source_Office, Source_Short=model.Source_Short, Source_Date = model.Source_Date, Source_Transaction = model.Source_Transaction , Source_Category=model.Source_Category, Source_Ref=model.Source_Ref, Source_Address=model.Source_Address, Source_Amount=model.Source_Amount, Source_Tenant=model.Source_Tenant, Source_Claims=model.Source_Claims, Source_Period=model.Source_Period, Source_Rent=model.Source_Rent});
        }

        public JsonResult DeleteSource(DASSource model)
        {

            var modelList = (List<DASSource>)Session["model"];
            var item = modelList.Find(x => x.Source_RowID == model.Source_RowID);
            modelList.Remove(item);
            Session["model"] = modelList;
            return Json(new { Source_RowID = model.Source_RowID });
        }
    }
}