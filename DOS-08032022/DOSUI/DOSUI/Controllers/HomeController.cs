﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DOSUI.Models;
using System.Security;

namespace DOSUI.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            string userName = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            ViewBag.UserName = userName;
            GetProcessControlStage();


            return View();
        }

        public int GetProcessControlStage()
        {
            int cntProcessControl = 0;

            ViewBag.CntProcessControlStage = 1;
            return cntProcessControl;
        }
        public ActionResult FirstStage()
        {

            string userName = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            ViewBag.UserName = userName;
            GetProcessControlStage();
            return View("~/Views/Home/_FirstStage.cshtml");
        }

        public ActionResult About()
        {

            

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}