
/****** Object:  StoredProcedure [dbo].[usp_UnallocatedFunds_Download]    Script Date: 20/05/2022 14:18:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

 -- Modified date: <16-04-2021> <Included Exception handling>
      
 --usp_UnallocatedFunds_Download 20,201,'View1'
ALTER Procedure [dbo].[usp_UnallocatedFunds_Download]            
@TrackerId int,          
@processId int,
@ViewName Varchar(100)              
          
AS              
Begin         
 BEGIN TRY        
              
  select UAF.ID,UAF.[Date],UAF.Amount,UPM.PaymentMethod,UAF.UnallocatedNumber,
  UAF.PaymentDetails,UAF.CANotes,UAF.MatterNumber,UAF.UnallocatedMoniesVF2,
   sts.[Status]      
      INTO #FilterResult
from tblUnallocatedFundsForm UAF       
LEFT JOIN [dbo].[tblUnallocatedFundsPaymentMethod] UPM on UAF.PaymentmethodID = UPM.ID      
LEFT JOIN [dbo].[tblUnallocatedFundsStatus] sts ON UAF.StatusID = sts.ID      
--LEFT JOIN [dbo].[tblUnallocatedFundsUnallocatedNo] UAFUA ON UAF.UnallocatedNoID = UAFUA.ID      
        
        
 where UAF.ID in (select distinct FormDataId        
    FROM [dbo].[tblTrackerForms]          
    with (nolock) where TrackerId=@TrackerId and ProcessId=@processId  )         
 order by 1 asc        
        
		 DECLARE @sql nvarchar(max);

SELECT  @sql = 'SELECT * FROM #FilterResult ' + dbo.FuncDynamicQueryResult(@TrackerId, @ViewName)

EXEC sp_executesql @sql

      

END TRY
BEGIN CATCH

        DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),
                @ErrorSeverity INT = ERROR_SEVERITY(),
                @ErrorNumber INT = ERROR_NUMBER();

		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);
 
END CATCH
DROP TABLE #FilterResult

     
End 
