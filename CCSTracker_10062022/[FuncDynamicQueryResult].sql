USE [CCSQA]
GO
/****** Object:  UserDefinedFunction [dbo].[FuncDynamicQueryResult]    Script Date: 31/05/2022 10:02:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
ALTER FUNCTION [dbo].[FuncDynamicQueryResult]
(
	@TrackerId int,
	@ViewName  Varchar(1000)
)
RETURNS nvarchar(max)
AS
BEGIN
	
	DECLARE @spResultModel varchar(300)  
DECLARE @SqlOperator varchar(20)  
DECLARE @Condition varchar(10)    
DECLARE @SearchName varchar(MAX) 
DECLARE @dataType varchar(20) 
DECLARE @Fromdate varchar(20)  
DECLARE @ToDate varchar(20) 
DECLARE @FirstNumber varchar(50)   
DECLARE @SecondNumber varchar(50) 
DECLARE @ResultCount INT


DECLARE @Result NVARCHAR(MAX)
DECLARE @search NVARCHAR(MAX)
DECLARE @searchRl1 NVARCHAR(MAX) = NULL
DECLARE @FirstCondition varchar(10) 
DECLARE @sql nvarchar(max);
  
DECLARE QRY_CURSOR CURSOR  
LOCAL  FORWARD_ONLY  FOR  
select dvc.spResultModel,SqlOperator,Condition,SearchName,dataType from [tblMultiViewQueryBulderDetails] mvqb
join [tblDynamicViewConfiguration] dvc on mvqb.ColumnRefID=dvc.Id
left join [TblQueryBuilderOperatorDetails] OD on mvqb.OperatorRefID=OD.ID
where mvqb.TrackerRefID=@TrackerId and mvqb.ViewName=@ViewName

select @ResultCount=Count(*) from [tblMultiViewQueryBulderDetails] mvqb
join [tblDynamicViewConfiguration] dvc on mvqb.ColumnRefID=dvc.Id
left join [TblQueryBuilderOperatorDetails] OD on mvqb.OperatorRefID=OD.ID
where mvqb.TrackerRefID=@TrackerId and mvqb.ViewName=@ViewName




IF(@ResultCount = 0 )
BEGIN 

SET @sql = 'WHERE 1=1'
END
ELSE IF(@ResultCount != 0 )
BEGIN

OPEN QRY_CURSOR  
FETCH NEXT FROM QRY_CURSOR INTO  @spResultModel ,@SqlOperator,@Condition,@SearchName,@dataType  
WHILE @@FETCH_STATUS = 0  
BEGIN 

 

IF ( @SqlOperator != 'Between')
    BEGIN
	if(@searchRl1 IS NULL)
    BEGIN
	
    SET @FirstCondition = @Condition
    SET @search = case when @SqlOperator = 'LIKE' or @SqlOperator = 'NOT LIKE' then  '''%' + @SearchName +'%''' else '''' + @SearchName +'''' end
	IF @dataType='datetime'
	    SET @searchRl1 =  'where CAST('+ @spResultModel + ' '+'as Date)'+' ' + @SqlOperator +' '+@search
    ELSE
    SET @searchRl1 = 'where'+' ' + @spResultModel +' ' + @SqlOperator +' '+@search
    
            SET  @sql = @searchRl1
	END
	ELSE if(@Condition = '')
    BEGIN
            SET @search = case when @SqlOperator = 'LIKE' or @SqlOperator = 'NOT LIKE' then  '''%' + @SearchName +'%''' else '''' + @SearchName +'''' end
           IF @dataType='datetime'
                 SET @searchRl1 = @sql +' '+ @FirstCondition +' ' + 'CAST('+ @spResultModel + ' '+'as Date)' +' ' + @SqlOperator +' '+@search
            ELSE
                  SET @searchRl1 = @sql +' '+ @FirstCondition +' ' + @spResultModel +' ' + @SqlOperator +' '+@search
			SET @FirstCondition = @Condition    
            SELECT  @sql = @searchRl1
 
	END
	ELSE if(@searchRl1 <> NULL or @Condition <> '')
    BEGIN
            SET @search = case when @SqlOperator = 'LIKE' or @SqlOperator = 'NOT LIKE' then  '''%' + @SearchName +'%''' else '''' + @SearchName +'''' end
            IF @dataType='datetime'
                 SET @searchRl1 = @sql +' '+ @FirstCondition +' ' + 'CAST('+ @spResultModel + ' '+'as Date)' +' ' + @SqlOperator +' '+@search
            ELSE
                  SET @searchRl1 = @sql +' '+ @FirstCondition +' ' + @spResultModel +' ' + @SqlOperator +' '+@search
			SET @FirstCondition = @Condition    
            SELECT  @sql = @searchRl1
 
 END

    END


	
IF ( @SqlOperator = 'Between')
    BEGIN
	 
	if(@dataType = 'datetime')
	BEGIN
 
	SET @Fromdate=(select top 1 value from STRING_SPLIT(@SearchName,',') order by value asc)
	SET @ToDate=(select top 1 value from STRING_SPLIT(@SearchName,',') order by value desc)
	END
	ELSE IF(@dataType = 'int' OR @dataType='money')
	BEGIN
	
      DECLARE  @numbers TABLE(Id INT Identity(1,1),Value int)
	  INSERT INTO @numbers 
	  select CAST(value as INT) from STRING_SPLIT(@SearchName,',')
	--SET @FirstNumber=(select top 1 value from STRING_SPLIT(@SearchName,',') order by value desc)
	--SET @SecondNumber=(select top 1 value from STRING_SPLIT(@SearchName,',') order by value asc)

	SET @FirstNumber = (select min(value) from @numbers)
	SET @SecondNumber = (select max(value) from @numbers)

	END


	if(@searchRl1 IS NULL)
    BEGIN
    SET @FirstCondition = @Condition
    SET @search = case when @SqlOperator = 'Between' AND @dataType = 'datetime' then '''' + @Fromdate +'''' + ' AND ' + '''' + @ToDate +'''' else @FirstNumber + ' AND ' +@SecondNumber end
    SET @searchRl1 = 'where'+' ' + @spResultModel +' ' + @SqlOperator +' '+@search    
            SET  @sql = @searchRl1
 
	END
	ELSE if(@Condition = '')
    BEGIN
            SET @search = case when @SqlOperator = 'Between' AND @dataType = 'datetime' then '''' + @Fromdate +'''' + ' AND ' + '''' + @ToDate +'''' else @FirstNumber + ' AND ' +@SecondNumber end
            SET @searchRl1 = @sql +' '+ @FirstCondition +' ' + @spResultModel +' ' + @SqlOperator +' '+@search
			SET @FirstCondition = @Condition    
            SELECT  @sql = @searchRl1
	END
	ELSE if(@searchRl1 <> NULL or @Condition <> '')
    BEGIN
            SET @search = case when @SqlOperator = 'Between' AND @dataType = 'datetime' then '''' + @Fromdate +'''' + ' AND ' + '''' + @ToDate +'''' else '''' + @FirstNumber +'''' + ' AND ' + '''' + @SecondNumber +'''' end
            SET @searchRl1 = @sql +' '+ @FirstCondition +' ' + @spResultModel +' ' + @SqlOperator +' '+@search
			SET @FirstCondition = @Condition    
            SELECT  @sql = @searchRl1
			
 
 END

    END

FETCH NEXT FROM QRY_CURSOR INTO  @spResultModel ,@SqlOperator,@Condition,@SearchName,@dataType 
END  
CLOSE QRY_CURSOR  
DEALLOCATE QRY_CURSOR  
END


 return @sql

END


