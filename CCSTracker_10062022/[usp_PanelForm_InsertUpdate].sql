USE [CCSQA]
GO
/****** Object:  StoredProcedure [dbo].[usp_PanelForm_InsertUpdate]    Script Date: 26/05/2022 11:52:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
-- =============================================  
-- Author:  <Author : CKusume>  
-- Create date: <12-04-2021>  
-- Modified date : <12-04-2021><13-04-2021,Sundar> <Included Exception handling>  
-- Description: <Description,The purpose of the Stored Procedure is to make CRUD operations on Panel Form tables>  
-- Generic Info: <I - Insert , U- Update, D- Delete R-Re activate>    
-- =============================================  
 -- [usp_PanelForm_InsertUpdate] 0,1,1,'abc','abc','abc','abc','abc','abc',1,1234,1,1,1,'abc','abc','I',23,'Arun',201,1,1
 --[usp_PanelForm_InsertUpdate] 1,1,1,'abc','abc','abcd','abc','abc','abc',1,1234,1,1,1,'abc','abc','U',23,'Arun',201,1,1
 --[usp_PanelForm_InsertUpdate] 0,1,1,'abc','abc','abc','abc','abc','abc',1,1234,1,1,1,'abc','abc','D',23,'Arun',201,1,1
 -- [usp_PanelForm_InsertUpdate] 0,1,1,'abc','abc','abc','abc','abc','abc',1,1234,1,1,1,'abc','abc','R',23,'Arun',201,1,1

  --select * from tblPanelForm

  --SELECT * FROM [dbo].[tblPanelForm] FOR SYSTEM_TIME ALL WHERE ID = 1

--SELECT * FROM [dbo].[tblPanelForm]

ALTER PROCEDURE [dbo].[usp_PanelForm_InsertUpdate]     
 -- Add the parameters for the stored procedure here    
 @ID int = 0,      
 @TypeOfPanelID int=NULL,
 @ReasonForPanelID Int NULL,
 @ClientName varchar(200)=NULL,      
 @Address varchar(200)=NULL,      
 @TMID varchar(200)=NULL,      
 @Details varchar(Max)=NULL,  
 @PanelFrim_FundsGoing varchar(200) =NULL ,  
 @PanelFrim_FundsComing varchar(200)=NULL,  
 @AwaitingFundsID int=NULL,  
 @Amount Money=NULL,  
 @ClientAccounts_complete bit=NULL,  
 @ActionrequiredByPanel bit=NULL,  
 @PanelTeamComplete bit=NULL,
@PanelFirmAllocated_Appointed [varchar](100) NULL,
@AttachFile Varchar(Max) Null,

      
 @StatementType  Varchar(50)=NULL,          
 @TrackerId int =NULL,            
 @CreatedBy Varchar(50)=NULL,          
 @processid int =NULL,          
 @RoutingID Int=NULL,    
 @selectedData varchar(100) = NULL      
AS    
BEGIN    
BEGIN TRY
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements    
 SET NOCOUNT ON;    
    
    
 Declare @trgProcessId int;       
    -- This performs INSERT action against PanelForm table    
IF @StatementType = 'I'              
 BEGIN           
           
   INSERT INTO dbo.tblPanelForm       
(TypeOfPanelID  ,
ReasonForPanelID ,
ClientName  ,      
Address ,      
TMID  ,      
Details,  
PanelFrim_FundsGoing,  
PanelFrim_FundsComing,  
AwaitingFundsID,  
Amount,  
ClientAccounts_complete,  
ActionrequiredByPanel,  
PanelTeamComplete, 
PanelFirmAllocated_Appointed,
AttachFile,

[Action],    
[ModifyingUser],
BeginDateLocalTime
       
)      
      
   Values      
(@TypeOfPanelID, 
@ReasonForPanelID,      
@ClientName  ,      
@Address  ,      
@TMID  ,      
@Details,    
@PanelFrim_FundsGoing,  
@PanelFrim_FundsComing,  
@AwaitingFundsID,  
@Amount,  
@ClientAccounts_complete,  
@ActionrequiredByPanel,  
@PanelTeamComplete,
@PanelFirmAllocated_Appointed,
@AttachFile,

@StatementType,    
@CreatedBy,
GETDATE())      
      
    Declare @FormDataId int;            
 set @FormDataId = SCOPE_IDENTITY()            
          
 SELECT @trgProcessId=trgProcessId FROM tblRoutingDetails    
  WHERE TrackerId= @TrackerId AND srcprocessid = @processid AND RoutingID = @RoutingID          
      
   -- This is common across all the trackers and perofrms INSERT action against tbltrackersform table     
    EXEC usp_TrackersForm_Insert @TrackerId, @CreatedBy, @trgProcessId,  @FormDataId    
       
 END       
    
    
  SELECT @trgProcessId=trgProcessId FROM tblRoutingDetails WHERE TrackerId= @TrackerId AND srcprocessid =@ProcessId              
 AND RoutingID = @RoutingID       
    
    
    
--This performs UPDATE action against PanelForm table    
IF @StatementType = 'U'              
  BEGIN           
     
    UPDATE dbo.tblPanelForm     
   SET      
[TypeOfPanelID] = @TypeOfPanelID ,
ReasonForPanelID= @ReasonForPanelID,
ClientName = @ClientName ,      
Address  = @Address,  
TMID  = @TMID,  
Details = @Details ,  
PanelFrim_FundsGoing = @PanelFrim_FundsGoing,  
PanelFrim_FundsComing = @PanelFrim_FundsComing,  
AwaitingFundsID = @AwaitingFundsID,  
Amount = @Amount,    
ClientAccounts_complete = @ClientAccounts_complete,  
ActionrequiredByPanel = @ActionrequiredByPanel,  
PanelTeamComplete = @PanelTeamComplete,
PanelFirmAllocated_Appointed = @PanelFirmAllocated_Appointed,
AttachFile = @AttachFile,

[ModifyingUser] = @CreatedBy,      
[Action] = @StatementType,
BeginDateLocalTime=GETDATE()
WHERE Id = @Id        
                    
      
 EXEC [dbo].[usp_TrackersForm_Update] @ID,@TrackerId, @ProcessId,@CreatedBy, @RoutingID, @selectedData,@StatementType         
            
         
  END       
      
--This performs DELETE/RE ACTIVATE action against PanelForm table    
IF (@StatementType = 'D' OR  @StatementType = 'R')            
  BEGIN           
    
    
    UPDATE dbo.tblPanelForm     
   SET      
       [ModifyingUser] = @CreatedBy,      
       [Action] = @StatementType      
 WHERE Id IN (SELECT CAST(value AS INTEGER) FROM STRING_SPLIT(@selectedData, ','))             
      
   -- This is common across all the trackers and UPDATING Process id in tbltrackersform table based on Routing details    
    EXEC [dbo].[usp_TrackersForm_Update] @ID,@TrackerId, @ProcessId,@CreatedBy, @RoutingID, @selectedData,@StatementType    
         
  END      
    
END TRY
BEGIN CATCH

        DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),
                @ErrorSeverity INT = ERROR_SEVERITY(),
                @ErrorNumber INT = ERROR_NUMBER();

		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);
 
END CATCH
     
END
