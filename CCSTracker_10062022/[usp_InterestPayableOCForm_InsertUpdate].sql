USE [CCSQA]
GO
/****** Object:  StoredProcedure [dbo].[usp_InterestPayableOCForm_InsertUpdate]    Script Date: 26/05/2022 11:52:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


  
  
    
-- =============================================    
-- Author:  <Author : Arun,Sundar>    
-- Create date: <10-04-2021>    
-- Modified date : <11-04-2021><13-04-2021,Sundar> <Included Exception handling>
-- Description: <Description,The purpose of the Stored Procedure is to make CRUD operations on InterestPayableOCForm tables>    
-- Generic Info: <I - Insert , U- Update, D- Delete R-Re activate>      
-- =============================================    
    
    
ALTER PROCEDURE [dbo].[usp_InterestPayableOCForm_InsertUpdate]     
(    
@ID int =0,    
@MatterNumber Varchar(50)=NULL,    
@CenterID Int =NULL,    
@TeamID Int =NULL,    
@DateFundsRevd Datetime=NULL,    
@PaymentDate Datetime=NULL,    
@FundsHeld Money=NULL,    
@ReasonForInterestPayment Varchar(Max)=NULL,    
@Posted Bit=NULL,    
@NumberOfDatesFundsHeld Int=NULL,    
@Formula_25 Decimal(18,2)=NULL,    
      
 @StatementType  Varchar(50)=NULL,          
 @TrackerId int =NULL,            
 @CreatedBy Varchar(256)=NULL,          
 @processid int =NULL,          
 @RoutingID Int=NULL,    
 @selectedData varchar(100) = NULL )     
    
AS    
BEGIN    
BEGIN TRY
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
    
    
 Declare @trgProcessId int;       
    -- This performs INSERT action against InterestPayableOCForm table    
IF @StatementType = 'I'              
 BEGIN           
           
   INSERT INTO   [dbo].[tblInterestPayableOCForm]    
(MatterNumber,    
CenterID ,    
TeamID ,    
DateFundsRevd ,    
PaymentDate ,    
FundsHeld ,    
ReasonForInterestPayment,    
Posted,    
NumberOfDatesFundsHeld,    
Formula_25 ,    
[Action],    
[ModifyingUser] )    
       
Values      
(@MatterNumber,    
@CenterID ,    
@TeamID ,    
@DateFundsRevd ,    
@PaymentDate ,    
@FundsHeld ,    
@ReasonForInterestPayment,    
@Posted,    
@NumberOfDatesFundsHeld,    
@Formula_25,    
@StatementType,    
@CreatedBy)      
      
    Declare @FormDataId int;            
 set @FormDataId = SCOPE_IDENTITY()            
          
 SELECT @trgProcessId=trgProcessId FROM tblRoutingDetails    
  WHERE TrackerId= @TrackerId AND srcprocessid = @processid AND RoutingID = @RoutingID          
         
   -- This is common across all the trackers and perofrms INSERT action against tbltrackersform table     
    EXEC usp_TrackersForm_Insert @TrackerId, @CreatedBy, @trgProcessId,  @FormDataId    
       
    
 END       
    
    
  SELECT @trgProcessId=trgProcessId FROM tblRoutingDetails WHERE TrackerId= @TrackerId AND     
  srcprocessid =@ProcessId              
 AND RoutingID = @RoutingID       
    
     
--This performs UPDATE action against InterestPayableOCForm table    
IF @StatementType = 'U'              
  BEGIN           
     
    UPDATE [dbo].[tblInterestPayableOCForm]     
   SET      
        MatterNumber =@MatterNumber,    
     CenterID = @CenterID ,    
     TeamID =@TeamID ,    
     DateFundsRevd = @DateFundsRevd ,    
     PaymentDate = @PaymentDate ,    
     FundsHeld = @FundsHeld,    
     ReasonForInterestPayment = @ReasonForInterestPayment,    
     Posted = @Posted,    
     NumberOfDatesFundsHeld=@NumberOfDatesFundsHeld,    
     Formula_25=@Formula_25,      
        [ModifyingUser] = @CreatedBy,      
        [Action] = @StatementType      
 WHERE Id = @Id        
                    
 EXEC [dbo].[usp_TrackersForm_Update] @Id,@TrackerId, @ProcessId,@CreatedBy, @RoutingID, @selectedData,@StatementType   
          
            
         
  END       
    
--     
--This performs DELETE/RE ACTIVATE action against InterestPayableOCForm table    
IF (@StatementType = 'D' OR  @StatementType = 'R')            
  BEGIN           
    
    
    UPDATE [dbo].[tblInterestPayableOCForm]      
   SET      
       [ModifyingUser] = @CreatedBy,      
       [Action] = @StatementType      
 WHERE Id IN (SELECT CAST(value AS INTEGER) FROM STRING_SPLIT(@selectedData, ','))             
      
  -- This is common across all the trackers and UPDATING Process id in tbltrackersform table based on Routing details    
    EXEC [dbo].[usp_TrackersForm_Update]  @Id,@TrackerId, @ProcessId,@CreatedBy, @RoutingID, @selectedData,@StatementType    
           
  END      
END TRY
BEGIN CATCH

        DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),
                @ErrorSeverity INT = ERROR_SEVERITY(),
                @ErrorNumber INT = ERROR_NUMBER();

		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);
 
END CATCH
    
     
END 
