USE [CCSQA]
GO
/****** Object:  StoredProcedure [dbo].[usp_UnallocatedFundsForm_InsertUpdate]    Script Date: 20/05/2022 11:36:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
  
-- =============================================  
-- Author:  <Author : chaitanya>  
-- Create date: <12-04-2021>  
-- Modified date : <12-04-2021> <13-04-2021,Sundar> <Included Exception handling>  
-- Description: <Description,The purpose of the Stored Procedure is to make CRUD operations on Unallocated Funds Form tables>  
-- Generic Info: <I - Insert , U- Update, D- Delete R-Re activate>    
-- =============================================  
  
--Exec [usp_usp_UnallocatedFundsForm_InsertUpdate] @id=0,@Date='2021/04/12 00:00:00',@Amount=500.00,@PaymentMethodID=1,@PaymentDetails='Test Payment',@CANotes='Test Ca Notes',@MatterNumber=123456,  @StatusID=3,  
--@UnallocatedNoID=2,@UnallocatedMoniesVF2='Test Monies',@StatementType='I',@TrackerId=20,@CreatedBy='CKusume',@processid=101,@RoutingID=0  
--Exec [usp_usp_UnallocatedFundsForm_InsertUpdate] @id=15,@Date='2021/04/12 00:00:00',@Amount=5000.00,@PaymentMethodID=1,@PaymentDetails='Test Payments',@CANotes='Test Ca Notes',@MatterNumber=123456,  @StatusID=2,  
--@UnallocatedNoID=3,@UnallocatedMoniesVF2='Test Monies',@StatementType='U',@TrackerId=20,@CreatedBy='CKusume',@processid=201,@RoutingID=1  
  
ALTER PROCEDURE [dbo].[usp_UnallocatedFundsForm_InsertUpdate]     
 -- Add the parameters for the stored procedure here    
 @ID int = 0,      
 @Date datetime=NULL,      
 @Amount money=NULL,      
 @PaymentMethodID int=NULL,      
 @PaymentDetails varchar(200)=NULL,      
 @CANotes varchar(Max)=NULL,  
 @MatterNumber varchar(50) =NULL ,  
 @StatusID int=NULL,  
 @UnallocatedNumber Varchar(100)=NULL,  
 @UnallocatedMoniesVF2 varchar(50)=NULL,  
      
 @StatementType  Varchar(50)=NULL,          
 @TrackerId int =NULL,            
 @CreatedBy Varchar(50)=NULL,          
 @processid int =NULL,          
 @RoutingID Int=NULL,    
 @selectedData varchar(100) = NULL      
AS    
BEGIN    
BEGIN TRY  
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements    
 SET NOCOUNT ON;    
    
    
 Declare @trgProcessId int;       
    -- This performs INSERT action against Unallocated Funds Form table    
IF @StatementType = 'I'              
 BEGIN           
           
   INSERT INTO dbo.tblUnallocatedFundsForm       
   ([Date]  ,      
   Amount  ,      
   PaymentMethodID ,      
   PaymentDetails  ,      
   CANotes,  
   MatterNumber,  
   StatusID,  
   UnallocatedNumber,  
   UnallocatedMoniesVF2,  
   [Action],    
   [ModifyingUser]     
       
)      
      
   Values      
   (@Date  ,      
    @Amount  ,      
    @PaymentMethodID  ,      
    @PaymentDetails  ,      
    @CANotes,    
 @MatterNumber,  
 @StatusID,  
 @UnallocatedNumber,  
 @UnallocatedMoniesVF2,  
    @StatementType,    
    @CreatedBy )      
      
    Declare @FormDataId int;            
 set @FormDataId = SCOPE_IDENTITY()            
          
 SELECT @trgProcessId=trgProcessId FROM tblRoutingDetails    
  WHERE TrackerId= @TrackerId AND srcprocessid = @processid AND RoutingID = @RoutingID          
      
   -- This is common across all the trackers and perofrms INSERT action against tbltrackersform table     
    EXEC usp_TrackersForm_Insert @TrackerId, @CreatedBy, @trgProcessId,  @FormDataId    
       
 END       
    
    
  SELECT @trgProcessId=trgProcessId FROM tblRoutingDetails WHERE TrackerId= @TrackerId AND srcprocessid =@ProcessId              
 AND RoutingID = @RoutingID       
    
    
    
--This performs UPDATE action against Unallocated Funds Form table    
IF @StatementType = 'U'              
  BEGIN           
     
    UPDATE dbo.tblUnallocatedFundsForm     
   SET      
       [Date] = @Date ,      
    Amount = @Amount ,      
       PaymentMethodID  = @PaymentMethodID,  
    PaymentDetails  = @PaymentDetails,  
       CANotes = @CANotes ,  
    MatterNumber = @MatterNumber,  
    StatusID = @StatusID,  
    UnallocatedNumber = @UnallocatedNumber,  
       UnallocatedMoniesVF2 = @UnallocatedMoniesVF2,      
       [ModifyingUser] = @CreatedBy,      
       [Action] = @StatementType      
 WHERE Id = @Id        
                    
      
 EXEC [dbo].[usp_TrackersForm_Update] @ID,@TrackerId, @ProcessId,@CreatedBy, @RoutingID, @selectedData,@StatementType         
            
         
  END       
      
--This performs DELETE/RE ACTIVATE action against Unallocated Funds Form table    
IF (@StatementType = 'D' OR  @StatementType = 'R')            
  BEGIN           
    
    
    UPDATE dbo.tblUnallocatedFundsForm     
   SET      
       [ModifyingUser] = @CreatedBy,      
       [Action] = @StatementType      
 WHERE Id IN (SELECT CAST(value AS INTEGER) FROM STRING_SPLIT(@selectedData, ','))             
      
   -- This is common across all the trackers and UPDATING Process id in tbltrackersform table based on Routing details    
    EXEC [dbo].[usp_TrackersForm_Update] @Id,@TrackerId, @ProcessId,@CreatedBy, @RoutingID, @selectedData,@StatementType   
         
  END      
END TRY  
BEGIN CATCH  
  
        DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),  
                @ErrorSeverity INT = ERROR_SEVERITY(),  
                @ErrorNumber INT = ERROR_NUMBER();  
  
  RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);  
   
END CATCH   
       
END  
