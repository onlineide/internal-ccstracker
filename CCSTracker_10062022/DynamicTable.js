﻿

$(document).ready(function () {


    $('.selectAllCheckBox').on('click', function () {

        // Check/uncheck all checkboxes in the table
        var rows = $(".dynamicTableClass tdata tr").find("input[type='checkbox']").prop('checked', this.checked);

        $('input[type="checkbox"]').prop('checked', this.checked);
    });


    $('.dynamicTableClass tbody').on('change', 'input[type="checkbox"]', function () {

        var tableRow = $(".dynamicTableClass tbody tr");
        var tdataCount = tableRow.length;
        var checkCount = 0;
        if (this.checked) {

            $('.dynamicTableClass tbody tr').each(function () {

                $(this).find('input:checked').each(function () {
                    checkCount += 1;
                });
            });

            if (tdataCount == checkCount) {
                $('.selectAllCheckBox').prop("checked", true);

            }
        }
        else {
            $('.selectAllCheckBox').prop("checked", false);
        }
    });

});

function HideButtons() {

    $(".commonShowHideButton").hide();

}

function applyDataTable(PageCount, TotalSum=null) {

    var columnCount = $('.dynamicTableClass th').length - 1;
    $('.dynamicTableClass').DataTable({
        pageLength: 20,
        order: [],
        columnDefs: [{ orderable: false, targets: [0, columnCount] }]
    });


    $(".dataTables_length").css("display", "block");
    $(".dataTables_length label").remove();

    $(".dataTables_length").append("<span>Total Count : " + PageCount + '</span>');

    if (TotalSum != "" && TotalSum!=null) {
    $(".dataTables_length").append("<span style='margin-left:20px;'>Total Sum : " + TotalSum + '</span>');
}


}


function checkedRecordsorNot() {
    var selectedData = [];
    let selectedCount = 0;

    $('.dynamicTableClass tbody tr').each(function () {

        $(this).find('input:checked').each(function () {
            selectedCount += 1;
            var getValueId = $(this).val();
            selectedData.push(getValueId);

        });
    });

    return [selectedData, selectedCount];
}

