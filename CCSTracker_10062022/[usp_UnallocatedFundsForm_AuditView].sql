USE [CCSQA]
GO
/****** Object:  StoredProcedure [dbo].[usp_UnallocatedFundsForm_AuditView]    Script Date: 23/05/2022 10:31:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ajay George Kalathil
-- Create date: 6th May 2021
-- Description:	This SP will be used to get the Audit Trial Version View for the front end system
-- =============================================


--Exec usp_UnallocatedFundsForm_AuditView 4,null,null
ALTER PROCEDURE [dbo].[usp_UnallocatedFundsForm_AuditView]
	-- Add the parameters for the stored procedure here
	@ID Int,  
	@FromDate datetime2 = '1900-01-01 00:00:00.0000000', 
	@ToDate datetime2	= '9999-12-31 23:59:59.9999999'
AS
BEGIN

  BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    
;WITH T
AS (
    SELECT		UAF.ID	           							
				,Date as Date, lag(Date) over(order by enddate asc) as Prev_Date
				,Amount as Amount , lag(Amount) over(order by enddate asc) as Prev_Amount				
				,PaymentMethod as PaymentMethod , lag(PaymentMethod) over(order by enddate asc) as Prev_PaymentMethod			
				,PaymentDetails as PaymentDetails , lag(PaymentDetails) over(order by enddate asc) as Prev_PaymentDetails
				,CANotes as CANotes , lag(CANotes) over(order by enddate asc) as Prev_CANotes
				,MatterNumber as MatterNumber , lag(MatterNumber) over(order by enddate asc) as Prev_MatterNumber
				,sts.Status as Status , lag(sts.Status) over(order by enddate asc) as Prev_Status	
				,UnallocatedNumber as UnallocatedNumber , lag(UnallocatedNumber) over(order by enddate asc) as Prev_UnallocatedNumber				
				,UnallocatedMoniesVF2 as UnallocatedMoniesVF2 , lag(UnallocatedMoniesVF2) over(order by enddate asc) as Prev_UnallocatedMoniesVF2										
				,ModifyingUser as ModifyingUser , lag(ModifyingUser) over(order by enddate asc) as Prev_ModifyingUser
				,Action as Action , lag(Action) over(order by enddate asc) as Prev_Action
				,BeginDate
				,EndDate
				,ROW_Number() over(partition by  UAF.Id Order by EndDate asc) as VersionNumber
    FROM dbo.tblUnallocatedFundsForm FOR SYSTEM_TIME ALL AS UAF       
LEFT JOIN [dbo].[tblUnallocatedFundsPaymentMethod] UPM on UAF.PaymentmethodID = UPM.ID      
LEFT JOIN [dbo].[tblUnallocatedFundsStatus] sts ON UAF.StatusID = sts.ID      
--LEFT JOIN [dbo].[tblUnallocatedFundsUnallocatedNo] UAFUA ON UAF.UnallocatedNoID = UAFUA.ID 
    WHERE UAF.id =@ID
)
SELECT T.BeginDate,
       T.EndDate,
       C.ColName as ColumnName,
	   t.VersionNumber,	   
       C.present_Value as CurrentValue,
       C.prev_value as PreviousValue,
	   T.ModifyingUser as ModifiedBy
FROM T
    CROSS APPLY
    (
        VALUES				
			 ('Date', Convert(varchar(10), T.Date,103) , Convert(varchar(10),T.Prev_Date,103))
			,('Amount', Convert(varchar, T.Amount) , Convert(varchar,T.Amount))
			,('Payment Method ', Convert(varchar, T.PaymentMethod) , Convert(varchar,T.Prev_PaymentMethod))			
			,('Payment Details', Convert(varchar, T.PaymentDetails) , Convert(varchar,T.Prev_PaymentDetails))
			,('CANotes', Convert(varchar, T.CANotes) , Convert(varchar,T.Prev_CANotes))
			,('Matter Number', Convert(varchar, T.MatterNumber) , Convert(varchar,T.Prev_MatterNumber))			
			,('Status ', Convert(varchar, T.Status) , Convert(varchar,T.Prev_Status))
			,('Unallocated Number ', Convert(varchar, T.UnallocatedNumber) , Convert(varchar,T.Prev_UnallocatedNumber))			
			,('Unallocated Monies VF2 ', Convert(varchar, T.UnallocatedMoniesVF2 ) , Convert(varchar,T.Prev_UnallocatedMoniesVF2))						
			,('ModifyingUser', Convert(varchar, T.ModifyingUser) , Convert(varchar,T.Prev_ModifyingUser))
			,('Action', Convert(varchar, T.Action) , Convert(varchar,T.Prev_Action))
			) AS C (ColName, present_Value, prev_value)
WHERE  EXISTS
(
    SELECT present_Value 
    EXCEPT 
    SELECT C.prev_value
)  and Convert(datetime,BeginDate) >= @FromDate and Convert(Datetime,BeginDate) <=@ToDate

END TRY
BEGIN CATCH

        DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),
                @ErrorSeverity INT = ERROR_SEVERITY(),
                @ErrorNumber INT = ERROR_NUMBER();

		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);
 
END CATCH
END

