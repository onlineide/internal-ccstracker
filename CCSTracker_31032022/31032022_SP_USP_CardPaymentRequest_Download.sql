
/****** Object:  StoredProcedure [dbo].[usp_CardPaymentRequest_Download]    Script Date: 31/03/2022 13:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 -- Modified date: <16-04-2021> <Included Exception handling>
    
 --[dbo].[usp_CardPaymentRequest_Download] 25,201 ,'View1'   
ALTER Procedure [dbo].[usp_CardPaymentRequest_Download]              
@TrackerId int,            
@processId int,
@ViewName Varchar(100)                
            
AS                
Begin           
 BEGIN TRY          
                
select	ID,	MatterID,	ClientName,	ContactNumber,	DateTimeToCall,	Amount,	PaymentReason,	PaymentTaken=case when PaymentTaken =1 then 'Yes' else 'No' end,
UnableToContact=case when UnableToContact =1 then 'Yes' else 'No' end, CANotes, RequesterName, AttachedFile,
case when ClientIsExpectingCall =1 then 'Yes' else 'No' end ClientIsExpectingCall
	INTO #FilterResult
from [dbo].[tblCardPaymentRequestForm]         
        
 where ID in (select distinct FormDataId          
    FROM [dbo].[tblTrackerForms]            
    with (nolock) where TrackerId=@TrackerId and ProcessId=@processId  )           
 order by 1 asc          
       
DECLARE @sql nvarchar(max);
SELECT  @sql = 'SELECT * FROM #FilterResult ' + dbo.FuncDynamicQueryResult(@TrackerId, @ViewName)
EXEC sp_executesql @sql
           
END TRY
BEGIN CATCH

        DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),
                @ErrorSeverity INT = ERROR_SEVERITY(),
                @ErrorNumber INT = ERROR_NUMBER();

		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);
 
END CATCH       
DROP TABLE #FilterResult

End 
