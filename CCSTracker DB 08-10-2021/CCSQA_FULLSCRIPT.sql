USE [master]
GO
/****** Object:  Database [CCSQA]    Script Date: 07/10/2021 13:24:19 ******/
CREATE DATABASE [CCSQA]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'CCSQA', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\CCSQA.mdf' , SIZE = 73728KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'CCSQA_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\CCSQA_log.ldf' , SIZE = 73728KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [CCSQA] SET COMPATIBILITY_LEVEL = 130
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [CCSQA].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [CCSQA] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [CCSQA] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [CCSQA] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [CCSQA] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [CCSQA] SET ARITHABORT OFF 
GO
ALTER DATABASE [CCSQA] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [CCSQA] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [CCSQA] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [CCSQA] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [CCSQA] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [CCSQA] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [CCSQA] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [CCSQA] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [CCSQA] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [CCSQA] SET  DISABLE_BROKER 
GO
ALTER DATABASE [CCSQA] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [CCSQA] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [CCSQA] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [CCSQA] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [CCSQA] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [CCSQA] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [CCSQA] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [CCSQA] SET RECOVERY FULL 
GO
ALTER DATABASE [CCSQA] SET  MULTI_USER 
GO
ALTER DATABASE [CCSQA] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [CCSQA] SET DB_CHAINING OFF 
GO
ALTER DATABASE [CCSQA] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [CCSQA] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [CCSQA] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'CCSQA', N'ON'
GO
ALTER DATABASE [CCSQA] SET QUERY_STORE = OFF
GO
USE [CCSQA]
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
USE [CCSQA]
GO
/****** Object:  User [PREPROD\ADM-VIR-Kamran.Rashi]    Script Date: 07/10/2021 13:24:19 ******/
CREATE USER [PREPROD\ADM-VIR-Kamran.Rashi] FOR LOGIN [PREPROD\ADM-VIR-Kamran.Rashi] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [PREPROD\ADM-VIR-Anand.Patil]    Script Date: 07/10/2021 13:24:19 ******/
CREATE USER [PREPROD\ADM-VIR-Anand.Patil] FOR LOGIN [PREPROD\ADM-VIR-Anand.Patil] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [ccsAdmin]    Script Date: 07/10/2021 13:24:19 ******/
CREATE USER [ccsAdmin] FOR LOGIN [ccsAdmin] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  Schema [PUB]    Script Date: 07/10/2021 13:24:19 ******/
CREATE SCHEMA [PUB]
GO
/****** Object:  UserDefinedTableType [dbo].[tblCompletionDiaryMockImport]    Script Date: 07/10/2021 13:24:19 ******/
CREATE TYPE [dbo].[tblCompletionDiaryMockImport] AS TABLE(
	[Team] [varchar](100) NULL,
	[FeeEarnerInitials] [varchar](100) NULL,
	[MatterNumber] [varchar](100) NULL,
	[ClientSurname] [varchar](100) NULL,
	[MatterType] [varchar](100) NULL,
	[MatterStatus] [varchar](100) NULL,
	[ExpectedCompletionDate] [varchar](100) NULL,
	[Notes] [varchar](100) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[tblQueryBuilderMockImport]    Script Date: 07/10/2021 13:24:19 ******/
CREATE TYPE [dbo].[tblQueryBuilderMockImport] AS TABLE(
	[TrackerRefID] [int] NULL,
	[ViewName] [varchar](100) NULL,
	[ColumnRefID] [int] NULL,
	[OperatorRefID] [int] NULL,
	[Condition] [varchar](10) NULL,
	[SearchName] [varchar](100) NULL,
	[SortValue] [int] NULL
)
GO
/****** Object:  UserDefinedFunction [dbo].[FuncDynamicQueryResult]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[FuncDynamicQueryResult]
(
	@TrackerId int,
	@ViewName  Varchar(1000)
)
RETURNS nvarchar(max)
AS
BEGIN
	
	DECLARE @spResultModel varchar(300)  
DECLARE @SqlOperator varchar(20)  
DECLARE @Condition varchar(10)    
DECLARE @SearchName varchar(MAX) 
DECLARE @dataType varchar(20) 
DECLARE @Fromdate varchar(20)  
DECLARE @ToDate varchar(20) 
DECLARE @FirstNumber varchar(50)   
DECLARE @SecondNumber varchar(50) 
DECLARE @ResultCount INT


DECLARE @Result NVARCHAR(MAX)
DECLARE @search NVARCHAR(MAX)
DECLARE @searchRl1 NVARCHAR(MAX) = NULL
DECLARE @FirstCondition varchar(10) 
DECLARE @sql nvarchar(max);
  
DECLARE QRY_CURSOR CURSOR  
LOCAL  FORWARD_ONLY  FOR  
select dvc.spResultModel,SqlOperator,Condition,SearchName,dataType from [tblMultiViewQueryBulderDetails] mvqb
join [tblDynamicViewConfiguration] dvc on mvqb.ColumnRefID=dvc.Id
left join [TblQueryBuilderOperatorDetails] OD on mvqb.OperatorRefID=OD.ID
where mvqb.TrackerRefID=@TrackerId and mvqb.ViewName=@ViewName

select @ResultCount=Count(*) from [tblMultiViewQueryBulderDetails] mvqb
join [tblDynamicViewConfiguration] dvc on mvqb.ColumnRefID=dvc.Id
left join [TblQueryBuilderOperatorDetails] OD on mvqb.OperatorRefID=OD.ID
where mvqb.TrackerRefID=@TrackerId and mvqb.ViewName=@ViewName




IF(@ResultCount = 0 )
BEGIN 

SET @sql = 'WHERE 1=1'
END
ELSE IF(@ResultCount != 0 )
BEGIN

OPEN QRY_CURSOR  
FETCH NEXT FROM QRY_CURSOR INTO  @spResultModel ,@SqlOperator,@Condition,@SearchName,@dataType  
WHILE @@FETCH_STATUS = 0  
BEGIN 

 

IF ( @SqlOperator != 'Between')
    BEGIN
	if(@searchRl1 IS NULL)
    BEGIN
    SET @FirstCondition = @Condition
    SET @search = case when @SqlOperator = 'LIKE' or @SqlOperator = 'NOT LIKE' then  '''%' + @SearchName +'%''' else '''' + @SearchName +'''' end
    SET @searchRl1 = 'where'+' ' + @spResultModel +' ' + @SqlOperator +' '+@search
    
            SET  @sql = @searchRl1
	END
	ELSE if(@Condition = '')
    BEGIN
            SET @search = case when @SqlOperator = 'LIKE' or @SqlOperator = 'NOT LIKE' then  '''%' + @SearchName +'%''' else '''' + @SearchName +'''' end
            SET @searchRl1 = @sql +' '+ @FirstCondition +' ' + @spResultModel +' ' + @SqlOperator +' '+@search
			SET @FirstCondition = @Condition    
            SELECT  @sql = @searchRl1
 
	END
	ELSE if(@searchRl1 <> NULL or @Condition <> '')
    BEGIN
            SET @search = case when @SqlOperator = 'LIKE' or @SqlOperator = 'NOT LIKE' then  '''%' + @SearchName +'%''' else '''' + @SearchName +'''' end
            SET @searchRl1 = @sql +' '+ @FirstCondition +' ' + @spResultModel +' ' + @SqlOperator +' '+@search
			SET @FirstCondition = @Condition    
            SELECT  @sql = @searchRl1
 
 END

    END



IF ( @SqlOperator = 'Between')
    BEGIN

	if(@dataType = 'datetime')
	BEGIN
	SET @Fromdate=(select top 1 value from STRING_SPLIT(@SearchName,',') order by value asc)
	SET @ToDate=(select top 1 value from STRING_SPLIT(@SearchName,',') order by value desc)
	END
	ELSE IF(@dataType = 'int')
	BEGIN
	SET @FirstNumber=(select top 1 value from STRING_SPLIT(@SearchName,',') order by value asc)
	SET @SecondNumber=(select top 1 value from STRING_SPLIT(@SearchName,',') order by value desc)
	END


	if(@searchRl1 IS NULL)
    BEGIN
    SET @FirstCondition = @Condition
    SET @search = case when @SqlOperator = 'Between' AND @dataType = 'datetime' then '''' + @Fromdate +'''' + ' AND ' + '''' + @ToDate +'''' else @FirstNumber + ' AND ' +@SecondNumber end
    SET @searchRl1 = 'where'+' ' + @spResultModel +' ' + @SqlOperator +' '+@search    
            SET  @sql = @searchRl1
 
	END
	ELSE if(@Condition = '')
    BEGIN
            SET @search = case when @SqlOperator = 'Between' AND @dataType = 'datetime' then '''' + @Fromdate +'''' + ' AND ' + '''' + @ToDate +'''' else @FirstNumber + ' AND ' +@SecondNumber end
            SET @searchRl1 = @sql +' '+ @FirstCondition +' ' + @spResultModel +' ' + @SqlOperator +' '+@search
			SET @FirstCondition = @Condition    
            SELECT  @sql = @searchRl1
	END
	ELSE if(@searchRl1 <> NULL or @Condition <> '')
    BEGIN
            SET @search = case when @SqlOperator = 'Between' AND @dataType = 'datetime' then '''' + @Fromdate +'''' + ' AND ' + '''' + @ToDate +'''' else '''' + @FirstNumber +'''' + ' AND ' + '''' + @SecondNumber +'''' end
            SET @searchRl1 = @sql +' '+ @FirstCondition +' ' + @spResultModel +' ' + @SqlOperator +' '+@search
			SET @FirstCondition = @Condition    
            SELECT  @sql = @searchRl1
			
 
 END

    END

FETCH NEXT FROM QRY_CURSOR INTO  @spResultModel ,@SqlOperator,@Condition,@SearchName,@dataType 
END  
CLOSE QRY_CURSOR  
DEALLOCATE QRY_CURSOR  
END


 return @sql

END


GO
/****** Object:  Table [dbo].[tblPanelFormHistory]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblPanelFormHistory](
	[ID] [int] NOT NULL,
	[TypeOfPanelID] [int] NOT NULL,
	[ReasonForPanelID] [int] NULL,
	[ClientName] [varchar](50) NOT NULL,
	[Address] [varchar](100) NOT NULL,
	[TMID] [varchar](50) NOT NULL,
	[Details] [varchar](max) NOT NULL,
	[PanelFrim_FundsGoing] [varchar](50) NOT NULL,
	[PanelFrim_FundsComing] [varchar](50) NULL,
	[AwaitingFundsId] [int] NOT NULL,
	[Amount] [bigint] NOT NULL,
	[ClientAccounts_complete] [bit] NULL,
	[ActionrequiredByPanel] [bit] NULL,
	[PanelTeamComplete] [bit] NULL,
	[PanelFirmAllocated_Appointed] [varchar](100) NULL,
	[AttachFile] [varchar](max) NULL,
	[ModifyingUser] [nvarchar](128) NULL,
	[Action] [varchar](1) NULL,
	[BeginDate] [datetime2](7) NOT NULL,
	[EndDate] [datetime2](7) NOT NULL,
	[BeginDateLocalTime] [datetime2](7) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Index [ix_tblPanelFormHistory]    Script Date: 07/10/2021 13:24:19 ******/
CREATE CLUSTERED INDEX [ix_tblPanelFormHistory] ON [dbo].[tblPanelFormHistory]
(
	[EndDate] ASC,
	[BeginDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblPanelForm]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblPanelForm](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TypeOfPanelID] [int] NOT NULL,
	[ReasonForPanelID] [int] NULL,
	[ClientName] [varchar](50) NOT NULL,
	[Address] [varchar](100) NOT NULL,
	[TMID] [varchar](50) NOT NULL,
	[Details] [varchar](max) NOT NULL,
	[PanelFrim_FundsGoing] [varchar](50) NOT NULL,
	[PanelFrim_FundsComing] [varchar](50) NULL,
	[AwaitingFundsId] [int] NOT NULL,
	[Amount] [bigint] NOT NULL,
	[ClientAccounts_complete] [bit] NULL,
	[ActionrequiredByPanel] [bit] NULL,
	[PanelTeamComplete] [bit] NULL,
	[PanelFirmAllocated_Appointed] [varchar](100) NULL,
	[AttachFile] [varchar](max) NULL,
	[ModifyingUser] [nvarchar](128) NULL,
	[Action] [varchar](1) NULL,
	[BeginDate] [datetime2](7) GENERATED ALWAYS AS ROW START NOT NULL,
	[EndDate] [datetime2](7) GENERATED ALWAYS AS ROW END NOT NULL,
	[BeginDateLocalTime] [datetime2](7) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
	PERIOD FOR SYSTEM_TIME ([BeginDate], [EndDate])
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
WITH
(
SYSTEM_VERSIONING = ON ( HISTORY_TABLE = [dbo].[tblPanelFormHistory] )
)
GO
/****** Object:  Table [dbo].[tblCompletionDiaryHistory]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblCompletionDiaryHistory](
	[ID] [int] NOT NULL,
	[MatterNumber] [varchar](20) NOT NULL,
	[TeamID] [int] NOT NULL,
	[ClientSurname] [varchar](20) NOT NULL,
	[MatterTypeID] [int] NOT NULL,
	[ExpectedCompletionDate] [datetime] NOT NULL,
	[PackReceived] [varchar](50) NULL,
	[PackChecked] [varchar](50) NULL,
	[CANotes_Amendments] [varchar](50) NULL,
	[VFImportedNotes] [varchar](50) NULL,
	[CanGo] [varchar](50) NULL,
	[SentForAuthorisation] [varchar](50) NULL,
	[Sent] [varchar](50) NULL,
	[LegallyCompleted] [datetime] NULL,
	[SLRBilled] [bit] NULL,
	[Healthcheck] [bit] NULL,
	[Action] [varchar](1) NULL,
	[BeginDate] [datetime2](7) NOT NULL,
	[EndDate] [datetime2](7) NOT NULL,
	[Center] [varchar](50) NULL,
	[Created] [datetime] NULL,
	[TransactionType] [varchar](50) NULL,
	[Modified] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CheckedBy] [varchar](50) NULL,
	[CompMonth] [varchar](50) NULL,
	[MatterNumberLinked] [varchar](20) NULL,
	[ModifiedBy] [varchar](50) NULL,
	[Version] [int] NULL,
	[ModifyingUser] [varchar](500) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblCompletionDiary]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblCompletionDiary](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[MatterNumber] [varchar](20) NOT NULL,
	[TeamID] [int] NOT NULL,
	[ClientSurname] [varchar](20) NOT NULL,
	[MatterTypeID] [int] NOT NULL,
	[ExpectedCompletionDate] [datetime] NOT NULL,
	[PackReceived] [varchar](50) NULL,
	[PackChecked] [varchar](50) NULL,
	[CANotes_Amendments] [varchar](50) NULL,
	[VFImportedNotes] [varchar](50) NULL,
	[CanGo] [varchar](50) NULL,
	[SentForAuthorisation] [varchar](50) NULL,
	[Sent] [varchar](50) NULL,
	[LegallyCompleted] [datetime] NULL,
	[SLRBilled] [bit] NULL,
	[Healthcheck] [bit] NULL,
	[Action] [varchar](1) NULL,
	[BeginDate] [datetime2](7) GENERATED ALWAYS AS ROW START NOT NULL,
	[EndDate] [datetime2](7) GENERATED ALWAYS AS ROW END NOT NULL,
	[Center] [varchar](50) NULL,
	[Created] [datetime] NULL,
	[TransactionType] [varchar](50) NULL,
	[Modified] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CheckedBy] [varchar](50) NULL,
	[CompMonth] [varchar](50) NULL,
	[MatterNumberLinked] [varchar](20) NULL,
	[ModifiedBy] [varchar](50) NULL,
	[Version] [int] NULL,
	[ModifyingUser] [varchar](500) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
	PERIOD FOR SYSTEM_TIME ([BeginDate], [EndDate])
) ON [PRIMARY]
WITH
(
SYSTEM_VERSIONING = ON ( HISTORY_TABLE = [dbo].[tblCompletionDiaryHistory] )
)
GO
/****** Object:  Table [dbo].[tbl3PartyComplaintsFormHistory]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl3PartyComplaintsFormHistory](
	[ID] [int] NOT NULL,
	[MatterNumber] [varchar](20) NOT NULL,
	[ComplainantName] [varchar](50) NULL,
	[CenterID] [int] NOT NULL,
	[TeamID] [int] NOT NULL,
	[DateReceived] [datetime] NOT NULL,
	[ExecComplaint] [bit] NOT NULL,
	[CCSUnresponsive] [bit] NULL,
	[CCSRaisingrepeatedEnquiries] [bit] NULL,
	[CCSNotForwardingPPWinGoodTime] [bit] NULL,
	[CCSCausingDelays] [bit] NULL,
	[SourceID] [int] NULL,
	[AttachFile] [varchar](max) NULL,
	[ModifyingUser] [nvarchar](128) NULL,
	[Action] [varchar](1) NULL,
	[BeginDate] [datetime2](7) NOT NULL,
	[EndDate] [datetime2](7) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl3PartyComplaintsForm]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl3PartyComplaintsForm](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[MatterNumber] [varchar](20) NOT NULL,
	[ComplainantName] [varchar](50) NULL,
	[CenterID] [int] NOT NULL,
	[TeamID] [int] NOT NULL,
	[DateReceived] [datetime] NOT NULL,
	[ExecComplaint] [bit] NOT NULL,
	[CCSUnresponsive] [bit] NULL,
	[CCSRaisingrepeatedEnquiries] [bit] NULL,
	[CCSNotForwardingPPWinGoodTime] [bit] NULL,
	[CCSCausingDelays] [bit] NULL,
	[SourceID] [int] NULL,
	[AttachFile] [varchar](max) NULL,
	[ModifyingUser] [nvarchar](128) NULL,
	[Action] [varchar](1) NULL,
	[BeginDate] [datetime2](7) GENERATED ALWAYS AS ROW START NOT NULL,
	[EndDate] [datetime2](7) GENERATED ALWAYS AS ROW END NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
	PERIOD FOR SYSTEM_TIME ([BeginDate], [EndDate])
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
WITH
(
SYSTEM_VERSIONING = ON ( HISTORY_TABLE = [dbo].[tbl3PartyComplaintsFormHistory] )
)
GO
/****** Object:  Table [dbo].[DissatisfactionFormHistory]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DissatisfactionFormHistory](
	[Id] [int] NOT NULL,
	[Receivedfrom] [varchar](50) NOT NULL,
	[Execcomplaint] [int] NOT NULL,
	[Source] [int] NULL,
	[Datereceived] [datetime] NULL,
	[MatterNumberID] [varchar](100) NULL,
	[ABS] [bit] NULL,
	[CenterID] [int] NOT NULL,
	[TeamID] [int] NOT NULL,
	[SubsidaryBranch] [varchar](50) NULL,
	[RegionID] [int] NULL,
	[AttributedTo] [varchar](50) NULL,
	[Keyareasofdissat] [varchar](max) NULL,
	[UpdatesUD] [bit] NULL,
	[PPW_PaperworkWentMissing] [bit] NULL,
	[TS_OCNotHappyWithTimescales] [bit] NULL,
	[ERR_ErrorsOnPaperwork] [bit] NULL,
	[CB_CallBacksnotReturned] [bit] NULL,
	[LOC_OCPreferredalocalservice] [bit] NULL,
	[WebsiteIssues] [bit] NULL,
	[INC_OCToldIncorrectThings] [bit] NULL,
	[PLS_HardToSpeakToPL] [bit] NULL,
	[EA_EASaidItWouldTakeLessTime] [bit] NULL,
	[COMP_ErrorAtCompletion] [bit] NULL,
	[POC_FewerPointsOfContact] [bit] NULL,
	[CW_LongCallWaits] [bit] NULL,
	[INF_OCAskedForInformedAlreadyProvided] [bit] NULL,
	[RUD_OCWasSpokenToRudely] [bit] NULL,
	[Sdlt] [bit] NULL,
	[ALLDelaysallocatingtoVFTeamPL] [bit] NULL,
	[Transactionstatus] [int] NULL,
	[Hasnotebeenaddedtocriticaltoconfirmdissatisfaction] [bit] NOT NULL,
	[Ensureteammanagerisawareofdissat] [bit] NOT NULL,
	[SendTCFexamplestoMelBeatty] [bit] NULL,
	[CETCheck] [int] NOT NULL,
	[Resolution] [varchar](200) NULL,
	[SingleMultiId] [int] NOT NULL,
	[Name] [varchar](100) NULL,
	[AttachFile] [varchar](max) NOT NULL,
	[ModifyingUser] [nvarchar](128) NULL,
	[Action] [varchar](1) NULL,
	[BeginDate] [datetime2](7) NOT NULL,
	[EndDate] [datetime2](7) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblDissatisfactionForm]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblDissatisfactionForm](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Receivedfrom] [varchar](50) NOT NULL,
	[Execcomplaint] [int] NOT NULL,
	[Source] [int] NULL,
	[Datereceived] [datetime] NULL,
	[MatterNumberID] [varchar](100) NULL,
	[ABS] [bit] NULL,
	[CenterID] [int] NOT NULL,
	[TeamID] [int] NOT NULL,
	[SubsidaryBranch] [varchar](50) NULL,
	[RegionID] [int] NULL,
	[AttributedTo] [varchar](50) NULL,
	[Keyareasofdissat] [varchar](max) NULL,
	[UpdatesUD] [bit] NULL,
	[PPW_PaperworkWentMissing] [bit] NULL,
	[TS_OCNotHappyWithTimescales] [bit] NULL,
	[ERR_ErrorsOnPaperwork] [bit] NULL,
	[CB_CallBacksnotReturned] [bit] NULL,
	[LOC_OCPreferredalocalservice] [bit] NULL,
	[WebsiteIssues] [bit] NULL,
	[INC_OCToldIncorrectThings] [bit] NULL,
	[PLS_HardToSpeakToPL] [bit] NULL,
	[EA_EASaidItWouldTakeLessTime] [bit] NULL,
	[COMP_ErrorAtCompletion] [bit] NULL,
	[POC_FewerPointsOfContact] [bit] NULL,
	[CW_LongCallWaits] [bit] NULL,
	[INF_OCAskedForInformedAlreadyProvided] [bit] NULL,
	[RUD_OCWasSpokenToRudely] [bit] NULL,
	[Sdlt] [bit] NULL,
	[ALLDelaysallocatingtoVFTeamPL] [bit] NULL,
	[Transactionstatus] [int] NULL,
	[Hasnotebeenaddedtocriticaltoconfirmdissatisfaction] [bit] NOT NULL,
	[Ensureteammanagerisawareofdissat] [bit] NOT NULL,
	[SendTCFexamplestoMelBeatty] [bit] NULL,
	[CETCheck] [int] NOT NULL,
	[Resolution] [varchar](200) NULL,
	[SingleMultiId] [int] NOT NULL,
	[Name] [varchar](100) NULL,
	[AttachFile] [varchar](max) NOT NULL,
	[ModifyingUser] [nvarchar](128) NULL,
	[Action] [varchar](1) NULL,
	[BeginDate] [datetime2](7) GENERATED ALWAYS AS ROW START NOT NULL,
	[EndDate] [datetime2](7) GENERATED ALWAYS AS ROW END NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
	PERIOD FOR SYSTEM_TIME ([BeginDate], [EndDate])
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
WITH
(
SYSTEM_VERSIONING = ON ( HISTORY_TABLE = [dbo].[DissatisfactionFormHistory] )
)
GO
/****** Object:  Table [dbo].[tblForfeitOfLeaseFormHistory]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblForfeitOfLeaseFormHistory](
	[ID] [int] NOT NULL,
	[ClientName] [varchar](50) NOT NULL,
	[MatterNumber] [varchar](50) NOT NULL,
	[CenterID] [int] NOT NULL,
	[PropertyAddress] [varchar](max) NOT NULL,
	[ExchangeDate] [datetime] NULL,
	[CompletionDate] [datetime] NULL,
	[PropertyPrice] [money] NOT NULL,
	[PremiumIPAmount] [money] NOT NULL,
	[FeeEarner] [varchar](max) NOT NULL,
	[CAOnly] [bit] NULL,
	[AttachFile] [varchar](max) NOT NULL,
	[ModifyingUser] [nvarchar](128) NULL,
	[Action] [varchar](1) NULL,
	[BeginDate] [datetime2](7) NOT NULL,
	[EndDate] [datetime2](7) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblForfeitOfLeaseForm]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblForfeitOfLeaseForm](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ClientName] [varchar](50) NOT NULL,
	[MatterNumber] [varchar](50) NOT NULL,
	[CenterID] [int] NOT NULL,
	[PropertyAddress] [varchar](max) NOT NULL,
	[ExchangeDate] [datetime] NULL,
	[CompletionDate] [datetime] NULL,
	[PropertyPrice] [money] NOT NULL,
	[PremiumIPAmount] [money] NOT NULL,
	[FeeEarner] [varchar](max) NOT NULL,
	[CAOnly] [bit] NULL,
	[AttachFile] [varchar](max) NOT NULL,
	[ModifyingUser] [nvarchar](128) NULL,
	[Action] [varchar](1) NULL,
	[BeginDate] [datetime2](7) GENERATED ALWAYS AS ROW START NOT NULL,
	[EndDate] [datetime2](7) GENERATED ALWAYS AS ROW END NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
	PERIOD FOR SYSTEM_TIME ([BeginDate], [EndDate])
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
WITH
(
SYSTEM_VERSIONING = ON ( HISTORY_TABLE = [dbo].[tblForfeitOfLeaseFormHistory] )
)
GO
/****** Object:  Table [dbo].[tblHighValueIndemnityHistory]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblHighValueIndemnityHistory](
	[ID] [int] NOT NULL,
	[MatterNumber] [varchar](50) NOT NULL,
	[ClientName] [varchar](50) NOT NULL,
	[CenterID] [int] NOT NULL,
	[PropertyAddress] [varchar](max) NOT NULL,
	[ExchangeDate] [datetime] NOT NULL,
	[CompletionDate] [datetime] NOT NULL,
	[PropertyPrice] [money] NOT NULL,
	[PremiumIPAmount] [money] NOT NULL,
	[FeeEarner] [varchar](50) NOT NULL,
	[CAOnly] [bit] NULL,
	[Notes] [varchar](max) NULL,
	[AttachFile] [varchar](max) NOT NULL,
	[ModifyingUser] [nvarchar](128) NULL,
	[Action] [varchar](1) NULL,
	[BeginDate] [datetime2](7) NOT NULL,
	[EndDate] [datetime2](7) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblHighValueIndemnityForm]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblHighValueIndemnityForm](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[MatterNumber] [varchar](50) NOT NULL,
	[ClientName] [varchar](50) NOT NULL,
	[CenterID] [int] NOT NULL,
	[PropertyAddress] [varchar](max) NOT NULL,
	[ExchangeDate] [datetime] NOT NULL,
	[CompletionDate] [datetime] NOT NULL,
	[PropertyPrice] [money] NOT NULL,
	[PremiumIPAmount] [money] NOT NULL,
	[FeeEarner] [varchar](50) NOT NULL,
	[CAOnly] [bit] NULL,
	[Notes] [varchar](max) NULL,
	[AttachFile] [varchar](max) NOT NULL,
	[ModifyingUser] [nvarchar](128) NULL,
	[Action] [varchar](1) NULL,
	[BeginDate] [datetime2](7) GENERATED ALWAYS AS ROW START NOT NULL,
	[EndDate] [datetime2](7) GENERATED ALWAYS AS ROW END NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
	PERIOD FOR SYSTEM_TIME ([BeginDate], [EndDate])
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
WITH
(
SYSTEM_VERSIONING = ON ( HISTORY_TABLE = [dbo].[tblHighValueIndemnityHistory] )
)
GO
/****** Object:  Table [dbo].[tblIndemnityPoliciesFormHistory]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblIndemnityPoliciesFormHistory](
	[ID] [int] NOT NULL,
	[MatterNumber] [varchar](50) NOT NULL,
	[CenterID] [int] NOT NULL,
	[Address] [varchar](max) NOT NULL,
	[Premium] [money] NOT NULL,
	[QuoteNumber] [varchar](50) NOT NULL,
	[StatusID] [int] NOT NULL,
	[CompletionDate] [datetime] NOT NULL,
	[ModifyingUser] [nvarchar](128) NULL,
	[Action] [varchar](1) NULL,
	[BeginDate] [datetime2](7) NOT NULL,
	[EndDate] [datetime2](7) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblIndemnityPoliciesForm]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblIndemnityPoliciesForm](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[MatterNumber] [varchar](50) NOT NULL,
	[CenterID] [int] NOT NULL,
	[Address] [varchar](max) NOT NULL,
	[Premium] [money] NOT NULL,
	[QuoteNumber] [varchar](50) NOT NULL,
	[StatusID] [int] NOT NULL,
	[CompletionDate] [datetime] NOT NULL,
	[ModifyingUser] [nvarchar](128) NULL,
	[Action] [varchar](1) NULL,
	[BeginDate] [datetime2](7) GENERATED ALWAYS AS ROW START NOT NULL,
	[EndDate] [datetime2](7) GENERATED ALWAYS AS ROW END NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
	PERIOD FOR SYSTEM_TIME ([BeginDate], [EndDate])
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
WITH
(
SYSTEM_VERSIONING = ON ( HISTORY_TABLE = [dbo].[tblIndemnityPoliciesFormHistory] )
)
GO
/****** Object:  Table [dbo].[tblInterestPayableOCFormHistory]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblInterestPayableOCFormHistory](
	[ID] [int] NOT NULL,
	[MatterNumber] [varchar](50) NOT NULL,
	[CenterID] [int] NOT NULL,
	[TeamID] [int] NOT NULL,
	[DateFundsRevd] [datetime] NOT NULL,
	[PaymentDate] [datetime] NOT NULL,
	[FundsHeld] [int] NOT NULL,
	[ReasonForInterestPayment] [varchar](max) NOT NULL,
	[Posted] [bit] NULL,
	[Formula_25] [decimal](18, 2) NOT NULL,
	[NumberOfDatesFundsHeld] [int] NOT NULL,
	[ModifyingUser] [nvarchar](128) NULL,
	[Action] [varchar](1) NULL,
	[BeginDate] [datetime2](7) NOT NULL,
	[EndDate] [datetime2](7) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblInterestPayableOCForm]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblInterestPayableOCForm](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[MatterNumber] [varchar](50) NOT NULL,
	[CenterID] [int] NOT NULL,
	[TeamID] [int] NOT NULL,
	[DateFundsRevd] [datetime] NOT NULL,
	[PaymentDate] [datetime] NOT NULL,
	[FundsHeld] [int] NOT NULL,
	[ReasonForInterestPayment] [varchar](max) NOT NULL,
	[Posted] [bit] NULL,
	[Formula_25] [decimal](18, 2) NOT NULL,
	[NumberOfDatesFundsHeld] [int] NOT NULL,
	[ModifyingUser] [nvarchar](128) NULL,
	[Action] [varchar](1) NULL,
	[BeginDate] [datetime2](7) GENERATED ALWAYS AS ROW START NOT NULL,
	[EndDate] [datetime2](7) GENERATED ALWAYS AS ROW END NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
	PERIOD FOR SYSTEM_TIME ([BeginDate], [EndDate])
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
WITH
(
SYSTEM_VERSIONING = ON ( HISTORY_TABLE = [dbo].[tblInterestPayableOCFormHistory] )
)
GO
/****** Object:  Table [dbo].[tblClientToClientTransfersFormHistory]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblClientToClientTransfersFormHistory](
	[ID] [int] NOT NULL,
	[MatterNumberFrom] [varchar](50) NOT NULL,
	[MatterNumberTo] [varchar](20) NOT NULL,
	[AmountTransfered] [money] NOT NULL,
	[Reason] [varchar](200) NOT NULL,
	[Transferred] [bit] NULL,
	[ModifyingUser] [nvarchar](128) NULL,
	[Action] [varchar](1) NULL,
	[BeginDate] [datetime2](7) NOT NULL,
	[EndDate] [datetime2](7) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblClientToClientTransfersForm]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblClientToClientTransfersForm](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[MatterNumberFrom] [varchar](50) NOT NULL,
	[MatterNumberTo] [varchar](20) NOT NULL,
	[AmountTransfered] [money] NOT NULL,
	[Reason] [varchar](200) NOT NULL,
	[Transferred] [bit] NULL,
	[ModifyingUser] [nvarchar](128) NULL,
	[Action] [varchar](1) NULL,
	[BeginDate] [datetime2](7) GENERATED ALWAYS AS ROW START NOT NULL,
	[EndDate] [datetime2](7) GENERATED ALWAYS AS ROW END NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
	PERIOD FOR SYSTEM_TIME ([BeginDate], [EndDate])
) ON [PRIMARY]
WITH
(
SYSTEM_VERSIONING = ON ( HISTORY_TABLE = [dbo].[tblClientToClientTransfersFormHistory] )
)
GO
/****** Object:  Table [dbo].[tblTaskLawyerReportFormHistory]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblTaskLawyerReportFormHistory](
	[ID] [int] NOT NULL,
	[CenterId] [int] NOT NULL,
	[KTL_MentorID] [int] NOT NULL,
	[Overtime] [bit] NOT NULL,
	[StartTime] [datetime] NOT NULL,
	[TeamId] [int] NOT NULL,
	[ABS] [bit] NULL,
	[Enter_FEInitialsID] [int] NOT NULL,
	[LawyerGradeID] [int] NOT NULL,
	[MatterNumber] [varchar](20) NOT NULL,
	[TaskID] [int] NOT NULL,
	[EndTime] [datetime] NOT NULL,
	[Notes_Comments] [varchar](max) NULL,
	[Annotated] [bit] NULL,
	[Admin] [varchar](50) NULL,
	[HolidayCoverage] [bit] NULL,
	[AdditionalFee] [bit] NULL,
	[ReasonForAdditionalFee] [varchar](100) NULL,
	[FeeAmount] [money] NULL,
	[VATDue] [money] NULL,
	[ModifyingUser] [nvarchar](128) NULL,
	[Action] [varchar](1) NULL,
	[BeginDate] [datetime2](7) NOT NULL,
	[EndDate] [datetime2](7) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblTaskLawyerReportForm]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblTaskLawyerReportForm](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CenterId] [int] NOT NULL,
	[KTL_MentorID] [int] NOT NULL,
	[Overtime] [bit] NOT NULL,
	[StartTime] [datetime] NOT NULL,
	[TeamId] [int] NOT NULL,
	[ABS] [bit] NULL,
	[Enter_FEInitialsID] [int] NOT NULL,
	[LawyerGradeID] [int] NOT NULL,
	[MatterNumber] [varchar](20) NOT NULL,
	[TaskID] [int] NOT NULL,
	[EndTime] [datetime] NOT NULL,
	[Notes_Comments] [varchar](max) NULL,
	[Annotated] [bit] NULL,
	[Admin] [varchar](50) NULL,
	[HolidayCoverage] [bit] NULL,
	[AdditionalFee] [bit] NULL,
	[ReasonForAdditionalFee] [varchar](100) NULL,
	[FeeAmount] [money] NULL,
	[VATDue] [money] NULL,
	[ModifyingUser] [nvarchar](128) NULL,
	[Action] [varchar](1) NULL,
	[BeginDate] [datetime2](7) GENERATED ALWAYS AS ROW START NOT NULL,
	[EndDate] [datetime2](7) GENERATED ALWAYS AS ROW END NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
	PERIOD FOR SYSTEM_TIME ([BeginDate], [EndDate])
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
WITH
(
SYSTEM_VERSIONING = ON ( HISTORY_TABLE = [dbo].[tblTaskLawyerReportFormHistory] )
)
GO
/****** Object:  Table [dbo].[SalesLeadFormHistory]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SalesLeadFormHistory](
	[ID] [int] NOT NULL,
	[ConsultantId] [int] NOT NULL,
	[LeadReceived] [datetime] NOT NULL,
	[SourceID] [int] NOT NULL,
	[AreaID] [int] NOT NULL,
	[BrandId] [int] NOT NULL,
	[BranchId] [int] NOT NULL,
	[ReferrerId] [int] NOT NULL,
	[Referrer_Other] [varchar](50) NULL,
	[TransactionId] [int] NULL,
	[CustomerName] [varchar](50) NOT NULL,
	[CustomerMobile] [int] NULL,
	[CustomerEmail] [varchar](50) NULL,
	[PropertyPrice] [money] NULL,
	[NextFollowUpDue] [datetime] NULL,
	[LeadStatusId] [int] NOT NULL,
	[PendingReasonId] [int] NULL,
	[DeclineReasonId] [int] NULL,
	[NotValidreasonId] [int] NULL,
	[Notes] [varchar](max) NULL,
	[PC] [bit] NULL,
	[InstructionDate] [datetime] NULL,
	[eDif_Or_TMID] [varchar](50) NULL,
	[TotalFeeQuoted] [varchar](50) NULL,
	[Discount] [money] NULL,
	[Revenue] [money] NULL,
	[AttachFile] [varchar](max) NULL,
	[ModifyingUser] [nvarchar](128) NULL,
	[Action] [varchar](1) NULL,
	[BeginDate] [datetime2](7) NOT NULL,
	[EndDate] [datetime2](7) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblSalesLeadForm]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblSalesLeadForm](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ConsultantId] [int] NOT NULL,
	[LeadReceived] [datetime] NOT NULL,
	[SourceID] [int] NOT NULL,
	[AreaID] [int] NOT NULL,
	[BrandId] [int] NOT NULL,
	[BranchId] [int] NOT NULL,
	[ReferrerId] [int] NOT NULL,
	[Referrer_Other] [varchar](50) NULL,
	[TransactionId] [int] NULL,
	[CustomerName] [varchar](50) NOT NULL,
	[CustomerMobile] [int] NULL,
	[CustomerEmail] [varchar](50) NULL,
	[PropertyPrice] [money] NULL,
	[NextFollowUpDue] [datetime] NULL,
	[LeadStatusId] [int] NOT NULL,
	[PendingReasonId] [int] NULL,
	[DeclineReasonId] [int] NULL,
	[NotValidreasonId] [int] NULL,
	[Notes] [varchar](max) NULL,
	[PC] [bit] NULL,
	[InstructionDate] [datetime] NULL,
	[eDif_Or_TMID] [varchar](50) NULL,
	[TotalFeeQuoted] [varchar](50) NULL,
	[Discount] [money] NULL,
	[Revenue] [money] NULL,
	[AttachFile] [varchar](max) NULL,
	[ModifyingUser] [nvarchar](128) NULL,
	[Action] [varchar](1) NULL,
	[BeginDate] [datetime2](7) GENERATED ALWAYS AS ROW START NOT NULL,
	[EndDate] [datetime2](7) GENERATED ALWAYS AS ROW END NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
	PERIOD FOR SYSTEM_TIME ([BeginDate], [EndDate])
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
WITH
(
SYSTEM_VERSIONING = ON ( HISTORY_TABLE = [dbo].[SalesLeadFormHistory] )
)
GO
/****** Object:  Table [dbo].[tblCreditCardRefundFormHistory]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblCreditCardRefundFormHistory](
	[ID] [int] NOT NULL,
	[ClientName] [varchar](50) NOT NULL,
	[MatterNumber] [varchar](50) NOT NULL,
	[InstructingBranchId] [int] NOT NULL,
	[eDIFPaymentRef] [varchar](20) NULL,
	[RefundAmount] [money] NOT NULL,
	[ReasonForRefundID] [int] NOT NULL,
	[FurtherDetail] [varchar](200) NULL,
	[StatusID] [int] NOT NULL,
	[ModifyingUser] [nvarchar](128) NULL,
	[Action] [varchar](1) NULL,
	[BeginDate] [datetime2](7) NOT NULL,
	[EndDate] [datetime2](7) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblCreditCardRefundForm]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblCreditCardRefundForm](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ClientName] [varchar](50) NOT NULL,
	[MatterNumber] [varchar](50) NOT NULL,
	[InstructingBranchId] [int] NOT NULL,
	[eDIFPaymentRef] [varchar](20) NULL,
	[RefundAmount] [money] NOT NULL,
	[ReasonForRefundID] [int] NOT NULL,
	[FurtherDetail] [varchar](200) NULL,
	[StatusID] [int] NOT NULL,
	[ModifyingUser] [nvarchar](128) NULL,
	[Action] [varchar](1) NULL,
	[BeginDate] [datetime2](7) GENERATED ALWAYS AS ROW START NOT NULL,
	[EndDate] [datetime2](7) GENERATED ALWAYS AS ROW END NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
	PERIOD FOR SYSTEM_TIME ([BeginDate], [EndDate])
) ON [PRIMARY]
WITH
(
SYSTEM_VERSIONING = ON ( HISTORY_TABLE = [dbo].[tblCreditCardRefundFormHistory] )
)
GO
/****** Object:  Table [dbo].[StopChequeFormHistory]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StopChequeFormHistory](
	[ID] [int] NOT NULL,
	[MatterNumber] [varchar](50) NOT NULL,
	[ClientName] [varchar](50) NOT NULL,
	[CenterID] [int] NOT NULL,
	[Payee] [varchar](50) NOT NULL,
	[IssuedDate] [datetime] NULL,
	[Amount] [money] NOT NULL,
	[ReasonForStoppingCheque] [varchar](max) NOT NULL,
	[AdvisePayeeChequeStopped] [varchar](max) NOT NULL,
	[AssignedTo] [varchar](50) NOT NULL,
	[StatusId] [int] NOT NULL,
	[ChqNo] [varchar](20) NULL,
	[ModifyingUser] [nvarchar](128) NULL,
	[Action] [varchar](1) NULL,
	[BeginDate] [datetime2](7) NOT NULL,
	[EndDate] [datetime2](7) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblStopChequeForm]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblStopChequeForm](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[MatterNumber] [varchar](50) NOT NULL,
	[ClientName] [varchar](50) NOT NULL,
	[CenterID] [int] NOT NULL,
	[Payee] [varchar](50) NOT NULL,
	[IssuedDate] [datetime] NULL,
	[Amount] [money] NOT NULL,
	[ReasonForStoppingCheque] [varchar](max) NOT NULL,
	[AdvisePayeeChequeStopped] [varchar](max) NOT NULL,
	[AssignedTo] [varchar](50) NOT NULL,
	[StatusId] [int] NOT NULL,
	[ChqNo] [varchar](20) NULL,
	[ModifyingUser] [nvarchar](128) NULL,
	[Action] [varchar](1) NULL,
	[BeginDate] [datetime2](7) GENERATED ALWAYS AS ROW START NOT NULL,
	[EndDate] [datetime2](7) GENERATED ALWAYS AS ROW END NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
	PERIOD FOR SYSTEM_TIME ([BeginDate], [EndDate])
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
WITH
(
SYSTEM_VERSIONING = ON ( HISTORY_TABLE = [dbo].[StopChequeFormHistory] )
)
GO
/****** Object:  Table [dbo].[UnallocatedFundsFormHistory]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UnallocatedFundsFormHistory](
	[ID] [int] NOT NULL,
	[Date] [datetime] NOT NULL,
	[Amount] [money] NOT NULL,
	[PaymentMethodID] [int] NOT NULL,
	[PaymentDetails] [varchar](50) NOT NULL,
	[CANotes] [varchar](max) NULL,
	[MatterNumber] [varchar](50) NULL,
	[StatusID] [int] NOT NULL,
	[UnallocatedNoID] [int] NOT NULL,
	[UnallocatedMoniesVF2] [varchar](50) NULL,
	[ModifyingUser] [nvarchar](128) NULL,
	[Action] [varchar](1) NULL,
	[BeginDate] [datetime2](7) NOT NULL,
	[EndDate] [datetime2](7) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblUnallocatedFundsForm]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblUnallocatedFundsForm](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Date] [datetime] NOT NULL,
	[Amount] [money] NOT NULL,
	[PaymentMethodID] [int] NOT NULL,
	[PaymentDetails] [varchar](50) NOT NULL,
	[CANotes] [varchar](max) NULL,
	[MatterNumber] [varchar](50) NULL,
	[StatusID] [int] NOT NULL,
	[UnallocatedNoID] [int] NOT NULL,
	[UnallocatedMoniesVF2] [varchar](50) NULL,
	[ModifyingUser] [nvarchar](128) NULL,
	[Action] [varchar](1) NULL,
	[BeginDate] [datetime2](7) GENERATED ALWAYS AS ROW START NOT NULL,
	[EndDate] [datetime2](7) GENERATED ALWAYS AS ROW END NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
	PERIOD FOR SYSTEM_TIME ([BeginDate], [EndDate])
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
WITH
(
SYSTEM_VERSIONING = ON ( HISTORY_TABLE = [dbo].[UnallocatedFundsFormHistory] )
)
GO
/****** Object:  Table [dbo].[tblBridgeControlFormHistory]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblBridgeControlFormHistory](
	[ID] [int] NOT NULL,
	[DateAuth] [datetime] NOT NULL,
	[SubmittedBy] [varchar](max) NOT NULL,
	[CenterID] [int] NOT NULL,
	[Team] [varchar](max) NOT NULL,
	[MatterNumber] [varchar](50) NOT NULL,
	[ClientName] [varchar](50) NOT NULL,
	[CategoryID] [int] NOT NULL,
	[MatterCompletionDate] [datetime] NULL,
	[Amount] [money] NOT NULL,
	[RiskId] [int] NOT NULL,
	[Explanation] [varchar](max) NOT NULL,
	[ActiontoRecoverFunds] [varchar](max) NOT NULL,
	[ExpectedDateofReturn] [datetime] NULL,
	[Authorisedby] [varchar](max) NOT NULL,
	[Comments] [varchar](max) NULL,
	[Outstanding] [money] NOT NULL,
	[DateRepaid] [datetime] NULL,
	[Dateofnextupdate] [datetime] NULL,
	[AttachFile] [varchar](max) NULL,
	[ModifyingUser] [nvarchar](128) NULL,
	[Action] [varchar](1) NULL,
	[BeginDate] [datetime2](7) NOT NULL,
	[EndDate] [datetime2](7) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblBridgeControlForm]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblBridgeControlForm](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DateAuth] [datetime] NOT NULL,
	[SubmittedBy] [varchar](max) NOT NULL,
	[CenterID] [int] NOT NULL,
	[Team] [varchar](max) NOT NULL,
	[MatterNumber] [varchar](50) NOT NULL,
	[ClientName] [varchar](50) NOT NULL,
	[CategoryID] [int] NOT NULL,
	[MatterCompletionDate] [datetime] NULL,
	[Amount] [money] NOT NULL,
	[RiskId] [int] NOT NULL,
	[Explanation] [varchar](max) NOT NULL,
	[ActiontoRecoverFunds] [varchar](max) NOT NULL,
	[ExpectedDateofReturn] [datetime] NULL,
	[Authorisedby] [varchar](max) NOT NULL,
	[Comments] [varchar](max) NULL,
	[Outstanding] [money] NOT NULL,
	[DateRepaid] [datetime] NULL,
	[Dateofnextupdate] [datetime] NULL,
	[AttachFile] [varchar](max) NULL,
	[ModifyingUser] [nvarchar](128) NULL,
	[Action] [varchar](1) NULL,
	[BeginDate] [datetime2](7) GENERATED ALWAYS AS ROW START NOT NULL,
	[EndDate] [datetime2](7) GENERATED ALWAYS AS ROW END NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
	PERIOD FOR SYSTEM_TIME ([BeginDate], [EndDate])
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
WITH
(
SYSTEM_VERSIONING = ON ( HISTORY_TABLE = [dbo].[tblBridgeControlFormHistory] )
)
GO
/****** Object:  Table [dbo].[tblCallCoachingLogHistory]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblCallCoachingLogHistory](
	[ID] [int] NOT NULL,
	[FeedbackdeliveredbyCET] [bit] NULL,
	[Month] [varchar](50) NOT NULL,
	[PL_LAName] [varchar](50) NULL,
	[RoleID] [int] NULL,
	[CentreID] [int] NULL,
	[TeamID] [int] NULL,
	[TMID] [int] NULL,
	[Numberofcalls] [varchar](50) NULL,
	[Reasonforobservation] [int] NULL,
	[ReasonforobservationOwnValue] [varchar](50) NULL,
	[STOP_Jargon] [bit] NULL,
	[STOP_Beingdefensive_argumentative] [bit] NULL,
	[STOP_RefusingTransferredCalls] [bit] NULL,
	[STOP_UsingInformalLanguage] [bit] NULL,
	[STOP_CriticisingEA_FS_OL] [bit] NULL,
	[STOP_Freetext] [varchar](max) NULL,
	[START_DPAOnAllCalls] [bit] NULL,
	[START_CheckUunderstanding] [bit] NULL,
	[START_AskAnyFurtherQuestions] [bit] NULL,
	[START_ExplainNextSteps] [bit] NULL,
	[START_ExplainUnavoidableJargon] [bit] NULL,
	[START_GiveDDI] [bit] NULL,
	[START_SoundConfident] [bit] NULL,
	[START_AdjustDeliveryToSuitCaller] [bit] NULL,
	[START_Freetext] [varchar](max) NULL,
	[CONTINUE_DPAOnAllCalls] [bit] NULL,
	[CONTINUE_CheckUnderstanding] [bit] NULL,
	[CONTINUE_AskAnyFurtherQuestions] [bit] NULL,
	[CONTINUE_ExplainNextSteps] [bit] NULL,
	[CONTINUE_ExplainUnavoidableJargon] [bit] NULL,
	[CONTINUE_GiveDDI] [bit] NULL,
	[CONTINUE_SoundConfident] [bit] NULL,
	[CONTINUE_AdjustDeliveryToSuitCaller] [bit] NULL,
	[CONTINUE_Empathy] [bit] NULL,
	[CONTINUE_BuildRapport] [bit] NULL,
	[CONTINUE_UsePositiveLanguage] [bit] NULL,
	[CONTINUE_Freetext] [varchar](max) NULL,
	[HistoryNotes] [varchar](max) NULL,
	[GeneralObservations] [varchar](max) NULL,
	[FeedbackFromResTeam] [varchar](max) NULL,
	[AttachFile] [varchar](max) NOT NULL,
	[ModifyingUser] [nvarchar](128) NULL,
	[Action] [varchar](1) NULL,
	[BeginDate] [datetime2](7) NOT NULL,
	[EndDate] [datetime2](7) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblCallCoachingLog]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblCallCoachingLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FeedbackdeliveredbyCET] [bit] NULL,
	[Month] [varchar](50) NOT NULL,
	[PL_LAName] [varchar](50) NULL,
	[RoleID] [int] NULL,
	[CentreID] [int] NULL,
	[TeamID] [int] NULL,
	[TMID] [int] NULL,
	[Numberofcalls] [varchar](50) NULL,
	[Reasonforobservation] [int] NULL,
	[ReasonforobservationOwnValue] [varchar](50) NULL,
	[STOP_Jargon] [bit] NULL,
	[STOP_Beingdefensive_argumentative] [bit] NULL,
	[STOP_RefusingTransferredCalls] [bit] NULL,
	[STOP_UsingInformalLanguage] [bit] NULL,
	[STOP_CriticisingEA_FS_OL] [bit] NULL,
	[STOP_Freetext] [varchar](max) NULL,
	[START_DPAOnAllCalls] [bit] NULL,
	[START_CheckUunderstanding] [bit] NULL,
	[START_AskAnyFurtherQuestions] [bit] NULL,
	[START_ExplainNextSteps] [bit] NULL,
	[START_ExplainUnavoidableJargon] [bit] NULL,
	[START_GiveDDI] [bit] NULL,
	[START_SoundConfident] [bit] NULL,
	[START_AdjustDeliveryToSuitCaller] [bit] NULL,
	[START_Freetext] [varchar](max) NULL,
	[CONTINUE_DPAOnAllCalls] [bit] NULL,
	[CONTINUE_CheckUnderstanding] [bit] NULL,
	[CONTINUE_AskAnyFurtherQuestions] [bit] NULL,
	[CONTINUE_ExplainNextSteps] [bit] NULL,
	[CONTINUE_ExplainUnavoidableJargon] [bit] NULL,
	[CONTINUE_GiveDDI] [bit] NULL,
	[CONTINUE_SoundConfident] [bit] NULL,
	[CONTINUE_AdjustDeliveryToSuitCaller] [bit] NULL,
	[CONTINUE_Empathy] [bit] NULL,
	[CONTINUE_BuildRapport] [bit] NULL,
	[CONTINUE_UsePositiveLanguage] [bit] NULL,
	[CONTINUE_Freetext] [varchar](max) NULL,
	[HistoryNotes] [varchar](max) NULL,
	[GeneralObservations] [varchar](max) NULL,
	[FeedbackFromResTeam] [varchar](max) NULL,
	[AttachFile] [varchar](max) NOT NULL,
	[ModifyingUser] [nvarchar](128) NULL,
	[Action] [varchar](1) NULL,
	[BeginDate] [datetime2](7) GENERATED ALWAYS AS ROW START NOT NULL,
	[EndDate] [datetime2](7) GENERATED ALWAYS AS ROW END NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
	PERIOD FOR SYSTEM_TIME ([BeginDate], [EndDate])
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
WITH
(
SYSTEM_VERSIONING = ON ( HISTORY_TABLE = [dbo].[tblCallCoachingLogHistory] )
)
GO
/****** Object:  Table [dbo].[tblTitleAllocationFormHistory]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblTitleAllocationFormHistory](
	[ID] [int] NOT NULL,
	[MatterNumber] [varchar](20) NOT NULL,
	[StatusID] [int] NULL,
	[CenterID] [int] NOT NULL,
	[TitleCheckerID] [int] NOT NULL,
	[CreationDateByWNS] [datetime] NOT NULL,
	[AssignedTo] [varchar](50) NOT NULL,
	[Message] [nvarchar](max) NULL,
	[MentorId] [int] NOT NULL,
	[DateAllocatedToMentor] [datetime] NULL,
	[MentorInitialCheckComplete] [datetime] NULL,
	[FirstTitleAmendmentsCompleted] [datetime] NULL,
	[OffshoreQCDone] [bit] NULL,
	[FinalNumberRevisionsNeeded] [int] NOT NULL,
	[NumberOfErrors] [int] NOT NULL,
	[FatalityOfErrorId] [int] NOT NULL,
	[DateCompleted] [datetime] NULL,
	[ABSMatter] [bit] NULL,
	[CategoryOfTitleId] [int] NULL,
	[Feedback] [nvarchar](max) NULL,
	[ErrorThemes] [bit] NULL,
	[AuditOrSignOffId] [int] NULL,
	[AttachFile] [varchar](max) NULL,
	[ModifyingUser] [nvarchar](128) NULL,
	[Action] [varchar](1) NULL,
	[BeginDate] [datetime2](7) NOT NULL,
	[EndDate] [datetime2](7) NOT NULL,
	[MentorSupportId] [int] NULL,
	[ErrorThemes1] [bit] NULL,
	[ErrorThemes2] [bit] NULL,
	[ErrorThemes3] [bit] NULL,
	[ErrorThemes4] [bit] NULL,
	[ErrorThemes5] [bit] NULL,
	[ErrorThemes6] [bit] NULL,
	[ErrorThemes7] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Index [ix_tblTitleAllocationFormHistory]    Script Date: 07/10/2021 13:24:19 ******/
CREATE CLUSTERED INDEX [ix_tblTitleAllocationFormHistory] ON [dbo].[tblTitleAllocationFormHistory]
(
	[EndDate] ASC,
	[BeginDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblTitleAllocationForm]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblTitleAllocationForm](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[MatterNumber] [varchar](20) NOT NULL,
	[StatusID] [int] NULL,
	[CenterID] [int] NOT NULL,
	[TitleCheckerID] [int] NOT NULL,
	[CreationDateByWNS] [datetime] NOT NULL,
	[AssignedTo] [varchar](50) NOT NULL,
	[Message] [nvarchar](max) NULL,
	[MentorId] [int] NOT NULL,
	[DateAllocatedToMentor] [datetime] NULL,
	[MentorInitialCheckComplete] [datetime] NULL,
	[FirstTitleAmendmentsCompleted] [datetime] NULL,
	[OffshoreQCDone] [bit] NULL,
	[FinalNumberRevisionsNeeded] [int] NOT NULL,
	[NumberOfErrors] [int] NOT NULL,
	[FatalityOfErrorId] [int] NOT NULL,
	[DateCompleted] [datetime] NULL,
	[ABSMatter] [bit] NULL,
	[CategoryOfTitleId] [int] NULL,
	[Feedback] [nvarchar](max) NULL,
	[ErrorThemes] [bit] NULL,
	[AuditOrSignOffId] [int] NULL,
	[AttachFile] [varchar](max) NULL,
	[ModifyingUser] [nvarchar](128) NULL,
	[Action] [varchar](1) NULL,
	[BeginDate] [datetime2](7) GENERATED ALWAYS AS ROW START NOT NULL,
	[EndDate] [datetime2](7) GENERATED ALWAYS AS ROW END NOT NULL,
	[MentorSupportId] [int] NULL,
	[ErrorThemes1] [bit] NULL,
	[ErrorThemes2] [bit] NULL,
	[ErrorThemes3] [bit] NULL,
	[ErrorThemes4] [bit] NULL,
	[ErrorThemes5] [bit] NULL,
	[ErrorThemes6] [bit] NULL,
	[ErrorThemes7] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
	PERIOD FOR SYSTEM_TIME ([BeginDate], [EndDate])
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
WITH
(
SYSTEM_VERSIONING = ON ( HISTORY_TABLE = [dbo].[tblTitleAllocationFormHistory] )
)
GO
/****** Object:  Table [dbo].[tblWriteOffsFormHistory]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblWriteOffsFormHistory](
	[ID] [int] NOT NULL,
	[PositionOfRequester] [varchar](50) NOT NULL,
	[CentreWriteOffAgainstID] [int] NOT NULL,
	[TeamWriteOffAgainstID] [int] NOT NULL,
	[FeeEarnersWriteOffAgainst] [varchar](100) NOT NULL,
	[MatterNumber] [varchar](50) NOT NULL,
	[ClientName] [varchar](50) NOT NULL,
	[CompletionDate] [datetime] NULL,
	[Amount] [money] NOT NULL,
	[ReasonforWriteOffID] [int] NOT NULL,
	[Narrative] [varchar](max) NOT NULL,
	[Authoriser] [varchar](50) NULL,
	[StatusID] [int] NOT NULL,
	[AttachFile] [varchar](max) NULL,
	[ModifyingUser] [nvarchar](128) NULL,
	[Action] [varchar](1) NULL,
	[BeginDate] [datetime2](7) NOT NULL,
	[EndDate] [datetime2](7) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblWriteOffsForm]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblWriteOffsForm](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PositionOfRequester] [varchar](50) NOT NULL,
	[CentreWriteOffAgainstID] [int] NOT NULL,
	[TeamWriteOffAgainstID] [int] NOT NULL,
	[FeeEarnersWriteOffAgainst] [varchar](100) NOT NULL,
	[MatterNumber] [varchar](50) NOT NULL,
	[ClientName] [varchar](50) NOT NULL,
	[CompletionDate] [datetime] NULL,
	[Amount] [money] NOT NULL,
	[ReasonforWriteOffID] [int] NOT NULL,
	[Narrative] [varchar](max) NOT NULL,
	[Authoriser] [varchar](50) NULL,
	[StatusID] [int] NOT NULL,
	[AttachFile] [varchar](max) NULL,
	[ModifyingUser] [nvarchar](128) NULL,
	[Action] [varchar](1) NULL,
	[BeginDate] [datetime2](7) GENERATED ALWAYS AS ROW START NOT NULL,
	[EndDate] [datetime2](7) GENERATED ALWAYS AS ROW END NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
	PERIOD FOR SYSTEM_TIME ([BeginDate], [EndDate])
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
WITH
(
SYSTEM_VERSIONING = ON ( HISTORY_TABLE = [dbo].[tblWriteOffsFormHistory] )
)
GO
/****** Object:  Table [dbo].[CardPaymentRequestFormHistory]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CardPaymentRequestFormHistory](
	[ID] [int] NOT NULL,
	[MatterID] [varchar](50) NOT NULL,
	[ClientName] [varchar](50) NOT NULL,
	[ContactNumber] [varchar](50) NOT NULL,
	[DateTimeToCall] [datetime] NOT NULL,
	[Amount] [money] NOT NULL,
	[PaymentReason] [varchar](max) NULL,
	[PaymentTaken] [bit] NULL,
	[UnableToContact] [bit] NULL,
	[CANotes] [varchar](max) NULL,
	[RequesterName] [varchar](50) NOT NULL,
	[AttachedFile] [varchar](max) NOT NULL,
	[ModifyingUser] [nvarchar](128) NULL,
	[Action] [varchar](1) NULL,
	[BeginDate] [datetime2](7) NOT NULL,
	[EndDate] [datetime2](7) NOT NULL,
	[ClientIsExpectingCall] [bit] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblCardPaymentRequestForm]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblCardPaymentRequestForm](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[MatterID] [varchar](50) NOT NULL,
	[ClientName] [varchar](50) NOT NULL,
	[ContactNumber] [varchar](50) NOT NULL,
	[DateTimeToCall] [datetime] NOT NULL,
	[Amount] [money] NOT NULL,
	[PaymentReason] [varchar](max) NULL,
	[PaymentTaken] [bit] NULL,
	[UnableToContact] [bit] NULL,
	[CANotes] [varchar](max) NULL,
	[RequesterName] [varchar](50) NOT NULL,
	[AttachedFile] [varchar](max) NOT NULL,
	[ModifyingUser] [nvarchar](128) NULL,
	[Action] [varchar](1) NULL,
	[BeginDate] [datetime2](7) GENERATED ALWAYS AS ROW START NOT NULL,
	[EndDate] [datetime2](7) GENERATED ALWAYS AS ROW END NOT NULL,
	[ClientIsExpectingCall] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
	PERIOD FOR SYSTEM_TIME ([BeginDate], [EndDate])
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
WITH
(
SYSTEM_VERSIONING = ON ( HISTORY_TABLE = [dbo].[CardPaymentRequestFormHistory] )
)
GO
/****** Object:  Table [dbo].[tblOutOfHoursIDCheckProcessFormHistory]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblOutOfHoursIDCheckProcessFormHistory](
	[ID] [int] NOT NULL,
	[Datetimetocall] [datetime] NOT NULL,
	[MatterNumber] [varchar](20) NOT NULL,
	[PersonNeedingAMLCheck] [varchar](50) NOT NULL,
	[BestNumberToCall] [varchar](50) NOT NULL,
	[AdditionalInstructions] [varchar](max) NULL,
	[PropertyLawyerEmailAddress] [varchar](50) NOT NULL,
	[EmailAddressYours] [varchar](50) NULL,
	[AMLCallSuccessfull] [bit] NULL,
	[ConsultantID] [int] NULL,
	[Consultant_Other] [varchar](50) NULL,
	[OOHNotes] [varchar](max) NULL,
	[AttachFile] [varchar](max) NULL,
	[ModifyingUser] [nvarchar](128) NULL,
	[Action] [varchar](1) NULL,
	[BeginDate] [datetime2](7) NOT NULL,
	[EndDate] [datetime2](7) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblOutOfHoursIDCheckProcessForm]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblOutOfHoursIDCheckProcessForm](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Datetimetocall] [datetime] NOT NULL,
	[MatterNumber] [varchar](20) NOT NULL,
	[PersonNeedingAMLCheck] [varchar](50) NOT NULL,
	[BestNumberToCall] [varchar](50) NOT NULL,
	[AdditionalInstructions] [varchar](max) NULL,
	[PropertyLawyerEmailAddress] [varchar](50) NOT NULL,
	[EmailAddressYours] [varchar](50) NULL,
	[AMLCallSuccessfull] [bit] NULL,
	[ConsultantID] [int] NULL,
	[Consultant_Other] [varchar](50) NULL,
	[OOHNotes] [varchar](max) NULL,
	[AttachFile] [varchar](max) NULL,
	[ModifyingUser] [nvarchar](128) NULL,
	[Action] [varchar](1) NULL,
	[BeginDate] [datetime2](7) GENERATED ALWAYS AS ROW START NOT NULL,
	[EndDate] [datetime2](7) GENERATED ALWAYS AS ROW END NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
	PERIOD FOR SYSTEM_TIME ([BeginDate], [EndDate])
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
WITH
(
SYSTEM_VERSIONING = ON ( HISTORY_TABLE = [dbo].[tblOutOfHoursIDCheckProcessFormHistory] )
)
GO
/****** Object:  Table [dbo].[tblComplaintsFormHistory]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblComplaintsFormHistory](
	[Id] [int] NOT NULL,
	[CenterID] [int] NOT NULL,
	[TeamID] [int] NOT NULL,
	[MatterNumberID] [varchar](100) NOT NULL,
	[ABSWith] [varchar](100) NULL,
	[FeeEarner] [varchar](100) NOT NULL,
	[ClientName] [varchar](100) NOT NULL,
	[TransactionTypeID] [int] NOT NULL,
	[SubsidaryBranch] [varchar](50) NULL,
	[RegionID] [int] NOT NULL,
	[InstructionDateReceived] [datetime] NULL,
	[FirstCallMadeOn] [datetime] NULL,
	[DateOfExchange] [datetime] NULL,
	[InstructionToExchange] [varchar](100) NOT NULL,
	[CompletionDate] [datetime] NOT NULL,
	[ComplaintReceivedDate] [datetime] NULL,
	[ComplaintReceivedID] [int] NULL,
	[ComplianceAwareness] [bit] NULL,
	[CriticalNote] [bit] NULL,
	[ResolutionTeamCopy] [bit] NULL,
	[AttributedTo] [varchar](100) NULL,
	[EnsurecopycomplaintsavedtofileandcataloguedinCustExperienceFolder] [bit] NULL,
	[SummaryIssues] [varchar](max) NOT NULL,
	[UpdatesUD] [bit] NULL,
	[PPW_PaperworkWentMissing] [bit] NULL,
	[TS_OCNotHappyWithTimescales] [bit] NULL,
	[ERR_ErrorsOnPaperwork] [bit] NULL,
	[CB_CallBacksnotReturned] [bit] NULL,
	[LOC_OCPreferredalocalservice] [bit] NULL,
	[WebsiteIssues] [bit] NULL,
	[INC_OCToldIncorrectThings] [bit] NULL,
	[PLS_HardToSpeakToPL] [bit] NULL,
	[EA_EASaidItWouldTakeLessTime] [bit] NULL,
	[COMP_ErrorAtCompletion] [bit] NULL,
	[POC_FewerPointsOfContact] [bit] NULL,
	[CW_LongCallWaits] [bit] NULL,
	[INF_OCAskedForInformedAlreadyProvided] [bit] NULL,
	[RUD_OCWasSpokenToRudely] [bit] NULL,
	[Sdlt] [bit] NULL,
	[RootCauseCommentary] [nvarchar](max) NULL,
	[ResponsiblePerson] [varchar](100) NOT NULL,
	[HoldingLetterIssuedDate] [datetime] NULL,
	[ComplaintResponseDueBy] [datetime] NULL,
	[ActualResponseIssued] [datetime] NULL,
	[ResponseReviewed] [varchar](100) NULL,
	[ComplaintUpheldorPartiallyUpheld] [bit] NOT NULL,
	[GestureOfGoodwillOffered] [bit] NULL,
	[AmountOfGoodwillOffered] [varchar](100) NULL,
	[PaymentsToThirdParties] [varchar](100) NULL,
	[L1DealtWithinSLA] [bit] NOT NULL,
	[ReasonForDelayInResponse] [bit] NULL,
	[ComplaintBeenEscalatedToStage2] [bit] NULL,
	[Level2ComplaintReceivedDate] [datetime] NULL,
	[Level2AcknowledgementIssueDate] [datetime] NULL,
	[Level2ComplaintResponseDueDate] [datetime] NULL,
	[Level2ComplaintResponseActualDate] [datetime] NULL,
	[L2DealtWithInSLA] [bit] NOT NULL,
	[SummaryFeedbackResolutions] [varchar](max) NULL,
	[SummaryFeedbackCompliance] [varchar](max) NULL,
	[CopySentToHOPL] [bit] NOT NULL,
	[ExecComplaint] [bit] NOT NULL,
	[SingleMultiId] [int] NOT NULL,
	[EnsureNoFeefoInviteSent] [bit] NOT NULL,
	[ComplaintClosed] [bit] NULL,
	[Attachments] [nvarchar](max) NULL,
	[Name] [varchar](100) NULL,
	[AttachFile] [nvarchar](max) NOT NULL,
	[ModifyingUser] [nvarchar](128) NULL,
	[Action] [varchar](1) NULL,
	[BeginDate] [datetime2](7) NOT NULL,
	[EndDate] [datetime2](7) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblComplaintsForm]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblComplaintsForm](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CenterID] [int] NOT NULL,
	[TeamID] [int] NOT NULL,
	[MatterNumberID] [varchar](100) NOT NULL,
	[ABSWith] [varchar](100) NULL,
	[FeeEarner] [varchar](100) NOT NULL,
	[ClientName] [varchar](100) NOT NULL,
	[TransactionTypeID] [int] NOT NULL,
	[SubsidaryBranch] [varchar](50) NULL,
	[RegionID] [int] NOT NULL,
	[InstructionDateReceived] [datetime] NULL,
	[FirstCallMadeOn] [datetime] NULL,
	[DateOfExchange] [datetime] NULL,
	[InstructionToExchange] [varchar](100) NOT NULL,
	[CompletionDate] [datetime] NOT NULL,
	[ComplaintReceivedDate] [datetime] NULL,
	[ComplaintReceivedID] [int] NULL,
	[ComplianceAwareness] [bit] NULL,
	[CriticalNote] [bit] NULL,
	[ResolutionTeamCopy] [bit] NULL,
	[AttributedTo] [varchar](100) NULL,
	[EnsurecopycomplaintsavedtofileandcataloguedinCustExperienceFolder] [bit] NULL,
	[SummaryIssues] [varchar](max) NOT NULL,
	[UpdatesUD] [bit] NULL,
	[PPW_PaperworkWentMissing] [bit] NULL,
	[TS_OCNotHappyWithTimescales] [bit] NULL,
	[ERR_ErrorsOnPaperwork] [bit] NULL,
	[CB_CallBacksnotReturned] [bit] NULL,
	[LOC_OCPreferredalocalservice] [bit] NULL,
	[WebsiteIssues] [bit] NULL,
	[INC_OCToldIncorrectThings] [bit] NULL,
	[PLS_HardToSpeakToPL] [bit] NULL,
	[EA_EASaidItWouldTakeLessTime] [bit] NULL,
	[COMP_ErrorAtCompletion] [bit] NULL,
	[POC_FewerPointsOfContact] [bit] NULL,
	[CW_LongCallWaits] [bit] NULL,
	[INF_OCAskedForInformedAlreadyProvided] [bit] NULL,
	[RUD_OCWasSpokenToRudely] [bit] NULL,
	[Sdlt] [bit] NULL,
	[RootCauseCommentary] [nvarchar](max) NULL,
	[ResponsiblePerson] [varchar](100) NOT NULL,
	[HoldingLetterIssuedDate] [datetime] NULL,
	[ComplaintResponseDueBy] [datetime] NULL,
	[ActualResponseIssued] [datetime] NULL,
	[ResponseReviewed] [varchar](100) NULL,
	[ComplaintUpheldorPartiallyUpheld] [bit] NOT NULL,
	[GestureOfGoodwillOffered] [bit] NULL,
	[AmountOfGoodwillOffered] [varchar](100) NULL,
	[PaymentsToThirdParties] [varchar](100) NULL,
	[L1DealtWithinSLA] [bit] NOT NULL,
	[ReasonForDelayInResponse] [bit] NULL,
	[ComplaintBeenEscalatedToStage2] [bit] NULL,
	[Level2ComplaintReceivedDate] [datetime] NULL,
	[Level2AcknowledgementIssueDate] [datetime] NULL,
	[Level2ComplaintResponseDueDate] [datetime] NULL,
	[Level2ComplaintResponseActualDate] [datetime] NULL,
	[L2DealtWithInSLA] [bit] NOT NULL,
	[SummaryFeedbackResolutions] [varchar](max) NULL,
	[SummaryFeedbackCompliance] [varchar](max) NULL,
	[CopySentToHOPL] [bit] NOT NULL,
	[ExecComplaint] [bit] NOT NULL,
	[SingleMultiId] [int] NOT NULL,
	[EnsureNoFeefoInviteSent] [bit] NOT NULL,
	[ComplaintClosed] [bit] NULL,
	[Attachments] [nvarchar](max) NULL,
	[Name] [varchar](100) NULL,
	[AttachFile] [nvarchar](max) NOT NULL,
	[ModifyingUser] [nvarchar](128) NULL,
	[Action] [varchar](1) NULL,
	[BeginDate] [datetime2](7) GENERATED ALWAYS AS ROW START NOT NULL,
	[EndDate] [datetime2](7) GENERATED ALWAYS AS ROW END NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
	PERIOD FOR SYSTEM_TIME ([BeginDate], [EndDate])
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
WITH
(
SYSTEM_VERSIONING = ON ( HISTORY_TABLE = [dbo].[tblComplaintsFormHistory] )
)
GO
/****** Object:  Table [dbo].[tblComplimentsFormHistory]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblComplimentsFormHistory](
	[ID] [int] NOT NULL,
	[ReceviedFrom] [varchar](50) NOT NULL,
	[DateReceived] [datetime] NOT NULL,
	[CustomerMatterNumber] [varchar](50) NOT NULL,
	[Center] [int] NOT NULL,
	[Team] [int] NOT NULL,
	[IndividualBeingComplimented] [varchar](100) NOT NULL,
	[RegionalSubsidary] [int] NOT NULL,
	[Branch] [int] NOT NULL,
	[Town] [varchar](50) NULL,
	[ComplimentTextFullInformation] [nvarchar](max) NOT NULL,
	[TATSpeedEfficiency] [bit] NULL,
	[ProActiveDrivingProgress] [bit] NULL,
	[SUPClientFeltSupportedappreciatedhelp] [bit] NULL,
	[COMGoodLevelOfCummunication] [bit] NULL,
	[PLEPleasentToSpeakTo] [bit] NULL,
	[CCUClearComprehensiveUpdates] [bit] NULL,
	[STRMadeItLessStressful] [bit] NULL,
	[RESQuickResponseToEmailsQuestions] [bit] NULL,
	[WhoSentToCET] [varchar](100) NULL,
	[NewsletterWorthy] [bit] NULL,
	[FileAttached] [varchar](max) NOT NULL,
	[ModifyingUser] [nvarchar](128) NULL,
	[Action] [varchar](1) NULL,
	[BeginDate] [datetime2](7) NOT NULL,
	[EndDate] [datetime2](7) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblComplimentsForm]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblComplimentsForm](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ReceviedFrom] [varchar](50) NOT NULL,
	[DateReceived] [datetime] NOT NULL,
	[CustomerMatterNumber] [varchar](50) NOT NULL,
	[Center] [int] NOT NULL,
	[Team] [int] NOT NULL,
	[IndividualBeingComplimented] [varchar](100) NOT NULL,
	[RegionalSubsidary] [int] NOT NULL,
	[Branch] [int] NOT NULL,
	[Town] [varchar](50) NULL,
	[ComplimentTextFullInformation] [nvarchar](max) NOT NULL,
	[TATSpeedEfficiency] [bit] NULL,
	[ProActiveDrivingProgress] [bit] NULL,
	[SUPClientFeltSupportedappreciatedhelp] [bit] NULL,
	[COMGoodLevelOfCummunication] [bit] NULL,
	[PLEPleasentToSpeakTo] [bit] NULL,
	[CCUClearComprehensiveUpdates] [bit] NULL,
	[STRMadeItLessStressful] [bit] NULL,
	[RESQuickResponseToEmailsQuestions] [bit] NULL,
	[WhoSentToCET] [varchar](100) NULL,
	[NewsletterWorthy] [bit] NULL,
	[FileAttached] [varchar](max) NOT NULL,
	[ModifyingUser] [nvarchar](128) NULL,
	[Action] [varchar](1) NULL,
	[BeginDate] [datetime2](7) GENERATED ALWAYS AS ROW START NOT NULL,
	[EndDate] [datetime2](7) GENERATED ALWAYS AS ROW END NOT NULL,
 CONSTRAINT [PK__tblCompl__3214EC276BFA67D8] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
	PERIOD FOR SYSTEM_TIME ([BeginDate], [EndDate])
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
WITH
(
SYSTEM_VERSIONING = ON ( HISTORY_TABLE = [dbo].[tblComplimentsFormHistory] )
)
GO
/****** Object:  Table [dbo].[tblContaminatedLandIndemnityFormHistory]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblContaminatedLandIndemnityFormHistory](
	[ID] [int] NOT NULL,
	[ClientName] [varchar](50) NOT NULL,
	[MatterNumber] [varchar](50) NOT NULL,
	[CenterID] [int] NOT NULL,
	[PropertyAddress] [varchar](max) NOT NULL,
	[ExchangeDate] [datetime] NULL,
	[CompletionDate] [datetime] NULL,
	[PropertyPrice] [money] NOT NULL,
	[PremiumIPAmount] [money] NOT NULL,
	[FeeEarner] [varchar](max) NOT NULL,
	[CAOnly] [bit] NULL,
	[AttachFile] [varchar](max) NOT NULL,
	[ModifyingUser] [nvarchar](128) NULL,
	[Action] [varchar](1) NULL,
	[BeginDate] [datetime2](7) NOT NULL,
	[EndDate] [datetime2](7) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblContaminatedLandIndemnityForm]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblContaminatedLandIndemnityForm](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ClientName] [varchar](50) NOT NULL,
	[MatterNumber] [varchar](50) NOT NULL,
	[CenterID] [int] NOT NULL,
	[PropertyAddress] [varchar](max) NOT NULL,
	[ExchangeDate] [datetime] NULL,
	[CompletionDate] [datetime] NULL,
	[PropertyPrice] [money] NOT NULL,
	[PremiumIPAmount] [money] NOT NULL,
	[FeeEarner] [varchar](max) NOT NULL,
	[CAOnly] [bit] NULL,
	[AttachFile] [varchar](max) NOT NULL,
	[ModifyingUser] [nvarchar](128) NULL,
	[Action] [varchar](1) NULL,
	[BeginDate] [datetime2](7) GENERATED ALWAYS AS ROW START NOT NULL,
	[EndDate] [datetime2](7) GENERATED ALWAYS AS ROW END NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
	PERIOD FOR SYSTEM_TIME ([BeginDate], [EndDate])
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
WITH
(
SYSTEM_VERSIONING = ON ( HISTORY_TABLE = [dbo].[tblContaminatedLandIndemnityFormHistory] )
)
GO
/****** Object:  Table [dbo].[CallCoachingLogHistory]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CallCoachingLogHistory](
	[ID] [int] NOT NULL,
	[FeedbackdeliveredbyCET] [bit] NULL,
	[Month] [varchar](50) NOT NULL,
	[PL_LAName] [varchar](50) NULL,
	[RoleID] [int] NULL,
	[CentreID] [int] NULL,
	[TeamID] [int] NULL,
	[TMID] [int] NULL,
	[Numberofcalls] [varchar](50) NULL,
	[Reasonforobservation] [int] NULL,
	[ReasonforobservationOwnValue] [varchar](50) NULL,
	[STOP_Jargon] [bit] NULL,
	[STOP_Beingdefensive_argumentative] [bit] NULL,
	[STOP_RefusingTransferredCalls] [bit] NULL,
	[STOP_UsingInformalLanguage] [bit] NULL,
	[STOP_CriticisingEA_FS_OL] [bit] NULL,
	[STOP_Freetext] [varchar](max) NULL,
	[START_DPAOnAllCalls] [bit] NULL,
	[START_CheckUunderstanding] [bit] NULL,
	[START_AskAnyFurtherQuestions] [bit] NULL,
	[START_ExplainNextSteps] [bit] NULL,
	[START_ExplainUnavoidableJargon] [bit] NULL,
	[START_GiveDDI] [bit] NULL,
	[START_SoundConfident] [bit] NULL,
	[START_AdjustDeliveryToSuitCaller] [bit] NULL,
	[START_Freetext] [varchar](max) NULL,
	[CONTINUE_DPAOnAllCalls] [bit] NULL,
	[CONTINUE_CheckUnderstanding] [bit] NULL,
	[CONTINUE_AskAnyFurtherQuestions] [bit] NULL,
	[CONTINUE_ExplainNextSteps] [bit] NULL,
	[CONTINUE_ExplainUnavoidableJargon] [bit] NULL,
	[CONTINUE_GiveDDI] [bit] NULL,
	[CONTINUE_SoundConfident] [bit] NULL,
	[CONTINUE_AdjustDeliveryToSuitCaller] [bit] NULL,
	[CONTINUE_Empathy] [bit] NULL,
	[CONTINUE_BuildRapport] [bit] NULL,
	[CONTINUE_UsePositiveLanguage] [bit] NULL,
	[CONTINUE_Freetext] [varchar](max) NULL,
	[HistoryNotes] [varchar](max) NULL,
	[GeneralObservations] [varchar](max) NULL,
	[FeedbackFromResTeam] [varchar](max) NULL,
	[AttachFile] [varchar](max) NOT NULL,
	[ModifyingUser] [nvarchar](128) NULL,
	[Action] [varchar](1) NULL,
	[BeginDate] [datetime2](7) NOT NULL,
	[EndDate] [datetime2](7) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ETLHistoryAuditLog]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ETLHistoryAuditLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DESCRIPTION] [varchar](50) NULL,
	[TOTALRECORD] [int] NULL,
	[CREATEDDATE] [datetime] NULL,
	[RECORDSINSERTED] [int] NULL,
	[RECORDSUPDATED] [int] NULL,
 CONSTRAINT [pk_CCS_Tracker_Test_AuditLog_ID] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ForfeitOfLeaseFormHistory]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ForfeitOfLeaseFormHistory](
	[ID] [int] NOT NULL,
	[ClientName] [varchar](50) NOT NULL,
	[MatterNumber] [varchar](50) NOT NULL,
	[CenterID] [int] NOT NULL,
	[PropertyAddress] [varchar](max) NOT NULL,
	[ExchangeDate] [datetime] NULL,
	[CompletionDate] [datetime] NULL,
	[PropertyPrice] [money] NOT NULL,
	[PremiumIPAmount] [money] NOT NULL,
	[FeeEarner] [varchar](max) NOT NULL,
	[CAOnly] [bit] NULL,
	[AttachFile] [varchar](max) NOT NULL,
	[ModifyingUser] [nvarchar](128) NULL,
	[Action] [varchar](1) NULL,
	[BeginDate] [datetime2](7) NOT NULL,
	[EndDate] [datetime2](7) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MSSQL_TemporalHistoryFor_1739153241]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MSSQL_TemporalHistoryFor_1739153241](
	[ID] [int] NOT NULL,
	[TypeOfPanelID] [int] NOT NULL,
	[ReasonForPanelID] [int] NULL,
	[ClientName] [varchar](50) NOT NULL,
	[Address] [varchar](100) NOT NULL,
	[TMID] [varchar](50) NOT NULL,
	[Details] [varchar](max) NOT NULL,
	[PanelFrim_FundsGoing] [varchar](50) NOT NULL,
	[PanelFrim_FundsComing] [varchar](50) NULL,
	[AwaitingFundsId] [int] NOT NULL,
	[Amount] [bigint] NOT NULL,
	[ClientAccounts_complete] [bit] NULL,
	[ActionrequiredByPanel] [bit] NULL,
	[PanelTeamComplete] [bit] NULL,
	[PanelFirmAllocated_Appointed] [varchar](100) NULL,
	[AttachFile] [varchar](max) NULL,
	[ModifyingUser] [nvarchar](128) NULL,
	[Action] [varchar](1) NULL,
	[BeginDate] [datetime2](7) NOT NULL,
	[EndDate] [datetime2](7) NOT NULL,
	[BeginDateLocalTime] [datetime2](7) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Index [ix_MSSQL_TemporalHistoryFor_1739153241]    Script Date: 07/10/2021 13:24:19 ******/
CREATE CLUSTERED INDEX [ix_MSSQL_TemporalHistoryFor_1739153241] ON [dbo].[MSSQL_TemporalHistoryFor_1739153241]
(
	[EndDate] ASC,
	[BeginDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl3rdPartySource]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl3rdPartySource](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Source] [varchar](100) NOT NULL,
 CONSTRAINT [PK_tbl3rdPartySource] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblArea]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblArea](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Area] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblAuditOrSignOff]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblAuditOrSignOff](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AuditOrSignOff] [varchar](50) NOT NULL,
 CONSTRAINT [PK_tblAuditOrSignOff] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblAwaitingFunds]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblAwaitingFunds](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AwaitingFunds] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblBranch]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblBranch](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Branch] [varchar](150) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblBrand]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblBrand](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Brand] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblBridgeControlCategory]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblBridgeControlCategory](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Category] [varchar](50) NULL,
 CONSTRAINT [PK_tblBridgeControlCategory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblBridgeControlCenter]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblBridgeControlCenter](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CenterName] [varchar](50) NOT NULL,
 CONSTRAINT [PK_tblBridgeControlCenter] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblBridgeControlRisk]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblBridgeControlRisk](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Risk] [varchar](50) NOT NULL,
 CONSTRAINT [PK_tblBridgeControlRisk] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblCallCoachingRole]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblCallCoachingRole](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[RoleName] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblCategoryOfTitle]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblCategoryOfTitle](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CategoryOfTitle] [varchar](50) NOT NULL,
 CONSTRAINT [PK_tblCategoryOfTitle] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblCenterName]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblCenterName](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CenterName] [varchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblCentreWriteOffAgainst]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblCentreWriteOffAgainst](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CentreWriteOffAgainst] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblCETCheck]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblCETCheck](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CETCheck] [varchar](50) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblClientDetails]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblClientDetails](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ClientName] [varchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblCompDiaryImport]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblCompDiaryImport](
	[Fee Earner Team] [varchar](100) NULL,
	[Matter Number] [varchar](20) NULL,
	[Client Surname] [varchar](20) NULL,
	[Matter Type] [varchar](50) NULL,
	[Expected Completion Date] [varchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblComplaintMode]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblComplaintMode](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[ComplaintMode] [varchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblCompletionDairyDateIntervals]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblCompletionDairyDateIntervals](
	[Id] [int] NOT NULL,
	[DateIntervalType] [varchar](50) NULL,
 CONSTRAINT [PK_tblCompletionDairyDateIntervals] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblCompletionDiaryImport]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblCompletionDiaryImport](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FeeEarner_Team] [varchar](100) NULL,
	[FeeEarnerInitials] [varchar](100) NULL,
	[MatterNumber] [varchar](100) NULL,
	[ClientSurname] [varchar](100) NULL,
	[MatterType] [varchar](100) NULL,
	[MatterStatus] [varchar](100) NULL,
	[ExpectedCompletionDate] [varchar](100) NULL,
	[IsMatterNumDups] [bit] NULL,
	[IsMandatoryColMissing] [bit] NULL,
	[IsExpCompDateFormatIssue] [bit] NULL,
	[LoadedOn] [datetime] NULL,
	[IsLoaded] [bit] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblCompletionDiaryImportErrorLog]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblCompletionDiaryImportErrorLog](
	[ID] [int] NOT NULL,
	[ErrorMessage] [varchar](max) NOT NULL,
	[ErrorSeverity] [int] NOT NULL,
	[ErrorState] [varchar](max) NOT NULL,
	[Source] [varchar](100) NOT NULL,
	[ErrorOccuredTimeStamp] [datetime] NOT NULL,
	[CUSTOMMESSAGE] [varchar](max) NOT NULL,
 CONSTRAINT [PK_tblCompletionDiaryImportErrorLog] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblCompliantsSource]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblCompliantsSource](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SourceName] [varchar](150) NOT NULL,
 CONSTRAINT [PK_tblCompliantsSource] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblConsultant]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblConsultant](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Consultant] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblCustomerMatterNumber]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblCustomerMatterNumber](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerMatterNumber] [varchar](50) NOT NULL,
	[BranchID] [int] NULL,
	[BridgeControlCenterID] [int] NULL,
	[CenterID] [int] NULL,
	[ClientID] [int] NULL,
	[FeeEarnerID] [int] NULL,
	[RegionID] [int] NULL,
	[TeamID] [int] NULL,
	[ReceivedFromID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblDeclineReason]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblDeclineReason](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DeclineReason] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblDelayResponse]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblDelayResponse](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DelayResponse1] [bit] NULL,
	[DelayResponse2] [bit] NULL,
	[DelayResponse3] [bit] NULL,
	[DelayResponse4] [bit] NULL,
	[DelayResponse5] [bit] NULL,
	[DelayResponse6] [bit] NULL,
	[DelayResponse7] [bit] NULL,
	[DelayResponse8] [bit] NULL,
	[CompliantsFormID] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblDynamicViewConfiguration]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblDynamicViewConfiguration](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ColumnName] [varchar](max) NOT NULL,
	[ColumnAliasName] [varchar](max) NOT NULL,
	[IsSelected] [bit] NOT NULL,
	[TrackerRefId] [int] NOT NULL,
	[ViewName] [varchar](50) NOT NULL,
	[isOrdered] [int] NULL,
	[IsMandatory] [bit] NULL,
	[dataType] [varchar](100) NULL,
	[spResultModel] [varchar](100) NULL,
 CONSTRAINT [PK_tblDynamicViewConfiguration] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblEnter_FEInitials]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblEnter_FEInitials](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Enter_FEInitials] [varchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblErrorThemes]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblErrorThemes](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ErrorThemes1] [bit] NULL,
	[ErrorThemes2] [bit] NULL,
	[ErrorThemes3] [bit] NULL,
	[ErrorThemes4] [bit] NULL,
	[ErrorThemes5] [bit] NULL,
	[ErrorThemes6] [bit] NULL,
	[ErrorThemes7] [bit] NULL,
	[TitleAllocationRefId] [int] NOT NULL,
 CONSTRAINT [PK_tblErrorThemes] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblExeccomplaint]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblExeccomplaint](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ExecComplaint] [varchar](50) NOT NULL,
 CONSTRAINT [PK_tblExeccomplaint] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblFatalityOfError]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblFatalityOfError](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FatalityOfError] [varchar](50) NOT NULL,
 CONSTRAINT [PK_tblFatalityOfError] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblFeeEarner]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblFeeEarner](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FeeEarner] [varchar](50) NOT NULL,
 CONSTRAINT [PK_tblFeeEarner] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblForfeitOfLeaseCenter]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblForfeitOfLeaseCenter](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Center] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblGenericViewDetails]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblGenericViewDetails](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TrackerId] [int] NOT NULL,
	[HeaderName] [varchar](100) NOT NULL,
	[ControllerName] [varchar](100) NOT NULL,
	[EditActionName] [varchar](100) NOT NULL,
	[DeleteActionName] [varchar](100) NOT NULL,
	[DownloadActionName] [varchar](100) NOT NULL,
	[ReviewActionName] [varchar](100) NOT NULL,
 CONSTRAINT [PK_tblGenericViewDetails] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblHighValueIndemnityCenter]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblHighValueIndemnityCenter](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Center] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblIndemnityPoliciesCenter]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblIndemnityPoliciesCenter](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Center] [varchar](50) NULL,
	[MatterRefId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblIndemnityPoliciesStatus]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblIndemnityPoliciesStatus](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Status] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblInstructingBranch]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblInstructingBranch](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[InstructingBranch] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblKTL_Mentor]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblKTL_Mentor](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[KTL_Mentor] [varchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblLawyerGrade]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblLawyerGrade](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[LawyerGrade] [varchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblLeadStatus]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblLeadStatus](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[LeadStatus] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblMatterType]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblMatterType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[MatterType] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblMentorSupport]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblMentorSupport](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[MentorSupport] [varchar](100) NOT NULL,
 CONSTRAINT [PK_tblMentorSupport] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblMonth]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblMonth](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MonthName] [varchar](50) NOT NULL,
 CONSTRAINT [PK_tblMonth] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblMultiViewConfiguration]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblMultiViewConfiguration](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ViewName] [varchar](100) NOT NULL,
	[TrackerId] [int] NOT NULL,
	[IsSelected] [bit] NOT NULL,
	[IsOrdered] [int] NOT NULL,
	[ColumnRefId] [int] NOT NULL,
	[IsActive] [bit] NULL,
	[CreateBy] [varchar](100) NULL,
	[CreateDate] [datetime] NULL,
	[UpdatedBy] [varchar](100) NULL,
	[UpdatedDate] [datetime] NULL,
	[TransactionStatus] [varchar](10) NULL,
 CONSTRAINT [PK_tblMultiViewConfiguration] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblMultiViewQueryBulderDetails]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblMultiViewQueryBulderDetails](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TrackerRefID] [int] NOT NULL,
	[ViewName] [varchar](100) NOT NULL,
	[ColumnRefID] [int] NOT NULL,
	[OperatorRefID] [int] NOT NULL,
	[Condition] [varchar](10) NULL,
	[SearchName] [varchar](100) NOT NULL,
	[IsOrdered] [int] NULL,
 CONSTRAINT [PK_tblMultiViewQueryBulderDetails] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblNotValidReason]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblNotValidReason](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[NotValidReason] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblOOHConsultant]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblOOHConsultant](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Consultant] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblOOHEmailConfig]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblOOHEmailConfig](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[MailSubject] [varchar](100) NOT NULL,
	[MailBody] [varchar](max) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblPendingReason]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblPendingReason](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PendingReason] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblProcess]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblProcess](
	[ProcessId] [int] NOT NULL,
	[ProcessName] [nvarchar](50) NOT NULL,
	[ProcessCreationDate] [smalldatetime] NOT NULL,
	[ProcessDescription] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_tblProcess] PRIMARY KEY CLUSTERED 
(
	[ProcessId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblQueryBuilderLog]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblQueryBuilderLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TrakcerRefId] [int] NOT NULL,
	[ViewName] [varchar](100) NOT NULL,
	[Querystring] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_tblQueryBuilderLog] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TblQueryBuilderOperatorDetails]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TblQueryBuilderOperatorDetails](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SqlOperator] [varchar](50) NOT NULL,
	[OperatorName] [varchar](50) NOT NULL,
	[Category] [varchar](10) NOT NULL,
 CONSTRAINT [PK_TblQueryBuilderOperatorDetails] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblReasonforobservation]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblReasonforobservation](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[observation] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblReasonForPanel]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblReasonForPanel](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ReasonForPanel] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblReasonForRefund]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblReasonForRefund](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ReasonForRefund] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblReasonforWriteOff]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblReasonforWriteOff](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ReasonforWriteOff] [varchar](250) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblReceivedFrom]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblReceivedFrom](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ReceivedFrom] [varchar](50) NOT NULL,
 CONSTRAINT [PK_ReceivedFrom] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblReferrer]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblReferrer](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Referrer] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblRegion]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblRegion](
	[Id] [int] NOT NULL,
	[Region] [varchar](50) NOT NULL,
 CONSTRAINT [PK_tbl_Region] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblRole]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblRole](
	[RoleId] [int] NOT NULL,
	[RoleName] [nvarchar](50) NOT NULL,
	[RoleCreationDate] [smalldatetime] NOT NULL,
	[RoleDescription] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_tblRole] PRIMARY KEY CLUSTERED 
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblRouting]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblRouting](
	[RoutingId] [int] NOT NULL,
	[RoutingCreationDate] [smalldatetime] NOT NULL,
	[RoutingDescription] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_tblRouting] PRIMARY KEY CLUSTERED 
(
	[RoutingId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblRoutingDetails]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblRoutingDetails](
	[TrackerId] [int] NOT NULL,
	[SrcProcessId] [int] NOT NULL,
	[RoutingId] [int] NOT NULL,
	[trgProcessId] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblSalesLeadBranch]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblSalesLeadBranch](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Branch] [varchar](150) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblSingleMulti]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblSingleMulti](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SingleMulti] [varchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblSource]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblSource](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SourceName] [varchar](50) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblStatus]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblStatus](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Status] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblStopChequeCenter]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblStopChequeCenter](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Center] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblStopChequeStatus]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblStopChequeStatus](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[STATUS] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblTask]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblTask](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Task] [varchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblTaskLawyerTeam]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblTaskLawyerTeam](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TeamName] [varchar](50) NOT NULL,
 CONSTRAINT [PK_tblTaskLawyerTeam] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblTeamCompletionDiary]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblTeamCompletionDiary](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TeamCompletionDiary] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblTeamName]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblTeamName](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TeamName] [varchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblTeamWriteOffAgainst]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblTeamWriteOffAgainst](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TeamWriteOffAgainst] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblTimeSlotDetails]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblTimeSlotDetails](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TimeSlot] [varchar](10) NOT NULL,
 CONSTRAINT [PK_tblTimeSlot] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblTitleAllocationCentre]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblTitleAllocationCentre](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CenterName] [varchar](50) NOT NULL,
 CONSTRAINT [PK_tblTitleAllocationCentre] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblTitleAllocationMentor]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblTitleAllocationMentor](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[MentorName] [varchar](100) NOT NULL,
 CONSTRAINT [PK_tblTitleAllocationMentor] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblTitleAllocationStatus]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblTitleAllocationStatus](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Status] [varchar](100) NOT NULL,
 CONSTRAINT [PK_tblTitleAllocationStatus] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblTitleChecker]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblTitleChecker](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TitleCheckerName] [varchar](100) NOT NULL,
 CONSTRAINT [PK_tblTitleChecker] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblTM]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblTM](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TM] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblTracker]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblTracker](
	[TrackerId] [int] IDENTITY(1,1) NOT NULL,
	[TrackerName] [nvarchar](50) NOT NULL,
	[TrackerCreationDate] [smalldatetime] NOT NULL,
	[TrackerDescription] [nvarchar](max) NOT NULL,
	[TrackerOwner] [nvarchar](50) NOT NULL,
	[TrackerTeam] [varchar](50) NULL,
	[IsActive] [bit] NULL,
	[TableName] [varchar](100) NULL,
 CONSTRAINT [PK_tblTracker] PRIMARY KEY CLUSTERED 
(
	[TrackerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblTrackerForms]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblTrackerForms](
	[FormId] [int] IDENTITY(1,1) NOT NULL,
	[TrackerId] [int] NOT NULL,
	[ProcessId] [int] NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[FormDataTable] [nvarchar](50) NOT NULL,
	[FormDataId] [int] NOT NULL,
 CONSTRAINT [PK_tblTrackerForms] PRIMARY KEY CLUSTERED 
(
	[FormId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblTrackerProcessRole]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblTrackerProcessRole](
	[TPRId] [int] IDENTITY(1,1) NOT NULL,
	[TrackerId] [int] NOT NULL,
	[ProcessId] [int] NOT NULL,
	[RoleId] [int] NOT NULL,
	[ActionName] [varchar](50) NULL,
	[ControllerName] [varchar](50) NULL,
 CONSTRAINT [PK_tbl_TrackerProcessRole] PRIMARY KEY CLUSTERED 
(
	[TPRId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblTransactionStatus]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblTransactionStatus](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TransactionStatus] [varchar](50) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblTransactionType]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblTransactionType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TransactionType] [varchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblTypeOfPanel]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblTypeOfPanel](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TypeOfPanel] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblUnallocatedFundsPaymentMethod]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblUnallocatedFundsPaymentMethod](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PaymentMethod] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblUnallocatedFundsStatus]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblUnallocatedFundsStatus](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Status] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblUnallocatedFundsUnallocatedNo]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblUnallocatedFundsUnallocatedNo](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UnallocatedNo] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblUser]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblUser](
	[DomainId] [varchar](20) NOT NULL,
	[RoleId] [int] NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblUsersList]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblUsersList](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](50) NOT NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[EmailId] [nvarchar](50) NULL,
 CONSTRAINT [PK_tblUsersList] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblWriteOffsStatus]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblWriteOffsStatus](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Status] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [PUB].[FEETR]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [PUB].[FEETR](
	[ID] [int] NOT NULL,
	[FEE-EARNER] [varchar](254) NOT NULL,
	[NAME] [varchar](254) NULL,
	[EXTENSION] [varchar](254) NULL,
	[FESTATUS] [int] NULL,
	[WEIGHTING] [decimal](18, 0) NULL,
	[AVAILABILITY] [decimal](18, 0) NULL,
	[COST-RATE] [decimal](18, 0) NULL,
	[CHARGE-RATE] [decimal](18, 0) NULL,
	[EXPENSE-RATE] [decimal](18, 0) NULL,
	[STD-DAY-START] [int] NULL,
	[STD-DAY-HRS] [decimal](18, 0) NULL,
	[SALARY-ACT] [int] NULL,
	[SALARY-NOT] [int] NULL,
	[UPLIFT] [decimal](18, 0) NULL,
	[SALARY-REL] [int] NULL,
	[EXP-PC] [int] NULL,
	[EXP-PS] [int] NULL,
	[EXP-TOT] [int] NULL,
	[HOURS] [int] NULL,
	[TARGET] [decimal](18, 0) NULL,
	[CURR] [bit] NULL,
	[DEPARTMENT] [varchar](254) NULL,
	[DAY-TARGET] [varchar](254) NULL,
	[EQUITY-PR] [varchar](254) NULL,
	[ROLLNO] [varchar](254) NULL,
	[CURRENCY-CODE] [varchar](254) NULL,
	[EMAIL] [varchar](254) NULL,
	[MOD-DATE] [date] NULL,
	[MOD-TIME] [int] NULL,
	[MOD-USER] [varchar](254) NULL,
	[USER-ID] [varchar](254) NULL,
	[NOT-FOR-PROFIT-COSTS] [bit] NULL,
	[ANALY-STATUS] [varchar](254) NULL,
	[FEE-CREDIT-FE] [bit] NULL,
	[LastUpdated] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[FEE-EARNER] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [PUB].[FEETR_History]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [PUB].[FEETR_History](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FEE-EARNER] [varchar](254) NOT NULL,
	[NAME] [varchar](254) NULL,
	[EXTENSION] [varchar](254) NULL,
	[FESTATUS] [int] NULL,
	[WEIGHTING] [decimal](18, 0) NULL,
	[AVAILABILITY] [decimal](18, 0) NULL,
	[COST-RATE] [decimal](18, 0) NULL,
	[CHARGE-RATE] [decimal](18, 0) NULL,
	[EXPENSE-RATE] [decimal](18, 0) NULL,
	[STD-DAY-START] [int] NULL,
	[STD-DAY-HRS] [decimal](18, 0) NULL,
	[SALARY-ACT] [int] NULL,
	[SALARY-NOT] [int] NULL,
	[UPLIFT] [decimal](18, 0) NULL,
	[SALARY-REL] [int] NULL,
	[EXP-PC] [int] NULL,
	[EXP-PS] [int] NULL,
	[EXP-TOT] [int] NULL,
	[HOURS] [int] NULL,
	[TARGET] [decimal](18, 0) NULL,
	[CURR] [bit] NULL,
	[DEPARTMENT] [varchar](254) NULL,
	[DAY-TARGET] [varchar](254) NULL,
	[EQUITY-PR] [varchar](254) NULL,
	[ROLLNO] [varchar](254) NULL,
	[CURRENCY-CODE] [varchar](254) NULL,
	[EMAIL] [varchar](254) NULL,
	[MOD-DATE] [date] NULL,
	[MOD-TIME] [int] NULL,
	[MOD-USER] [varchar](254) NULL,
	[USER-ID] [varchar](254) NULL,
	[NOT-FOR-PROFIT-COSTS] [bit] NULL,
	[ANALY-STATUS] [varchar](254) NULL,
	[FEE-CREDIT-FE] [bit] NULL,
	[EffectiveDate] [date] NOT NULL,
	[EndDate] [date] NOT NULL,
	[CurrentRecord] [varchar](1) NOT NULL,
	[LastUpdated] [datetime] NOT NULL,
	[LoadedDate] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [PUB].[MATAC]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [PUB].[MATAC](
	[MT-CODE] [varchar](254) NOT NULL,
	[BANK-OFFICE] [varchar](254) NULL,
	[BANK-CLIENT] [varchar](254) NULL,
	[BANK-DEPOSIT] [varchar](254) NULL,
	[BAL-UC] [decimal](18, 0) NULL,
	[BAL-UPB] [decimal](18, 0) NULL,
	[BAL-UBD] [decimal](18, 0) NULL,
	[BAL-CLI] [decimal](18, 0) NULL,
	[BAL-DEP] [decimal](18, 0) NULL,
	[BAL-UPC] [decimal](18, 0) NULL,
	[D-LIMIT] [int] NULL,
	[DATE-LAST-POST] [date] NULL,
	[CL-CODE] [varchar](254) NULL,
	[TOT-DISBS] [decimal](18, 0) NULL,
	[MOD-DATE] [date] NULL,
	[MOD-TIME] [int] NULL,
	[MOD-USER] [varchar](254) NULL,
	[LastUpdated] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[MT-CODE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [PUB].[MATAC_History]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [PUB].[MATAC_History](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[MT-CODE] [varchar](254) NOT NULL,
	[BANK-OFFICE] [varchar](254) NULL,
	[BANK-CLIENT] [varchar](254) NULL,
	[BANK-DEPOSIT] [varchar](254) NULL,
	[BAL-UC] [decimal](18, 0) NULL,
	[BAL-UPB] [decimal](18, 0) NULL,
	[BAL-UBD] [decimal](18, 0) NULL,
	[BAL-CLI] [decimal](18, 0) NULL,
	[BAL-DEP] [decimal](18, 0) NULL,
	[BAL-UPC] [decimal](18, 0) NULL,
	[D-LIMIT] [int] NULL,
	[DATE-LAST-POST] [date] NULL,
	[CL-CODE] [varchar](254) NULL,
	[TOT-DISBS] [decimal](18, 0) NULL,
	[MOD-DATE] [date] NULL,
	[MOD-TIME] [int] NULL,
	[MOD-USER] [varchar](254) NULL,
	[EffectiveDate] [date] NOT NULL,
	[EndDate] [date] NOT NULL,
	[CurrentRecord] [varchar](1) NOT NULL,
	[LastUpdated] [datetime] NOT NULL,
	[LoadedDate] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [PUB].[MATDB]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [PUB].[MATDB](
	[BRANCH] [varchar](254) NULL,
	[MT-CODE] [varchar](254) NOT NULL,
	[CL-CODE] [varchar](254) NULL,
	[MT-TYPE] [varchar](254) NULL,
	[DESCRIPTION] [varchar](254) NULL,
	[FEE-EARNER] [varchar](254) NULL,
	[DATE-OPENED] [date] NULL,
	[ST-CODE] [varchar](254) NULL,
	[SUPERVISOR] [varchar](254) NULL,
	[ESTIMATE] [int] NULL,
	[DATE-ESTIMATE] [date] NULL,
	[DATE-REVIEW1] [date] NULL,
	[DATE-REVIEW2] [date] NULL,
	[REFERENCE] [varchar](254) NULL,
	[NOTES] [varchar](max) NULL,
	[REMINDERS] [bit] NULL,
	[DATE-CLOSED] [date] NULL,
	[RETENTION] [int] NULL,
	[HOLD-FLAG] [bit] NULL,
	[NO-BILLS] [int] NULL,
	[BILLD-ANTI-FLAG] [varchar](254) NULL,
	[REGISTER] [varchar](254) NULL,
	[ARCHIVED-FLAG] [bit] NULL,
	[INT-SCAN] [bit] NULL,
	[POST_LOCK] [varchar](254) NULL,
	[OTHER-PARTY] [varchar](254) NULL,
	[LOCATION] [varchar](254) NULL,
	[MOD-DATE] [date] NULL,
	[YRPR-OPENED] [varchar](254) NULL,
	[MOD-TIME] [int] NULL,
	[RISK-LEVEL] [varchar](254) NULL,
	[MOD-USER] [varchar](254) NULL,
	[INTRO-CODE] [varchar](254) NULL,
	[MAT-GRP] [varchar](254) NULL,
	[ARCHIVE-ID] [int] NULL,
	[INTEREST-GROUP] [varchar](254) NULL,
	[DESTRUCTION-DATE] [varchar](254) NULL,
	[PERMITTED-USERS] [varchar](254) NULL,
	[CSB-SCHEME] [varchar](254) NULL,
	[MKT-ACTIVITY] [varchar](254) NULL,
	[ONVERTED-ID] [varchar](254) NULL,
	[ANALY-STATUS] [varchar](254) NULL,
	[DATE-REDACTED] [date] NULL,
	[REDACTED-BY] [char](16) NULL,
	[NEVER-REDACT] [bit] NULL,
	[ACCESS-GROUP-OVERRIDE] [bit] NULL,
	[LastUpdated] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[MT-CODE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [PUB].[MATDB_History]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [PUB].[MATDB_History](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[BRANCH] [varchar](254) NULL,
	[MT-CODE] [varchar](254) NOT NULL,
	[CL-CODE] [varchar](254) NULL,
	[MT-TYPE] [varchar](254) NULL,
	[DESCRIPTION] [varchar](254) NULL,
	[FEE-EARNER] [varchar](254) NULL,
	[DATE-OPENED] [date] NULL,
	[ST-CODE] [varchar](254) NULL,
	[SUPERVISOR] [varchar](254) NULL,
	[ESTIMATE] [int] NULL,
	[DATE-ESTIMATE] [date] NULL,
	[DATE-REVIEW1] [date] NULL,
	[DATE-REVIEW2] [date] NULL,
	[REFERENCE] [varchar](254) NULL,
	[NOTES] [varchar](max) NULL,
	[REMINDERS] [bit] NULL,
	[DATE-CLOSED] [date] NULL,
	[RETENTION] [int] NULL,
	[HOLD-FLAG] [bit] NULL,
	[NO-BILLS] [int] NULL,
	[BILLD-ANTI-FLAG] [varchar](254) NULL,
	[REGISTER] [varchar](254) NULL,
	[ARCHIVED-FLAG] [bit] NULL,
	[INT-SCAN] [bit] NULL,
	[POST_LOCK] [varchar](254) NULL,
	[OTHER-PARTY] [varchar](254) NULL,
	[LOCATION] [varchar](254) NULL,
	[MOD-DATE] [date] NULL,
	[YRPR-OPENED] [varchar](254) NULL,
	[MOD-TIME] [int] NULL,
	[RISK-LEVEL] [varchar](254) NULL,
	[MOD-USER] [varchar](254) NULL,
	[INTRO-CODE] [varchar](254) NULL,
	[MAT-GRP] [varchar](254) NULL,
	[ARCHIVE-ID] [int] NULL,
	[INTEREST-GROUP] [varchar](254) NULL,
	[DESTRUCTION-DATE] [varchar](254) NULL,
	[PERMITTED-USERS] [varchar](254) NULL,
	[CSB-SCHEME] [varchar](254) NULL,
	[MKT-ACTIVITY] [varchar](254) NULL,
	[ONVERTED-ID] [varchar](254) NULL,
	[ANALY-STATUS] [varchar](254) NULL,
	[DATE-REDACTED] [date] NULL,
	[REDACTED-BY] [char](16) NULL,
	[NEVER-REDACT] [bit] NULL,
	[ACCESS-GROUP-OVERRIDE] [bit] NULL,
	[EffectiveDate] [date] NOT NULL,
	[EndDate] [date] NOT NULL,
	[CurrentRecord] [varchar](1) NOT NULL,
	[LastUpdated] [datetime] NOT NULL,
	[LoadedDate] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[ETLHistoryAuditLog] ADD  DEFAULT (getdate()) FOR [CREATEDDATE]
GO
ALTER TABLE [dbo].[tblBridgeControlForm] ADD  CONSTRAINT [df_BridgeControlForm_ModifyingUser]  DEFAULT (coalesce(suser_name(suser_id()),user_name())) FOR [ModifyingUser]
GO
ALTER TABLE [dbo].[tblBridgeControlForm] ADD  DEFAULT (sysutcdatetime()) FOR [BeginDate]
GO
ALTER TABLE [dbo].[tblBridgeControlForm] ADD  DEFAULT (CONVERT([datetime2],'9999-12-31 23:59:59')) FOR [EndDate]
GO
ALTER TABLE [dbo].[tblCallCoachingLog] ADD  CONSTRAINT [df_CallCoachingLog]  DEFAULT (coalesce(suser_name(suser_id()),user_name())) FOR [ModifyingUser]
GO
ALTER TABLE [dbo].[tblCallCoachingLog] ADD  DEFAULT (sysutcdatetime()) FOR [BeginDate]
GO
ALTER TABLE [dbo].[tblCallCoachingLog] ADD  DEFAULT (CONVERT([datetime2],'9999-12-31 23:59:59')) FOR [EndDate]
GO
ALTER TABLE [dbo].[tblCardPaymentRequestForm] ADD  CONSTRAINT [df_CardPaymentRequestForm]  DEFAULT (coalesce(suser_name(suser_id()),user_name())) FOR [ModifyingUser]
GO
ALTER TABLE [dbo].[tblCardPaymentRequestForm] ADD  DEFAULT (sysutcdatetime()) FOR [BeginDate]
GO
ALTER TABLE [dbo].[tblCardPaymentRequestForm] ADD  DEFAULT (CONVERT([datetime2],'9999-12-31 23:59:59')) FOR [EndDate]
GO
ALTER TABLE [dbo].[tblClientToClientTransfersForm] ADD  CONSTRAINT [df_ClientToClientTransfer_ModifyingUser]  DEFAULT (coalesce(suser_name(suser_id()),user_name())) FOR [ModifyingUser]
GO
ALTER TABLE [dbo].[tblClientToClientTransfersForm] ADD  DEFAULT (sysutcdatetime()) FOR [BeginDate]
GO
ALTER TABLE [dbo].[tblClientToClientTransfersForm] ADD  DEFAULT (CONVERT([datetime2],'9999-12-31 23:59:59')) FOR [EndDate]
GO
ALTER TABLE [dbo].[tblComplaintsForm] ADD  CONSTRAINT [df_ComplaintsForm]  DEFAULT (coalesce(suser_name(suser_id()),user_name())) FOR [ModifyingUser]
GO
ALTER TABLE [dbo].[tblComplaintsForm] ADD  DEFAULT (sysutcdatetime()) FOR [BeginDate]
GO
ALTER TABLE [dbo].[tblComplaintsForm] ADD  DEFAULT (CONVERT([datetime2],'9999-12-31 23:59:59')) FOR [EndDate]
GO
ALTER TABLE [dbo].[tblCompletionDiaryImport] ADD  DEFAULT ((0)) FOR [IsMatterNumDups]
GO
ALTER TABLE [dbo].[tblCompletionDiaryImport] ADD  DEFAULT ((0)) FOR [IsMandatoryColMissing]
GO
ALTER TABLE [dbo].[tblCompletionDiaryImport] ADD  DEFAULT ((0)) FOR [IsExpCompDateFormatIssue]
GO
ALTER TABLE [dbo].[tblCompletionDiaryImport] ADD  DEFAULT (getdate()) FOR [LoadedOn]
GO
ALTER TABLE [dbo].[tblCompletionDiaryImport] ADD  DEFAULT ((0)) FOR [IsLoaded]
GO
ALTER TABLE [dbo].[tblCompletionDiaryImportErrorLog] ADD  CONSTRAINT [DF_tblCompletionDiaryImportErrorLog]  DEFAULT (getdate()) FOR [ErrorOccuredTimeStamp]
GO
ALTER TABLE [dbo].[tblComplimentsForm] ADD  CONSTRAINT [df_ComplimentsForm]  DEFAULT (coalesce(suser_name(suser_id()),user_name())) FOR [ModifyingUser]
GO
ALTER TABLE [dbo].[tblComplimentsForm] ADD  DEFAULT (sysutcdatetime()) FOR [BeginDate]
GO
ALTER TABLE [dbo].[tblComplimentsForm] ADD  DEFAULT (CONVERT([datetime2],'9999-12-31 23:59:59')) FOR [EndDate]
GO
ALTER TABLE [dbo].[tblContaminatedLandIndemnityForm] ADD  CONSTRAINT [df_ContaminatedLandIndemnityForm]  DEFAULT (coalesce(suser_name(suser_id()),user_name())) FOR [ModifyingUser]
GO
ALTER TABLE [dbo].[tblContaminatedLandIndemnityForm] ADD  DEFAULT (sysutcdatetime()) FOR [BeginDate]
GO
ALTER TABLE [dbo].[tblContaminatedLandIndemnityForm] ADD  DEFAULT (CONVERT([datetime2],'9999-12-31 23:59:59')) FOR [EndDate]
GO
ALTER TABLE [dbo].[tblCreditCardRefundForm] ADD  CONSTRAINT [df_CreditCardRefundForm_ModifyingUser]  DEFAULT (coalesce(suser_name(suser_id()),user_name())) FOR [ModifyingUser]
GO
ALTER TABLE [dbo].[tblCreditCardRefundForm] ADD  DEFAULT (sysutcdatetime()) FOR [BeginDate]
GO
ALTER TABLE [dbo].[tblCreditCardRefundForm] ADD  DEFAULT (CONVERT([datetime2],'9999-12-31 23:59:59')) FOR [EndDate]
GO
ALTER TABLE [dbo].[tblDissatisfactionForm] ADD  CONSTRAINT [df_DissatisfactionForm]  DEFAULT (coalesce(suser_name(suser_id()),user_name())) FOR [ModifyingUser]
GO
ALTER TABLE [dbo].[tblDissatisfactionForm] ADD  DEFAULT (sysutcdatetime()) FOR [BeginDate]
GO
ALTER TABLE [dbo].[tblDissatisfactionForm] ADD  DEFAULT (CONVERT([datetime2],'9999-12-31 23:59:59')) FOR [EndDate]
GO
ALTER TABLE [dbo].[tblForfeitOfLeaseForm] ADD  CONSTRAINT [df_ForfeitOfLeaseForm]  DEFAULT (coalesce(suser_name(suser_id()),user_name())) FOR [ModifyingUser]
GO
ALTER TABLE [dbo].[tblForfeitOfLeaseForm] ADD  DEFAULT (sysutcdatetime()) FOR [BeginDate]
GO
ALTER TABLE [dbo].[tblForfeitOfLeaseForm] ADD  DEFAULT (CONVERT([datetime2],'9999-12-31 23:59:59')) FOR [EndDate]
GO
ALTER TABLE [dbo].[tblHighValueIndemnityForm] ADD  CONSTRAINT [df_HighValueIndemnityForm_ModifyingUser]  DEFAULT (coalesce(suser_name(suser_id()),user_name())) FOR [ModifyingUser]
GO
ALTER TABLE [dbo].[tblHighValueIndemnityForm] ADD  DEFAULT (sysutcdatetime()) FOR [BeginDate]
GO
ALTER TABLE [dbo].[tblHighValueIndemnityForm] ADD  DEFAULT (CONVERT([datetime2],'9999-12-31 23:59:59')) FOR [EndDate]
GO
ALTER TABLE [dbo].[tblIndemnityPoliciesForm] ADD  CONSTRAINT [df_IndemnityPoliciesForm]  DEFAULT (coalesce(suser_name(suser_id()),user_name())) FOR [ModifyingUser]
GO
ALTER TABLE [dbo].[tblIndemnityPoliciesForm] ADD  DEFAULT (sysutcdatetime()) FOR [BeginDate]
GO
ALTER TABLE [dbo].[tblIndemnityPoliciesForm] ADD  DEFAULT (CONVERT([datetime2],'9999-12-31 23:59:59')) FOR [EndDate]
GO
ALTER TABLE [dbo].[tblInterestPayableOCForm] ADD  DEFAULT ((0)) FOR [Formula_25]
GO
ALTER TABLE [dbo].[tblInterestPayableOCForm] ADD  DEFAULT ((0)) FOR [NumberOfDatesFundsHeld]
GO
ALTER TABLE [dbo].[tblInterestPayableOCForm] ADD  CONSTRAINT [df_InterestPayableOCForm_ModifyingUser]  DEFAULT (coalesce(suser_name(suser_id()),user_name())) FOR [ModifyingUser]
GO
ALTER TABLE [dbo].[tblInterestPayableOCForm] ADD  DEFAULT (sysutcdatetime()) FOR [BeginDate]
GO
ALTER TABLE [dbo].[tblInterestPayableOCForm] ADD  DEFAULT (CONVERT([datetime2],'9999-12-31 23:59:59')) FOR [EndDate]
GO
ALTER TABLE [dbo].[tblSalesLeadForm] ADD  CONSTRAINT [df_SalesLeadForm]  DEFAULT (coalesce(suser_name(suser_id()),user_name())) FOR [ModifyingUser]
GO
ALTER TABLE [dbo].[tblSalesLeadForm] ADD  DEFAULT (sysutcdatetime()) FOR [BeginDate]
GO
ALTER TABLE [dbo].[tblSalesLeadForm] ADD  DEFAULT (CONVERT([datetime2],'9999-12-31 23:59:59')) FOR [EndDate]
GO
ALTER TABLE [dbo].[tblStopChequeForm] ADD  CONSTRAINT [df_StopChequeForm]  DEFAULT (coalesce(suser_name(suser_id()),user_name())) FOR [ModifyingUser]
GO
ALTER TABLE [dbo].[tblStopChequeForm] ADD  DEFAULT (sysutcdatetime()) FOR [BeginDate]
GO
ALTER TABLE [dbo].[tblStopChequeForm] ADD  DEFAULT (CONVERT([datetime2],'9999-12-31 23:59:59')) FOR [EndDate]
GO
ALTER TABLE [dbo].[tblUnallocatedFundsForm] ADD  CONSTRAINT [df_tblUnallocatedFundsForm]  DEFAULT (coalesce(suser_name(suser_id()),user_name())) FOR [ModifyingUser]
GO
ALTER TABLE [dbo].[tblUnallocatedFundsForm] ADD  DEFAULT (sysutcdatetime()) FOR [BeginDate]
GO
ALTER TABLE [dbo].[tblUnallocatedFundsForm] ADD  DEFAULT (CONVERT([datetime2],'9999-12-31 23:59:59')) FOR [EndDate]
GO
ALTER TABLE [dbo].[tblWriteOffsForm] ADD  CONSTRAINT [df_WriteOffsForm_ModifyingUser]  DEFAULT (coalesce(suser_name(suser_id()),user_name())) FOR [ModifyingUser]
GO
ALTER TABLE [dbo].[tblWriteOffsForm] ADD  DEFAULT (sysutcdatetime()) FOR [BeginDate]
GO
ALTER TABLE [dbo].[tblWriteOffsForm] ADD  DEFAULT (CONVERT([datetime2],'9999-12-31 23:59:59')) FOR [EndDate]
GO
ALTER TABLE [PUB].[FEETR] ADD  DEFAULT (getdate()) FOR [LastUpdated]
GO
ALTER TABLE [PUB].[FEETR_History] ADD  DEFAULT (getdate()) FOR [EffectiveDate]
GO
ALTER TABLE [PUB].[FEETR_History] ADD  DEFAULT ('12/31/9999') FOR [EndDate]
GO
ALTER TABLE [PUB].[FEETR_History] ADD  DEFAULT ('Y') FOR [CurrentRecord]
GO
ALTER TABLE [PUB].[FEETR_History] ADD  DEFAULT (getdate()) FOR [LastUpdated]
GO
ALTER TABLE [PUB].[FEETR_History] ADD  DEFAULT (getdate()) FOR [LoadedDate]
GO
ALTER TABLE [PUB].[MATAC] ADD  DEFAULT (getdate()) FOR [LastUpdated]
GO
ALTER TABLE [PUB].[MATAC_History] ADD  DEFAULT (getdate()) FOR [EffectiveDate]
GO
ALTER TABLE [PUB].[MATAC_History] ADD  DEFAULT ('12/31/9999') FOR [EndDate]
GO
ALTER TABLE [PUB].[MATAC_History] ADD  DEFAULT ('Y') FOR [CurrentRecord]
GO
ALTER TABLE [PUB].[MATAC_History] ADD  DEFAULT (getdate()) FOR [LastUpdated]
GO
ALTER TABLE [PUB].[MATAC_History] ADD  DEFAULT (getdate()) FOR [LoadedDate]
GO
ALTER TABLE [PUB].[MATDB] ADD  DEFAULT (getdate()) FOR [LastUpdated]
GO
ALTER TABLE [PUB].[MATDB_History] ADD  DEFAULT (getdate()) FOR [EffectiveDate]
GO
ALTER TABLE [PUB].[MATDB_History] ADD  DEFAULT ('12/31/9999') FOR [EndDate]
GO
ALTER TABLE [PUB].[MATDB_History] ADD  DEFAULT ('Y') FOR [CurrentRecord]
GO
ALTER TABLE [PUB].[MATDB_History] ADD  DEFAULT (getdate()) FOR [LastUpdated]
GO
ALTER TABLE [PUB].[MATDB_History] ADD  DEFAULT (getdate()) FOR [LoadedDate]
GO
ALTER TABLE [dbo].[tblCustomerMatterNumber]  WITH CHECK ADD  CONSTRAINT [FK_CustomerMatter_Branch] FOREIGN KEY([BranchID])
REFERENCES [dbo].[tblBranch] ([ID])
GO
ALTER TABLE [dbo].[tblCustomerMatterNumber] CHECK CONSTRAINT [FK_CustomerMatter_Branch]
GO
ALTER TABLE [dbo].[tblCustomerMatterNumber]  WITH CHECK ADD  CONSTRAINT [FK_CustomerMatter_BridgeControlCenter] FOREIGN KEY([BridgeControlCenterID])
REFERENCES [dbo].[tblBridgeControlCenter] ([Id])
GO
ALTER TABLE [dbo].[tblCustomerMatterNumber] CHECK CONSTRAINT [FK_CustomerMatter_BridgeControlCenter]
GO
ALTER TABLE [dbo].[tblCustomerMatterNumber]  WITH CHECK ADD  CONSTRAINT [FK_CustomerMatter_Center] FOREIGN KEY([CenterID])
REFERENCES [dbo].[tblCenterName] ([Id])
GO
ALTER TABLE [dbo].[tblCustomerMatterNumber] CHECK CONSTRAINT [FK_CustomerMatter_Center]
GO
ALTER TABLE [dbo].[tblCustomerMatterNumber]  WITH CHECK ADD  CONSTRAINT [FK_CustomerMatter_Client] FOREIGN KEY([ClientID])
REFERENCES [dbo].[tblClientDetails] ([ID])
GO
ALTER TABLE [dbo].[tblCustomerMatterNumber] CHECK CONSTRAINT [FK_CustomerMatter_Client]
GO
ALTER TABLE [dbo].[tblCustomerMatterNumber]  WITH CHECK ADD  CONSTRAINT [FK_CustomerMatter_FeeEarner] FOREIGN KEY([FeeEarnerID])
REFERENCES [dbo].[tblFeeEarner] ([ID])
GO
ALTER TABLE [dbo].[tblCustomerMatterNumber] CHECK CONSTRAINT [FK_CustomerMatter_FeeEarner]
GO
ALTER TABLE [dbo].[tblCustomerMatterNumber]  WITH CHECK ADD  CONSTRAINT [FK_CustomerMatter_ReceivedFrom] FOREIGN KEY([ReceivedFromID])
REFERENCES [dbo].[tblReceivedFrom] ([ID])
GO
ALTER TABLE [dbo].[tblCustomerMatterNumber] CHECK CONSTRAINT [FK_CustomerMatter_ReceivedFrom]
GO
ALTER TABLE [dbo].[tblCustomerMatterNumber]  WITH CHECK ADD  CONSTRAINT [FK_CustomerMatter_Region] FOREIGN KEY([RegionID])
REFERENCES [dbo].[tblRegion] ([Id])
GO
ALTER TABLE [dbo].[tblCustomerMatterNumber] CHECK CONSTRAINT [FK_CustomerMatter_Region]
GO
ALTER TABLE [dbo].[tblCustomerMatterNumber]  WITH CHECK ADD  CONSTRAINT [FK_CustomerMatter_Team] FOREIGN KEY([TeamID])
REFERENCES [dbo].[tblTeamName] ([ID])
GO
ALTER TABLE [dbo].[tblCustomerMatterNumber] CHECK CONSTRAINT [FK_CustomerMatter_Team]
GO
ALTER TABLE [dbo].[tblDelayResponse]  WITH CHECK ADD  CONSTRAINT [FK_tblDelayResponse_CompliantsFormID] FOREIGN KEY([CompliantsFormID])
REFERENCES [dbo].[tblComplaintsForm] ([Id])
GO
ALTER TABLE [dbo].[tblDelayResponse] CHECK CONSTRAINT [FK_tblDelayResponse_CompliantsFormID]
GO
ALTER TABLE [dbo].[tblErrorThemes]  WITH CHECK ADD  CONSTRAINT [FK_tblErrorThemes_tblTitleAllocationForm] FOREIGN KEY([TitleAllocationRefId])
REFERENCES [dbo].[tblTitleAllocationForm] ([ID])
GO
ALTER TABLE [dbo].[tblErrorThemes] CHECK CONSTRAINT [FK_tblErrorThemes_tblTitleAllocationForm]
GO
ALTER TABLE [dbo].[tblRoutingDetails]  WITH CHECK ADD  CONSTRAINT [FK_tblRoutingDetails_tblProcess] FOREIGN KEY([SrcProcessId])
REFERENCES [dbo].[tblProcess] ([ProcessId])
GO
ALTER TABLE [dbo].[tblRoutingDetails] CHECK CONSTRAINT [FK_tblRoutingDetails_tblProcess]
GO
ALTER TABLE [dbo].[tblRoutingDetails]  WITH CHECK ADD  CONSTRAINT [FK_tblRoutingDetails_tblProcess1] FOREIGN KEY([trgProcessId])
REFERENCES [dbo].[tblProcess] ([ProcessId])
GO
ALTER TABLE [dbo].[tblRoutingDetails] CHECK CONSTRAINT [FK_tblRoutingDetails_tblProcess1]
GO
ALTER TABLE [dbo].[tblRoutingDetails]  WITH CHECK ADD  CONSTRAINT [FK_tblRoutingDetails_tblRouting] FOREIGN KEY([RoutingId])
REFERENCES [dbo].[tblRouting] ([RoutingId])
GO
ALTER TABLE [dbo].[tblRoutingDetails] CHECK CONSTRAINT [FK_tblRoutingDetails_tblRouting]
GO
ALTER TABLE [dbo].[tblRoutingDetails]  WITH CHECK ADD  CONSTRAINT [FK_tblRoutingDetails_tblTracker] FOREIGN KEY([TrackerId])
REFERENCES [dbo].[tblTracker] ([TrackerId])
GO
ALTER TABLE [dbo].[tblRoutingDetails] CHECK CONSTRAINT [FK_tblRoutingDetails_tblTracker]
GO
ALTER TABLE [dbo].[tblTrackerForms]  WITH CHECK ADD  CONSTRAINT [FK_tblTrackerForms_tblProcess] FOREIGN KEY([ProcessId])
REFERENCES [dbo].[tblProcess] ([ProcessId])
GO
ALTER TABLE [dbo].[tblTrackerForms] CHECK CONSTRAINT [FK_tblTrackerForms_tblProcess]
GO
ALTER TABLE [dbo].[tblTrackerForms]  WITH CHECK ADD  CONSTRAINT [FK_tblTrackerForms_tblTracker] FOREIGN KEY([TrackerId])
REFERENCES [dbo].[tblTracker] ([TrackerId])
GO
ALTER TABLE [dbo].[tblTrackerForms] CHECK CONSTRAINT [FK_tblTrackerForms_tblTracker]
GO
ALTER TABLE [dbo].[tblTrackerProcessRole]  WITH CHECK ADD  CONSTRAINT [FK_tbl_TrackerProcessRole_tblProcess] FOREIGN KEY([ProcessId])
REFERENCES [dbo].[tblProcess] ([ProcessId])
GO
ALTER TABLE [dbo].[tblTrackerProcessRole] CHECK CONSTRAINT [FK_tbl_TrackerProcessRole_tblProcess]
GO
ALTER TABLE [dbo].[tblTrackerProcessRole]  WITH CHECK ADD  CONSTRAINT [FK_tbl_TrackerProcessRole_tblRole] FOREIGN KEY([RoleId])
REFERENCES [dbo].[tblRole] ([RoleId])
GO
ALTER TABLE [dbo].[tblTrackerProcessRole] CHECK CONSTRAINT [FK_tbl_TrackerProcessRole_tblRole]
GO
ALTER TABLE [dbo].[tblTrackerProcessRole]  WITH CHECK ADD  CONSTRAINT [FK_tbl_TrackerProcessRole_tblTracker] FOREIGN KEY([TrackerId])
REFERENCES [dbo].[tblTracker] ([TrackerId])
GO
ALTER TABLE [dbo].[tblTrackerProcessRole] CHECK CONSTRAINT [FK_tbl_TrackerProcessRole_tblTracker]
GO
ALTER TABLE [dbo].[tblUser]  WITH CHECK ADD  CONSTRAINT [FK__tblUser__RoleId__52593CB8] FOREIGN KEY([RoleId])
REFERENCES [dbo].[tblRole] ([RoleId])
GO
ALTER TABLE [dbo].[tblUser] CHECK CONSTRAINT [FK__tblUser__RoleId__52593CB8]
GO
/****** Object:  StoredProcedure [dbo].[Sp_GenericTrackersForm]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================  
-- Author:  <Author,,Shivaiah>  
-- Create date: <Create Date,10-04-2021>  
-- Description: <Description,Inserting or updating routing details in tbltrackrsform table>  
-- =============================================  
CREATE PROCEDURE [dbo].[Sp_GenericTrackersForm]   
 -- Add the parameters for the stored procedure here          
 @TrackerId int,          
 @CreatedBy Varchar(50),        
 @processid int,        
 @FormDataId int  
  
AS  
BEGIN  
   
 SET NOCOUNT ON;  
  
  
    INSERT INTO tblTrackerForms            
                        (           
                         TrackerId,            
                         ProcessId,            
                         CreationDate,            
                         CreatedBy,          
                         FormDataTable,          
                         FormDataId)            
            VALUES     (            
                         @TrackerId,            
                         @processid,            
                         GETDATE(),            
                         @CreatedBy,          
                        'tblTrackerForms',          
                         @FormDataId)      
  
END  
GO
/****** Object:  StoredProcedure [dbo].[usp_3PartyComplaints_Download]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

  
/****** Script for SelectTopNRows command from SSMS  ******/    
    
-- [usp_3PartyComplaints_Download] 6,201,'Review1'  
CREATE Procedure [dbo].[usp_3PartyComplaints_Download]                  
@TrackerId int,                
@processId int,
@ViewName Varchar(100)
                
AS                    
Begin        
SET NOCOUNT OFF
 SET FMTONLY OFF
BEGIN TRY          
    
SELECT [ID] = PC.ID    
      ,[MatterNumber]    
      ,[ComplainantName]    
      ,Center.CenterName     
      ,Team.Teamname    
      ,[DateReceived]    
      ,[ExecComplaint]= CASE WHEN PC.ExecComplaint = 1 then 'Yes' else 'No' END    
      ,[CCSUnresponsive] = CASE WHEN PC.CCSUnresponsive = 1 then 'Yes' else 'No' END    
      ,[CCSRaisingrepeatedEnquiries]  = CASE WHEN PC.CCSRaisingrepeatedEnquiries = 1 then 'Yes' else 'No' END    
      ,[CCSNotForwardingPPWinGoodTime]  = CASE WHEN PC.CCSNotForwardingPPWinGoodTime = 1 then 'Yes' else 'No' END    
      ,[CCSCausingDelays] = CASE WHEN PC.CCSCausingDelays = 1 then 'Yes' else 'No' END    
      ,PS.Source    
      ,[AttachFile] INTO #FilterResult
  FROM [dbo].[tbl3PartyComplaintsForm] PC    
  LEFT JOIN [dbo].[tblCenterName] CENTER ON PC.CenterID = CENTER.ID    
  LEFT JOIN [dbo].[tblTeamName] TEAM ON PC.TeamID = TEAM.ID    
  LEFT JOIN [dbo].[tbl3rdPartySource] PS ON PC.SourceID = PS.ID    

where PC.ID in (select distinct FormDataId      
    FROM [dbo].[tblTrackerForms]        
    with (nolock) where TrackerId=@TrackerId and ProcessId=@processId  )       
 order by 1 asc      

 DECLARE @sql nvarchar(max);

SELECT  @sql = 'SELECT ID,[MatterNumber]    
      ,ComplainantName    
      ,CenterName     
      ,Teamname    
      ,DateReceived ,ExecComplaint,CCSUnresponsive,CCSRaisingrepeatedEnquiries,CCSNotForwardingPPWinGoodTime,CCSCausingDelays,Source,AttachFile FROM #FilterResult ' + dbo.FuncDynamicQueryResult(@TrackerId, @ViewName)

EXEC sp_executesql @sql

      
      
END TRY    
BEGIN CATCH    
    
        DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),    
                @ErrorSeverity INT = ERROR_SEVERITY(),    
                @ErrorNumber INT = ERROR_NUMBER();    
    
  RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);    
     
END CATCH    

DROP TABLE #FilterResult
End          
GO
/****** Object:  StoredProcedure [dbo].[usp_3PartyComplaintsForm_AuditView]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ajay George Kalathil
-- Create date: 6th May 2021
-- Description:	This SP will be used to get the Audit Trial Version View for the front end system
-- =============================================


--Exec usp_3PartyComplaintsForm_AuditView 2,null,null
CREATE PROCEDURE [dbo].[usp_3PartyComplaintsForm_AuditView]
	-- Add the parameters for the stored procedure here
	@ID Int,  
	@FromDate datetime2 = '1900-01-01 00:00:00.0000000', 
	@ToDate datetime2	= '9999-12-31 23:59:59.9999999'
AS
BEGIN

  BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    
;WITH T
AS (
    SELECT		PC.ID	
	            ,MatterNumber as MatterNumber , lag(MatterNumber) over(order by enddate asc) as Prev_MatterNumber
				,ComplainantName as ComplainantName , lag(ComplainantName) over(order by enddate asc) as Prev_ComplainantName
				,CenterName as Center , lag(CenterName) over(order by enddate asc) as Prev_Center
				,TeamName as Team, lag(TeamName) over(order by enddate asc) as Prev_Team
				,DateReceived as DateReceived , lag(DateReceived) over(order by enddate asc) as Prev_DateReceived
				,ISNULL(ExecComplaint,0) as ExecComplaint , ISNULL(lag(ExecComplaint) over(order by enddate asc),0) as Prev_ExecComplaint
				,ISNULL(CCSUnresponsive,0) as CCSUnresponsive , ISNULL(lag(CCSUnresponsive) over(order by enddate asc),0) as Prev_CCSUnresponsive
				,ISNULL(CCSRaisingrepeatedEnquiries,0) as CCSRaisingrepeatedEnquiries , ISNULL(lag(CCSRaisingrepeatedEnquiries) over(order by enddate asc),0) as Prev_CCSRaisingrepeatedEnquiries
				,ISNULL(CCSNotForwardingPPWinGoodTime,0) as CCSNotForwardingPPWinGoodTime , ISNULL(lag(CCSNotForwardingPPWinGoodTime) over(order by enddate asc),0) as Prev_CCSNotForwardingPPWinGoodTime				
				,ISNULL(CCSCausingDelays,0) as CCSCausingDelays , ISNULL(lag(CCSCausingDelays) over(order by enddate asc),0) as Prev_CCSCausingDelays
				,Source as Source , lag(Source) over(order by enddate asc) as Prev_Source	
				,AttachFile as AttachFile , lag(AttachFile) over(order by enddate asc) as Prev_AttachFile				
				,ModifyingUser as ModifyingUser , lag(ModifyingUser) over(order by enddate asc) as Prev_ModifyingUser
				,Action as Action , lag(Action) over(order by enddate asc) as Prev_Action
				,BeginDate
				,EndDate
				,ROW_Number() over(partition by  PC.Id Order by EndDate asc) as VersionNumber
    FROM dbo.tbl3PartyComplaintsForm FOR SYSTEM_TIME ALL AS PC    
  LEFT JOIN [dbo].[tblCenterName] CENTER ON PC.CenterID = CENTER.ID    
  LEFT JOIN [dbo].[tblTeamName] TEAM ON PC.TeamID = TEAM.ID    
  LEFT JOIN [dbo].[tbl3rdPartySource] PS ON PC.SourceID = PS.ID 
    WHERE PC.ID =@ID
)
SELECT T.BeginDate,
       T.EndDate,
       C.ColName as ColumnName,
	   t.VersionNumber,	   
       C.present_Value as CurrentValue,
       C.prev_value as PreviousValue,
	   T.ModifyingUser as ModifiedBy
FROM T
    CROSS APPLY
    (
        VALUES		    
			 ('Matter Number', Convert(varchar, T.MatterNumber) , Convert(varchar,T.Prev_MatterNumber))
			,('Complainant Name', Convert(varchar, T.ComplainantName) , Convert(varchar,T.Prev_ComplainantName))
			,('Center', Convert(varchar, T.Center) , Convert(varchar,T.Prev_Center))	
			,('Team', Convert(varchar, T.Team) , Convert(varchar,T.Prev_Team))
			,('Date Received', Convert(varchar(10), T.DateReceived,103) , Convert(varchar(10),T.Prev_DateReceived,103))
			,('Exec Complaint', Convert(varchar,case when T.ExecComplaint=1 then 'Yes' else 'No' end) , Convert(varchar,case when T.Prev_ExecComplaint=1 then 'Yes' else 'No' end))
			,('CCS Unresponsive', Convert(varchar, case when T.CCSUnresponsive=1 then 'Yes' else 'No' end) , Convert(varchar,case when T.Prev_CCSUnresponsive=1 then 'Yes' else 'No' end))			
			,('CCS Raising repeated Enquiries', Convert(varchar,case when T.CCSRaisingrepeatedEnquiries=1 then 'Yes' else 'No' end) , Convert(varchar,case when T.Prev_CCSRaisingrepeatedEnquiries=1 then 'Yes' else 'No' end))
			,('CCS Not Forwarding PP Win Good Time', Convert(varchar,case when T.CCSNotForwardingPPWinGoodTime=1 then 'Yes' else 'No' end) , Convert(varchar,case when T.Prev_CCSNotForwardingPPWinGoodTime=1 then 'Yes' else 'No' end))
			,('CCS Causing Delays', Convert(varchar, case when T.CCSCausingDelays=1 then 'Yes' else 'No' end) , Convert(varchar,case when T.Prev_CCSCausingDelays=1 then 'Yes' else 'No' end))			
			,('Source', Convert(varchar, T.Source) , Convert(varchar,T.Prev_Source))
			,('Attach File', Convert(varchar, T.AttachFile) , Convert(varchar,T.Prev_AttachFile)) 						
			,('ModifyingUser', Convert(varchar, T.ModifyingUser) , Convert(varchar,T.Prev_ModifyingUser))
			,('Action', Convert(varchar, T.Action) , Convert(varchar,T.Prev_Action))
			) AS C (ColName, present_Value, prev_value)
WHERE  EXISTS
(
    SELECT present_Value 
    EXCEPT 
    SELECT C.prev_value
)  and Convert(datetime,BeginDate) >= @FromDate and Convert(Datetime,BeginDate) <=@ToDate

END TRY
BEGIN CATCH

        DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),
                @ErrorSeverity INT = ERROR_SEVERITY(),
                @ErrorNumber INT = ERROR_NUMBER();

		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);
 
END CATCH
END

GO
/****** Object:  StoredProcedure [dbo].[usp_3PartyComplaintsForm_InsertUpdate]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_3PartyComplaintsForm_InsertUpdate] 
(
@ID INT = Null,
@MatterNumber Varchar(20) =NULL,
@ComplainantName Varchar(50) =NULL,
@CenterID Int =NULL,
@TeamID Int =NULL,
@DateReceived Datetime =NULL,
@ExecComplaint Bit =NULL,
@CCSUnresponsive Bit =NULL,
@CCSRaisingrepeatedEnquiries Bit =NULL,
@CCSNotForwardingPPWinGoodTime Bit =NULL,
@CCSCausingDelays Bit =NULL,
@SourceID int =NULL,
@AttachFile Varchar(Max) =NULL, 
  
 @StatementType  Varchar(50)=NULL,      
 @TrackerId int =NULL,        
 @CreatedBy Varchar(50)=NULL,      
 @processid int =NULL,      
 @RoutingID Int=NULL,
 @selectedData varchar(100) = NULL ) 

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	Declare @trgProcessId int;   
    -- It does the Insert operation for dbo.tbl3PartyComplaintsForm table.
IF @StatementType = 'I'          
 BEGIN       
       
  INSERT INTO dbo.tbl3PartyComplaintsForm  
   (  
--ID ,
MatterNumber,
ComplainantName ,
CenterID ,
TeamID ,
DateReceived ,
ExecComplaint ,
CCSUnresponsive ,
CCSRaisingrepeatedEnquiries ,
CCSNotForwardingPPWinGoodTime ,
CCSCausingDelays ,
SourceID ,
AttachFile , 
[Action],
[ModifyingUser] )
   
Values  
( 
--@ID ,
@MatterNumber,
@ComplainantName ,
@CenterID ,
@TeamID ,
@DateReceived ,
@ExecComplaint ,
@CCSUnresponsive ,
@CCSRaisingrepeatedEnquiries ,
@CCSNotForwardingPPWinGoodTime ,
@CCSCausingDelays ,
@SourceID ,
@AttachFile , 
@StatementType,
@CreatedBy)  
  
    Declare @FormDataId int;        
	set @FormDataId = SCOPE_IDENTITY()        
      
	SELECT @trgProcessId=trgProcessId FROM tblRoutingDetails
		WHERE TrackerId= @TrackerId AND srcprocessid = @processid AND RoutingID = @RoutingID      
  
   -- This is common for across all the trackers it does the inserting records into tbltrackersform table. 
    EXEC Sp_GenericTrackersForm  @TrackerId, @CreatedBy, @trgProcessId,  @FormDataId
   
 END   


  SELECT @trgProcessId=trgProcessId FROM tblRoutingDetails WHERE TrackerId= @TrackerId AND 
  srcprocessid =@ProcessId          
 AND RoutingID = @RoutingID   



-- It does the update operation for dbo.tbl3PartyComplaintsForm table.
IF @StatementType = 'U'          
  BEGIN       
	
UPDATE dbo.tbl3PartyComplaintsForm Set
MatterNumber = @MatterNumber,
ComplainantName = @ComplainantName,
CenterID =@CenterID,
TeamID =@TeamID,
DateReceived =@DateReceived,
ExecComplaint = @ExecComplaint,
CCSUnresponsive =@CCSUnresponsive ,
CCSRaisingrepeatedEnquiries =@CCSRaisingrepeatedEnquiries,
CCSNotForwardingPPWinGoodTime =@CCSNotForwardingPPWinGoodTime,
CCSCausingDelays =@CCSCausingDelays ,
SourceID = @SourceID,
AttachFile = @AttachFile, 
[Action] =@StatementType,
[ModifyingUser]  =@CreatedBy
WHERE Id = @Id    
                
  -- This is common across all the trackers and perofrms UPDATE action against tbltrackersform table   
     EXEC [dbo].[usp_TrackersForm_Update] @ID,@TrackerId, @ProcessId,@CreatedBy, @RoutingID, @selectedData,@StatementType  
      
       
  END     

-- It does the Delete or activate operation for dbo.tbl3PartyComplaintsForm table.
IF (@StatementType = 'D' OR  @StatementType = 'R')        
  BEGIN       


    UPDATE dbo.tbl3PartyComplaintsForm  
			SET  
				   [ModifyingUser] = @CreatedBy,  
				   [Action] = @StatementType  
	WHERE Id IN (SELECT CAST(value AS INTEGER) FROM STRING_SPLIT(@selectedData, ','))         
  
   --This is common for across all the trackers it does the updating routing details('according no.of records') into tbltrackersform table. 
	 EXEC [dbo].[usp_TrackersForm_Update] @ID,@TrackerId, @ProcessId,@CreatedBy, @RoutingID, @selectedData,@StatementType
     
  END  


	
END
GO
/****** Object:  StoredProcedure [dbo].[usp_ActiveBalancedetails]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[usp_ActiveBalancedetails]
@ITEMNOTE VARCHAR(250)=NULL,
@ReportType int,
@Fromdate date= null,
@Todate date= null
AS
BEGIN
if
@ReportType=1
begin
IF (@ITEMNOTE LIKE '%Cardiff%' or @ITEMNOTE LIKE '%Manchester%' or @ITEMNOTE IS NULL)
BEGIN
Select
distinct MATDB_0."Notes" As 'Legal Team' ,
FEETR_0.NAME As 'Fee Earner',
MATDB_0."MT-CODE" As 'Matter Number',
MATDB_0.Description As 'Matter Description',
MATAC_0."BAL-CLI" As 'Client Balance',
MATDB_0."No-BILLS" As 'Number of Bills',
CASE 
WHEN [Notes]Like'%Cardiff%' then 'Cardiff'
WHEN [Notes]Like'%Manchester%' then 'Manchester'
else 'OTHER'
end as [Legal Team1]
--INTO #OVER

From
PUB.FEETR FEETR_0, PUB.MATAC MATAC_0, PUB.MATDB MATDB_0

Where
MATAC_0."MT-CODE"= MATDB_0."MT-CODE" and
FEETR_0."NAME" = MATDB_0."FEE-EARNER" AND
MATAC_0."BAL-CLI">0 AND (MATDB_0.NOTES LIKE '%' + @ITEMNOTE + '%' or @ITEMNOTE is NULL)
END
ELSE
BEGIN
SELECT
distinct MATDB_0."Notes" As 'Legal Team' ,
FEETR_0.NAME As 'Fee Earner',
MATDB_0."MT-CODE" As 'Matter Number',
MATDB_0.Description As 'Matter Description',
MATAC_0."BAL-CLI" As 'Client Balance',
MATDB_0."No-BILLS" As 'Number of Bills',
CASE WHEN [Notes]Like'%Cardiff%' then 'Cardiff'
WHEN [Notes]Like'%Manchester%' then 'Manchester'
else 'OTHER'
end as [Legal Team1]
--INTO #OVER

From
PUB.FEETR FEETR_0, PUB.MATAC MATAC_0, PUB.MATDB MATDB_0

Where
MATAC_0."MT-CODE"= MATDB_0."MT-CODE" and
FEETR_0."NAME" = MATDB_0."FEE-EARNER" AND
MATAC_0."BAL-CLI">0 AND (MATDB_0.NOTES NOT LIKE '%Cardiff%' and MATDB_0.NOTES NOT LIKE '%Manchester%')
END
end
else
begin
IF (@ITEMNOTE LIKE '%Cardiff%' or @ITEMNOTE LIKE '%Manchester%' or @ITEMNOTE IS NULL)
BEGIN
Select
distinct MATDB_0."Notes" As 'Legal Team' ,
FEETR_0.NAME As 'Fee Earner',
MATDB_0."MT-CODE" As 'Matter Number',
MATDB_0.Description As 'Matter Description',
MATAC_0."BAL-CLI" As 'Client Balance',
MATDB_0."No-BILLS" As 'Number of Bills',
CASE WHEN [Notes]Like'%Cardiff%' then 'Cardiff'
WHEN [Notes]Like'%Manchester%' then 'Manchester'
else 'OTHER'
end as [Legal Team1]
--INTO #OVER

From
PUB.FEETR_History FEETR_0, PUB.MATAC_History MATAC_0, PUB.MATDB_History MATDB_0

Where
(FEETR_0.[EffectiveDate] between @Fromdate and @Todate) and
(MATAC_0.[EffectiveDate] between @Fromdate and @Todate) and
(MATDB_0.[EffectiveDate] between @Fromdate and @Todate) and
MATAC_0."MT-CODE"= MATDB_0."MT-CODE" and
FEETR_0."NAME" = MATDB_0."FEE-EARNER" AND
MATAC_0."BAL-CLI">0 AND (MATDB_0.NOTES LIKE '%' + @ITEMNOTE + '%' or @ITEMNOTE is NULL)
END
ELSE
BEGIN
SELECT
distinct MATDB_0."Notes" As 'Legal Team' ,
FEETR_0.NAME As 'Fee Earner',
MATDB_0."MT-CODE" As 'Matter Number',
MATDB_0.Description As 'Matter Description',
MATAC_0."BAL-CLI" As 'Client Balance',
MATDB_0."No-BILLS" As 'Number of Bills',
CASE WHEN [Notes]Like'%Cardiff%' then 'Cardiff'
WHEN [Notes]Like'%Manchester%' then 'Manchester'
else 'OTHER'
end as [Legal Team1]
--INTO #OVER

From
PUB.FEETR_History FEETR_0, PUB.MATAC_History MATAC_0, PUB.MATDB_History MATDB_0

Where
(FEETR_0.[EffectiveDate] between @Fromdate and @Todate) and
(MATAC_0.[EffectiveDate] between @Fromdate and @Todate) and
(MATDB_0.[EffectiveDate] between @Fromdate and @Todate) and
MATAC_0."MT-CODE"= MATDB_0."MT-CODE" and
FEETR_0."NAME" = MATDB_0."FEE-EARNER" AND
MATAC_0."BAL-CLI">0 AND (MATDB_0.NOTES NOT LIKE '%Cardiff%' and MATDB_0.NOTES NOT LIKE '%Manchester%')

EXCEPT
SELECT
MATDB_0."Notes" As 'Legal Team' ,
FEETR_0.NAME As 'Fee Earner',
MATDB_0."MT-CODE" As 'Matter Number',
MATDB_0.Description As 'Matter Description',
MATAC_0."BAL-CLI" As 'Client Balance',
MATDB_0."No-BILLS" As 'Number of Bills',
CASE WHEN [Notes]Like'%Cardiff%' then 'Cardiff'
WHEN [Notes]Like'%Manchester%' then 'Manchester'
else 'OTHER'
end as [Legal Team1]
--INTO #OVER
From
PUB.FEETR_History FEETR_0, PUB.MATAC_History MATAC_0, PUB.MATDB_History MATDB_0
Where 1=0
--drop table #over
end
end
END--pROCEDURE END
GO
/****** Object:  StoredProcedure [dbo].[usp_ActiveBalances]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[usp_ActiveBalances]
@ReportType int= null,
@Fromdate date= null,
@Todate date= null
AS
BEGIN
if
@ReportType=1
BEGIN
Select
distinct MATDB_0."Notes" As [LEGAL TEAM] ,
FEETR_0.NAME As 'Fee Earner',
MATDB_0."MT-CODE" As [Matter Number],
MATDB_0.Description As 'Matter Description',
MATAC_0."BAL-CLI" As [Client Balance],
MATDB_0."No-BILLS" As 'Number of Bills'
INTO #OVER
From
PUB.FEETR FEETR_0, PUB.MATAC MATAC_0, PUB.MATDB MATDB_0
Where
MATAC_0."MT-CODE"= MATDB_0."MT-CODE" and
FEETR_0."NAME" = MATDB_0."FEE-EARNER" AND
MATAC_0."BAL-CLI">0 --AND (MATDB_0."NOTES") LIKE 'Cardiff%')
SELECT CASE
WHEN [Legal Team] LIKE '%Cardiff%' THEN 'Cardiff'
WHEN [Legal Team] LIKE '%Manchester%' THEN 'Manchester'
ELSE  'OTHER'
end 
[LEGAL TEAM],SUM([Client Balance])[VALUES], COUNT([Matter Number]) [NO Of Files] FROM #OVER
GROUP BY CASE
WHEN [Legal Team] LIKE '%Cardiff%' THEN 'Cardiff'
WHEN [Legal Team] LIKE '%Manchester%' THEN 'Manchester'
ELSE 'OTHER'
end
drop table #over
end
else
begin
Select
distinct MATDB_0."Notes" As [LEGAL TEAM] ,
FEETR_0.NAME As 'Fee Earner',
MATDB_0."MT-CODE" As [Matter Number],
MATDB_0.Description As 'Matter Description',
MATAC_0."BAL-CLI" As [Client Balance],
MATDB_0."No-BILLS" As 'Number of Bills'
INTO #OVER1
From
PUB.FEETR_History FEETR_0, PUB.MATAC_History MATAC_0, PUB.MATDB_History MATDB_0
Where
(FEETR_0.[EffectiveDate] between @Fromdate and @Todate) and
(MATAC_0.[EffectiveDate] between @Fromdate and @Todate) and
(MATDB_0.[EffectiveDate] between @Fromdate and @Todate) and
MATAC_0."MT-CODE"= MATDB_0."MT-CODE" and
FEETR_0."NAME" = MATDB_0."FEE-EARNER" AND
MATAC_0."BAL-CLI">0 --AND (MATDB_0."NOTES") LIKE 'Cardiff%')
EXCEPT
Select
MATDB_0."Notes" As 'Legal Team' ,
FEETR_0.NAME As 'Fee Earner',
MATDB_0."MT-CODE" As 'Matter Number',
MATDB_0.Description As 'Matter Description',
MATAC_0."BAL-CLI" As 'Client Balance',
MATDB_0."No-BILLS" As 'Number of Bills'
--INTO #OVER
From
PUB.FEETR FEETR_0, PUB.MATAC MATAC_0, PUB.MATDB MATDB_0
Where 1=0
SELECT CASE
WHEN [Legal Team] LIKE '%Cardiff%' THEN 'Cardiff'
WHEN [Legal Team] LIKE '%Manchester%' THEN 'Manchester'
ELSE  'OTHER'
end 
[LEGAL TEAM],SUM([Client Balance])[VALUES], COUNT([Matter Number]) [NO Of Files] FROM #OVER1
GROUP BY CASE
WHEN [Legal Team] LIKE '%Cardiff%' THEN 'Cardiff'
WHEN [Legal Team] LIKE '%Manchester%' THEN 'Manchester'
ELSE 'OTHER'
end
--drop table #OVER1
--end

drop table #over1
end
end
GO
/****** Object:  StoredProcedure [dbo].[usp_BackupBalances]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[usp_BackupBalances]
@ReportType int,
@Fromdate date= null,
@Todate date= null
AS
BEGIN
if
@ReportType=1
BEGIN
Select
MATDB_0."Notes" As 'Legal Team',
FEETR_0.NAME As 'Fee Earner',
MATDB_0."MT-CODE" As 'Matter Number',
MATDB_0.Description As 'Matter Description',
MATAC_0."BAL-CLI" As 'Client',
MATAC_0."BAL-UBD" As 'Unpaid Disbs',
MATAC_0."BAL-UPB" As 'Unpaid Bills',
MATAC_0."BAL-CLI"+MATAC_0."BAL-UBD"+MATAC_0."BAL-UPB" As 'Balance'

From PUB.FEETR FEETR_0, PUB.MATAC MATAC_0, PUB.MATDB MATDB_0

Where
MATAC_0."MT-CODE"= MATDB_0."MT-CODE" and
FEETR_0."NAME" = MATDB_0."FEE-EARNER" AND
((MATAC_0."BAL-CLI" >0) OR (MATAC_0."BAL-UBD" <0) OR (MATAC_0."BAL-UPB" <0))
order by MATDB_0.Notes
end
else
begin
Select
MATDB_0."Notes" As 'Legal Team',
FEETR_0.NAME As 'Fee Earner',
MATDB_0."MT-CODE" As 'Matter Number',
MATDB_0.Description As 'Matter Description',
MATAC_0."BAL-CLI" As 'Client',
MATAC_0."BAL-UBD" As 'Unpaid Disbs',
MATAC_0."BAL-UPB" As 'Unpaid Bills',
MATAC_0."BAL-CLI"+MATAC_0."BAL-UBD"+MATAC_0."BAL-UPB" As 'Balance'

From PUB.FEETR_History FEETR_0, PUB.MATAC_HISTORY MATAC_0, PUB.MATDB_HISTORY MATDB_0

Where
(FEETR_0.[EffectiveDate] between @Fromdate and @Todate) and
(MATAC_0.[EffectiveDate] between @Fromdate and @Todate) and
(MATDB_0.[EffectiveDate] between @Fromdate and @Todate) and
MATAC_0."MT-CODE"= MATDB_0."MT-CODE" and
FEETR_0."NAME" = MATDB_0."FEE-EARNER" AND
((MATAC_0."BAL-CLI" >0) OR (MATAC_0."BAL-UBD" <0) OR (MATAC_0."BAL-UPB" <0))
--CurrentRecord = 'Y'and
--MATAC_0.EndDate between @Fromdate and @Todate
EXCEPT
Select
MATDB_0."Notes" As 'Legal Team',
FEETR_0.NAME As 'Fee Earner',
MATDB_0."MT-CODE" As 'Matter Number',
MATDB_0.Description As 'Matter Description',
MATAC_0."BAL-CLI" As 'Client',
MATAC_0."BAL-UBD" As 'Unpaid Disbs',
MATAC_0."BAL-UPB" As 'Unpaid Bills',
MATAC_0."BAL-CLI"+MATAC_0."BAL-UBD"+MATAC_0."BAL-UPB" As 'Balance'
From PUB.FEETR_History FEETR_0, PUB.MATAC_HISTORY MATAC_0, PUB.MATDB_HISTORY MATDB_0 where 1=0
order by MATDB_0.Notes
end
end
GO
/****** Object:  StoredProcedure [dbo].[usp_BridgeControl_Download]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Modified date: <13-04-2021,Sundar> <Included Exception handling>
-- [usp_BridgeControl_Download] 7,201
CREATE Procedure [dbo].[usp_BridgeControl_Download]              
@TrackerId int,            
@processId int,
@ViewName Varchar(100)                
            
AS                
Begin           
 BEGIN TRY      
 Select  
	   ID = BC.ID  
	  ,[DateAuth]
      ,[SubmittedBy]
      ,Center = CENTER.CenterName
      ,[Team]
      ,[MatterNumber]
      ,[ClientName]
      ,Category=objCategory.Category
      ,[MatterCompletionDate]
      ,[Amount]
      ,Risk=objRisk.Risk
      ,[Explanation]
      ,[ActiontoRecoverFunds]
      ,[ExpectedDateofReturn]
      ,[Authorisedby]
      ,[Comments]
      ,[Outstanding]
      ,[DateRepaid]
      ,[Dateofnextupdate]
      ,[AttachFile]  INTO #FilterResult
 From [dbo].tblBridgeControlForm BC  
 Left Join [dbo].[tblCenterName] CENTER on BC.CenterID = CENTER.Id
 Left Join [dbo].tblBridgeControlCategory objCategory on BC.CategoryID = objCategory.Id
 Left Join [dbo].tblBridgeControlRisk objRisk on BC.RiskId = objRisk.Id  
 
 
  
 where BC.ID in (select distinct FormDataId  
    FROM [dbo].[tblTrackerForms]    
    with (nolock) where TrackerId=@TrackerId and ProcessId=@processId  )   
 order by 1 asc  
  
  DECLARE @sql nvarchar(max);

SELECT  @sql = 'SELECT * FROM #FilterResult ' + dbo.FuncDynamicQueryResult(@TrackerId, @ViewName)

EXEC sp_executesql @sql
  
END TRY
BEGIN CATCH

        DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),
                @ErrorSeverity INT = ERROR_SEVERITY(),
                @ErrorNumber INT = ERROR_NUMBER();

		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);
 
END CATCH
DROP TABLE #FilterResult

End      
      
GO
/****** Object:  StoredProcedure [dbo].[usp_BridgeControlForm_AuditView]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ajay George Kalathil
-- Create date: 6th May 2021
-- Modified Date : 13th May 2021 <By : Sundar> <To replace Centre,Category and Risk IDs with its value>
-- Description:	This SP will be used to get the Audit Trial Version View for the front end system
-- =============================================


--Exec usp_BridgeControlForm_AuditView 16,null,null
CREATE PROCEDURE [dbo].[usp_BridgeControlForm_AuditView]
	-- Add the parameters for the stored procedure here
	@ID Int,  
	@FromDate datetime2 = '1900-01-01 00:00:00.0000000', 
	@ToDate datetime2	= '9999-12-31 23:59:59.9999999'
AS
BEGIN

  BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	;WITH T
AS (
    SELECT		BC.ID
				,DateAuth as DateAuth , lag(DateAuth) over(order by enddate asc) as Prev_DateAuth
				,SubmittedBy as SubmittedBy , lag(SubmittedBy) over(order by enddate asc) as Prev_SubmittedBy
				,C.[CenterName] as Center,lag(C.[CenterName]) over(order by enddate) as Prev_Center
				--,CenterID as CenterID , lag(CenterID) over(order by enddate asc) as Prev_CenterID
				,Team as Team , lag(Team) over(order by enddate asc) as Prev_Team
				,MatterNumber as MatterNumber , lag(MatterNumber) over(order by enddate asc) as Prev_MatterNumber
				,ClientName as ClientName , lag(ClientName) over(order by enddate asc) as Prev_ClientName
				,Cat.Category as Category , lag(Category) over(order by enddate asc) as Prev_Category
				--,CategoryID as CategoryID , lag(CategoryID) over(order by enddate asc) as Prev_CategoryID
				,MatterCompletionDate as MatterCompletionDate , lag(MatterCompletionDate) over(order by enddate asc) as Prev_MatterCompletionDate
				,Amount as Amount , lag(Amount) over(order by enddate asc) as Prev_Amount
				,R.Risk as Risk , lag(Risk) over(order by enddate asc) as Prev_Risk
				--,RiskId as RiskId , lag(RiskId) over(order by enddate asc) as Prev_RiskId
				,Explanation as Explanation , lag(Explanation) over(order by enddate asc) as Prev_Explanation
				,ActiontoRecoverFunds as ActiontoRecoverFunds , lag(ActiontoRecoverFunds) over(order by enddate asc) as Prev_ActiontoRecoverFunds
				,ExpectedDateofReturn as ExpectedDateofReturn , lag(ExpectedDateofReturn) over(order by enddate asc) as Prev_ExpectedDateofReturn
				,Authorisedby as Authorisedby , lag(Authorisedby) over(order by enddate asc) as Prev_Authorisedby
				,Comments as Comments , lag(Comments) over(order by enddate asc) as Prev_Comments
				,Outstanding as Outstanding , lag(Outstanding) over(order by enddate asc) as Prev_Outstanding
				,DateRepaid as DateRepaid , lag(DateRepaid) over(order by enddate asc) as Prev_DateRepaid
				,Dateofnextupdate as Dateofnextupdate , lag(Dateofnextupdate) over(order by enddate asc) as Prev_Dateofnextupdate
				,AttachFile as AttachFile , lag(AttachFile) over(order by enddate asc) as Prev_AttachFile
				,ModifyingUser as ModifyingUser , lag(ModifyingUser) over(order by enddate asc) as Prev_ModifyingUser
				,Action as Action , lag(Action) over(order by enddate asc) as Prev_Action
				,BeginDate
				,EndDate
				,ROW_Number() over(partition by  BC.Id Order by EndDate asc) as VersionNumber
    FROM dbo.tblBridgeControlForm FOR SYSTEM_TIME ALL as BC
	 LEFT JOIN [dbo].[tblCenterName] C on  BC.CenterID = C.ID
	 LEFT JOIN [dbo].[tblBridgeControlCategory] Cat on BC.CategoryID = Cat.ID
	 LEFT JOIN [dbo].[tblBridgeControlRisk] R on BC.RiskId = R.ID
		
    WHERE BC.id = @ID
)
SELECT T.BeginDate,
       T.EndDate,
       C.ColName as ColumnName,
	   t.VersionNumber,	   
       C.present_Value as CurrentValue,
       C.prev_value as PreviousValue,
	   T.ModifyingUser as ModifiedBy
FROM T
    CROSS APPLY
    (
        VALUES
			('Date Auth', Convert(varchar(10), T.DateAuth,103) , Convert(varchar(10),T.Prev_DateAuth,103))
			,('Submitted By', Convert(varchar, T.SubmittedBy) , Convert(varchar,T.Prev_SubmittedBy))
			,('Center', Convert(varchar, T.Center) , Convert(varchar,T.Prev_Center))
			,('Team', Convert(varchar, T.Team) , Convert(varchar,T.Prev_Team))
			,('Matter Number', Convert(varchar, T.MatterNumber) , Convert(varchar,T.Prev_MatterNumber))
			,('Client Name', Convert(varchar, T.ClientName) , Convert(varchar,T.Prev_ClientName))
			,('Category', Convert(varchar, T.Category) , Convert(varchar,T.Prev_Category))
			,('Matter Completion Date', Convert(varchar(10), T.MatterCompletionDate,103) , Convert(varchar(10),T.Prev_MatterCompletionDate,103))
			,('Amount', Convert(varchar, T.Amount) , Convert(varchar,T.Prev_Amount))
			,('Risk', Convert(varchar, T.Risk) , Convert(varchar,T.Prev_Risk))
			,('Explanation', Convert(varchar, T.Explanation) , Convert(varchar,T.Prev_Explanation))
			,('Action To Recover Funds', Convert(varchar, T.ActiontoRecoverFunds) , Convert(varchar,T.Prev_ActiontoRecoverFunds))
			,('Expected Date of Return', Convert(varchar(10), T.ExpectedDateofReturn,103) , Convert(varchar(10),T.Prev_ExpectedDateofReturn,103))
			,('Authorised By', Convert(varchar, T.Authorisedby) , Convert(varchar,T.Prev_Authorisedby))
			,('Comments', Convert(varchar, T.Comments) , Convert(varchar,T.Prev_Comments))
			,('Outstanding', Convert(varchar, T.Outstanding) , Convert(varchar,T.Prev_Outstanding))
			,('Date Repaid', Convert(varchar(10), T.DateRepaid,103) , Convert(varchar(10),T.Prev_DateRepaid,103))
			,('Date of next update', Convert(varchar(10), T.Dateofnextupdate,103) , Convert(varchar(10),T.Prev_Dateofnextupdate,103))
			,('Attach File', Convert(varchar, T.AttachFile) , Convert(varchar,T.Prev_AttachFile))
			,('ModifyingUser', Convert(varchar, T.ModifyingUser) , Convert(varchar,T.Prev_ModifyingUser))
			,('Action', Convert(varchar, T.Action) , Convert(varchar,T.Prev_Action))
			) AS C (ColName, present_Value, prev_value)
WHERE  EXISTS
(
    SELECT present_Value 
    EXCEPT 
    SELECT C.prev_value
)  and Convert(datetime,BeginDate) >= @FromDate and Convert(Datetime,BeginDate) <=@ToDate

END TRY
BEGIN CATCH

        DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),
                @ErrorSeverity INT = ERROR_SEVERITY(),
                @ErrorNumber INT = ERROR_NUMBER();

		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);
 
END CATCH
END

GO
/****** Object:  StoredProcedure [dbo].[usp_BridgeControlForm_InsertUpdate]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================  
-- Author:  <Author : Sundar>  
-- Create date: <12-04-2021>  
-- Modified date: <13-04-2021> <Included Exception handling>
-- Description: <Description,The purpose of the Stored Procedure is to make CRUD operations on BridgeControlForm tables>  
-- Generic Info: <I - Insert , U- Update, D- Delete R-Re activate>    
-- =============================================  
  
  
 -- INSERT EXEC usp_BridgeControlForm_InsertUpdate 0,'2021-03-31 14:06:01.827','test',1,'test','1245','test',2,'2021-03-31 14:06:01.827',100,3,'test','test','2021-03-31 14:06:01.827','test','test',200,'2021-03-31 14:06:01.827','2021-03-31 14:06:01.827','aaa.txt','I',7,'Sundar',101,0  
 -- UPDATE EXEC usp_BridgeControlForm_InsertUpdate 12,'2021-03-31 14:06:01.827','test',1,'test','1245','test',2,'2021-03-31 14:06:01.827',100,3,'test','test','2021-03-31 14:06:01.827','test','test',200,'2021-03-31 14:06:01.827','2021-03-31 14:06:01.827','aaa.txt','U',7,'Sundar',201,1  
 -- DELETE EXEC usp_BridgeControlForm_InsertUpdate 0,'2021-03-31 14:06:01.827','test',1,'test','1245','test',2,'2021-03-31 14:06:01.827',100,3,'test','test','2021-03-31 14:06:01.827','test','test',200,'2021-03-31 14:06:01.827','2021-03-31 14:06:01.827','aaa.txt','D',7,'Sundar',201,0,12  
 -- Re-activate EXEC usp_BridgeControlForm_InsertUpdate 0,'2021-03-31 14:06:01.827','test',1,'test','1245','test',2,'2021-03-31 14:06:01.827',100,3,'test','test','2021-03-31 14:06:01.827','test','test',200,'2021-03-31 14:06:01.827','2021-03-31 14:06:01.827','aaa.txt','R',7,'Sundar',601,9,12  

--SELECT * FROM [dbo].[tblBridgeControlForm] FOR SYSTEM_TIME ALL WHERE ID = 12

CREATE PROCEDURE [dbo].[usp_BridgeControlForm_InsertUpdate]    
 -- Add the parameters for the stored procedure here  
 @ID Int= 0,  
 @DateAuth datetime = NULL,   
 @SubmittedBy varchar(max) = NULL,  
 @CenterID int = NULL,  
 @Team varchar(max)= NULL,  
 @MatterNumber varchar(50) = NULL,  
 @ClientName varchar(50) = NULL,  
 @CategoryID int = NULL,  
 @MatterCompletionDate datetime = NULL,  
 @Amount money = NULL,  
 @RiskId int = NULL,  
 @Explanation varchar(max)= NULL,  
 @ActiontoRecoverFunds varchar(max) = NULL,  
 @ExpectedDateofReturn datetime = NULL,  
 @Authorisedby varchar(max) = NULL,  
 @Comments varchar(max) = NULL,  
 @Outstanding money = NULL,  
 @DateRepaid datetime = NULL,  
 @Dateofnextupdate datetime = NULL,  
 @AttachFile varchar(max) = NULL,   
    
 @StatementType  Varchar(1)=NULL,        
 @TrackerId int =NULL,          
 @CreatedBy Varchar(256)=NULL,        
 @processid int =NULL,        
 @RoutingID Int=NULL,  
 @selectedData varchar(100) = NULL    
       
AS            
  BEGIN        
  BEGIN TRY
-- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements  
 SET NOCOUNT ON;  
  
  
 Declare @trgProcessId int;     
    -- This performs INSERT action against BridgeControlForm table  
IF @StatementType = 'I'            
 BEGIN      
 INSERT INTO [dbo].[tblBridgeControlForm]  
           ([DateAuth]  
           ,[SubmittedBy]  
           ,[CenterID]  
           ,[Team]  
           ,[MatterNumber]  
           ,[ClientName]  
           ,[CategoryID]  
           ,[MatterCompletionDate]  
           ,[Amount]  
           ,[RiskId]  
           ,[Explanation]  
           ,[ActiontoRecoverFunds]  
           ,[ExpectedDateofReturn]  
           ,[Authorisedby]  
           ,[Comments]  
           ,[Outstanding]  
           ,[DateRepaid]  
           ,[Dateofnextupdate]  
           ,[AttachFile]  
           ,[Action]  
           ,[ModifyingUser]   
     )  
     VALUES  
   (    
   @DateAuth   
           ,@SubmittedBy   
           ,@CenterID   
           ,@Team   
           ,@MatterNumber   
           ,@ClientName   
           ,@CategoryID   
           ,@MatterCompletionDate   
           ,@Amount   
           ,@RiskId   
           ,@Explanation   
           ,@ActiontoRecoverFunds   
           ,@ExpectedDateofReturn   
           ,@Authorisedby   
           ,@Comments   
           ,@Outstanding   
           ,@DateRepaid   
           ,@Dateofnextupdate   
           ,@AttachFile    
           ,@StatementType  
           ,@CreatedBy   
 )    
Declare @FormDataId int;          
 set @FormDataId = SCOPE_IDENTITY()          
        
 SELECT @trgProcessId=trgProcessId FROM tblRoutingDetails  
  WHERE TrackerId= @TrackerId AND srcprocessid = @processid AND RoutingID = @RoutingID        
    
   -- This is common across all the trackers and perofrms INSERT action against tbltrackersform table   
    EXEC usp_TrackersForm_Insert @TrackerId, @CreatedBy, @trgProcessId,  @FormDataId  
     
 END     
  
  
  SELECT @trgProcessId=trgProcessId FROM tblRoutingDetails WHERE TrackerId= @TrackerId AND srcprocessid =@ProcessId            
 AND RoutingID = @RoutingID     
  
  
  
--This performs UPDATE action against BridgeControlForm table  
IF @StatementType = 'U'            
  BEGIN       
         
 UPDATE [dbo].[tblBridgeControlForm]  
   SET [DateAuth] = @DateAuth  
      ,[SubmittedBy] = @SubmittedBy  
      ,[CenterID] = @CenterID  
      ,[Team] =@Team  
      ,[MatterNumber] = @MatterNumber  
      ,[ClientName] = @ClientName  
      ,[CategoryID] = @CategoryID  
      ,[MatterCompletionDate] = @MatterCompletionDate  
      ,[Amount] = @Amount  
      ,[RiskId] = @RiskId  
      ,[Explanation] = @Explanation  
      ,[ActiontoRecoverFunds] = @ActiontoRecoverFunds  
      ,[ExpectedDateofReturn] = @ExpectedDateofReturn  
      ,[Authorisedby] = @Authorisedby  
      ,[Comments] = @Comments  
      ,[Outstanding] = @Outstanding  
      ,[DateRepaid] = @DateRepaid  
      ,[Dateofnextupdate] = @Dateofnextupdate  
      ,[AttachFile] = @AttachFile  
	  ,[ModifyingUser] = @CreatedBy    
      ,[Action] = @StatementType  
 WHERE ID=@ID  
        
   -- This is common across all the trackers and perofrms UPDATE action against tbltrackersform table   
     EXEC [dbo].[usp_TrackersForm_Update] @ID,@TrackerId, @ProcessId,@CreatedBy, @RoutingID, @selectedData,@StatementType  
      
       
  END     
    
--This performs DELETE/RE ACTIVATE action against BridgeControlForm table  
IF (@StatementType = 'D' OR  @StatementType = 'R')          
  BEGIN         
  
  
    UPDATE dbo.tblBridgeControlForm   
   SET    
       [ModifyingUser] = @CreatedBy,    
       [Action] = @StatementType    
 WHERE Id IN (SELECT CAST(value AS INTEGER) FROM STRING_SPLIT(@selectedData, ','))           
    
   -- This is common across all the trackers and UPDATING Process id in tbltrackersform table based on Routing details  
    EXEC [dbo].[usp_TrackersForm_Update] @ID,@TrackerId, @ProcessId,@CreatedBy, @RoutingID, @selectedData,@StatementType  
       
  END    
  
END TRY
BEGIN CATCH

        DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),
                @ErrorSeverity INT = ERROR_SEVERITY(),
                @ErrorNumber INT = ERROR_NUMBER();

		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);
 
END CATCH
  
   
END
GO
/****** Object:  StoredProcedure [dbo].[usp_BridgeControlReport]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[usp_BridgeControlReport]
@ReportType int,
@Fromdate date= null,
@Todate date= null
AS
BEGIN
if
@ReportType=1
begin
SELECT
DateAuth, SubmittedBy, CenterID AS Center, Team, MatterNumber, ClientName, CategoryID AS Category , MatterCompletionDate, 
Amount, RiskId AS Risk, Explanation, ActiontoRecoverFunds, ExpectedDateofReturn, Outstanding, DateRepaid, (Amount-Outstanding) as Paid, 'Item' as Itemtype, 'Not Applicable' as Path
FROM TBLBRIDGECONTROLFORM
WHERE 
Outstanding>0 and Dateofnextupdate<= getdate()
end
else
select DateAuth, SubmittedBy, CenterID AS Center, Team, MatterNumber, ClientName, CategoryID AS Category , MatterCompletionDate, 
Amount, RiskId AS Risk, Explanation, ActiontoRecoverFunds, ExpectedDateofReturn, Outstanding, DateRepaid, (Amount-Outstanding) as Paid, 
'Item' as Itemtype, 'Not Applicable' as Path
FROM TBLBRIDGECONTROLFORM  for system_time all where BeginDate >= cast(@Fromdate as date)
and enddate <= cast(@Todate as date)
end
GO
/****** Object:  StoredProcedure [dbo].[usp_CalCoachingLog_Download]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
   --<Modified date><16-04-2021,Sundar> <Included Exception handling>
-- [sp_GetAllCalCoachingLogDownload] 3,201     
CREATE Procedure [dbo].[usp_CalCoachingLog_Download]      
@TrackerId int,    
@processId int,
@ViewName Varchar(100)        
    
AS        
Begin   
BEGIN TRY  
        
 SELECT  Id = com.Id,  
       FeedbackdeliveredbyCET = case when com.FeedbackdeliveredbyCET = 1 then 'Yes' else 'No' end,   
  
    com.Month,  
    com.PL_LAName,  
    objRole.RoleName,  
    center.CenterName,  
    team.TeamName,  
    objTM.TM,  
    com.Numberofcalls,  
    com.ReasonforobservationOwnValue,  
    STOP_Jargon = case when com.STOP_Jargon = 1 then 'Yes' else 'No' end,   
    STOP_Beingdefensive_argumentative = case when com.STOP_Beingdefensive_argumentative = 1 then 'Yes' else 'No' end,   
    STOP_RefusingTransferredCalls = case when com.STOP_RefusingTransferredCalls = 1 then 'Yes' else 'No' end,   
    STOP_UsingInformalLanguage = case when com.STOP_UsingInformalLanguage = 1 then 'Yes' else 'No' end,   
    STOP_CriticisingEA_FS_OL = case when com.STOP_CriticisingEA_FS_OL = 1 then 'Yes' else 'No' end,   
    com.STOP_Freetext,  
    START_DPAOnAllCalls = case when com.START_DPAOnAllCalls = 1 then 'Yes' else 'No' end,   
    START_CheckUunderstanding = case when com.START_CheckUunderstanding = 1 then 'Yes' else 'No' end,   
    START_AskAnyFurtherQuestions = case when com.START_AskAnyFurtherQuestions = 1 then 'Yes' else 'No' end,   
    START_ExplainNextSteps = case when com.START_ExplainNextSteps = 1 then 'Yes' else 'No' end,   
    START_ExplainUnavoidableJargon = case when com.START_ExplainUnavoidableJargon = 1 then 'Yes' else 'No' end,   
    START_GiveDDI = case when com.START_GiveDDI = 1 then 'Yes' else 'No' end,   
    START_SoundConfident = case when com.START_SoundConfident = 1 then 'Yes' else 'No' end,   
    START_AdjustDeliveryToSuitCaller = case when com.START_AdjustDeliveryToSuitCaller = 1 then 'Yes' else 'No' end,   
    com.START_Freetext,  
    CONTINUE_DPAOnAllCalls = case when com.CONTINUE_DPAOnAllCalls = 1 then 'Yes' else 'No' end,   
    CONTINUE_CheckUnderstanding = case when com.CONTINUE_CheckUnderstanding = 1 then 'Yes' else 'No' end,   
    CONTINUE_AskAnyFurtherQuestions = case when com.CONTINUE_AskAnyFurtherQuestions = 1 then 'Yes' else 'No' end,   
    CONTINUE_ExplainNextSteps = case when com.CONTINUE_ExplainNextSteps = 1 then 'Yes' else 'No' end,   
    CONTINUE_ExplainUnavoidableJargon = case when com.CONTINUE_ExplainUnavoidableJargon = 1 then 'Yes' else 'No' end,   
    CONTINUE_GiveDDI = case when com.CONTINUE_GiveDDI = 1 then 'Yes' else 'No' end,   
    CONTINUE_SoundConfident = case when com.CONTINUE_SoundConfident = 1 then 'Yes' else 'No' end,   
    CONTINUE_AdjustDeliveryToSuitCaller = case when com.CONTINUE_AdjustDeliveryToSuitCaller = 1 then 'Yes' else 'No' end,   
    CONTINUE_Empathy = case when com.CONTINUE_Empathy = 1 then 'Yes' else 'No' end,   
    CONTINUE_BuildRapport = case when com.CONTINUE_BuildRapport = 1 then 'Yes' else 'No' end,   
    CONTINUE_UsePositiveLanguage = case when com.CONTINUE_UsePositiveLanguage = 1 then 'Yes' else 'No' end,   
    com.CONTINUE_Freetext,  
    com.HistoryNotes,  
    com.GeneralObservations,  
    com.FeedbackFromResTeam,  
    com.AttachFile  
  
  INTO #FilterResult  
  
from  [dbo].[tblCallCoachingLog]  com   
  
 left join [dbo].[tblCenterName] center on com.CentreID = center.Id  
 left join [dbo].[tblTeamName] team on com.TeamID = team.ID  
 left join [dbo].[tblCallCoachingRole] objRole on com.RoleID = objRole.ID  
 left join [dbo].[tblTM] objTM on com.TMID = objTM.Id  
  
 where com.ID in (select distinct FormDataId  
    FROM [dbo].[tblTrackerForms]    
    with (nolock) where TrackerId=@TrackerId and ProcessId=@processId  )   
 order by 1 asc  
  
   DECLARE @sql nvarchar(max);

SELECT  @sql = 'SELECT * FROM #FilterResult ' + dbo.FuncDynamicQueryResult(@TrackerId, @ViewName)

EXEC sp_executesql @sql
      
   
END TRY
BEGIN CATCH

        DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),
                @ErrorSeverity INT = ERROR_SEVERITY(),
                @ErrorNumber INT = ERROR_NUMBER();

		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);
 
END CATCH
DROP TABLE #FilterResult

End      
      
GO
/****** Object:  StoredProcedure [dbo].[usp_CallCoachingLog_InsertUpdate]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


  
-- =============================================  
-- Author:  <Author : Sivanathan>  
-- Create date: <12-04-2021>  
-- Modified date : <12-04-2021>,<13-04-2021,Sundar> <Included Exception handling>
-- Description: <Description,The purpose of the Stored Procedure is to make CRUD operations on CalCoachingLog tables>  
-- Generic Info: <I - Insert , U- Update, D- Delete R-Re activate>    
-- =============================================  
  
-- Insert [spClientToClientTransfersForm] 0,'1356','25654',1000,'test1',1,'I',9,'csuman',101,0,'31,32'  
-- Update [spClientToClientTransfersForm] 33,'5465','5645',311,'test65651',0,'U',9,'csuman',201,1,'31,32'  
-- Delete [spClientToClientTransfersForm] 0,'1356','25654',1000,'test1',1,'D',9,'csuman',201,0,'31,32,33'  
-- Reactivate  [spClientToClientTransfersForm] 0,'1356','25654',1000,'test1',1,'R',9,'csuman',601,9,'33'  
  
  
  
  
CREATE PROCEDURE [dbo].[usp_CallCoachingLog_InsertUpdate]   
 -- Add the parameters for the stored procedure here  
 @ID int =0,  
 @FeedbackdeliveredbyCET bit =NULL,    
 @Month varchar(50) =NULL,    
 @PL_LAName varchar(50) =NULL,    
 @RoleID int =NULL,    
 @CentreID int =NULL,    
 @TeamID int =NULL,    
 @TMID int =NULL,    
 @Numberofcalls varchar(50) =NULL,    
 @Reasonforobservation int =NULL,    
 @ReasonforobservationOwnValue Varchar(50) =NULL,    
 @STOP_Jargon bit =NULL,    
 @STOP_Beingdefensive_argumentative bit =NULL,    
 @STOP_RefusingTransferredCalls bit =NULL,    
 @STOP_UsingInformalLanguage bit =NULL,    
 @STOP_CriticisingEA_FS_OL bit =NULL,    
 @STOP_Freetext varchar(max) =NULL,    
 @START_DPAOnAllCalls bit =NULL,    
 @START_CheckUunderstanding bit =NULL,    
 @START_AskAnyFurtherQuestions bit =NULL,    
 @START_ExplainNextSteps bit =NULL,    
 @START_ExplainUnavoidableJargon bit =NULL,    
 @START_GiveDDI bit =NULL,    
 @START_SoundConfident bit =NULL,    
 @START_AdjustDeliveryToSuitCaller bit =NULL,    
 @START_Freetext varchar(max) =NULL,    
 @CONTINUE_DPAOnAllCalls bit =NULL,    
 @CONTINUE_CheckUnderstanding bit =NULL,    
 @CONTINUE_AskAnyFurtherQuestions bit =NULL,    
 @CONTINUE_ExplainNextSteps bit =NULL,    
 @CONTINUE_ExplainUnavoidableJargon bit =NULL,    
 @CONTINUE_GiveDDI bit =NULL,    
 @CONTINUE_SoundConfident bit =NULL,    
 @CONTINUE_AdjustDeliveryToSuitCaller bit =NULL,    
 @CONTINUE_Empathy bit =NULL,    
 @CONTINUE_BuildRapport bit =NULL,    
 @CONTINUE_UsePositiveLanguage bit =NULL,    
 @CONTINUE_Freetext varchar(max) =NULL,    
 @HistoryNotes varchar(max) =NULL,    
 @GeneralObservations varchar(max) =NULL,    
 @FeedbackFromResTeam varchar(max) =NULL,    
 @AttachFile varchar(max) =NULL,    
 @StatementType  Varchar(50)=NULL,        
 @TrackerId int =NULL,          
 @CreatedBy Varchar(256)=NULL,       
 @ProcessId INT=NULL,            
 @RoutingID Int=NULL,  
 @selectedData varchar(100) = NULL      
AS  
BEGIN  
BEGIN TRY
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements  
 SET NOCOUNT ON;  
  
  
 Declare @trgProcessId int;     
    -- This performs INSERT action against tblCallCoachingLog table  
IF @StatementType = 'I'            
 BEGIN         
         
            INSERT INTO [dbo].[tblCallCoachingLog](  
 ---ID int IDENTITY(1,1) NOT NULL,  
 FeedbackdeliveredbyCET ,  
 [Month] ,  
 PL_LAName ,  
 RoleID  ,  
 CentreID ,  
 TeamID  ,  
 TMID  ,  
 Numberofcalls ,  
 Reasonforobservation  ,  
 ReasonforobservationOwnValue,  
 STOP_Jargon  ,  
 STOP_Beingdefensive_argumentative  ,  
 STOP_RefusingTransferredCalls  ,  
 STOP_UsingInformalLanguage  ,  
 STOP_CriticisingEA_FS_OL  ,  
 STOP_Freetext ,  
 START_DPAOnAllCalls  ,  
 START_CheckUunderstanding  ,  
 START_AskAnyFurtherQuestions  ,  
 START_ExplainNextSteps  ,  
 START_ExplainUnavoidableJargon  ,  
 START_GiveDDI  ,  
 START_SoundConfident  ,  
 START_AdjustDeliveryToSuitCaller  ,  
 START_Freetext ,  
 CONTINUE_DPAOnAllCalls  ,  
 CONTINUE_CheckUnderstanding  ,  
 CONTINUE_AskAnyFurtherQuestions  ,  
 CONTINUE_ExplainNextSteps  ,  
 CONTINUE_ExplainUnavoidableJargon  ,  
 CONTINUE_GiveDDI  ,  
 CONTINUE_SoundConfident  ,  
 CONTINUE_AdjustDeliveryToSuitCaller  ,  
 CONTINUE_Empathy  ,  
 CONTINUE_BuildRapport  ,  
 CONTINUE_UsePositiveLanguage  ,  
 CONTINUE_Freetext ,  
 HistoryNotes ,  
 GeneralObservations ,  
 FeedbackFromResTeam ,  
 AttachFile,  
 [Action],  
 ModifyingUser  
  )  
 Values(  
  
 @FeedbackdeliveredbyCET ,  
 @Month ,  
 @PL_LAName ,  
 @RoleID  ,  
 @CentreID ,  
 @TeamID  ,  
 @TMID  ,  
 @Numberofcalls ,  
 @Reasonforobservation  ,  
 @ReasonforobservationOwnValue,  
 @STOP_Jargon  ,  
 @STOP_Beingdefensive_argumentative  ,  
 @STOP_RefusingTransferredCalls  ,  
 @STOP_UsingInformalLanguage  ,  
 @STOP_CriticisingEA_FS_OL  ,  
 @STOP_Freetext ,  
 @START_DPAOnAllCalls  ,  
 @START_CheckUunderstanding  ,  
 @START_AskAnyFurtherQuestions  ,  
 @START_ExplainNextSteps  ,  
 @START_ExplainUnavoidableJargon  ,  
 @START_GiveDDI  ,  
 @START_SoundConfident  ,  
 @START_AdjustDeliveryToSuitCaller  ,  
 @START_Freetext ,  
 @CONTINUE_DPAOnAllCalls  ,  
 @CONTINUE_CheckUnderstanding  ,  
 @CONTINUE_AskAnyFurtherQuestions  ,  
 @CONTINUE_ExplainNextSteps  ,  
 @CONTINUE_ExplainUnavoidableJargon  ,  
 @CONTINUE_GiveDDI  ,  
 @CONTINUE_SoundConfident  ,  
 @CONTINUE_AdjustDeliveryToSuitCaller  ,  
 @CONTINUE_Empathy  ,  
 @CONTINUE_BuildRapport  ,  
 @CONTINUE_UsePositiveLanguage  ,  
 @CONTINUE_Freetext ,  
 @HistoryNotes ,  
 @GeneralObservations ,  
 @FeedbackFromResTeam ,  
 @AttachFile,  
 @StatementType,  
 @CreatedBy)  
    
    Declare @FormDataId int;          
 set @FormDataId = SCOPE_IDENTITY()          
        
 SELECT @trgProcessId=trgProcessId FROM tblRoutingDetails  
  WHERE TrackerId= @TrackerId AND srcprocessid = @processid AND RoutingID = @RoutingID        
    
   -- This is common across all the trackers and perofrms INSERT action against tbltrackersform table   
    EXEC usp_TrackersForm_Insert @TrackerId, @CreatedBy, @trgProcessId,  @FormDataId    
     
 END     
  
  
--This performs UPDATE action against tblCallCoachingLog table  
IF @StatementType = 'U'            
  BEGIN         
   
UPDATE [dbo].[tblCallCoachingLog] SET  
 FeedbackdeliveredbyCET = @FeedbackdeliveredbyCET ,  
 [Month] = @Month,  
 PL_LAName = @PL_LAName,  
 RoleID = @RoleID ,  
 CentreID = @CentreID,  
 TeamID =  @TeamID,  
 TMID =  @TMID,  
 Numberofcalls = @Numberofcalls,  
 Reasonforobservation  = @Reasonforobservation ,  
 ReasonforobservationOwnValue = @ReasonforobservationOwnValue,  
 STOP_Jargon = @STOP_Jargon ,  
 STOP_Beingdefensive_argumentative = @STOP_Beingdefensive_argumentative ,  
 STOP_RefusingTransferredCalls   = @STOP_RefusingTransferredCalls,  
 STOP_UsingInformalLanguage  = @STOP_UsingInformalLanguage ,  
 STOP_CriticisingEA_FS_OL = @STOP_CriticisingEA_FS_OL ,  
 STOP_Freetext  = @STOP_Freetext,  
 START_DPAOnAllCalls = @START_DPAOnAllCalls ,  
 START_CheckUunderstanding = @START_CheckUunderstanding ,  
 START_AskAnyFurtherQuestions = @START_AskAnyFurtherQuestions ,  
 START_ExplainNextSteps  = @START_ExplainNextSteps ,  
 START_ExplainUnavoidableJargon = @START_ExplainUnavoidableJargon ,  
 START_GiveDDI = @START_GiveDDI ,  
 START_SoundConfident =  @START_SoundConfident ,  
 START_AdjustDeliveryToSuitCaller = @START_AdjustDeliveryToSuitCaller ,  
 START_Freetext = @START_Freetext ,  
 CONTINUE_DPAOnAllCalls  = @CONTINUE_DPAOnAllCalls ,  
 CONTINUE_CheckUnderstanding = @CONTINUE_CheckUnderstanding ,  
 CONTINUE_AskAnyFurtherQuestions  = @CONTINUE_AskAnyFurtherQuestions ,  
 CONTINUE_ExplainNextSteps = @CONTINUE_ExplainNextSteps ,  
 CONTINUE_ExplainUnavoidableJargon  = @CONTINUE_ExplainUnavoidableJargon ,  
 CONTINUE_GiveDDI  = @CONTINUE_GiveDDI ,  
 CONTINUE_SoundConfident = @CONTINUE_SoundConfident ,  
 CONTINUE_AdjustDeliveryToSuitCaller = @CONTINUE_AdjustDeliveryToSuitCaller ,  
 CONTINUE_Empathy  = @CONTINUE_Empathy ,  
 CONTINUE_BuildRapport = @CONTINUE_BuildRapport ,  
 CONTINUE_UsePositiveLanguage = @CONTINUE_UsePositiveLanguage ,  
 CONTINUE_Freetext = @CONTINUE_Freetext ,  
 HistoryNotes  = @HistoryNotes,  
 GeneralObservations  = @GeneralObservations,  
 FeedbackFromResTeam  = @FeedbackFromResTeam,  
 AttachFile = @AttachFile,  
 [Action]=@StatementType,  
 ModifyingUser=@CreatedBy  
 where Id = @Id   
                     
           
     select @trgProcessId=trgProcessId From tblRoutingDetails   
     where TrackerId= @TrackerId and srcprocessid =@ProcessId          
    And RoutingID = @RoutingID          
  
EXEC [dbo].[usp_TrackersForm_Update] @ID,@TrackerId, @ProcessId,@CreatedBy, @RoutingID, @selectedData,@StatementType         
          
       
  END     
    
--This performs DELETE/RE ACTIVATE action against tblCallCoachingLog table  
IF (@StatementType = 'D' OR  @StatementType = 'R')          
  BEGIN         
  
  
    UPDATE dbo.tblCallCoachingLog   
   SET    
       [ModifyingUser] = @CreatedBy,    
       [Action] = @StatementType    
 WHERE Id IN (SELECT CAST(value AS INTEGER) FROM STRING_SPLIT(@selectedData, ','))           
    
   -- This is common across all the trackers and UPDATING Process id in tbltrackersform table based on Routing details  
    EXEC [dbo].[usp_TrackersForm_Update]  @ID,@TrackerId, @ProcessId,@CreatedBy, @RoutingID, @selectedData,@StatementType  
       
  END    
  END TRY
BEGIN CATCH

        DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),
                @ErrorSeverity INT = ERROR_SEVERITY(),
                @ErrorNumber INT = ERROR_NUMBER();

		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);
 
END CATCH
     
END  
GO
/****** Object:  StoredProcedure [dbo].[usp_CallCoachingLogForm_AuditView]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



--Exec usp_CallCoachingLogForm_AuditView 7,null,null
CREATE PROCEDURE [dbo].[usp_CallCoachingLogForm_AuditView]
	-- Add the parameters for the stored procedure here
	@ID Int,  
	@FromDate datetime2 = '1900-01-01 00:00:00.0000000', 
	@ToDate datetime2	= '9999-12-31 23:59:59.9999999'
AS
BEGIN

  BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    
;WITH T
AS (
    SELECT		cal.ID
				,IsNull(FeedbackdeliveredbyCET,0) as FeedbackdeliveredbyCET ,IsNull(lag(FeedbackdeliveredbyCET) over(order by enddate asc),0) as Prev_FeedbackdeliveredbyCET
				,Month as Month , lag(Month) over(order by enddate asc) as Prev_Month
				,PL_LAName as PL_LAName , lag(PL_LAName) over(order by enddate asc) as Prev_PL_LAName
				,RoleName as RoleName , lag(RoleName) over(order by enddate asc) as Prev_RoleName
				,CenterName as Centre , lag(CenterName) over(order by enddate asc) as Prev_Centre
				,TeamName as Team , lag(TeamName) over(order by enddate asc) as Prev_Team
				,TM as TM , lag(TM) over(order by enddate asc) as Prev_TM
				,Numberofcalls as Numberofcalls , lag(Numberofcalls) over(order by enddate asc) as Prev_Numberofcalls
				,Reasonforobservation as Reasonforobservation , lag(Reasonforobservation) over(order by enddate asc) as Prev_Reasonforobservation
				,IsNull(STOP_Jargon,0) as STOP_Jargon , IsNull(lag(STOP_Jargon) over(order by enddate asc),0) as Prev_STOP_Jargon
				,IsNull(STOP_Beingdefensive_argumentative,0) as STOP_Beingdefensive_argumentative , IsNull(lag(STOP_Beingdefensive_argumentative) over(order by enddate asc),0) as Prev_STOP_Beingdefensive_argumentative
				,IsNull(STOP_RefusingTransferredCalls,0) as STOP_RefusingTransferredCalls , IsNull(lag(STOP_RefusingTransferredCalls) over(order by enddate asc),0) as Prev_STOP_RefusingTransferredCalls
				,IsNull(STOP_UsingInformalLanguage,0) as STOP_UsingInformalLanguage , IsNull(lag(STOP_UsingInformalLanguage) over(order by enddate asc),0) as Prev_STOP_UsingInformalLanguage
				,IsNull(STOP_CriticisingEA_FS_OL,0) as STOP_CriticisingEA_FS_OL , IsNull(lag(STOP_CriticisingEA_FS_OL) over(order by enddate asc),0) as Prev_STOP_CriticisingEA_FS_OL
				,STOP_Freetext as STOP_Freetext , lag(STOP_Freetext) over(order by enddate asc) as Prev_STOP_Freetext
				,IsNull(START_DPAOnAllCalls,0) as START_DPAOnAllCalls , IsNull(lag(START_DPAOnAllCalls) over(order by enddate asc),0) as Prev_START_DPAOnAllCalls
				,IsNull(START_CheckUunderstanding,0) as START_CheckUunderstanding , IsNull(lag(START_CheckUunderstanding) over(order by enddate asc),0) as Prev_START_CheckUunderstanding
				,IsNull(START_AskAnyFurtherQuestions,0) as START_AskAnyFurtherQuestions , IsNull(lag(START_AskAnyFurtherQuestions) over(order by enddate asc),0) as Prev_START_AskAnyFurtherQuestions
				,IsNull(START_ExplainNextSteps,0) as START_ExplainNextSteps , IsNull(lag(START_ExplainNextSteps) over(order by enddate asc),0) as Prev_START_ExplainNextSteps
				,IsNull(START_ExplainUnavoidableJargon,0) as START_ExplainUnavoidableJargon , IsNull(lag(START_ExplainUnavoidableJargon) over(order by enddate asc),0) as Prev_START_ExplainUnavoidableJargon
				,IsNull(START_GiveDDI,0) as START_GiveDDI , IsNull(lag(START_GiveDDI) over(order by enddate asc),0) as Prev_START_GiveDDI
				,IsNull(START_SoundConfident,0) as START_SoundConfident , IsNull(lag(START_SoundConfident) over(order by enddate asc),0) as Prev_START_SoundConfident
				,IsNull(START_AdjustDeliveryToSuitCaller,0) as START_AdjustDeliveryToSuitCaller , IsNull(lag(START_AdjustDeliveryToSuitCaller) over(order by enddate asc),0) as Prev_START_AdjustDeliveryToSuitCaller
				,START_Freetext as START_Freetext , lag(START_Freetext) over(order by enddate asc) as Prev_START_Freetext
				,IsNull(CONTINUE_DPAOnAllCalls,0) as CONTINUE_DPAOnAllCalls , IsNull(lag(CONTINUE_DPAOnAllCalls) over(order by enddate asc),0) as Prev_CONTINUE_DPAOnAllCalls
				,IsNull(CONTINUE_CheckUnderstanding,0) as CONTINUE_CheckUnderstanding , IsNull(lag(CONTINUE_CheckUnderstanding) over(order by enddate asc),0) as Prev_CONTINUE_CheckUnderstanding
				,IsNull(CONTINUE_AskAnyFurtherQuestions,0) as CONTINUE_AskAnyFurtherQuestions , IsNull(lag(CONTINUE_AskAnyFurtherQuestions) over(order by enddate asc),0) as Prev_CONTINUE_AskAnyFurtherQuestions
				,IsNull(CONTINUE_ExplainNextSteps,0) as CONTINUE_ExplainNextSteps , IsNull(lag(CONTINUE_ExplainNextSteps) over(order by enddate asc),0) as Prev_CONTINUE_ExplainNextSteps
				,IsNull(CONTINUE_ExplainUnavoidableJargon,0) as CONTINUE_ExplainUnavoidableJargon , IsNull(lag(CONTINUE_ExplainUnavoidableJargon) over(order by enddate asc),0) as Prev_CONTINUE_ExplainUnavoidableJargon
			    ,IsNull(CONTINUE_GiveDDI,0) as CONTINUE_GiveDDI , IsNull(lag(CONTINUE_GiveDDI) over(order by enddate asc),0) as Prev_CONTINUE_GiveDDI
				,IsNull(CONTINUE_SoundConfident,0) as CONTINUE_SoundConfident , IsNull(lag(CONTINUE_SoundConfident) over(order by enddate asc),0) as Prev_CONTINUE_SoundConfident
				,IsNull(CONTINUE_AdjustDeliveryToSuitCaller,0) as CONTINUE_AdjustDeliveryToSuitCaller , IsNull(lag(CONTINUE_AdjustDeliveryToSuitCaller) over(order by enddate asc),0) as Prev_CONTINUE_AdjustDeliveryToSuitCaller
	            ,IsNull(CONTINUE_Empathy,0) as CONTINUE_Empathy , IsNull(lag(CONTINUE_Empathy) over(order by enddate asc),0) as Prev_CONTINUE_Empathy
				,IsNull(CONTINUE_BuildRapport,0) as CONTINUE_BuildRapport , IsNull(lag(CONTINUE_BuildRapport) over(order by enddate asc),0) as Prev_CONTINUE_BuildRapport
				,IsNull(CONTINUE_UsePositiveLanguage,0) as CONTINUE_UsePositiveLanguage , IsNull(lag(CONTINUE_UsePositiveLanguage) over(order by enddate asc),0) as Prev_CONTINUE_UsePositiveLanguage
				,CONTINUE_Freetext as CONTINUE_Freetext , lag(CONTINUE_Freetext) over(order by enddate asc) as Prev_CONTINUE_Freetext
				,Historynotes as Historynotes , lag(Historynotes) over(order by enddate asc) as Prev_Historynotes
				,Generalobservations as Generalobservations , lag(Generalobservations) over(order by enddate asc) as Prev_Generalobservations
				,FeedbackfromResTeam as FeedbackfromResTeam , lag(FeedbackfromResTeam) over(order by enddate asc) as Prev_FeedbackfromResTeam
				,AttachFile as AttachFile , lag(AttachFile) over(order by enddate asc) as Prev_AttachFile
				,ModifyingUser as ModifyingUser , lag(ModifyingUser) over(order by enddate asc) as Prev_ModifyingUser
				,Action as Action , lag(Action) over(order by enddate asc) as Prev_Action
				,BeginDate
				,EndDate
				,ROW_Number() over(partition by  cal.Id Order by EndDate asc) as VersionNumber
    FROM dbo.tblCallCoachingLog FOR SYSTEM_TIME ALL AS cal   
  
 left join [dbo].[tblCenterName] center on cal.CentreID = center.Id  
 left join [dbo].[tblTeamName] team on cal.TeamID = team.ID  
 left join [dbo].[tblCallCoachingRole] objRole on cal.RoleID = objRole.ID  
 left join [dbo].[tblTM] objTM on cal.TMID = objTM.Id 
    WHERE cal.id =@ID
)
SELECT T.BeginDate,
       T.EndDate,
       C.ColName as ColumnName,
	   t.VersionNumber,	   
       C.present_Value as CurrentValue,
       C.prev_value as PreviousValue,
	   T.ModifyingUser as ModifiedBy
FROM T
    CROSS APPLY
    (
        VALUES
			('Feedback delivered by CET', Convert(varchar, case when T.FeedbackdeliveredbyCET=1 then 'Yes' else 'No' end) , Convert(varchar,case when T.Prev_FeedbackdeliveredbyCET=1 then 'Yes' else 'No' end))
			,('Month', Convert(varchar, T.Month) , Convert(varchar,T.Prev_Month))
			,('PL/LA Name', Convert(varchar, T.PL_LAName) , Convert(varchar,T.Prev_PL_LAName))
			,('Role', Convert(varchar, T.RoleName) , Convert(varchar,T.Prev_RoleName))
			,('Center', Convert(varchar, T.Centre) , Convert(varchar,T.Prev_Centre))
			,('Team', Convert(varchar, T.Team) , Convert(varchar,T.Prev_Team))
	        ,('TM', Convert(varchar, T.TM) , Convert(varchar,T.Prev_TM))
			,('Number of calls', Convert(varchar, T.Numberofcalls) , Convert(varchar,T.Prev_Numberofcalls))
			,('Reason for observation', Convert(varchar, T.Reasonforobservation) , Convert(varchar(10),T.Prev_Reasonforobservation))
			,('STOP - Jargon.', Convert(varchar,case when T.STOP_Jargon=1 then 'Yes' else 'No' end) , Convert(varchar,case when T.Prev_STOP_Jargon=1 then 'Yes' else 'No' end))
			,('STOP - Being defensive / argumentative.', Convert(varchar,case when T.STOP_Beingdefensive_argumentative=1 then 'Yes' else 'No' end) , Convert(varchar,case when T.Prev_STOP_Beingdefensive_argumentative=1 then 'Yes' else 'No' end))
			,('STOP - Refusing transferred calls.', Convert(varchar,case when T.STOP_RefusingTransferredCalls=1 then 'Yes' else 'No' end) , Convert(varchar,case when T.Prev_STOP_RefusingTransferredCalls=1 then 'Yes' else 'No' end))
			,('STOP - Using informal language.', Convert(varchar,case when T.STOP_UsingInformalLanguage=1 then 'Yes' else 'No' end ) , Convert(varchar,case when T.Prev_STOP_UsingInformalLanguage=1 then 'Yes' else 'No' end))
			,('STOP - Criticising EA / FS / OL.', Convert(varchar,case when T.STOP_CriticisingEA_FS_OL=1 then 'Yes' else 'No' end) , Convert(varchar,case when T.Prev_STOP_CriticisingEA_FS_OL=1 then 'Yes' else 'No' end))
			,('STOP - Freetext', Convert(varchar, T.STOP_Freetext) , Convert(varchar,T.Prev_STOP_Freetext))
			,('START - DPA on all calls.', Convert(varchar,case when T.START_DPAOnAllCalls=1 then 'Yes' else 'No' end) , Convert(varchar,case when T.Prev_START_DPAOnAllCalls=1 then 'Yes' else 'No' end))
			,('START - Check understanding.', Convert(varchar,case when T.START_CheckUunderstanding=1 then 'Yes' else 'No' end) , Convert(varchar,case when T.Prev_START_CheckUunderstanding=1 then 'Yes' else 'No' end))
			,('START - Ask any further questions?', Convert(varchar,case when T.START_AskAnyFurtherQuestions=1 then 'Yes' else 'No' end) , Convert(varchar,case when T.Prev_START_AskAnyFurtherQuestions=1 then 'Yes' else 'No' end))
			,('START - Explain next steps.', Convert(varchar,case when T.START_ExplainNextSteps=1 then 'Yes' else 'No' end) , Convert(varchar,case when T.Prev_START_ExplainNextSteps=1 then 'Yes' else 'No' end))
			,('START - Explain unavoidable jargon.', Convert(varchar,case when T.START_ExplainUnavoidableJargon=1 then 'Yes' else 'No' end) , Convert(varchar,case when T.Prev_START_ExplainUnavoidableJargon=1 then 'Yes' else 'No' end))
			,('START - Give DDI.', Convert(varchar,case when T.START_GiveDDI=1 then 'Yes' else 'No' end) , Convert(varchar,case when T.Prev_START_GiveDDI=1 then 'Yes' else 'No' end))
			,('START - Sound confident.', Convert(varchar,case when T.START_SoundConfident=1 then 'Yes' else 'No' end) , Convert(varchar,case when T.Prev_START_SoundConfident=1 then 'Yes' else 'No' end) )
			,('START - Adjust delivery to suit caller.', Convert(varchar,case when T.START_AdjustDeliveryToSuitCaller=1 then 'Yes' else 'No' end) , Convert(varchar,case when T.Prev_START_AdjustDeliveryToSuitCaller=1 then 'Yes' else 'No' end) )
			,('START - Freetext', Convert(varchar, T.START_Freetext) , Convert(varchar,T.Prev_START_Freetext))
			,('CONTINUE - DPA on all calls.', Convert(varchar,case when  T.CONTINUE_DPAOnAllCalls =1 then 'Yes' else 'No' end), Convert(varchar,case when T.Prev_CONTINUE_DPAOnAllCalls =1 then 'Yes' else 'No' end))
			,('CONTINUE - Check understanding.', Convert(varchar,case when  T.CONTINUE_CheckUnderstanding =1 then 'Yes' else 'No' end), Convert(varchar,case when T.Prev_CONTINUE_CheckUnderstanding =1 then 'Yes' else 'No' end))
			,('CONTINUE - Ask any further questions?', Convert(varchar,case when  T.CONTINUE_AskAnyFurtherQuestions =1 then 'Yes' else 'No' end), Convert(varchar,case when T.Prev_CONTINUE_AskAnyFurtherQuestions =1 then 'Yes' else 'No' end))
			,('CONTINUE - Explain next steps.', Convert(varchar,case when  T.CONTINUE_ExplainNextSteps =1 then 'Yes' else 'No' end), Convert(varchar,case when T.Prev_CONTINUE_ExplainNextSteps =1 then 'Yes' else 'No' end))
			,('CONTINUE - Explain unavoidable jargon.', Convert(varchar,case when  T.CONTINUE_ExplainUnavoidableJargon =1 then 'Yes' else 'No' end), Convert(varchar,case when T.Prev_CONTINUE_ExplainUnavoidableJargon =1 then 'Yes' else 'No' end))
			,('CONTINUE - Give DDI.', Convert(varchar,case when  T.CONTINUE_GiveDDI =1 then 'Yes' else 'No' end), Convert(varchar,case when T.Prev_CONTINUE_GiveDDI =1 then 'Yes' else 'No' end))
			,('CONTINUE - Sound confident.', Convert(varchar,case when  T.CONTINUE_SoundConfident =1 then 'Yes' else 'No' end), Convert(varchar,case when T.Prev_CONTINUE_SoundConfident =1 then 'Yes' else 'No' end))
			,('CONTINUE - Adjust delivery to suit caller.', Convert(varchar,case when  T.CONTINUE_AdjustDeliveryToSuitCaller =1 then 'Yes' else 'No' end), Convert(varchar,case when T.Prev_CONTINUE_AdjustDeliveryToSuitCaller =1 then 'Yes' else 'No' end))
			,('CONTINUE - Empathy.', Convert(varchar,case when  T.CONTINUE_Empathy =1 then 'Yes' else 'No' end), Convert(varchar,case when T.Prev_CONTINUE_Empathy =1 then 'Yes' else 'No' end))
			,('CONTINUE - Build rapport.', Convert(varchar,case when  T.CONTINUE_BuildRapport =1 then 'Yes' else 'No' end), Convert(varchar,case when T.Prev_CONTINUE_BuildRapport =1 then 'Yes' else 'No' end))
			,('CONTINUE - Use positive language.', Convert(varchar,case when  T.CONTINUE_UsePositiveLanguage =1 then 'Yes' else 'No' end), Convert(varchar,case when T.Prev_CONTINUE_UsePositiveLanguage =1 then 'Yes' else 'No' end))
			,('CONTINUE - Freetext', Convert(varchar, T.CONTINUE_Freetext) , Convert(varchar,T.Prev_CONTINUE_Freetext))
			,('History notes?', Convert(varchar, T.Historynotes) , Convert(varchar,T.Prev_Historynotes))
			,('General observations', Convert(varchar, T.Generalobservations) , Convert(varchar,T.Prev_Generalobservations))
			,('Feedback from Res Team', Convert(varchar, T.FeedbackfromResTeam) , Convert(varchar,T.Prev_FeedbackfromResTeam))
			,('Attach File', Convert(varchar, T.AttachFile) , Convert(varchar,T.Prev_AttachFile))
			,('ModifyingUser', Convert(varchar, T.ModifyingUser) , Convert(varchar,T.Prev_ModifyingUser))
			,('Action', Convert(varchar, T.Action) , Convert(varchar,T.Prev_Action))
			) AS C (ColName, present_Value, prev_value)
WHERE  EXISTS
(
    SELECT present_Value 
    EXCEPT 
    SELECT C.prev_value
)  and Convert(datetime,BeginDate) >= @FromDate and Convert(Datetime,BeginDate) <=@ToDate

END TRY
BEGIN CATCH

        DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),
                @ErrorSeverity INT = ERROR_SEVERITY(),
                @ErrorNumber INT = ERROR_NUMBER();

		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);
 
END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[usp_CardPaymentRequest_Download]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 -- Modified date: <16-04-2021> <Included Exception handling>
    
 --[dbo].[sp_GetAllCardPaymentRequestDownload] 25,201    
CREATE Procedure [dbo].[usp_CardPaymentRequest_Download]              
@TrackerId int,            
@processId int,
@ViewName Varchar(100)                
            
AS                
Begin           
 BEGIN TRY          
                
select	ID,	MatterID,	ClientName,	ContactNumber,	DateTimeToCall,	Amount,	PaymentReason,	PaymentTaken,	UnableToContact, CANotes, RequesterName, AttachedFile,
case when ClientIsExpectingCall =1 then 'Yes' else 'No' end ClientIsExpectingCall
	INTO #FilterResult
from [dbo].[tblCardPaymentRequestForm]         
          
 where ID in (select distinct FormDataId          
    FROM [dbo].[tblTrackerForms]            
    with (nolock) where TrackerId=@TrackerId and ProcessId=@processId  )           
 order by 1 asc          
          
DECLARE @sql nvarchar(max);

SELECT  @sql = 'SELECT * FROM #FilterResult ' + dbo.FuncDynamicQueryResult(@TrackerId, @ViewName)

EXEC sp_executesql @sql
           
END TRY
BEGIN CATCH

        DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),
                @ErrorSeverity INT = ERROR_SEVERITY(),
                @ErrorNumber INT = ERROR_NUMBER();

		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);
 
END CATCH       
DROP TABLE #FilterResult

End 
GO
/****** Object:  StoredProcedure [dbo].[usp_CardPaymentRequestForm_InsertUpdate]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author : kumari>
-- Create date: <10-04-2021>
-- Modified date : <11-04-2021><13-04-2021,Sundar> <Included Exception handling>
-- Description:	<Description,The purpose of the Stored Procedure is to make CRUD operations on CardPaymentRequestForm tables>
-- Generic Info: <I - Insert , U- Update, D- Delete R-Re activate>  
-- =============================================

--  [usp_CardPaymentRequestForm_InsertUpdate] 0,'25654','test','8143966110','2021/04/12',10000,'test',0,0,'test','kumari','testAttachmenttest','I',25,'kumaric',101,0,null
-- [usp_CardPaymentRequestForm_InsertUpdate] 5,'cvfdv','frdf','eff','2021/04/12',10000,'fref',0,0,'test','suman','testAttachmenttest1','U',25,'kumaric',201,1,null
-- [usp_CardPaymentRequestForm_InsertUpdate] 0,'25654','test','8143966110',null,10000,'test',0,0,'test','kumari','testAttachment1','D',25,'kumaric',201,0,'10'
-- [usp_CardPaymentRequestForm_InsertUpdate] 0,'25654','test','8143966110','2021/04/12',10000,'test',0,0,'test','kumari','testAttachment1','R',25,'kumaric',601,9,'4'




CREATE PROCEDURE [dbo].[usp_CardPaymentRequestForm_InsertUpdate] 
	-- Add the parameters for the stored procedure here
@ID int = 0,  
@MatterID varchar(50)=null ,
@ClientName varchar(50)=null ,
@ContactNumber varchar(50)=null ,
@DateTimeToCall Datetime=null,
@Amount money =null,
@PaymentReason varchar(max)=null ,
@PaymentTaken bit =null,
@UnableToContact bit =null,
@CANotes varchar(max)=null ,
@RequesterName varchar(50) =null,
@AttachedFile varchar(max)=null ,

  
 @StatementType  Varchar(50)=NULL,      
 @TrackerId int =NULL,        
 @CreatedBy NVarchar(256)=NULL,      
 @processid int =NULL,      
 @RoutingID Int=NULL,
 @selectedData varchar(100) = NULL,
 @ClientIsExpectingCall  bit=null 
AS
BEGIN
BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements
	SET NOCOUNT ON;


	Declare @trgProcessId int;   
    -- This performs INSERT action against ClientToClientTransfersForm table
IF @StatementType = 'I'          
 BEGIN   
 
   INSERT INTO tblCardPaymentRequestForm
( 
	--@ID int IDENTITY(1,1) ,
	MatterID ,
	ClientName  ,
	ContactNumber  ,
	DateTimeToCall ,
	Amount  ,
	PaymentReason ,
	PaymentTaken  ,
	UnableToContact  ,
	CANotes  ,
	RequesterName  ,
	AttachedFile,
    ModifyingUser ,
	[Action],
	ClientIsExpectingCall)
Values

( 
	--@ID int IDENTITY(1,1) ,
	@MatterID ,
	@ClientName  ,
	@ContactNumber  ,
	@DateTimeToCall ,
	@Amount  ,
	@PaymentReason ,
	@PaymentTaken  ,
	@UnableToContact  ,
	@CANotes  ,
	@RequesterName  ,
	@AttachedFile,
	@CreatedBy,
	@StatementType,
	@ClientIsExpectingCall)
  
    Declare @FormDataId int;        
	set @FormDataId = SCOPE_IDENTITY()        
      
	SELECT @trgProcessId=trgProcessId FROM tblRoutingDetails
		WHERE TrackerId= @TrackerId AND srcprocessid = @processid AND RoutingID = @RoutingID      
  
   -- This is common across all the trackers and perofrms INSERT action against tbltrackersform table 
    EXEC usp_TrackersForm_Insert @TrackerId, @CreatedBy, @trgProcessId,  @FormDataId
   
 END   


  SELECT @trgProcessId=trgProcessId FROM tblRoutingDetails WHERE TrackerId= @TrackerId AND srcprocessid =@ProcessId          
 AND RoutingID = @RoutingID   



--This performs UPDATE action against ClientToClientTransfersForm table
IF @StatementType = 'U'          
  BEGIN       
	
   UPDATE  tblCardPaymentRequestForm SET
 
	--@ID int IDENTITY(1,1) ,
	MatterID = @MatterID,
	ClientName = @ClientName ,
	ContactNumber = @ContactNumber ,
	DateTimeToCall = @DateTimeToCall ,
	Amount  = @Amount ,
	PaymentReason  = @PaymentReason,
	PaymentTaken  = @PaymentTaken ,
	UnableToContact = @UnableToContact  ,
	CANotes = @CANotes ,
	RequesterName  = @RequesterName ,
	AttachedFile  = @AttachedFile,
	ModifyingUser = @CreatedBy,  
	[Action] = @StatementType,
	@ClientIsExpectingCall=ClientIsExpectingCall
where Id = @Id
                
  
	EXEC [dbo].[usp_TrackersForm_Update] @ID,@TrackerId, @ProcessId,@CreatedBy, @RoutingID, @selectedData,@StatementType   
        
     
  END   
  
--This performs DELETE/RE ACTIVATE action against ClientToClientTransfersForm table
IF (@StatementType = 'D' OR  @StatementType = 'R')        
  BEGIN       


    UPDATE dbo.tblCardPaymentRequestForm 
			SET  
				   [ModifyingUser] = @CreatedBy,  
				   [Action] = @StatementType  
	WHERE Id IN (SELECT CAST(value AS INTEGER) FROM STRING_SPLIT(@selectedData, ','))         
  
   -- This is common across all the trackers and UPDATING Process id in tbltrackersform table based on Routing details
   	EXEC [dbo].[usp_TrackersForm_Update] @ID,@TrackerId, @ProcessId,@CreatedBy, @RoutingID, @selectedData,@StatementType
     
  END  

  END TRY
BEGIN CATCH

        DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),
                @ErrorSeverity INT = ERROR_SEVERITY(),
                @ErrorNumber INT = ERROR_NUMBER();

		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);
 
END CATCH
	
END
GO
/****** Object:  StoredProcedure [dbo].[usp_CardPaymentRequestsForm_AuditView]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


--Exec usp_CardPaymentRequestsForm_AuditView 2,null,null
CREATE PROCEDURE [dbo].[usp_CardPaymentRequestsForm_AuditView]
	-- Add the parameters for the stored procedure here
	@ID Int,  
	@FromDate datetime2 = '1900-01-01 00:00:00.0000000', 
	@ToDate datetime2	= '9999-12-31 23:59:59.9999999'
AS
BEGIN

  BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    
;WITH T
AS (
    SELECT		ID
				,MatterID as MatterID , lag(MatterID) over(order by enddate asc) as Prev_MatterID
				,ClientName as ClientName , lag(ClientName) over(order by enddate asc) as Prev_ClientName
				,ContactNumber as ContactNumber , lag(ContactNumber) over(order by enddate asc) as Prev_ContactNumber
				,DateTimeToCall as DateTimeToCall , lag(DateTimeToCall) over(order by enddate asc) as Prev_DateTimeToCall
				,Amount as Amount , lag(Amount) over(order by enddate asc) as Prev_Amount
				,PaymentReason as PaymentReason , lag(PaymentReason) over(order by enddate asc) as Prev_PaymentReason
				,PaymentTaken as PaymentTaken , lag(PaymentTaken) over(order by enddate asc) as Prev_PaymentTaken
				,ISNULL(UnableToContact,0) as UnableToContact , ISNULL(lag(UnableToContact) over(order by enddate asc),0) as Prev_UnableToContact
				,CANotes as CANotes , lag(CANotes) over(order by enddate asc) as Prev_CANotes
				,RequesterName as RequesterName , lag(RequesterName) over(order by enddate asc) as Prev_RequesterName
				,AttachedFile as AttachedFile , lag(AttachedFile) over(order by enddate asc) as Prev_AttachedFile
				,ModifyingUser as ModifyingUser , lag(ModifyingUser) over(order by enddate asc) as Prev_ModifyingUser
				,Action as Action , lag(Action) over(order by enddate asc) as Prev_Action
				,BeginDate
				,EndDate
				,ROW_Number() over(partition by  Id Order by EndDate asc) as VersionNumber
    FROM dbo.tblCardPaymentRequestForm FOR SYSTEM_TIME ALL
    WHERE id =@ID
)
SELECT T.BeginDate,
       T.EndDate,
       C.ColName as ColumnName,
	   t.VersionNumber,	   
       C.present_Value as CurrentValue,
       C.prev_value as PreviousValue,
	   T.ModifyingUser as ModifiedBy
FROM T
    CROSS APPLY
    (
        VALUES
			('Matter number', Convert(varchar,T.MatterID) , Convert(varchar,T.Prev_MatterID))
			,('Client Name(Full Name)', Convert(varchar, T.ClientName) , Convert(varchar,T.Prev_ClientName))
			,('Contact Numbers', Convert(varchar, T.ContactNumber) , Convert(varchar,T.Prev_ContactNumber))
			,('Date & Time To Call', Convert(varchar(10), T.DateTimeToCall,103)+' '+ SUBSTRING( convert(varchar, T.DateTimeToCall,108),1,5) , Convert(varchar(10),T.Prev_DateTimeToCall,103)+' '+ SUBSTRING( convert(varchar, T.Prev_DateTimeToCall,108),1,5))
			,('Amount', Convert(varchar, T.Amount) , Convert(varchar,T.Prev_Amount))
			,('Reason for Payment', Convert(varchar, T.PaymentReason) , Convert(varchar,T.Prev_PaymentReason))
			,('Payment Taken', Convert(varchar, T.PaymentTaken) , Convert(varchar,T.Prev_PaymentTaken))
			,('(CA only) Unable to contact', Convert(varchar, case when T.UnableToContact =1 then 'Yes' else 'No' end) , Convert(varchar,case when T.Prev_UnableToContact =1 then 'Yes' else 'No' end))
			,('CA (Only) Notes', Convert(varchar, T.CANotes) , Convert(varchar,T.Prev_CANotes))
			,('Requestor Name', Convert(varchar, T.RequesterName) , Convert(varchar,T.Prev_RequesterName))
			,('Attach a file:', Convert(varchar, T.AttachedFile) , Convert(varchar,T.Prev_AttachedFile))
			,('ModifyingUser', Convert(varchar, T.ModifyingUser) , Convert(varchar,T.Prev_ModifyingUser))
			,('Action', Convert(varchar, T.Action) , Convert(varchar,T.Prev_Action))
			) AS C (ColName, present_Value, prev_value)
WHERE  EXISTS
(
    SELECT present_Value 
    EXCEPT 
    SELECT C.prev_value
)  and Convert(datetime,BeginDate) >= @FromDate and Convert(Datetime,BeginDate) <=@ToDate

END TRY
BEGIN CATCH

        DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),
                @ErrorSeverity INT = ERROR_SEVERITY(),
                @ErrorNumber INT = ERROR_NUMBER();

		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);
 
END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[usp_CCS_GenericTableDetails]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
    
      
-- [usp_CCS_GenericTableDetails] 6,'View1'     
CREATE Procedure [dbo].[usp_CCS_GenericTableDetails]      
@TrackerId int,
@ViewName Varchar(100)
AS        
Begin        
        

select DVC.Id,columnaliasname,tablename,ColumnName,dataType,
tblQueryViewDetails.TrackerRefID,
tblQueryViewDetails.ViewName,
	ColumnRefID=DVC.Id,
	OperatorRefID,
	Condition,
	SearchName,
	tblQueryViewDetails.IsOrdered
from [dbo].[tblDynamicViewConfiguration] DVC  
left join tbltracker Tracker on DVC.Trackerrefid = Tracker.Trackerid  
left join
(
select 
	TrackerRefID,
	ViewName,
	ColumnRefID,
	OperatorRefID,
	Condition,
	SearchName,
	IsOrdered
from [tblMultiViewQueryBulderDetails] where TrackerRefID=@TrackerId and ViewName=@ViewName
)as tblQueryViewDetails
on DVC.Id=tblQueryViewDetails.ColumnRefID  
where DVC.trackerrefid=@TrackerId and DVC.viewname='configure'   
order by tblQueryViewDetails.TrackerRefID, tblQueryViewDetails.IsOrdered  desc
  
        
End      
      
GO
/****** Object:  StoredProcedure [dbo].[usp_CCS_WorkFlow]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
    
-- [sp_CCS_WorkFlow] 'arunreddyp'   
CREATE Procedure [dbo].[usp_CCS_WorkFlow]    
@UserName Varchar(100)        
AS      
Begin      
      
Select TblUser.domainid,tblTrackerProcessRole.Trackerid,tblTrackerProcessRole.Processid,tblTrackerProcessRole.RoleId,      
tbltracker.TrackerName,tblProcess.Processname,tbltracker.[TrackerTeam],tblTrackerProcessRole.ActionName,  
tblTrackerProcessRole.ControllerName   
from TblUser      
Join      
tblTrackerProcessRole on tbluser.Roleid = tblTrackerProcessRole.Roleid      
Join      
tbltracker on tbltracker.trackerid=tblTrackerProcessRole.trackerid      
Join       
tblProcess on tblProcess .processid= tblTrackerProcessRole.Processid      
where tbluser.DomainId = @UserName  and tblTracker.Isactive = 1    
order by TrackerId,[TrackerTeam]
      
End    
    
GO
/****** Object:  StoredProcedure [dbo].[usp_ClientToClientTransfers_Download]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

 -- Modified date: <16-04-2021> <Included Exception handling> 
      
 --[usp_ClientToClientTransfers_Download] 9,201,'view1'  
CREATE Procedure [dbo].[usp_ClientToClientTransfers_Download]            
@TrackerId int,          
@processId int,
@ViewName Varchar(100)              
          
AS              
Begin         
 BEGIN TRY        
              
  select CTCT.ID, CTCT.[MatterNumberFrom], CTCT.[MatterNumberTo], CTCT.[AmountTransfered], CTCT.[Reason], CTCT.[Transferred]     
      
      INTO #FilterResult
from [dbo].[tblClientToClientTransfersForm] CTCT       
    
        
        
 where CTCT.ID in (select distinct FormDataId        
    FROM [dbo].[tblTrackerForms]          
    with (nolock) where TrackerId=@TrackerId and ProcessId=@processId  )         
 order by 1 asc     
 
 
 DECLARE @sql nvarchar(max);

SELECT  @sql = 'SELECT * FROM #FilterResult ' + dbo.FuncDynamicQueryResult(@TrackerId, @ViewName)

EXEC sp_executesql @sql

        
END TRY
BEGIN CATCH

        DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),
                @ErrorSeverity INT = ERROR_SEVERITY(),
                @ErrorNumber INT = ERROR_NUMBER();

		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);
 
END CATCH    
DROP TABLE #FilterResult

End 
GO
/****** Object:  StoredProcedure [dbo].[usp_ClienttoClientTransfersForm_AuditView]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


--Exec usp_ClienttoClientTransfersForm_AuditView 47,null,null
CREATE PROCEDURE [dbo].[usp_ClienttoClientTransfersForm_AuditView]
	-- Add the parameters for the stored procedure here
	@ID Int,  
	@FromDate datetime2 = '1900-01-01 00:00:00.0000000', 
	@ToDate datetime2	= '9999-12-31 23:59:59.9999999'
AS
BEGIN

  BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    
;WITH T
AS (
    SELECT		ID
				,MatterNumberFrom as MatterNumberFrom , lag(MatterNumberFrom) over(order by enddate asc) as Prev_MatterNumberFrom
				,MatterNumberTo as MatterNumberTo , lag(MatterNumberTo) over(order by enddate asc) as Prev_MatterNumberTo
				,AmountTransfered as AmountTransfered , lag(AmountTransfered) over(order by enddate asc) as Prev_AmountTransfered
				,Reason as Reason , lag(Reason) over(order by enddate asc) as Prev_Reason
				,ISNULL(Transferred,0) as Transferred , ISNULL(lag(Transferred) over(order by enddate asc),0) as Prev_Transferred
				,ModifyingUser as ModifyingUser , lag(ModifyingUser) over(order by enddate asc) as Prev_ModifyingUser
				,Action as Action , lag(Action) over(order by enddate asc) as Prev_Action
				,BeginDate
				,EndDate
				,ROW_Number() over(partition by  Id Order by EndDate asc) as VersionNumber
    FROM dbo.tblClienttoClientTransfersForm FOR SYSTEM_TIME ALL
    WHERE id =@ID
)
SELECT T.BeginDate,
       T.EndDate,
       C.ColName as ColumnName,
	   t.VersionNumber,	   
       C.present_Value as CurrentValue,
       C.prev_value as PreviousValue,
	   T.ModifyingUser as ModifiedBy
FROM T
    CROSS APPLY
    (
        VALUES
			
			('From Matter Number', Convert(varchar, T.MatterNumberFrom) , Convert(varchar,T.Prev_MatterNumberFrom))
			,('To Matter Number', Convert(varchar, T.MatterNumberTo) , Convert(varchar,T.Prev_MatterNumberTo))
			,('Amount to be transferred', Convert(varchar, T.AmountTransfered) , Convert(varchar,T.Prev_AmountTransfered))
			,('Reason', Convert(varchar, T.Reason) , Convert(varchar,T.Prev_Reason))
			,('Transferred', Convert(varchar, case when T.Transferred =1 then 'Yes' else 'No' end) , Convert(varchar, case when T.Prev_Transferred =1 then 'Yes' else 'No' end))
			,('ModifyingUser', Convert(varchar, T.ModifyingUser) , Convert(varchar,T.Prev_ModifyingUser))
			,('Action', Convert(varchar, T.Action) , Convert(varchar,T.Prev_Action))
			) AS C (ColName, present_Value, prev_value)
WHERE  EXISTS
(
    SELECT present_Value 
    EXCEPT 
    SELECT C.prev_value
)  and Convert(datetime,BeginDate) >= @FromDate and Convert(Datetime,BeginDate) <=@ToDate

END TRY
BEGIN CATCH

        DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),
                @ErrorSeverity INT = ERROR_SEVERITY(),
                @ErrorNumber INT = ERROR_NUMBER();

		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);
 
END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[usp_ClientToClientTransfersForm_InsertUpdate]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================  
-- Author:  <Author : Shivaiah,Sundar>  
-- Create date: <10-04-2021>  
-- Modified date : <11-04-2021><13-04-2021,Sundar> <Included Exception handling>   
-- Description: <Description,The purpose of the Stored Procedure is to make CRUD operations on ClientToClientTransfersForm tables>  
-- Generic Info: <I - Insert , U- Update, D- Delete R-Re activate>    
-- =============================================  
  
-- Insert [spClientToClientTransfersForm] 0,'1356','25654',1000,'test1',1,'I',9,'csuman',101,0,'31,32'  
-- Update [spClientToClientTransfersForm] 33,'5465','5645',311,'test65651',0,'U',9,'csuman',201,1,'31,32'  
-- Delete [spClientToClientTransfersForm] 0,'1356','25654',1000,'test1',1,'D',9,'csuman',201,0,'31,32,33'  
-- Reactivate  [spClientToClientTransfersForm] 0,'1356','25654',1000,'test1',1,'R',9,'csuman',601,9,'33'  
  
  
  
  
CREATE PROCEDURE [dbo].[usp_ClientToClientTransfersForm_InsertUpdate]   
 -- Add the parameters for the stored procedure here  
 @ID int = 0,    
 @MatterNumberFrom varchar(50)=NULL,    
 @MatterNumberTo varchar(20)=NULL,    
 @AmountTransfered money=NULL,    
 @Reason varchar(200)=NULL,    
 @Transferred bit=NULL,    
    
 @StatementType  Varchar(50)=NULL,        
 @TrackerId int =NULL,          
 @CreatedBy nvarchar(256)=NULL,        
 @processid int =NULL,        
 @RoutingID Int=NULL,  
 @selectedData varchar(100) = NULL    
AS  
BEGIN  
BEGIN TRY
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements  
 SET NOCOUNT ON;  
  
  
 Declare @trgProcessId int;     
    -- This performs INSERT action against ClientToClientTransfersForm table  
IF @StatementType = 'I'            
 BEGIN         
         
   INSERT INTO dbo.tblClientToClientTransfersForm     
   (MatterNumberFrom  ,    
   MatterNumberTo  ,    
   AmountTransfered  ,    
   Reason  ,    
   Transferred,  
   [Action],  
   [ModifyingUser]   
     
)    
    
   Values    
   (@MatterNumberFrom  ,    
    @MatterNumberTo  ,    
    @AmountTransfered  ,    
    @Reason  ,    
    @Transferred,  
    @StatementType,  
    @CreatedBy )    
    
    Declare @FormDataId int;          
 set @FormDataId = SCOPE_IDENTITY()          
        
 SELECT @trgProcessId=trgProcessId FROM tblRoutingDetails  
  WHERE TrackerId= @TrackerId AND srcprocessid = @processid AND RoutingID = @RoutingID        
    
   -- This is common across all the trackers and perofrms INSERT action against tbltrackersform table   
    EXEC usp_TrackersForm_Insert @TrackerId, @CreatedBy, @trgProcessId,  @FormDataId  
     
 END     
  
  
  SELECT @trgProcessId=trgProcessId FROM tblRoutingDetails WHERE TrackerId= @TrackerId AND srcprocessid =@ProcessId            
 AND RoutingID = @RoutingID     
  
  
  
--This performs UPDATE action against ClientToClientTransfersForm table  
IF @StatementType = 'U'            
  BEGIN         
   
    UPDATE dbo.tblClientToClientTransfersForm   
   SET    
       MatterNumberFrom = @MatterNumberFrom ,    
       MatterNumberTo = @MatterNumberTo ,    
       AmountTransfered  = @AmountTransfered,    
       Reason = @Reason ,    
       Transferred = @Transferred,    
       [ModifyingUser] = @CreatedBy,    
       [Action] = @StatementType    
 WHERE Id = @Id      
                  
    
     EXEC [dbo].[usp_TrackersForm_Update] @Id,@TrackerId, @ProcessId,@CreatedBy, @RoutingID, @selectedData ,@StatementType    
       
  END     
    
--This performs DELETE/RE ACTIVATE action against ClientToClientTransfersForm table  
IF (@StatementType = 'D' OR  @StatementType = 'R')          
  BEGIN         
  
  
    UPDATE dbo.tblClientToClientTransfersForm   
   SET    
       [ModifyingUser] = @CreatedBy,    
       [Action] = @StatementType    
 WHERE Id IN (SELECT CAST(value AS INTEGER) FROM STRING_SPLIT(@selectedData, ','))           
    
   -- This is common across all the trackers and UPDATING Process id in tbltrackersform table based on Routing details  
    EXEC [dbo].[usp_TrackersForm_Update] @Id,@TrackerId, @ProcessId,@CreatedBy, @RoutingID, @selectedData ,@StatementType 
       
  END    
  
END TRY
BEGIN CATCH

        DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),
                @ErrorSeverity INT = ERROR_SEVERITY(),
                @ErrorNumber INT = ERROR_NUMBER();

		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);
 
END CATCH   
END  
GO
/****** Object:  StoredProcedure [dbo].[usp_ComplaintsForm_AuditView]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


--Exec usp_ComplaintsForm_AuditView 6,null,null
CREATE PROCEDURE [dbo].[usp_ComplaintsForm_AuditView]
	-- Add the parameters for the stored procedure here
	@ID Int,  
	@FromDate datetime2 = '1900-01-01 00:00:00.0000000', 
	@ToDate datetime2	= '9999-12-31 23:59:59.9999999'
AS
BEGIN

  BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    
;WITH T
AS (
    SELECT		com.ID
	            ,CenterName as Center , lag(CenterName) over(order by enddate asc) as Prev_Center
				,TeamName as Team , lag(TeamName) over(order by enddate asc) as Prev_Team
				,MatterNumberID as MatterNumberID , lag(MatterNumberID) over(order by enddate asc) as Prev_MatterNumberID
				,ABSWith as ABSWith , lag(ABSWith) over(order by enddate asc) as Prev_ABSWith
				,FeeEarner as FeeEarner , lag(FeeEarner) over(order by enddate asc) as Prev_FeeEarner
				,ClientName as ClientName , lag(ClientName) over(order by enddate asc) as Prev_ClientName
				,TransactionType as TransactionType , lag(TransactionType) over(order by enddate asc) as Prev_TransactionType
				,SubsidaryBranch as SubsidaryBranch , lag(SubsidaryBranch) over(order by enddate asc) as Prev_SubsidaryBranch
				,Region as Region , lag(Region) over(order by enddate asc) as Prev_Region
				,InstructionDateReceived as InstructionDateReceived , lag(InstructionDateReceived) over(order by enddate asc) as Prev_InstructionDateReceived
	            ,FirstCallMadeOn as FirstCallMadeOn , lag(FirstCallMadeOn) over(order by enddate asc) as Prev_FirstCallMadeOn
			    ,DateOfExchange as DateOfExchange , lag(DateOfExchange) over(order by enddate asc) as Prev_DateOfExchange
				,InstructionToExchange as InstructionToExchange , lag(InstructionToExchange) over(order by enddate asc) as Prev_InstructionToExchange
			    ,CompletionDate as CompletionDate , lag(CompletionDate) over(order by enddate asc) as Prev_CompletionDate
			    ,ComplaintReceivedDate as ComplaintReceivedDate , lag(ComplaintReceivedDate) over(order by enddate asc) as Prev_ComplaintReceivedDate
				,objCompliant.ComplaintMode as ComplaintMode , lag(objCompliant.ComplaintMode) over(order by enddate asc) as Prev_ComplaintMode				
				,AttributedTo as AttributedTo , lag(AttributedTo) over(order by enddate asc) as Prev_AttributedTo				
				,SummaryIssues as SummaryIssues , lag(SummaryIssues) over(order by enddate asc) as Prev_SummaryIssues				
				,RootCauseCommentary as RootCauseCommentary , lag(RootCauseCommentary) over(order by enddate asc) as Prev_RootCauseCommentary
				,ResponsiblePerson as ResponsiblePerson , lag(ResponsiblePerson) over(order by enddate asc) as Prev_ResponsiblePerson
		  	    ,HoldingLetterIssuedDate as HoldingLetterIssuedDate , lag(HoldingLetterIssuedDate) over(order by enddate asc) as Prev_HoldingLetterIssuedDate
			    ,ComplaintResponseDueBy as ComplaintResponseDueBy , lag(ComplaintResponseDueBy) over(order by enddate asc) as Prev_ComplaintResponseDueBy
			    ,ActualResponseIssued as ActualResponseIssued , lag(ActualResponseIssued) over(order by enddate asc) as Prev_ActualResponseIssued
			    ,ResponseReviewed as ResponseReviewed , lag(ResponseReviewed) over(order by enddate asc) as Prev_ResponseReviewed		        
		        ,AmountOfGoodwillOffered as AmountOfGoodwillOffered , lag(AmountOfGoodwillOffered) over(order by enddate asc) as Prev_AmountOfGoodwillOffered
				,PaymentsToThirdParties as PaymentsToThirdParties , lag(PaymentsToThirdParties) over(order by enddate asc) as Prev_PaymentsToThirdParties
				,Level2ComplaintReceivedDate as Level2ComplaintReceivedDate , lag(Level2ComplaintReceivedDate) over(order by enddate asc) as Prev_Level2ComplaintReceivedDate
				,Level2AcknowledgementIssueDate as Level2AcknowledgementIssueDate , lag(Level2AcknowledgementIssueDate) over(order by enddate asc) as Prev_Level2AcknowledgementIssueDate
				,Level2ComplaintResponseDueDate as Level2ComplaintResponseDueDate , lag(Level2ComplaintResponseDueDate) over(order by enddate asc) as Prev_Level2ComplaintResponseDueDate
				,Level2ComplaintResponseActualDate as Level2ComplaintResponseActualDate , lag(Level2ComplaintResponseActualDate) over(order by enddate asc) as Prev_Level2ComplaintResponseActualDate				
				,SummaryFeedbackResolutions as SummaryFeedbackResolutions , lag(SummaryFeedbackResolutions) over(order by enddate asc) as Prev_SummaryFeedbackResolutions
				,SummaryFeedbackCompliance as SummaryFeedbackCompliance , lag(SummaryFeedbackCompliance) over(order by enddate asc) as Prev_SummaryFeedbackCompliance							
				,SingleMulti as SingleMulti , lag(SingleMulti) over(order by enddate asc) as Prev_SingleMulti
				,ISNULL(ComplianceAwareness ,0) as ComplianceAwareness ,ISNULL( lag(ComplianceAwareness) over(order by enddate asc) ,0) as Prev_ComplianceAwareness
				,ISNULL(CriticalNote ,0) as CriticalNote ,ISNULL( lag(CriticalNote) over(order by enddate asc) ,0) as Prev_CriticalNote
				,ISNULL(ResolutionTeamCopy ,0) as ResolutionTeamCopy ,ISNULL( lag(ResolutionTeamCopy) over(order by enddate asc) ,0) as Prev_ResolutionTeamCopy
				,ISNULL(EnsurecopycomplaintsavedtofileandcataloguedinCustExperienceFolder ,0) as EnsurecopycomplaintsavedtofileandcataloguedinCustExperienceFolder ,ISNULL( lag(EnsurecopycomplaintsavedtofileandcataloguedinCustExperienceFolder) over(order by enddate asc) ,0) as Prev_EnsurecopycomplaintsavedtofileandcataloguedinCustExperienceFolder
				,ISNULL(UpdatesUD ,0) as UpdatesUD ,ISNULL( lag(UpdatesUD) over(order by enddate asc) ,0) as Prev_UpdatesUD
				,ISNULL(PPW_PaperworkWentMissing ,0) as PPW_PaperworkWentMissing ,ISNULL( lag(PPW_PaperworkWentMissing) over(order by enddate asc) ,0) as Prev_PPW_PaperworkWentMissing
				,ISNULL(TS_OCNotHappyWithTimescales ,0) as TS_OCNotHappyWithTimescales ,ISNULL( lag(TS_OCNotHappyWithTimescales) over(order by enddate asc) ,0) as Prev_TS_OCNotHappyWithTimescales
				,ISNULL(ERR_ErrorsOnPaperwork ,0) as ERR_ErrorsOnPaperwork ,ISNULL( lag(ERR_ErrorsOnPaperwork) over(order by enddate asc) ,0) as Prev_ERR_ErrorsOnPaperwork
				,ISNULL(CB_CallBacksnotReturned ,0) as CB_CallBacksnotReturned ,ISNULL( lag(CB_CallBacksnotReturned) over(order by enddate asc) ,0) as Prev_CB_CallBacksnotReturned
				,ISNULL(LOC_OCPreferredalocalservice ,0) as LOC_OCPreferredalocalservice ,ISNULL( lag(LOC_OCPreferredalocalservice) over(order by enddate asc) ,0) as Prev_LOC_OCPreferredalocalservice
				,ISNULL(WebsiteIssues ,0) as WebsiteIssues ,ISNULL( lag(WebsiteIssues) over(order by enddate asc) ,0) as Prev_WebsiteIssues
				,ISNULL(INC_OCToldIncorrectThings ,0) as INC_OCToldIncorrectThings ,ISNULL( lag(INC_OCToldIncorrectThings) over(order by enddate asc) ,0) as Prev_INC_OCToldIncorrectThings
				,ISNULL(PLS_HardToSpeakToPL ,0) as PLS_HardToSpeakToPL ,ISNULL( lag(PLS_HardToSpeakToPL) over(order by enddate asc) ,0) as Prev_PLS_HardToSpeakToPL
				,ISNULL(EA_EASaidItWouldTakeLessTime ,0) as EA_EASaidItWouldTakeLessTime ,ISNULL( lag(EA_EASaidItWouldTakeLessTime) over(order by enddate asc) ,0) as Prev_EA_EASaidItWouldTakeLessTime
				,ISNULL(COMP_ErrorAtCompletion ,0) as COMP_ErrorAtCompletion ,ISNULL( lag(COMP_ErrorAtCompletion) over(order by enddate asc) ,0) as Prev_COMP_ErrorAtCompletion
				,ISNULL(POC_FewerPointsOfContact ,0) as POC_FewerPointsOfContact ,ISNULL( lag(POC_FewerPointsOfContact) over(order by enddate asc) ,0) as Prev_POC_FewerPointsOfContact
				,ISNULL(CW_LongCallWaits ,0) as CW_LongCallWaits ,ISNULL( lag(CW_LongCallWaits) over(order by enddate asc) ,0) as Prev_CW_LongCallWaits
				,ISNULL(INF_OCAskedForInformedAlreadyProvided ,0) as INF_OCAskedForInformedAlreadyProvided ,ISNULL( lag(INF_OCAskedForInformedAlreadyProvided) over(order by enddate asc) ,0) as Prev_INF_OCAskedForInformedAlreadyProvided
				,ISNULL(RUD_OCWasSpokenToRudely ,0) as RUD_OCWasSpokenToRudely ,ISNULL( lag(RUD_OCWasSpokenToRudely) over(order by enddate asc) ,0) as Prev_RUD_OCWasSpokenToRudely
				,ISNULL(Sdlt ,0) as Sdlt ,ISNULL( lag(Sdlt) over(order by enddate asc) ,0) as Prev_Sdlt
				,ISNULL(ComplaintUpheldorPartiallyUpheld ,0) as ComplaintUpheldorPartiallyUpheld ,ISNULL( lag(ComplaintUpheldorPartiallyUpheld) over(order by enddate asc) ,0) as Prev_ComplaintUpheldorPartiallyUpheld
				,ISNULL(GestureOfGoodwillOffered ,0) as GestureOfGoodwillOffered ,ISNULL( lag(GestureOfGoodwillOffered) over(order by enddate asc) ,0) as Prev_GestureOfGoodwillOffered
				,ISNULL(L1DealtWithinSLA ,0) as L1DealtWithinSLA ,ISNULL( lag(L1DealtWithinSLA) over(order by enddate asc) ,0) as Prev_L1DealtWithinSLA
				,ISNULL(ReasonForDelayInResponse ,0) as ReasonForDelayInResponse ,ISNULL( lag(ReasonForDelayInResponse) over(order by enddate asc) ,0) as Prev_ReasonForDelayInResponse
				,ISNULL(ComplaintBeenEscalatedToStage2 ,0) as ComplaintBeenEscalatedToStage2 ,ISNULL( lag(ComplaintBeenEscalatedToStage2) over(order by enddate asc) ,0) as Prev_ComplaintBeenEscalatedToStage2
				,ISNULL(L2DealtWithInSLA ,0) as L2DealtWithInSLA ,ISNULL( lag(L2DealtWithInSLA) over(order by enddate asc) ,0) as Prev_L2DealtWithInSLA
				,ISNULL(CopySentToHOPL ,0) as CopySentToHOPL ,ISNULL( lag(CopySentToHOPL) over(order by enddate asc) ,0) as Prev_CopySentToHOPL
				,ISNULL(Execcomplaint ,0) as Execcomplaint ,ISNULL( lag(Execcomplaint) over(order by enddate asc) ,0) as Prev_Execcomplaint
				,ISNULL(EnsureNoFeefoInviteSent ,0) as EnsureNoFeefoInviteSent ,ISNULL( lag(EnsureNoFeefoInviteSent) over(order by enddate asc) ,0) as Prev_EnsureNoFeefoInviteSent
				,ISNULL(ComplaintClosed ,0) as ComplaintClosed ,ISNULL( lag(ComplaintClosed) over(order by enddate asc) ,0) as Prev_ComplaintClosed
				,AttachFile as AttachFile , lag(AttachFile) over(order by enddate asc) as Prev_AttachFile
				,ModifyingUser as ModifyingUser , lag(ModifyingUser) over(order by enddate asc) as Prev_ModifyingUser
				,Action as Action , lag(Action) over(order by enddate asc) as Prev_Action
				,BeginDate
				,EndDate
				,ROW_Number() over(partition by  com.Id Order by EndDate asc) as VersionNumber
    FROM dbo.tblComplaintsForm FOR SYSTEM_TIME ALL AS com 

	left join [dbo].[tblCenterName] center on com.CenterID = center.Id
	left join [dbo].[tblTeamName] team on com.TeamID = team.ID
	left join [dbo].[tblRegion] reg on com.RegionID = reg.Id
	left join [dbo].[tblTransactionType] objTrantype on com.TransactionTypeID = objTrantype.[Id]
	left join [dbo].[tblSingleMulti] objSingleMulti on com.SingleMultiId = objSingleMulti.[Id]
	left join [dbo].[tblComplaintMode] objCompliant on com.ComplaintReceivedID = objCompliant.id
    WHERE com.id =@ID
)
SELECT T.BeginDate,
       T.EndDate,
       C.ColName as ColumnName,
	   t.VersionNumber,	   
       C.present_Value as CurrentValue,
       C.prev_value as PreviousValue,
	   T.ModifyingUser as ModifiedBy
FROM T
    CROSS APPLY
    (
        VALUES
		    ('Centre', Convert(varchar, T.Center) , Convert(varchar,T.Prev_Center))
            ,('Team', Convert(varchar, T.Team) , Convert(varchar,T.Prev_Team))
			,('Matter Number', Convert(varchar, T.MatterNumberID) , Convert(varchar,T.Prev_MatterNumberID))
			,('ABS With', Convert(varchar, T.ABSWith) , Convert(varchar,T.Prev_ABSWith))
			,('Fee Earner', Convert(varchar, T.FeeEarner) , Convert(varchar,T.Prev_FeeEarner))
			,('Client Name', Convert(varchar, T.ClientName) , Convert(varchar,T.Prev_ClientName))
			,('Transaction Type', Convert(varchar, T.TransactionType) , Convert(varchar,T.Prev_TransactionType))
			,('Subsidiary / Branch', Convert(varchar, T.SubsidaryBranch) , Convert(varchar,T.Prev_SubsidaryBranch))
			,('Region', Convert(varchar, T.Region) , Convert(varchar,T.Prev_Region))
            ,('Date instruction received', Convert(varchar(10), T.InstructionDateReceived,103) ,Convert(varchar(10),T.Prev_InstructionDateReceived,103))
			,('Date 1st call made',Convert(varchar(10), T.FirstCallMadeOn,103) , Convert(varchar(10),T.Prev_FirstCallMadeOn,103))
			,('Date Of Exchange', Convert(varchar(10), T.DateOfExchange,103) , Convert(varchar(10),T.Prev_DateOfExchange,103))
			,('Time taken from instruction to exchange', Convert(varchar, T.InstructionToExchange) , Convert(varchar,T.Prev_InstructionToExchange))
			,('Completion Date', Convert(varchar(10), T.CompletionDate,103) , Convert(varchar(10),T.Prev_CompletionDate,103))
			,('Complaint received Date', Convert(varchar(10), T.ComplaintReceivedDate,103) , Convert(varchar(10),T.Prev_ComplaintReceivedDate,103))
			,('How was the complaint received?',T.ComplaintMode , Convert(varchar,T.Prev_ComplaintMode))
			,('Do compliance need to be made aware?', Convert(varchar,case when T.ComplianceAwareness=1 then 'Yes' else 'No' end) , Convert(varchar,case when T.Prev_ComplianceAwareness=1 then 'Yes' else 'No' end))
			,('Has a note been added to critical?', Convert(varchar,case when T.CriticalNote=1 then 'Yes' else 'No' end) , Convert(varchar,case when T.Prev_CriticalNote=1 then 'Yes' else 'No' end))
			,('Remember to send copy to Resolutions team', Convert(varchar, T.ResolutionTeamCopy) , Convert(varchar,T.Prev_ResolutionTeamCopy))
			,('Attributed to?', Convert(varchar, T.AttributedTo) , Convert(varchar,T.Prev_AttributedTo))
			,('Ensure copy complaint saved to file and catalogued in Cust Experience folder', Convert(varchar, T.EnsurecopycomplaintsavedtofileandcataloguedinCustExperienceFolder) , Convert(varchar,T.Prev_EnsurecopycomplaintsavedtofileandcataloguedinCustExperienceFolder))
			,('Summary of issues', Convert(varchar, T.SummaryIssues) , Convert(varchar,T.Prev_SummaryIssues))
			,('UD-Updates', Convert(varchar, case when  T.UpdatesUD =1 then 'Yes' else 'No' end) , Convert(varchar, case when T.Prev_UpdatesUD =1 then 'Yes' else 'No' end))
			,('PPW-Paperwork went missing', Convert(varchar, case when  T.PPW_PaperworkWentMissing =1 then 'Yes' else 'No' end) , Convert(varchar, case when T.Prev_PPW_PaperworkWentMissing =1 then 'Yes' else 'No' end))
			,('TS-OC not happy with timescales', Convert(varchar, case when  T.TS_OCNotHappyWithTimescales =1 then 'Yes' else 'No' end) , Convert(varchar, case when T.Prev_TS_OCNotHappyWithTimescales =1 then 'Yes' else 'No' end))
			,('ERR-Errors on paperwork', Convert(varchar, case when  T.ERR_ErrorsOnPaperwork =1 then 'Yes' else 'No' end) , Convert(varchar, case when T.Prev_ERR_ErrorsOnPaperwork =1 then 'Yes' else 'No' end))
			,('CB-Call backs not returned', Convert(varchar, case when  T.CB_CallBacksnotReturned =1 then 'Yes' else 'No' end) , Convert(varchar, case when T.Prev_CB_CallBacksnotReturned =1 then 'Yes' else 'No' end))
			,('LOC-OC preferred a local service', Convert(varchar, case when  T.LOC_OCPreferredalocalservice =1 then 'Yes' else 'No' end) , Convert(varchar, case when T.Prev_LOC_OCPreferredalocalservice =1 then 'Yes' else 'No' end))
			,('WEB-Website issues', Convert(varchar, case when  T.WebsiteIssues =1 then 'Yes' else 'No' end) , Convert(varchar, case when T.Prev_WebsiteIssues =1 then 'Yes' else 'No' end))
			,('INC-OC told incorrect things', Convert(varchar, case when  T.INC_OCToldIncorrectThings =1 then 'Yes' else 'No' end) , Convert(varchar, case when T.Prev_INC_OCToldIncorrectThings =1 then 'Yes' else 'No' end))
			,('PLS-Hard to speak to PL', Convert(varchar, case when  T.PLS_HardToSpeakToPL =1 then 'Yes' else 'No' end) , Convert(varchar, case when T.Prev_PLS_HardToSpeakToPL =1 then 'Yes' else 'No' end))
			,('EA-EA said it would take less time', Convert(varchar, case when  T.EA_EASaidItWouldTakeLessTime =1 then 'Yes' else 'No' end) , Convert(varchar, case when T.Prev_EA_EASaidItWouldTakeLessTime =1 then 'Yes' else 'No' end))
			,('COMP-Error at completion', Convert(varchar, case when  T.COMP_ErrorAtCompletion =1 then 'Yes' else 'No' end) , Convert(varchar, case when T.Prev_COMP_ErrorAtCompletion =1 then 'Yes' else 'No' end))
			,('POC-Fewer points of contact', Convert(varchar, case when  T.POC_FewerPointsOfContact =1 then 'Yes' else 'No' end) , Convert(varchar, case when T.Prev_POC_FewerPointsOfContact =1 then 'Yes' else 'No' end))
			,('CW-Long call waits', Convert(varchar, case when  T.CW_LongCallWaits =1 then 'Yes' else 'No' end) , Convert(varchar, case when T.Prev_CW_LongCallWaits =1 then 'Yes' else 'No' end))
			,('INF-Asked for information already provided', Convert(varchar, case when  T.INF_OCAskedForInformedAlreadyProvided =1 then 'Yes' else 'No' end) , Convert(varchar, case when T.Prev_INF_OCAskedForInformedAlreadyProvided =1 then 'Yes' else 'No' end))
			,('RUD-Client was spoken to rudely', Convert(varchar, case when  T.RUD_OCWasSpokenToRudely =1 then 'Yes' else 'No' end) , Convert(varchar, case when T.Prev_RUD_OCWasSpokenToRudely =1 then 'Yes' else 'No' end))
			,('SDLT', Convert(varchar, case when  T.Sdlt =1 then 'Yes' else 'No' end) , Convert(varchar, case when T.Prev_Sdlt =1 then 'Yes' else 'No' end))
			,('Root cause commentary', Convert(varchar, T.RootCauseCommentary) , Convert(varchar,T.Prev_RootCauseCommentary))
			,('Responsible person', Convert(varchar, T.ResponsiblePerson) , Convert(varchar,T.Prev_ResponsiblePerson))
			,('Holding letter issued date', Convert(varchar(10), T.HoldingLetterIssuedDate,103) , Convert(varchar(10),T.Prev_HoldingLetterIssuedDate,103))
			,('Complaint response due by', Convert(varchar(10), T.ComplaintResponseDueBy,103) , Convert(varchar(10),T.Prev_ComplaintResponseDueBy,103))
			,('Actual response issued (date)',Convert(varchar(10), T.ActualResponseIssued,103) , Convert(varchar(10),T.Prev_ActualResponseIssued,103))
			,('Response reviewed by?', Convert(varchar, T.ResponseReviewed) , Convert(varchar,T.Prev_ResponseReviewed))
			,('Complaint Upheld or Partially Upheld?', Convert(varchar, case when  T.ComplaintUpheldorPartiallyUpheld =1 then 'Yes' else 'No' end) , Convert(varchar,case when  T.Prev_ComplaintUpheldorPartiallyUpheld =1 then 'Yes' else 'No' end))
			,('Gesture of goodwill offered?', Convert(varchar, case when  T.GestureOfGoodwillOffered =1 then 'Yes' else 'No' end) , Convert(varchar,case when  T.Prev_GestureOfGoodwillOffered =1 then 'Yes' else 'No' end))
			,('Amount of goodwill offered?', Convert(varchar, T.AmountOfGoodwillOffered) , Convert(varchar,T.Prev_AmountOfGoodwillOffered))
			,('Payments to third parties', Convert(varchar, T.PaymentsToThirdParties) , Convert(varchar,T.Prev_PaymentsToThirdParties))
			,('L1 Dealt with in SLA?', Convert(varchar,case when  T.L1DealtWithinSLA =1 then 'Yes' else 'No' end) , Convert(varchar,case when  T.Prev_L1DealtWithinSLA =1 then 'Yes' else 'No' end))
			,('Reason for delay in response', Convert(varchar,case when  T.ReasonForDelayInResponse =1 then 'Yes' else 'No' end) , Convert(varchar,case when  T.Prev_ReasonForDelayInResponse =1 then 'Yes' else 'No' end))
			,('Has complaint been escalated to stage 2?', Convert(varchar, case when  T.ComplaintBeenEscalatedToStage2 =1 then 'Yes' else 'No' end) , Convert(varchar,case when  T.Prev_ComplaintBeenEscalatedToStage2 =1 then 'Yes' else 'No' end))
			,('Level 2 complaint received date', Convert(varchar(10), T.Level2ComplaintReceivedDate,103) , Convert(varchar(10),T.Prev_Level2ComplaintReceivedDate,103))
			,('Level 2 acknowledgement issue date', Convert(varchar(10), T.Level2AcknowledgementIssueDate,103) , Convert(varchar(10),T.Prev_Level2AcknowledgementIssueDate,103))
			,('Level 2 complaint response due date', Convert(varchar(10), T.Level2ComplaintResponseDueDate,103) , Convert(varchar(10),T.Prev_Level2ComplaintResponseDueDate,103))
			,('Level 2 complaint response actual date', Convert(varchar(10), T.Level2ComplaintResponseActualDate,103) , Convert(varchar(10),T.Prev_Level2ComplaintResponseActualDate,103))
			,('L2 Dealt with in SLA?', Convert(varchar,case when  T.L2DealtWithInSLA =1 then 'Yes' else 'No' end) , Convert(varchar,case when  T.Prev_L2DealtWithInSLA =1 then 'Yes' else 'No' end))
			,('Summary of any feedback from Resolutions', Convert(varchar, T.SummaryFeedbackResolutions) , Convert(varchar,T.Prev_SummaryFeedbackResolutions))
			,('Summary of any feedback from Compliance', Convert(varchar, T.SummaryFeedbackCompliance) , Convert(varchar,T.Prev_SummaryFeedbackCompliance))
			,('Copy sent to HOPL', Convert(varchar,case when  T.CopySentToHOPL =1 then 'Yes' else 'No' end) , Convert(varchar,case when  T.Prev_CopySentToHOPL =1 then 'Yes' else 'No' end))
			,('Exec complaint?', Convert(varchar,case when  T.Execcomplaint =1 then 'Yes' else 'No' end) , Convert(varchar,case when  T.Prev_Execcomplaint =1 then 'Yes' else 'No' end))
			,('Single / Multi', Convert(varchar, T.SingleMulti) , Convert(varchar,T.Prev_SingleMulti))
			,('Ensure no Feefo invite is sent', Convert(varchar,case when  T.EnsureNoFeefoInviteSent =1 then 'Yes' else 'No' end ) , Convert(varchar,case when  T.Prev_EnsureNoFeefoInviteSent =1 then 'Yes' else 'No' end))
			,('Complaint closed?', Convert(varchar,case when  T.ComplaintClosed =1 then 'Yes' else 'No' end) , Convert(varchar,case when  T.Prev_ComplaintClosed =1 then 'Yes' else 'No' end))
			,('Attach a file:', Convert(varchar, T.AttachFile) , Convert(varchar,T.Prev_AttachFile))
			,('ModifyingUser', Convert(varchar, T.ModifyingUser) , Convert(varchar,T.Prev_ModifyingUser))
			,('Action', Convert(varchar, T.Action) , Convert(varchar,T.Prev_Action))
			) AS C (ColName, present_Value, prev_value)
WHERE  EXISTS
(
    SELECT present_Value 
    EXCEPT 
    SELECT C.prev_value
)  and Convert(datetime,BeginDate) >= @FromDate and Convert(Datetime,BeginDate) <=@ToDate

END TRY
BEGIN CATCH

        DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),
                @ErrorSeverity INT = ERROR_SEVERITY(),
                @ErrorNumber INT = ERROR_NUMBER();

		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);
 
END CATCH
END

GO
/****** Object:  StoredProcedure [dbo].[usp_ComplaintsForm_InsertUpdate]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
    
    
-- =============================================        
-- Author:  <Author,,Shivaiah>        
-- Create date: <Create Date,12-04-2021>     
-- Modified date: <13-04-2021,Sundar> <Included Exception handling>  
-- Description: <Description,The purpose od the sp is to make crud operations for ComplaintsForm tables ,>        
-- Generic Info: <I - Insert , U- Update, D- Delete ,A - Activate>          
-- =============================================        
        
CREATE PROCEDURE [dbo].[usp_ComplaintsForm_InsertUpdate]        
 -- Add the parameters for the stored procedure here        
 @Id int = 0  ,        
 @CenterID int = null ,        
 @TeamID int = null ,        
 @MatterNumberID varchar(100) = null,        
 @ABSWith varchar(100) = null,       
 @FeeEarner varchar(100) = null,       
 @ClientName varchar(100) = null,       
 @TransactionTypeID int = null,       
 @SubsidaryBranch varchar(50) = null,        
 @RegionID int = null,       
 @InstructionDateReceived datetime = null,        
 @FirstCallMadeOn datetime = null,        
 @DateOfExchange datetime = null,        
 @InstructionToExchange varchar(100) = null,       
 @CompletionDate datetime = null,       
 @ComplaintReceivedDate datetime = null,        
 @ComplaintReceivedID int = null,        
 @ComplianceAwareness bit = null,        
 @CriticalNote bit = null,        
 @ResolutionTeamCopy bit = null,        
 @AttributedTo varchar(100) = null,        
 @EnsurecopycomplaintsavedtofileandcataloguedinCustExperienceFolder bit = null,        
 @SummaryIssues varchar(max) = null,       
 @UpdatesUD bit = null,        
 @PPW_PaperworkWentMissing bit = null,        
 @TS_OCNotHappyWithTimescales bit = null,        
 @ERR_ErrorsOnPaperwork bit = null,        
 @CB_CallBacksnotReturned bit = null,        
 @LOC_OCPreferredalocalservice bit = null,        
 @WebsiteIssues bit = null,        
 @INC_OCToldIncorrectThings bit = null,        
 @PLS_HardToSpeakToPL bit = null,        
 @EA_EASaidItWouldTakeLessTime bit = null,        
 @COMP_ErrorAtCompletion bit = null,        
 @POC_FewerPointsOfContact bit = null,        
 @CW_LongCallWaits bit = null,        
 @INF_OCAskedForInformedAlreadyProvided bit = null,        
 @RUD_OCWasSpokenToRudely bit = null,        
 @Sdlt bit = null,        
 @RootCauseCommentary nvarchar(max) = null,        
 @ResponsiblePerson varchar(100) = null,       
 @HoldingLetterIssuedDate datetime = null,        
 @ComplaintResponseDueBy datetime = null,        
 @ActualResponseIssued datetime = null,        
 @ResponseReviewed varchar(100) = null,        
 @ComplaintUpheldorPartiallyUpheld bit = null,       
 @GestureOfGoodwillOffered bit = null,        
 @AmountOfGoodwillOffered varchar(100) = null,        
 @PaymentsToThirdParties varchar(100) = null,        
 @L1DealtWithinSLA bit = null,       
 @ReasonForDelayInResponse bit = null,        
 @ComplaintBeenEscalatedToStage2 bit = null,        
 @Level2ComplaintReceivedDate datetime = null,        
 @Level2AcknowledgementIssueDate datetime = null,        
 @Level2ComplaintResponseDueDate datetime = null,        
 @Level2ComplaintResponseActualDate datetime = null,        
 @L2DealtWithInSLA bit = null,       
 @SummaryFeedbackResolutions nvarchar(max) = null,        
 @SummaryFeedbackCompliance nvarchar(max) = null,        
 @CopySentToHOPL bit = null,       
 @ExecComplaint bit = null,       
 @SingleMultiId int = null,       
 @EnsureNoFeefoInviteSent bit = null,       
 @ComplaintClosed bit = null,        
 @Attachments nvarchar(max) = null,        
 @Name varchar(100) = null,        
 @AttachFile nvarchar(max) = null,       
        
 @DelayResponse1 bit = null,        
 @DelayResponse2 bit = null,        
 @DelayResponse3 bit = null,        
 @DelayResponse4 bit = null,        
 @DelayResponse5 bit = null,        
 @DelayResponse6 bit = null,        
 @DelayResponse7 bit = null,        
 @DelayResponse8 bit = null,        
        
 @StatementType  Varchar(50) = null,          
 @TrackerId INT = null,           
 @processid INT = null,           
 @CreatedBy Varchar(256) = null,            
 @RoutingID Int = null,      
 @selectedData varchar(100) = NULL          
AS        
BEGIN      
BEGIN TRY  
 -- SET NOCOUNT ON added to prevent extra result sets from        
 -- interfering with SELECT statements.        
 SET NOCOUNT ON;        
        
        
 Declare @trgProcessId int;           
    -- It does the Insert operation for ComplaintsForm table.        
IF @StatementType = 'I'                  
 BEGIN               
               
    INSERT INTO tblComplaintsForm(          
       
       CenterID        
      ,TeamID        
      ,MatterNumberID        
      ,ABSWith        
      ,FeeEarner        
      ,ClientName        
      ,TransactionTypeID        
      ,SubsidaryBranch        
      ,RegionID        
      ,InstructionDateReceived        
      ,FirstCallMadeOn        
      ,DateOfExchange        
      ,InstructionToExchange        
      ,CompletionDate        
      ,ComplaintReceivedDate        
      ,ComplaintReceivedID        
      ,ComplianceAwareness        
      ,CriticalNote        
      ,ResolutionTeamCopy        
      ,AttributedTo        
      ,EnsurecopycomplaintsavedtofileandcataloguedinCustExperienceFolder        
      ,SummaryIssues        
      ,UpdatesUD        
      ,PPW_PaperworkWentMissing        
      ,TS_OCNotHappyWithTimescales        
      ,ERR_ErrorsOnPaperwork        
      ,CB_CallBacksnotReturned        
      ,LOC_OCPreferredalocalservice        
      ,WebsiteIssues        
      ,INC_OCToldIncorrectThings        
      ,PLS_HardToSpeakToPL        
      ,EA_EASaidItWouldTakeLessTime        
      ,COMP_ErrorAtCompletion        
      ,POC_FewerPointsOfContact        
      ,CW_LongCallWaits        
      ,INF_OCAskedForInformedAlreadyProvided        
      ,RUD_OCWasSpokenToRudely        
      ,Sdlt        
      ,RootCauseCommentary        
      ,ResponsiblePerson        
      ,HoldingLetterIssuedDate        
      ,ComplaintResponseDueBy        
      ,ActualResponseIssued        
      ,ResponseReviewed        
      ,ComplaintUpheldorPartiallyUpheld        
      ,GestureOfGoodwillOffered        
      ,AmountOfGoodwillOffered        
      ,PaymentsToThirdParties        
      ,L1DealtWithinSLA        
      ,ReasonForDelayInResponse        
      ,ComplaintBeenEscalatedToStage2        
      ,Level2ComplaintReceivedDate        
      ,Level2AcknowledgementIssueDate        
      ,Level2ComplaintResponseDueDate        
      ,Level2ComplaintResponseActualDate        
      ,L2DealtWithInSLA        
      ,SummaryFeedbackResolutions        
      ,SummaryFeedbackCompliance        
      ,CopySentToHOPL        
      ,ExecComplaint        
      ,SingleMultiId        
      ,EnsureNoFeefoInviteSent        
      ,ComplaintClosed        
      ,Attachments        
      ,[Name]        
      ,AttachFile       
   ,[Action]        
      ,[ModifyingUser]         
           
)          
          
   Values          
   (        
       @CenterID        
      ,@TeamID        
      ,@MatterNumberID        
      ,@ABSWith        
      ,@FeeEarner        
      ,@ClientName        
      ,@TransactionTypeID        
      ,@SubsidaryBranch        
      ,@RegionID        
      ,@InstructionDateReceived        
      ,@FirstCallMadeOn        
      ,@DateOfExchange        
      ,@InstructionToExchange        
      ,@CompletionDate        
      ,@ComplaintReceivedDate        
      ,@ComplaintReceivedID        
      ,@ComplianceAwareness        
      ,@CriticalNote        
      ,@ResolutionTeamCopy        
      ,@AttributedTo        
      ,@EnsurecopycomplaintsavedtofileandcataloguedinCustExperienceFolder        
      ,@SummaryIssues        
      ,@UpdatesUD        
      ,@PPW_PaperworkWentMissing        
      ,@TS_OCNotHappyWithTimescales        
      ,@ERR_ErrorsOnPaperwork        
      ,@CB_CallBacksnotReturned        
      ,@LOC_OCPreferredalocalservice        
      ,@WebsiteIssues        
      ,@INC_OCToldIncorrectThings        
      ,@PLS_HardToSpeakToPL        
      ,@EA_EASaidItWouldTakeLessTime        
      ,@COMP_ErrorAtCompletion        
      ,@POC_FewerPointsOfContact        
      ,@CW_LongCallWaits        
      ,@INF_OCAskedForInformedAlreadyProvided        
      ,@RUD_OCWasSpokenToRudely        
      ,@Sdlt        
      ,@RootCauseCommentary        
      ,@ResponsiblePerson        
      ,@HoldingLetterIssuedDate        
      ,@ComplaintResponseDueBy        
      ,@ActualResponseIssued        
      ,@ResponseReviewed        
      ,@ComplaintUpheldorPartiallyUpheld        
      ,@GestureOfGoodwillOffered        
      ,@AmountOfGoodwillOffered        
      ,@PaymentsToThirdParties        
      ,@L1DealtWithinSLA        
      ,@ReasonForDelayInResponse        
      ,@ComplaintBeenEscalatedToStage2        
      ,@Level2ComplaintReceivedDate        
      ,@Level2AcknowledgementIssueDate        
      ,@Level2ComplaintResponseDueDate        
      ,@Level2ComplaintResponseActualDate        
      ,@L2DealtWithInSLA        
      ,@SummaryFeedbackResolutions        
      ,@SummaryFeedbackCompliance        
      ,@CopySentToHOPL        
      ,@ExecComplaint        
      ,@SingleMultiId        
      ,@EnsureNoFeefoInviteSent        
      ,@ComplaintClosed        
      ,@Attachments        
      ,@Name        
      ,@AttachFile        
   ,@StatementType        
      ,@CreatedBy )          
          
    Declare @FormDataId int;                
 set @FormDataId = SCOPE_IDENTITY()                
              
INSERT INTO tblDelayResponse(   
   
 DelayResponse1  ,  
 DelayResponse2  ,  
 DelayResponse3  ,  
 DelayResponse4  ,  
 DelayResponse5  ,  
 DelayResponse6  ,  
 DelayResponse7  ,  
 DelayResponse8  ,  
 CompliantsFormID )  
 Values(   
   
 @DelayResponse1  ,  
 @DelayResponse2  ,  
 @DelayResponse3  ,  
 @DelayResponse4  ,  
 @DelayResponse5  ,  
 @DelayResponse6  ,  
 @DelayResponse7  ,  
 @DelayResponse8  ,  
 @FormDataId )  


 SELECT @trgProcessId=trgProcessId FROM tblRoutingDetails        
  WHERE TrackerId= @TrackerId AND srcprocessid = @processid AND RoutingID = @RoutingID              
          
   -- This is common for across all the trackers it does the inserting records into tbltrackersform table.         
    EXEC Sp_GenericTrackersForm  @TrackerId, @CreatedBy, @trgProcessId,  @FormDataId        
           
 END           
        
        
  SELECT @trgProcessId=trgProcessId FROM tblRoutingDetails WHERE TrackerId= @TrackerId AND srcprocessid =@ProcessId                  
 AND RoutingID = @RoutingID           
        
        
        
-- It does the update operation for ComplaintsForm table.        
IF @StatementType = 'U'                  
  BEGIN               
         
UPDATE tblComplaintsForm SET               
             
CenterID= @CenterID,              
TeamID= @TeamID,              
MatterNumberID= @MatterNumberID,              
ABSWith= @ABSWith,              
FeeEarner = @FeeEarner,              
ClientName =@ClientName,              
TransactionTypeID = @TransactionTypeID,              
SubsidaryBranch = @SubsidaryBranch,              
RegionID = @RegionID,              
InstructionDateReceived = @InstructionDateReceived,              
FirstCallMadeOn =  @FirstCallMadeOn,              
DateOfExchange = @DateOfExchange,              
InstructionToExchange = @InstructionToExchange,              
CompletionDate = @CompletionDate,              
ComplaintReceivedDate = @ComplaintReceivedDate,              
ComplaintReceivedID =@ComplaintReceivedID,              
ComplianceAwareness = @ComplianceAwareness,              
CriticalNote = @CriticalNote,              
ResolutionTeamCopy = @ResolutionTeamCopy,              
AttributedTo = @AttributedTo,              
EnsurecopycomplaintsavedtofileandcataloguedinCustExperienceFolder = @EnsurecopycomplaintsavedtofileandcataloguedinCustExperienceFolder,              
SummaryIssues = @SummaryIssues,              
UpdatesUD = @UpdatesUD,              
PPW_PaperworkWentMissing = @PPW_PaperworkWentMissing,              
TS_OCNotHappyWithTimescales = @TS_OCNotHappyWithTimescales,              
ERR_ErrorsOnPaperwork = @ERR_ErrorsOnPaperwork,              
CB_CallBacksnotReturned = @CB_CallBacksnotReturned,              
LOC_OCPreferredalocalservice = @LOC_OCPreferredalocalservice,              
WebsiteIssues = @WebsiteIssues,              
INC_OCToldIncorrectThings = @INC_OCToldIncorrectThings,              
PLS_HardToSpeakToPL = @PLS_HardToSpeakToPL,              
EA_EASaidItWouldTakeLessTime = @EA_EASaidItWouldTakeLessTime,              
COMP_ErrorAtCompletion= @COMP_ErrorAtCompletion,              
POC_FewerPointsOfContact = @POC_FewerPointsOfContact,              
CW_LongCallWaits = @CW_LongCallWaits,              
INF_OCAskedForInformedAlreadyProvided = @INF_OCAskedForInformedAlreadyProvided,              
RUD_OCWasSpokenToRudely = @RUD_OCWasSpokenToRudely,              
Sdlt = @Sdlt,              
RootCauseCommentary = @RootCauseCommentary,              
ResponsiblePerson = @ResponsiblePerson,              
HoldingLetterIssuedDate =@HoldingLetterIssuedDate,              
ComplaintResponseDueBy= @ComplaintResponseDueBy,              
ActualResponseIssued = @ActualResponseIssued,              
ResponseReviewed = @ResponseReviewed,              
ComplaintUpheldorPartiallyUpheld = @ComplaintUpheldorPartiallyUpheld,              
GestureOfGoodwillOffered = @GestureOfGoodwillOffered,              
AmountOfGoodwillOffered = @AmountOfGoodwillOffered,              
PaymentsToThirdParties = @PaymentsToThirdParties,              
L1DealtWithinSLA = @L1DealtWithinSLA,              
ReasonForDelayInResponse= @ReasonForDelayInResponse,              
ComplaintBeenEscalatedToStage2= @ComplaintBeenEscalatedToStage2,              
Level2ComplaintReceivedDate = @Level2ComplaintReceivedDate,              
Level2AcknowledgementIssueDate = @Level2AcknowledgementIssueDate,              
Level2ComplaintResponseDueDate = @Level2ComplaintResponseDueDate,              
Level2ComplaintResponseActualDate = @Level2ComplaintResponseActualDate,              
L2DealtWithInSLA = @L2DealtWithInSLA,              
SummaryFeedbackResolutions = @SummaryFeedbackResolutions,              
SummaryFeedbackCompliance = @SummaryFeedbackCompliance,              
CopySentToHOPL = @CopySentToHOPL,              
ExecComplaint = @ExecComplaint,              
SingleMultiId = @SingleMultiId,              
EnsureNoFeefoInviteSent = @EnsureNoFeefoInviteSent,              
ComplaintClosed = @ComplaintClosed,              
Attachments = @Attachments,              
[Name] = @Name,              
AttachFile  =  @AttachFile  ,      
[Action]   = @StatementType,      
[ModifyingUser]  = @CreatedBy      
where ID = @Id           
                        
 EXEC [dbo].[usp_TrackersForm_Update] @Id, @TrackerId, @ProcessId,@CreatedBy, @RoutingID, @selectedData ,@StatementType   
                
             
  END           
        
-- It does the Delete or activate operation for ComplaintsForm table.        
IF (@StatementType = 'D' OR  @StatementType = 'R')                
  BEGIN               
        
        
    UPDATE dbo.tblComplaintsForm         
   SET          
       [ModifyingUser] = @CreatedBy,          
       [Action] = @StatementType          
 WHERE Id IN (SELECT CAST(value AS INTEGER) FROM STRING_SPLIT(@selectedData, ','))                 
          
   --This is common for across all the trackers it does the updating routing details('according no.of records') into tbltrackersform table.         
 EXEC [dbo].[usp_TrackersForm_Update] @Id, @TrackerId, @ProcessId,@CreatedBy, @RoutingID, @selectedData,@StatementType    
             
  END          
        
END TRY  
BEGIN CATCH  
  
        DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),  
                @ErrorSeverity INT = ERROR_SEVERITY(),  
                @ErrorNumber INT = ERROR_NUMBER();  
  
  RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);  
   
END CATCH  
         
END 
GO
/****** Object:  StoredProcedure [dbo].[usp_CompletionDairy_Download]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
-- Modified date: <16-04-2021> <Included Exception handling>    
-- [usp_CompletionDairy_Download] 27,201,'view1'   
CREATE Procedure [dbo].[usp_CompletionDairy_Download]    
@TrackerId int,  
@processId int,
@ViewName Varchar(100)

  
AS      
Begin 
BEGIN TRY
      
SELECT   
         Com.Id,
         [TeamName]=Team.TeamCompletionDiary  ,
         MT.MatterType,
		 Com.MatterNumber,
		 Com.ClientSurname,
		 Com.ExpectedCompletionDate,
		 Com.PackReceived,
		 Com.PackChecked,
		 Com.CANotes_Amendments,
		 Com.CanGo,
		 COm.VFImportedNotes,
		 Com.SentForAuthorisation,
		 Com.Sent,
		 Com.LegallyCompleted,
		 Com.Center,
		 Com.Created,
		 Com.CreatedBy,
		 Com.TransactionType,
		 CASE WHEN Com.SLRBilled=1 THEN 'Yes' ELSE 'NO' END AS SLRBilled,
		 CASE WHEN Com.Healthcheck=1 THEN 'Yes' ELSE 'NO' END AS HealthCheck,
		 Com.Modified,
		 Com.ModifiedBy,
		 Com.MatterNumberLinked,
		 Com.CompMonth,
		 Com.CheckedBy,
		 Com.Version

		 INTO #FilterResult
from  [dbo].[tblCompletionDiary]  com 

	left join [dbo].tblmattertype MT on Com.MatterTypeId=MT.Id
	left join [dbo].tblTeamCompletionDiary team on com.TeamID = team.ID
	where com.id in (select distinct FormDataId
				FROM [dbo].[tblTrackerForms]  
				with (nolock) where TrackerId=@TrackerId and ProcessId=@processId  ) 


 DECLARE @sql nvarchar(max);

SELECT  @sql = 'SELECT * FROM #FilterResult ' + dbo.FuncDynamicQueryResult(@TrackerId, @ViewName)

EXEC sp_executesql @sql

END TRY
BEGIN CATCH

        DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),
                @ErrorSeverity INT = ERROR_SEVERITY(),
                @ErrorNumber INT = ERROR_NUMBER();

		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);
 
END CATCH
End    
    
GO
/****** Object:  StoredProcedure [dbo].[usp_CompletionDiary_Excel]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
      
CREATE PROCEDURE [dbo].[usp_CompletionDiary_Excel](@STARTDATE NVARCHAR(MAX)='',@ENDDATE NVARCHAR(MAX)='')      
AS      
BEGIN      
  BEGIN TRY      
     IF RTRIM(LTRIM(@STARTDATE))='' OR RTRIM(LTRIM(@ENDDATE))=''        
     BEGIN      
       SELECT  Team.[TeamCompletionDiary] as Team      
   , [MatterNumber] as [Matter Number]      
   ,[ClientSurname] as [Client Surname]      
   ,MT.MatterType   as [Matter Type]      
   ,[ExpectedCompletionDate]  as [Expected Completion Date]      
   ,[PackReceived] as [Pack Received]      
   ,[PackChecked] as [Pack Checked]      
   ,[CanGo] as [Can Go]      
   ,[Sent]      
   ,[SentForAuthorisation]  as [Sent for Authorisation]      
   ,Center      
   ,Created      
   ,TransactionType      
   ,Modified      
   ,'Item' as [Item Type]      
   ,'clientaccounting/Lists/Completion Diary' as [Path]      
   ,CreatedBy      
            ,[CANotes_Amendments] as [CA Notes / Amendments]       
   ,'VF2' as [System]      
        FROM [dbo].[tblCompletionDiary] CDH        
        LEFT Join [dbo].[tblTeamCompletionDiary] Team on CDH.TeamID = Team.ID        
        LEFT JOIN [dbo].[tblMatterType] MT on CDH.MatterTypeID = MT.ID        
    WHERE [ACTION]<>'D'    
      
  RETURN      
           
     END      
      
      
  SELECT  Team.[TeamCompletionDiary]  as Team      
   , [MatterNumber] as [Matter Number]      
   ,[ClientSurname] as [Client Surname]      
   ,MT.MatterType   as [Matter Type]      
   ,[ExpectedCompletionDate]  as [Expected Completion Date]      
   ,[PackReceived] as [Pack Received]      
   ,[PackChecked] as [Pack Checked]      
   ,[CanGo] as [Can Go]      
   ,[Sent]      
   ,[SentForAuthorisation]  as [Sent for Authorisation]      
   ,Center      
   ,Created      
   ,TransactionType      
   ,Modified      
   ,'Item' as [Item Type]      
   ,'clientaccounting/Lists/Completion Diary' as [Path]      
   ,CreatedBy      
            ,[CANotes_Amendments] as [CA Notes / Amendments]       
   ,'VF2' as [System]      
        FROM [dbo].[tblCompletionDiary] CDH        
        LEFT Join [dbo].[tblTeamCompletionDiary] Team on CDH.TeamID = Team.ID        
        LEFT JOIN [dbo].[tblMatterType] MT on CDH.MatterTypeID = MT.ID        
  Where CDH.[ExpectedCompletionDate] between @STARTDATE AND @ENDDATE      
    AND [ACTION]<>'D'    
      
   END TRY      
   BEGIN CATCH      
           DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),                
                @ErrorSeverity INT = ERROR_SEVERITY(),                
                @ErrorNumber INT = ERROR_NUMBER();                
                
   RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);                
   END CATCH      
      
END 
GO
/****** Object:  StoredProcedure [dbo].[usp_CompletionDiary_InsertUpdate]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






CREATE PROCEDURE [dbo].[usp_CompletionDiary_InsertUpdate]     
 -- Add the parameters for the stored procedure here    
 @ID int = 0,     
@MatterNumber Varchar(20) NULL,
@TeamID int NULL,
@ClientSurname Varchar(20) NULL,
@MatterTypeID int NULL,
@ExpectedCompletionDate datetime NULL,
@PackReceived Varchar(50)  Null,
@PackChecked  Varchar(50)  Null,
@CANotes_Amendments  Varchar(50)  Null,
@VFImportedNotes  Varchar(50)  Null,
@CanGo  Varchar(50)  Null,
@SentForAuthorisation  Varchar(50)  Null,
@Sent  Varchar(50)  Null,
@LegallyCompleted datetime  Null,
@SLRBilled Bit Null,
@Healthcheck Bit Null,

@Center Varchar(50)  Null,
@Created datetime Null,
@TransactionType Varchar(50)  Null,
@Modified datetime Null,
@completiondiarycreatedby Varchar(50) Null,
@CheckedBy Varchar(50)  Null, 
@CompMonth Varchar(50)  Null,  
@MatterNumberLinked Varchar(20)  Null,
@ModifiedBy Varchar(50) Null,
@Version int NULL,

 @StatementType  Varchar(50)=NULL,          
 @TrackerId int =NULL,            
 @CreatedBy Varchar(50)=NULL,          
 @processid int =NULL,          
 @RoutingID Int=NULL,    
 @selectedData varchar(100) = NULL      
AS    
BEGIN    
BEGIN TRY  
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements    
 SET NOCOUNT ON;    
    
    
 Declare @trgProcessId int;       
    -- This performs INSERT action against CompletionDiary Form table    
IF @StatementType = 'I'              
 BEGIN           
           
   INSERT INTO dbo.tblCompletionDiary       
   (	    
		MatterNumber ,
		TeamID  ,
		ClientSurname  ,
		MatterTypeID  ,
		ExpectedCompletionDate  ,
		PackReceived   ,
		PackChecked    ,
		CANotes_Amendments    ,
		VFImportedNotes    ,
		CanGo    ,
		SentForAuthorisation    ,
		[Sent],
		LegallyCompleted   ,
		SLRBilled  ,
		Healthcheck  ,
		[Action],    
        [Center],
        [Created],
        [TransactionType],
        [Modified],
        [CreatedBy],
        [CheckedBy],
        [CompMonth],
		[MatterNumberLinked],
		[ModifiedBy],
		[Version],
		ModifyingUser)

			Values (
			     
		@MatterNumber ,
		@TeamID  ,
		@ClientSurname  ,
		@MatterTypeID  ,
		@ExpectedCompletionDate  ,
		@PackReceived   ,
		@PackChecked    ,
		@CANotes_Amendments    ,
		@VFImportedNotes    ,
		@CanGo    ,
		@SentForAuthorisation    ,
		@Sent    ,
		@LegallyCompleted   ,
		@SLRBilled  ,
		@Healthcheck  ,
		@StatementType,    
		@Center,
		@Created,
		@TransactionType,
		@Modified,
		@completiondiarycreatedby,
		@CheckedBy,
		@CompMonth,
		@MatterNumberLinked,
		@ModifiedBy,
		@Version,
		@CreatedBy
		)
		
		 Declare @FormDataId int;            
 set @FormDataId = SCOPE_IDENTITY()            
          
 SELECT @trgProcessId=trgProcessId FROM tblRoutingDetails    
  WHERE TrackerId= @TrackerId AND srcprocessid = @processid AND RoutingID = @RoutingID          
      
   -- This is common across all the trackers and perofrms INSERT action against tbltrackersform table     
    EXEC usp_TrackersForm_Insert @TrackerId, @CreatedBy, @trgProcessId,  @FormDataId    
       
 END       
    
    
  SELECT @trgProcessId=trgProcessId FROM tblRoutingDetails WHERE TrackerId= @TrackerId AND srcprocessid =@ProcessId              
 AND RoutingID = @RoutingID       
    
    
    
--This performs UPDATE action against Unallocated Funds Form table    
IF @StatementType = 'U'              
  BEGIN           
     
    UPDATE dbo.[tblCompletionDiary]     SET   
	    
MatterNumber = @MatterNumber ,
TeamID = @TeamID  ,
ClientSurname= @ClientSurname  ,
MatterTypeID = @MatterTypeID ,
ExpectedCompletionDate  = @ExpectedCompletionDate ,
PackReceived  = @PackReceived,
PackChecked  = @PackChecked  ,
CANotes_Amendments  = @CANotes_Amendments   ,
VFImportedNotes  = @VFImportedNotes  ,
CanGo   = @CanGo  ,
SentForAuthorisation  = @SentForAuthorisation  ,
[Sent]   = @Sent  ,
LegallyCompleted  = @LegallyCompleted ,
SLRBilled   = @SLRBilled,
Healthcheck = @Healthcheck ,
[Center] = @Center,
[Created] = @Created,
[TransactionType] = @TransactionType,
[Modified]	=	@Modified,
[CreatedBy] = @completiondiarycreatedby,
[CheckedBy] = @CheckedBy,
[CompMonth] = @CompMonth,
[ModifiedBy] = @ModifiedBy,     
[MatterNumberLinked] = @MatterNumberLinked,
[Version] = @Version,
[Action] = @StatementType,
ModifyingUser=@CreatedBy


 WHERE Id = @Id 
 
  EXEC [dbo].[usp_TrackersForm_Update] @ID,@TrackerId, @ProcessId,@CreatedBy, @RoutingID, @selectedData,@StatementType         
            
         
  END       
      
--This performs DELETE/RE ACTIVATE action against [tblCompletionDiary] Form table    
IF (@StatementType = 'D' OR  @StatementType = 'R')            
  BEGIN           
    
    
    UPDATE dbo.[tblCompletionDiary]     
   SET      
       ModifyingUser = @CreatedBy,      
       [Action] = @StatementType      
 WHERE Id IN (SELECT CAST(value AS INTEGER) FROM STRING_SPLIT(@selectedData, ','))             
      
   -- This is common across all the trackers and UPDATING Process id in tbltrackersform table based on Routing details    
    EXEC [dbo].[usp_TrackersForm_Update] @Id,@TrackerId, @ProcessId,@CreatedBy, @RoutingID, @selectedData,@StatementType   
         
  END      
END TRY  
BEGIN CATCH  
  
        DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),  
                @ErrorSeverity INT = ERROR_SEVERITY(),  
                @ErrorNumber INT = ERROR_NUMBER();  
  
  RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);  
   
END CATCH   
       
END  
GO
/****** Object:  StoredProcedure [dbo].[usp_CompletionDiaryForm_AuditView]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ajay George Kalathil
-- Create date: 6th May 2021
-- Description:	This SP will be used to get the Audit Trial Version View for the front end system
-- =============================================


--Exec usp_CompletionDiaryForm_AuditView 1,null,null
CREATE PROCEDURE [dbo].[usp_CompletionDiaryForm_AuditView]
	-- Add the parameters for the stored procedure here
	@ID Int,  
	@FromDate datetime2 = '1900-01-01 00:00:00.0000000', 
	@ToDate datetime2	= '9999-12-31 23:59:59.9999999'
AS
BEGIN

  BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    
;WITH T
AS (
    SELECT		com.ID	
	            ,TeamCompletionDiary as TeamCompletionDiary , lag(TeamCompletionDiary) over(order by enddate asc) as Prev_TeamCompletionDiary
	            ,MatterType as MatterType , lag(MatterType) over(order by enddate asc) as Prev_MatterType
	            ,MatterNumber as MatterNumber , lag(MatterNumber) over(order by enddate asc) as Prev_MatterNumber
				
	            ,ClientSurname as ClientSurname , lag(ClientSurname) over(order by enddate asc) as Prev_ClientSurname
	            ,PackReceived as PackReceived , lag(PackReceived) over(order by enddate asc) as Prev_PackReceived
	            ,PackChecked as PackChecked , lag(PackChecked) over(order by enddate asc) as Prev_PackChecked
	            ,CANotes_Amendments as CANotes_Amendments , lag(CANotes_Amendments) over(order by enddate asc) as Prev_CANotes_Amendments
	            ,CanGo as CanGo , lag(CanGo) over(order by enddate asc) as Prev_CanGo
	            ,VFImportedNotes as VFImportedNotes , lag(VFImportedNotes) over(order by enddate asc) as Prev_VFImportedNotes
	            ,SentForAuthorisation as SentForAuthorisation , lag(SentForAuthorisation) over(order by enddate asc) as Prev_SentForAuthorisation
	            ,Sent as Sent , lag(Sent) over(order by enddate asc) as Prev_Sent
	            ,LegallyCompleted as LegallyCompleted , lag(LegallyCompleted) over(order by enddate asc) as Prev_LegallyCompleted
	            ,Center as Center , lag(Center) over(order by enddate asc) as Prev_Center
	            ,TransactionType as TransactionType , lag(TransactionType) over(order by enddate asc) as Prev_TransactionType
	            ,Created as Created , lag(Created) over(order by enddate asc) as Prev_Created
	            ,CreatedBy as CreatedBy , lag(CreatedBy) over(order by enddate asc) as Prev_CreatedBy

	            ,ExpectedCompletionDate as ExpectedCompletionDate , lag(ExpectedCompletionDate) over(order by enddate asc) as Prev_ExpectedCompletionDate				
				,ISNULL(SLRBilled,0) as SLRBilled , ISNULL(lag(SLRBilled) over(order by enddate asc),0) as Prev_SLRBilled
				,ISNULL(Healthcheck,0) as Healthcheck , ISNULL(lag(Healthcheck) over(order by enddate asc),0) as Prev_Healthcheck	
	            ,Modified as Modified , lag(Modified) over(order by enddate asc) as Prev_Modified
	            ,ModifiedBy as ModifiedBy , lag(ModifiedBy) over(order by enddate asc) as Prev_ModifiedBy
	            ,MatterNumberLinked as MatterNumberLinked , lag(MatterNumberLinked) over(order by enddate asc) as Prev_MatterNumberLinked
	            ,CompMonth as CompMonth , lag(CompMonth) over(order by enddate asc) as Prev_CompMonth
	            ,CheckedBy as CheckedBy , lag(CheckedBy) over(order by enddate asc) as Prev_CheckedBy
	            ,Version as Version , lag(Version) over(order by enddate asc) as Prev_Version								
				,ModifyingUser as ModifyingUser , lag(ModifyingUser) over(order by enddate asc) as Prev_ModifyingUser
				,Action as Action , lag(Action) over(order by enddate asc) as Prev_Action
				,BeginDate
				,EndDate
				,ROW_Number() over(partition by  com.Id Order by EndDate asc) as VersionNumber
    FROM dbo.tblCompletionDiary FOR SYSTEM_TIME ALL AS com 
	left join [dbo].tblmattertype MT on Com.MatterTypeId=MT.Id
	left join [dbo].tblTeamCompletionDiary team on com.TeamID = team.ID
    WHERE com.ID =@ID
)
SELECT T.BeginDate,
       T.EndDate,
       C.ColName as ColumnName,
	   t.VersionNumber,	   
       C.present_Value as CurrentValue,
       C.prev_value as PreviousValue,
	   T.ModifyingUser as ModifiedBy
FROM T
    CROSS APPLY
    (
        VALUES		    
			('Team', Convert(varchar, T.TeamCompletionDiary) , Convert(varchar,T.TeamCompletionDiary))
			,('Matter Type', Convert(varchar, T.MatterType) , Convert(varchar,T.Prev_MatterType))
			,('Matter Number', Convert(varchar, T.MatterNumber) , Convert(varchar,T.Prev_MatterNumber))
			,('Client Surname', Convert(varchar, T.ClientSurname) , Convert(varchar,T.Prev_ClientSurname))			 
			,('Expected Completion Date', Convert(varchar(10), T.ExpectedCompletionDate,103) , Convert(varchar(10),T.Prev_ExpectedCompletionDate,103))
			,('Pack Received', Convert(varchar, T.PackReceived) , Convert(varchar,T.Prev_PackReceived))			 
			,('Pack checked', Convert(varchar, T.PackChecked) , Convert(varchar,T.PackChecked))			 
			,('CA Notes / Amendments', Convert(varchar, T.CANotes_Amendments) , Convert(varchar,T.Prev_CANotes_Amendments))			 
			,('Can Go', Convert(varchar, T.CanGo) , Convert(varchar,T.Prev_CanGo))			 
			,('VF Imported Notes', Convert(varchar, T.VFImportedNotes) , Convert(varchar,T.Prev_VFImportedNotes))			 
			,('Sent for Authorisation', Convert(varchar, T.SentForAuthorisation) , Convert(varchar,T.Prev_SentForAuthorisation))			 
			,('Sent', Convert(varchar, T.Sent) , Convert(varchar,T.Sent))			 
			,('Legally completed', Convert(varchar(10), T.LegallyCompleted,103) , Convert(varchar(10),T.Prev_LegallyCompleted,103))			 
			,('Centre', Convert(varchar, T.Center) , Convert(varchar,T.Prev_Center))			 
			,('Created', Convert(varchar, T.Created) , Convert(varchar,T.Prev_Created))				
			,('Created By', Convert(varchar, T.CreatedBy) , Convert(varchar,T.Prev_CreatedBy))			 
			,('Transaction Type', Convert(varchar, T.TransactionType) , Convert(varchar,T.Prev_TransactionType))					
			,('SLR Billed', Convert(varchar,case when T.SLRBilled=1 then 'Yes' else 'No' end) , Convert(varchar,case when T.Prev_SLRBilled=1 then 'Yes' else 'No' end))
			,('Health check', Convert(varchar, case when T.Healthcheck=1 then 'Yes' else 'No' end) , Convert(varchar,case when T.Prev_Healthcheck=1 then 'Yes' else 'No' end))
			,('Modified', Convert(varchar, T.Modified) , Convert(varchar,T.Prev_Modified))			 
			,('Modified By', Convert(varchar, T.ModifiedBy) , Convert(varchar,T.Prev_ModifiedBy))			
			,('Matter Number (linked to item)', Convert(varchar, T.MatterNumberLinked) , Convert(varchar,T.Prev_MatterNumberLinked))		
			,('Comp Month', Convert(varchar, T.CompMonth) , Convert(varchar,T.Prev_CompMonth))		
			,('Checked By', Convert(varchar, T.CheckedBy) , Convert(varchar,T.Prev_CheckedBy))		
			,('Version', Convert(varchar, T.Version) , Convert(varchar,T.Prev_Version))								
			,('ModifyingUser', Convert(varchar, T.ModifyingUser) , Convert(varchar,T.Prev_ModifyingUser))
			,('Action', Convert(varchar, T.Action) , Convert(varchar,T.Prev_Action))
) AS C (ColName, present_Value, prev_value)
WHERE  EXISTS
(
    SELECT present_Value 
    EXCEPT 
    SELECT C.prev_value
)  and Convert(datetime,BeginDate) >= @FromDate and Convert(Datetime,BeginDate) <=@ToDate

END TRY
BEGIN CATCH

        DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),
                @ErrorSeverity INT = ERROR_SEVERITY(),
                @ErrorNumber INT = ERROR_NUMBER();

		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);
 
END CATCH
END

GO
/****** Object:  StoredProcedure [dbo].[usp_CompletionDiaryImport]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- [usp_CompletionDiaryImport] 
CREATE Procedure [dbo].[usp_CompletionDiaryImport]              
@completionDiaryType tblCompletionDiaryMockImport readonly               
            
AS                
Begin           

TRUNCATE TABLE [tblCompletionDiaryImport]
INSERT INTO [dbo].[tblCompletionDiaryImport](
FeeEarner_Team,
FeeEarnerInitials,
MatterNumber,
ClientSurname,
MatterType,
MatterStatus,
ExpectedCompletionDate)
SELECT 
	Team  ,
    FeeEarnerInitials  ,
    MatterNumber  ,
    ClientSurname  ,
    MatterType  ,
    MatterStatus  ,
	ExpectedCompletionDate 
	FROM @completionDiaryType 

EXEC [dbo].[usp_CompletionDiaryTransImport]
End      
      
GO
/****** Object:  StoredProcedure [dbo].[usp_CompletionDiaryTransImport]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_CompletionDiaryTransImport]
AS    
BEGIN   

BEGIN TRY  
 SET NOCOUNT ON; 
DROP TABLE IF EXISTS #TMPMATNUM;
select [MatterNumber],[LoadedOn] INTO #TMPMATNUM from [dbo].[tblCompletionDiaryImport] A
where ([IsMatterNumDups] = 0) and exists (select [MatterNumber] from [dbo].[tblCompletionDiary] B WHERE  A.[MatterNumber] =
B.[MatterNumber])
IF EXISTS (SELECT * FROM #TMPMATNUM)
BEGIN
UPDATE A
set [IsMatterNumDups] = 1
from [dbo].[tblCompletionDiaryImport] A
join #TMPMATNUM B on A.[MatterNumber] = B.[MatterNumber] and A.[LoadedOn] = B.[LoadedOn];
END

UPDATE MC
set [IsMandatoryColMissing] = 1
from [dbo].[tblCompletionDiaryImport] MC
where ([IsMandatoryColMissing] = 0) and  (([MatterNumber] is null or [MatterNumber] = ' ') OR ([FeeEarner_Team] is null or [FeeEarner_Team] = ' ') OR
([ClientSurname] is null or [ClientSurname] = ' ') OR ([MatterType] is null or [MatterType] = ' ') OR
([ExpectedCompletionDate] is null or [ExpectedCompletionDate] = ' '));

SET DATEFORMAT dmy;  
UPDATE CD
set [IsExpCompDateFormatIssue] = 1
from [dbo].[tblCompletionDiaryImport] CD
where ([IsExpCompDateFormatIssue] = 0) and ([ExpectedCompletionDate] is not null)
and (isdate([ExpectedCompletionDate]) = 0)

DROP TABLE IF EXISTS #TMPInsert;
select [MatterNumber],[ClientSurname],[ExpectedCompletionDate],[FeeEarner_Team],[MatterType],[LoadedOn],
[IsMandatoryColMissing],
CASE 
CHARINDEX(' ', LTRIM([FeeEarner_Team]), 1)
WHEN 0 THEN LTRIM([FeeEarner_Team])
ELSE  SUBSTRING(LTRIM([FeeEarner_Team]), 1, CHARINDEX(' ',LTRIM([FeeEarner_Team]), 1) - 1)
END as Center,
(DATENAME(MONTH,[ExpectedCompletionDate])) + '-' 
	+ RIGHT('00' + CAST(YEAR([ExpectedCompletionDate]) AS VARCHAR),2) as CompMonth,
CASE 
WHEN ([MatterType]= 'P' OR [MatterType] = 'S' OR [MatterType] = 'SP') THEN 'Transactional'
ELSE [MatterType]
END AS TransactionType
--(left([FeeEarner_Team],charindex(' ', [FeeEarner_Team]) -1))as Center
into #TMPInsert 
from [dbo].[tblCompletionDiaryImport]
where (IsLoaded = 0 and [IsMatterNumDups] = 0 and [IsMandatoryColMissing] = 0 and 
[IsExpCompDateFormatIssue] = 0);

alter table #TMPInsert
add [TeamID] int,[MatterTypeID] int;

UPDATE A
SET [TeamID] = B.ID,
[MatterTypeID] = C.ID
FROM #TMPInsert A JOIN [dbo].[tblTeamCompletionDiary] B
ON A.[FeeEarner_Team] = B.[TeamCompletionDiary]
JOIN [dbo].[tblMatterType] C ON A.[MatterType] = C.[MatterType];


UPDATE CI
set CI.[IsMandatoryColMissing] = 1
from [dbo].[tblCompletionDiaryImport] CI
JOIN #TMPInsert T ON CI.[MatterNumber] = T.[MatterNumber]
where ((T.[TeamID] is null or T.[TeamID] = ' ') OR
 (T.[MatterTypeID] is null or T.[MatterTypeID] = ' '));

 UPDATE T
set T.[IsMandatoryColMissing] = 1
from #TMPInsert T 
JOIN [dbo].[tblCompletionDiaryImport] CI
  ON  T.[MatterNumber] = CI.[MatterNumber] 
where ((T.[TeamID] is null or T.[TeamID] = ' ') OR
 (T.[MatterTypeID] is null or T.[MatterTypeID] = ' '));

Insert into [dbo].[tblCompletionDiary] ([MatterNumber],[TeamID],[ClientSurname],[MatterTypeID],
[ExpectedCompletionDate],[Center],[CompMonth],[TransactionType],[Version])
select [MatterNumber],[TeamID],[ClientSurname],[MatterTypeID],
[ExpectedCompletionDate],[Center],[CompMonth],[TransactionType],1 from #TMPInsert
where [IsMandatoryColMissing] = 0;

UPDATE A
SET [IsLoaded] = 1
FROM [dbo].[tblCompletionDiaryImport] A
JOIN #TMPInsert B ON A.[MatterNumber] = B.[MatterNumber] 
AND A.[LoadedOn] = B.[LoadedOn]
AND A.[IsMandatoryColMissing] = 0;
DROP TABLE IF EXISTS #TMPMATNUM;
DROP TABLE IF EXISTS #TMPInsert;
END TRY  
BEGIN CATCH  
  
        DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),  
                @ErrorSeverity INT = ERROR_SEVERITY(),  
                @ErrorNumber INT = ERROR_NUMBER();  
  
  RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);  
   
END CATCH 

END



GO
/****** Object:  StoredProcedure [dbo].[usp_Compliants_Download]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
-- Modified date: <16-04-2021> <Included Exception handling>    
-- [usp_Compliants_Download] 1,201,'view1'   
CREATE Procedure [dbo].[usp_Compliants_Download]    
@TrackerId int,  
@processId int,
@ViewName Varchar(100)      
  
AS      
Begin 
BEGIN TRY
      
SELECT   Id = com.Id,
				center.CenterName,
				team.TeamName,
				MatterNumber = com.MatterNumberID,
				com.ABSWith,
				com.FeeEarner,
				com.ClientName,
				TransactionType=objTrantype.[TransactionType],
				com.SubsidaryBranch,
				reg.[Region],
				com.InstructionDateReceived,
				com.FirstCallMadeOn,
				com.DateOfExchange,
				com.InstructionToExchange,
				com.CompletionDate,
				com.ComplaintReceivedDate,
				objCompliant.[ComplaintMode],
			    ComplianceAwareness = case when com.ComplianceAwareness = 1 then 'Yes' else 'No' end, 
			    CriticalNote = case when com.CriticalNote = 1 then 'Yes' else 'No' end, 
			    ResolutionTeamCopy = case when com.ResolutionTeamCopy = 1 then 'Yes' else 'No' end, 
				com.AttributedTo,
				EnsurecopycomplaintsavedtofileandcataloguedinCustExperienceFolder = case when com.EnsurecopycomplaintsavedtofileandcataloguedinCustExperienceFolder = 1 then 'Yes' else 'No' end,
				com.SummaryIssues,

				UpdatesUD = case when com.UpdatesUD = 1 then 'Yes' else 'No' end, 
				PPW_PaperworkWentMissing = case when com.PPW_PaperworkWentMissing = 1 then 'Yes' else 'No' end, 
				TS_OCNotHappyWithTimescales = case when com.TS_OCNotHappyWithTimescales = 1 then 'Yes' else 'No' end, 
				ERR_ErrorsOnPaperwork = case when com.ERR_ErrorsOnPaperwork = 1 then 'Yes' else 'No' end, 
				CB_CallBacksnotReturned = case when com.CB_CallBacksnotReturned = 1 then 'Yes' else 'No' end, 

				LOC_OCPreferredalocalservice = case when com.LOC_OCPreferredalocalservice = 1 then 'Yes' else 'No' end, 
				WebsiteIssues = case when com.WebsiteIssues = 1 then 'Yes' else 'No' end, 

				INC_OCToldIncorrectThings = case when com.INC_OCToldIncorrectThings = 1 then 'Yes' else 'No' end, 
				PLS_HardToSpeakToPL = case when com.PLS_HardToSpeakToPL = 1 then 'Yes' else 'No' end, 
				EA_EASaidItWouldTakeLessTime = case when com.EA_EASaidItWouldTakeLessTime = 1 then 'Yes' else 'No' end, 
				COMP_ErrorAtCompletion = case when com.COMP_ErrorAtCompletion = 1 then 'Yes' else 'No' end, 
				POC_FewerPointsOfContact = case when com.POC_FewerPointsOfContact = 1 then 'Yes' else 'No' end, 
				CW_LongCallWaits = case when com.CW_LongCallWaits = 1 then 'Yes' else 'No' end, 
				INF_OCAskedForInformedAlreadyProvided = case when com.INF_OCAskedForInformedAlreadyProvided = 1 then 'Yes' else 'No' end, 
				RUD_OCWasSpokenToRudely = case when com.RUD_OCWasSpokenToRudely = 1 then 'Yes' else 'No' end, 
				Sdlt = case when com.Sdlt = 1 then 'Yes' else 'No' end,
				com.RootCauseCommentary,
				com.ResponsiblePerson,
				com.HoldingLetterIssuedDate,
				com.ComplaintResponseDueBy,
				com.ActualResponseIssued,
				com.ResponseReviewed,
				ComplaintUpheldorPartiallyUpheld = case when com.ComplaintUpheldorPartiallyUpheld = 1 then 'Yes' else 'No' end, 
				GestureOfGoodwillOffered = case when com.GestureOfGoodwillOffered = 1 then 'Yes' else 'No' end, 
				com.AmountOfGoodwillOffered,
				com.PaymentsToThirdParties,
				L1DealtWithinSLA = case when com.L1DealtWithinSLA = 1 then 'Yes' else 'No' end,
				Systemissues = case when objDelay.[DelayResponse1] = 1 then 'Yes' else 'No' end,
				TMabsence = case when objDelay.[DelayResponse2] = 1 then 'Yes' else 'No' end,
				Waitingonrescompliance = case when objDelay.[DelayResponse3] = 1 then 'Yes' else 'No' end,
				Waitingon3rdparty = case when objDelay.[DelayResponse4] = 1 then 'Yes' else 'No' end,
				Waitingforfurtherinformationfromclient = case when objDelay.[DelayResponse5] = 1 then 'Yes' else 'No' end,
				TMHOPLworkload = case when objDelay.[DelayResponse6] = 1 then 'Yes' else 'No' end,
				Legalmatterbeingdealtwithbycompliance = case when objDelay.[DelayResponse7] = 1 then 'Yes' else 'No' end,
				MatthewLewis = case when objDelay.[DelayResponse8] = 1 then 'Yes' else 'No' end,
				ComplaintBeenEscalatedToStage2 = case when com.ComplaintBeenEscalatedToStage2 = 1 then 'Yes' else 'No' end,
				com.Level2ComplaintReceivedDate,
				com.Level2AcknowledgementIssueDate,
				com.Level2ComplaintResponseDueDate,
				com.Level2ComplaintResponseActualDate,
				L2DealtWithInSLA = case when com.L2DealtWithInSLA = 1 then 'Yes' else 'No' end,
				com.SummaryFeedbackResolutions,
				com.SummaryFeedbackCompliance,
				CopySentToHOPL = case when com.CopySentToHOPL = 1 then 'Yes' else 'No' end,
				ExecComplaint = case when com.ExecComplaint = 1 then 'Yes' else 'No' end,
				SingleMultiId =objSingleMulti.[SingleMulti],
				EnsureNoFeefoInviteSent = case when com.EnsureNoFeefoInviteSent = 1 then 'Yes' else 'No' end,
				ComplaintClosed = case when com.ComplaintClosed = 1 then 'Yes' else 'No' end,
				com.Name,
				com.AttachFile


				INTO #FilterResult

from  [dbo].[tblComplaintsForm]  com 

	left join [dbo].[tblCenterName] center on com.CenterID = center.Id
	left join [dbo].[tblTeamName] team on com.TeamID = team.ID
	left join [dbo].[tblRegion] reg on com.RegionID = reg.Id
	left join [dbo].[tblTransactionType] objTrantype on com.TransactionTypeID = objTrantype.[Id]
	left join [dbo].[tblSingleMulti] objSingleMulti on com.SingleMultiId = objSingleMulti.[Id]
	left join [dbo].[tblComplaintMode] objCompliant on com.ComplaintReceivedID = objCompliant.[Id]
	left join [dbo].[tblDelayResponse] objDelay on com.Id = objDelay.[CompliantsFormID]


	where com.id in (select distinct FormDataId
				FROM [dbo].[tblTrackerForms]  
				with (nolock) where TrackerId=@TrackerId and ProcessId=@processId  ) 

				 DECLARE @sql nvarchar(max);

SELECT  @sql = 'SELECT * FROM #FilterResult ' + dbo.FuncDynamicQueryResult(@TrackerId, @ViewName)

EXEC sp_executesql @sql


END TRY
BEGIN CATCH

        DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),
                @ErrorSeverity INT = ERROR_SEVERITY(),
                @ErrorNumber INT = ERROR_NUMBER();

		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);
 
END CATCH
DROP TABLE #FilterResult

End    
    
GO
/****** Object:  StoredProcedure [dbo].[usp_Compliments_Download]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Modified date: <16-04-2021> <Included Exception handling>
    
      
-- [sp_GetAllFormData] 5,201     
CREATE Procedure [dbo].[usp_Compliments_Download]      
@TrackerId int,    
@processId int,
@ViewName Varchar(100)        
    
AS        
Begin   
BEGIN TRY  
        
SELECT   ID = com.ID,center.CenterName,team.TeamName,  
    CustomerMatterNumber = com.CustomerMatterNumber,  
            IndividualBeingComplimented = com.IndividualBeingComplimented,  
      reg.Region,   
  
     objbranch.Branch,  
     com.Town,  
      ComplimentTextFullInformation = com.ComplimentTextFullInformation,  
      TATSpeedEfficiency = case when com.TATSpeedEfficiency = 1 then 'Yes' else 'No' end,   
      ProActiveDrivingProgress = case when com.ProActiveDrivingProgress = 1 then 'Yes' else 'No' end,   
      SUPClientFeltSupportedappreciatedhelp = case when com.SUPClientFeltSupportedappreciatedhelp = 1 then 'Yes' else 'No' end,   
      COMGoodLevelOfCummunication = case when com.COMGoodLevelOfCummunication = 1 then 'Yes' else 'No' end,   
      FileAttached = com.FileAttached,  
      PLEPleasentToSpeakTo = case when com.PLEPleasentToSpeakTo = 1 then 'Yes' else 'No' end,   
      CCUClearComprehensiveUpdates = case when com.CCUClearComprehensiveUpdates = 1 then 'Yes' else 'No' end,   
  
      STRMadeItLessStressful = case when com.STRMadeItLessStressful = 1 then 'Yes' else 'No' end,   
  
      RESQuickResponseToEmailsQuestions = case when com.RESQuickResponseToEmailsQuestions = 1 then 'Yes' else 'No' end,   
      NewsletterWorthy = case when com.NewsletterWorthy = 1 then 'Yes' else 'No' end,   
  
      WhoSentToCET = com.WhoSentToCET,  
                                    DateReceived = com.DateReceived,  
                                    ReceviedFrom = com.ReceviedFrom    
  
  
  
  INTO #FilterResult
from  [tblComplimentsForm]  com   
  
 left join [dbo].[tblCenterName] center on com.Center = center.Id  
 left join [dbo].[tblTeamName] team on com.Team = team.ID  
 left join [dbo].[tblRegion] reg on com.RegionalSubsidary = reg.Id  
 left join [dbo].[tblBranch] objbranch on com.Branch = objbranch.ID  
  
  
 where com.id in (select distinct FormDataId  
    FROM [dbo].[tblTrackerForms]    
    with (nolock) where TrackerId=@TrackerId and ProcessId=@processId  )   

	 DECLARE @sql nvarchar(max);

SELECT  @sql = 'SELECT * FROM #FilterResult ' + dbo.FuncDynamicQueryResult(@TrackerId, @ViewName)

EXEC sp_executesql @sql
  
END TRY
BEGIN CATCH

        DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),
                @ErrorSeverity INT = ERROR_SEVERITY(),
                @ErrorNumber INT = ERROR_NUMBER();

		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);
 
END CATCH
DROP TABLE #FilterResult

End      
      
GO
/****** Object:  StoredProcedure [dbo].[usp_ComplimentsForm_AuditView]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ajay George Kalathil
-- Create date: 6th May 2021
-- Description:	This SP will be used to get the Audit Trial Version View for the front end system
-- =============================================


--Exec usp_ComplimentsForm_AuditView 4,null,null
CREATE PROCEDURE [dbo].[usp_ComplimentsForm_AuditView]
	-- Add the parameters for the stored procedure here
	@ID Int,  
	@FromDate datetime2 = '1900-01-01 00:00:00.0000000', 
	@ToDate datetime2	= '9999-12-31 23:59:59.9999999'
AS
BEGIN

  BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    
;WITH T
AS (
    SELECT		com.ID	           							
				,ReceviedFrom as ReceviedFrom , lag(ReceviedFrom) over(order by enddate asc) as Prev_ReceviedFrom			
				,DateReceived as DateReceived , lag(DateReceived) over(order by enddate asc) as Prev_DateReceived
				,CustomerMatterNumber as CustomerMatterNumber , lag(CustomerMatterNumber) over(order by enddate asc) as Prev_CustomerMatterNumber
				,CenterName as Center , lag(CenterName) over(order by enddate asc) as Prev_Center
				,TeamName as Team , lag(TeamName) over(order by enddate asc) as Prev_Team
				,IndividualBeingComplimented as IndividualBeingComplimented , lag(IndividualBeingComplimented) over(order by enddate asc) as Prev_IndividualBeingComplimented
				,Region as Region , lag(Region) over(order by enddate asc) as Prev_Region
				,objbranch.Branch as Branch , lag(objbranch.Branch) over(order by enddate asc) as Prev_Branch
				,Town as Town , lag(Town) over(order by enddate asc) as Prev_Town
				,ComplimentTextFullInformation as ComplimentTextFullInformation , lag(ComplimentTextFullInformation) over(order by enddate asc) as Prev_ComplimentTextFullInformation
				,ISNULL(TATSpeedEfficiency ,0) as TATSpeedEfficiency ,ISNULL( lag(TATSpeedEfficiency) over(order by enddate asc) ,0) as Prev_TATSpeedEfficiency
				,ISNULL(ProActiveDrivingProgress ,0) as ProActiveDrivingProgress ,ISNULL( lag(ProActiveDrivingProgress) over(order by enddate asc) ,0) as Prev_ProActiveDrivingProgress
				,ISNULL(SUPClientFeltSupportedappreciatedhelp ,0) as SUPClientFeltSupportedappreciatedhelp ,ISNULL( lag(SUPClientFeltSupportedappreciatedhelp) over(order by enddate asc) ,0) as Prev_SUPClientFeltSupportedappreciatedhelp
				,ISNULL(COMGoodLevelOfCummunication ,0) as COMGoodLevelOfCummunication ,ISNULL( lag(COMGoodLevelOfCummunication) over(order by enddate asc) ,0) as Prev_COMGoodLevelOfCummunication
				,ISNULL(PLEPleasentToSpeakTo ,0) as PLEPleasentToSpeakTo ,ISNULL( lag(PLEPleasentToSpeakTo) over(order by enddate asc) ,0) as Prev_PLEPleasentToSpeakTo
				,ISNULL(CCUClearComprehensiveUpdates ,0) as CCUClearComprehensiveUpdates ,ISNULL( lag(CCUClearComprehensiveUpdates) over(order by enddate asc) ,0) as Prev_CCUClearComprehensiveUpdates
				,ISNULL(STRMadeItLessStressful ,0) as STRMadeItLessStressful ,ISNULL( lag(STRMadeItLessStressful) over(order by enddate asc) ,0) as Prev_STRMadeItLessStressful
				,ISNULL(RESQuickResponseToEmailsQuestions ,0) as RESQuickResponseToEmailsQuestions ,ISNULL( lag(RESQuickResponseToEmailsQuestions) over(order by enddate asc) ,0) as Prev_RESQuickResponseToEmailsQuestions
				,WhoSentToCET as WhoSentToCET , lag(WhoSentToCET) over(order by enddate asc) as Prev_WhoSentToCET
				,ISNULL(NewsletterWorthy ,0) as NewsletterWorthy ,ISNULL( lag(NewsletterWorthy) over(order by enddate asc) ,0) as Prev_NewsletterWorthy
				,FileAttached as FileAttached , lag(FileAttached) over(order by enddate asc) as Prev_FileAttached											
				,ModifyingUser as ModifyingUser , lag(ModifyingUser) over(order by enddate asc) as Prev_ModifyingUser
				,Action as Action , lag(Action) over(order by enddate asc) as Prev_Action
				,BeginDate
				,EndDate
				,ROW_Number() over(partition by  com.Id Order by EndDate asc) as VersionNumber
    FROM dbo.tblComplimentsForm FOR SYSTEM_TIME ALL AS com   
  
 LEFT JOIN [dbo].[tblCenterName] center on com.Center = center.Id  
 LEFT JOIN [dbo].[tblTeamName] team on com.Team = team.ID  
 LEFT JOIN [dbo].[tblRegion] reg on com.RegionalSubsidary = reg.Id  
 LEFT JOIN [dbo].[tblBranch] objbranch on com.Branch = objbranch.ID
    WHERE com.ID =@ID
)
SELECT T.BeginDate,
       T.EndDate,
       C.ColName as ColumnName,
	   t.VersionNumber,	   
       C.present_Value as CurrentValue,
       C.prev_value as PreviousValue,
	   T.ModifyingUser as ModifiedBy
FROM T
    CROSS APPLY
    (
        VALUES			    					
			 ('Recevied From', Convert(varchar, T.ReceviedFrom) , Convert(varchar,T.Prev_ReceviedFrom))		
			,('Date Received', Convert(varchar(10), T.DateReceived,103) , Convert(varchar(10),T.Prev_DateReceived,103))	
			,('Customer Matter Number ', Convert(varchar, T.CustomerMatterNumber) , Convert(varchar,T.Prev_CustomerMatterNumber))
			,('Center ', Convert(varchar, T.Center) , Convert(varchar,T.Prev_Center))
			,('Team ', Convert(varchar, T.Team) , Convert(varchar,T.Prev_Team))
			,('Individual Being Complimented', Convert(varchar, T.IndividualBeingComplimented) , Convert(varchar,T.Prev_IndividualBeingComplimented))
			,('Regional Subsidary', Convert(varchar, T.Region) , Convert(varchar,T.Prev_Region))
			,('Branch', Convert(varchar, T.Branch) , Convert(varchar,T.Prev_Branch))
			,('Town ', Convert(varchar, T.Town) , Convert(varchar,T.Prev_Town))
			,('Compliment Text Full Information', Convert(varchar, T.ComplimentTextFullInformation) , Convert(varchar,T.Prev_ComplimentTextFullInformation))
			,('TAT Speed Efficiency', Convert(varchar, case when  T.TATSpeedEfficiency =1 then 'Yes' else 'No' end) , Convert(varchar, case when T.Prev_TATSpeedEfficiency=1 then 'Yes' else 'No' end))
			,('Pro Active Driving Progress', Convert(varchar, case when  T.ProActiveDrivingProgress =1 then 'Yes' else 'No' end) , Convert(varchar, case when T.Prev_ProActiveDrivingProgress = 1 then 'Yes' else 'No' end))
			,('SUP Client Felt Supported appreciated help', Convert(varchar, case when  T.SUPClientFeltSupportedappreciatedhelp =1 then 'Yes' else 'No' end) , Convert(varchar, case when T.SUPClientFeltSupportedappreciatedhelp =1 then 'Yes' else 'No' end))
			,('COM Good Level Of Cummunication  ', Convert(varchar, case when  T.COMGoodLevelOfCummunication =1 then 'Yes' else 'No' end) , Convert(varchar, case when T.Prev_COMGoodLevelOfCummunication =1 then 'Yes' else 'No' end))
			,('PLE Pleasent To Speak To ', Convert(varchar, case when  T.PLEPleasentToSpeakTo =1 then 'Yes' else 'No' end) , Convert(varchar, case when T.Prev_PLEPleasentToSpeakTo =1 then 'Yes' else 'No' end))
			,('CCU Clea Comprehensive Updates', Convert(varchar, case when  T.CCUClearComprehensiveUpdates =1 then 'Yes' else 'No' end) , Convert(varchar, case when T.Prev_CCUClearComprehensiveUpdates =1 then 'Yes' else 'No' end))
			,('STR Made It Less Stressful', Convert(varchar, case when  T.STRMadeItLessStressful =1 then 'Yes' else 'No' end) , Convert(varchar, case when T.Prev_STRMadeItLessStressful =1 then 'Yes' else 'No' end))
			,('RES Quick Response To Emails Questions', Convert(varchar, case when  T.RESQuickResponseToEmailsQuestions  =1 then 'Yes' else 'No' end) , Convert(varchar, case when T.Prev_RESQuickResponseToEmailsQuestions =1 then 'Yes' else 'No' end))
			,('Who Sent To CET', Convert(varchar, T.WhoSentToCET) , Convert(varchar,T.Prev_WhoSentToCET))				
			,('News letter Worthy', Convert(varchar,case when  T.NewsletterWorthy  =1 then 'Yes' else 'No' end) , Convert(varchar,case when  T.Prev_NewsletterWorthy  =1 then 'Yes' else 'No' end))     
			,('File Attached', Convert(varchar, T.FileAttached) , Convert(varchar,T.Prev_FileAttached))							
			,('ModifyingUser', Convert(varchar, T.ModifyingUser) , Convert(varchar,T.Prev_ModifyingUser))
			,('Action', Convert(varchar, T.Action) , Convert(varchar,T.Prev_Action))
			) AS C (ColName, present_Value, prev_value)
WHERE  EXISTS
(
    SELECT present_Value 
    EXCEPT 
    SELECT C.prev_value
)  and Convert(datetime,BeginDate) >= @FromDate and Convert(Datetime,BeginDate) <=@ToDate

END TRY
BEGIN CATCH

        DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),
                @ErrorSeverity INT = ERROR_SEVERITY(),
                @ErrorNumber INT = ERROR_NUMBER();

		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);
 
END CATCH
END

GO
/****** Object:  StoredProcedure [dbo].[usp_ComplimentsForm_InsertUpdate]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <Author,,Shivaiah>  
-- Create date: <Create Date,12-04-2021>  
-- Modified date: <13-04-2021,Sundar> <Included Exception handling>
-- Description: <Description,The purpose od the sp is to make crud operations for ComplimentsForm tables ,>  
-- Generic Info: <I - Insert , U- Update, D- Delete ,A - Activate>    
-- =============================================  
  
CREATE PROCEDURE [dbo].[usp_ComplimentsForm_InsertUpdate]   
 -- Add the parameters for the stored procedure here  
 @Id int = 0  ,  
 @ReceviedFrom varchar(50),  
 @DateReceived datetime,  
 @CustomerMatterNumber Varchar(50),  
 @Center int,  
 @Team int,  
 @IndividualBeingComplimented Varchar(100) ,  
 @RegionalSubsidary int,  
 @Branch int,  
 @Town Varchar(50),  
 @ComplimentText Nvarchar(MAX) ,  
 @TATSpeedEfficiency Bit ,  
 @ProActiveDrivingProgress Bit ,  
 @SUPClientFeltSupportedappreciatedhelp Bit ,  
 @COMGoodLevelOfCummunication Bit ,  
 @PLEPleasentToSpeakTo Bit ,  
 @CCUClearComprehensiveUpdates Bit ,  
 @STRMadeItLessStressful Bit ,  
 @RESQuickResponseToEmailsQuestions Bit ,  
 @WhoSentToCET Varchar(100) ,  
 @NewsletterWorthy Bit ,  
 @Fileattached Varchar(MAX) ,  
   
 @StatementType  Varchar(50),  
 @TrackerId INT,    
 @ProcessId INT,    
 @CreatedBy Varchar(256),  
 @RoutingID Int,  
 @selectedData varchar(100) = NULL    
AS  
BEGIN  
BEGIN TRY
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
  
  
 Declare @trgProcessId int;     
    -- It does the Insert operation for ComplimentsForm table.  
IF @StatementType = 'I'            
 BEGIN         
         
    INSERT INTO tblComplimentsForm  
   (    
  
 ReceviedFrom,  
 DateReceived ,  
 CustomerMatterNumber,  
 Center ,  
 Team ,  
 IndividualBeingComplimented ,  
 RegionalSubsidary ,  
 Branch,  
 Town,  
 ComplimentTextFullInformation,  
 TATSpeedEfficiency,  
 ProActiveDrivingProgress ,  
 SUPClientFeltSupportedappreciatedhelp ,  
 COMGoodLevelOfCummunication ,  
 PLEPleasentToSpeakTo,  
 CCUClearComprehensiveUpdates,  
 STRMadeItLessStressful,  
 RESQuickResponseToEmailsQuestions,  
 WhoSentToCET ,  
 NewsletterWorthy,  
 FileAttached, 
 [Action],  
 [ModifyingUser]   
     
)    
    
   Values    
   (  
      @ReceviedFrom ,  
 @DateReceived ,  
 @CustomerMatterNumber ,  
 @Center ,  
 @Team ,  
 @IndividualBeingComplimented ,  
 @RegionalSubsidary ,  
 @Branch ,  
 @Town ,  
 @ComplimentText,  
 @TATSpeedEfficiency ,  
 @ProActiveDrivingProgress  ,  
 @SUPClientFeltSupportedappreciatedhelp  ,  
 @COMGoodLevelOfCummunication  ,  
 @PLEPleasentToSpeakTo  ,  
 @CCUClearComprehensiveUpdates  ,  
 @STRMadeItLessStressful  ,  
 @RESQuickResponseToEmailsQuestions  ,  
 @WhoSentToCET  ,  
 @NewsletterWorthy,  
 @Fileattached   
	  ,@StatementType  
      ,@CreatedBy )    
    
    Declare @FormDataId int;          
 set @FormDataId = SCOPE_IDENTITY()          
        
 SELECT @trgProcessId=trgProcessId FROM tblRoutingDetails  
  WHERE TrackerId= @TrackerId AND srcprocessid = @processid AND RoutingID = @RoutingID        
    
   -- This is common for across all the trackers it does the inserting records into tbltrackersform table.   
    EXEC Sp_GenericTrackersForm  @TrackerId, @CreatedBy, @trgProcessId,  @FormDataId  
     
 END     
  
  
  SELECT @trgProcessId=trgProcessId FROM tblRoutingDetails WHERE TrackerId= @TrackerId AND srcprocessid =@ProcessId            
 AND RoutingID = @RoutingID     
  
  
  
-- It does the update operation for ComplimentsForm table.  
IF @StatementType = 'U'            
  BEGIN         
   
UPDATE tblComplimentsForm SET    
     
    
 --ID int Primary Key,    
 ReceviedFrom = @ReceviedFrom ,    
 DateReceived = @DateReceived ,    
 CustomerMatterNumber = @CustomerMatterNumber ,    
 Center = @Center ,    
 Team  = @Team ,    
 IndividualBeingComplimented = @IndividualBeingComplimented  ,    
 RegionalSubsidary =  @RegionalSubsidary  ,    
 Branch = @Branch ,    
 Town = @Town ,    
 ComplimentTextFullInformation = @ComplimentText,    
 TATSpeedEfficiency= @TATSpeedEfficiency ,    
 ProActiveDrivingProgress  = @ProActiveDrivingProgress ,    
 SUPClientFeltSupportedappreciatedhelp= @SUPClientFeltSupportedappreciatedhelp  ,    
 COMGoodLevelOfCummunication = @COMGoodLevelOfCummunication ,    
 PLEPleasentToSpeakTo= @PLEPleasentToSpeakTo ,    
 CCUClearComprehensiveUpdates= @CCUClearComprehensiveUpdates ,    
 STRMadeItLessStressful = @STRMadeItLessStressful ,    
 RESQuickResponseToEmailsQuestions = @RESQuickResponseToEmailsQuestions ,    
 WhoSentToCET = @WhoSentToCET  ,    
 NewsletterWorthy = @NewsletterWorthy,    
 FileAttached =@FileAttached, 
[Action]   = @StatementType,
[ModifyingUser]  = @CreatedBy
where ID = @Id     
                  
 EXEC [dbo].[usp_TrackersForm_Update] @Id, @TrackerId, @ProcessId,@CreatedBy, @RoutingID, @selectedData,@StatementType  
          
       
  END     
  
-- It does the Delete or activate operation for ComplimentsForm table.  
IF (@StatementType = 'D' OR  @StatementType = 'R')          
  BEGIN         
  
  
    UPDATE dbo.tblComplimentsForm   
   SET    
       [ModifyingUser] = @CreatedBy,    
       [Action] = @StatementType    
 WHERE Id IN (SELECT CAST(value AS INTEGER) FROM STRING_SPLIT(@selectedData, ','))           
    
   --This is common for across all the trackers it does the updating routing details('according no.of records') into tbltrackersform table.   
 EXEC [dbo].[usp_TrackersForm_Update] @Id, @TrackerId, @ProcessId,@CreatedBy, @RoutingID, @selectedData,@StatementType  
       
  END    
  
 END TRY
BEGIN CATCH

        DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),
                @ErrorSeverity INT = ERROR_SEVERITY(),
                @ErrorNumber INT = ERROR_NUMBER();

		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);
 
END CATCH 
   
END  
GO
/****** Object:  StoredProcedure [dbo].[usp_ContaminatedLandIndemnity_Download]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
 -- Modified date: <16-04-2021> <Included Exception handling>
  -- [usp_ContaminatedLandIndemnity_Download] 13 , 201,'view'
CREATE Procedure [dbo].[usp_ContaminatedLandIndemnity_Download]                
@TrackerId int,              
@processId int,
@ViewName Varchar(100)                  
              
AS                  
Begin             
 BEGIN TRY        
 Select    
 ID = CLI.ID ,    
 ClientName ,    
 MatterNumber ,    
 CENTER.CenterName as Center ,    
 PropertyAddress ,    
 ExchangeDate ,    
 CompletionDate ,    
 PropertyPrice,    
 PremiumIPAmount ,    
 FeeEarner ,    
 CAOnly = case when CLI.CAOnly = 1 then 'Yes' else 'No' end,    
 AttachFile   INTO #FilterResult  
 From [dbo].[tblContaminatedLandIndemnityForm] CLI    
 Left Join tblcentername CENTER on CLI.CenterID = CENTER.ID    
    
 where CLI.id in (select distinct FormDataId    
    FROM [dbo].[tblTrackerForms]      
    with (nolock) where TrackerId=@TrackerId and ProcessId=@processId  )     
 order by 1 asc    

 
 DECLARE @sql nvarchar(max);

SELECT  @sql = 'SELECT * FROM #FilterResult ' + dbo.FuncDynamicQueryResult(@TrackerId, @ViewName)

EXEC sp_executesql @sql

END TRY
BEGIN CATCH

        DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),
                @ErrorSeverity INT = ERROR_SEVERITY(),
                @ErrorNumber INT = ERROR_NUMBER();

		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);
 
END CATCH
DROP TABLE #FilterResult
  
End        
GO
/****** Object:  StoredProcedure [dbo].[usp_ContaminatedLandIndemnityForm_AuditView]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


--Exec usp_ContaminatedLandIndemnityForm_AuditView 5,null,null
CREATE PROCEDURE [dbo].[usp_ContaminatedLandIndemnityForm_AuditView]
	-- Add the parameters for the stored procedure here
	@ID Int,  
	@FromDate datetime2 = '1900-01-01 00:00:00.0000000', 
	@ToDate datetime2	= '9999-12-31 23:59:59.9999999'
AS
BEGIN

  BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    
;WITH T
AS (
    SELECT		CLI.ID
	            ,ClientName as ClientName , lag(ClientName) over(order by enddate asc) as Prev_ClientName
				,MatterNumber as MatterNumber , lag(MatterNumber) over(order by enddate asc) as Prev_MatterNumber
				,centername as Center , lag(centername) over(order by enddate asc) as Prev_Center
				,PropertyAddress as PropertyAddress , lag(PropertyAddress) over(order by enddate asc) as Prev_PropertyAddress
				,ExchangeDate as ExchangeDate , lag(ExchangeDate) over(order by enddate asc) as Prev_ExchangeDate
				,CompletionDate as CompletionDate , lag(CompletionDate) over(order by enddate asc) as Prev_CompletionDate
				,PropertyPrice as PropertyPrice , lag(PropertyPrice) over(order by enddate asc) as Prev_PropertyPrice
				,PremiumIPAmount as PremiumIPAmount , lag(PremiumIPAmount) over(order by enddate asc) as Prev_PremiumIPAmount
				,FeeEarner as FeeEarner , lag(FeeEarner) over(order by enddate asc) as Prev_FeeEarner
				,ISNULL(CAOnly,0) as CAOnly , ISNULL(lag(CAOnly) over(order by enddate asc),0) as Prev_CAOnly
				,AttachFile as AttachFile , lag(AttachFile) over(order by enddate asc) as Prev_AttachFile
				,ModifyingUser as ModifyingUser , lag(ModifyingUser) over(order by enddate asc) as Prev_ModifyingUser
				,Action as Action , lag(Action) over(order by enddate asc) as Prev_Action
				,BeginDate
				,EndDate
				,ROW_Number() over(partition by  CLI.Id Order by EndDate asc) as VersionNumber
    FROM dbo.tblContaminatedLandIndemnityForm FOR SYSTEM_TIME ALL AS CLI    
 Left Join tblcentername CENTER on CLI.CenterID = CENTER.ID
    WHERE CLI.id =@ID
)
SELECT T.BeginDate,
       T.EndDate,
       C.ColName as ColumnName,
	   t.VersionNumber,	   
       C.present_Value as CurrentValue,
       C.prev_value as PreviousValue,
	   T.ModifyingUser as ModifiedBy
FROM T
    CROSS APPLY
    (
        VALUES
		     ('Client Name', Convert(varchar, T.ClientName) , Convert(varchar,T.Prev_ClientName))
			,('Matter number', Convert(varchar,T.MatterNumber) , Convert(varchar,T.Prev_MatterNumber))
			,('Centre', Convert(varchar, T.Center) , Convert(varchar,T.Prev_Center))
			,('Property Address', Convert(varchar, T.PropertyAddress) , Convert(varchar,T.Prev_PropertyAddress))
			,('Exchange Date', Convert(varchar(10), T.ExchangeDate,103) , Convert(varchar(10),T.Prev_ExchangeDate,103))
			,('Completion Date', Convert(varchar(10), T.CompletionDate,103) , Convert(varchar(10),T.Prev_CompletionDate,103))
			,('Property Price', Convert(varchar, T.PropertyPrice) , Convert(varchar,T.Prev_PropertyPrice))
			,('Premium IP Amount', Convert(varchar, T.PremiumIPAmount) , Convert(varchar,T.Prev_PremiumIPAmount))
			,('Fee Earner', Convert(varchar, T.FeeEarner) , Convert(varchar,T.Prev_FeeEarner))
			,('CA only', Convert(varchar,case when T.CAOnly=1 then 'Yes' else 'No' end) , Convert(varchar,case when T.Prev_CAOnly=1 then 'Yes' else 'No' end))
			,('Attach a file:', Convert(varchar, T.AttachFile) , Convert(varchar,T.Prev_AttachFile))
			,('ModifyingUser', Convert(varchar, T.ModifyingUser) , Convert(varchar,T.Prev_ModifyingUser))
			,('Action', Convert(varchar, T.Action) , Convert(varchar,T.Prev_Action))
			) AS C (ColName, present_Value, prev_value)
WHERE  EXISTS
(
    SELECT present_Value 
    EXCEPT 
    SELECT C.prev_value
)  and Convert(datetime,BeginDate) >= @FromDate and Convert(Datetime,BeginDate) <=@ToDate

END TRY
BEGIN CATCH

        DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),
                @ErrorSeverity INT = ERROR_SEVERITY(),
                @ErrorNumber INT = ERROR_NUMBER();

		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);
 
END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[usp_ContaminatedLandIndemnityForm_InsertUpdate]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================  
-- Author:  <Author : Sundar>  
-- Create date: <12-04-2021>  
-- Modified date: <13-04-2021> <Included Exception handling> 
-- Description: <Description,The purpose of the Stored Procedure is to make CRUD operations on ContaminatedLandIndemnityForm tables>  
-- Generic Info: <I - Insert , U- Update, D- Delete R-Re activate>    
-- =============================================  
 
 -- INSERT EXEC usp_ContaminatedLandIndemnityForm_InsertUpdate 0,EEE,'12345',1,'Address5','2021-03-31 14:06:01.827','2021-04-12 14:06:01.827',100.00,3123.00,'test',0,'aaa.txt','I',10,'Sundar',101,0  
 -- UPDATE EXEC usp_ContaminatedLandIndemnityForm_InsertUpdate 1,'AAA','12345',1,'Address1 Update','2021-03-31 14:06:01.827','2021-04-12 14:06:01.827',100.00,3123.00,'test',0,'aaa.txt','U',10,'Sundar',201,1  
 -- DELETE EXEC usp_ContaminatedLandIndemnityForm_InsertUpdate 0,'AAA','12345',1,'Address1 Update','2021-03-31 14:06:01.827','2021-04-12 14:06:01.827',100.00,3123.00,'test',0,'aaa.txt','D',10,'Sundar',201,0,1 
 -- Re-activate EXEC usp_ContaminatedLandIndemnityForm_InsertUpdate 0,'AAA','12345',1,'Address1 Update','2021-03-31 14:06:01.827','2021-04-12 14:06:01.827',100.00,3123.00,'test',0,'aaa.txt','R',10,'Sundar',601,9,1 

 
  
CREATE PROCEDURE [dbo].[usp_ContaminatedLandIndemnityForm_InsertUpdate]
-- Add the parameters for the stored procedure here  
 @ID INT = 0,
 @ClientName varchar(50) = NULL,  
 @MatterNumber varchar(50) = NULL,   
 @CenterID int  = NULL,   
 @PropertyAddress varchar(max) = NULL,    
 @ExchangeDate datetime  = NULL,    
 @CompletionDate datetime = NULL,    
 @PropertyPrice money = NULL,    
 @PremiumIPAmount money  = NULL,    
 @FeeEarner varchar(max)  = NULL,    
 @CAOnly bit = NULL,    
 @AttachFile varchar(max) = NULL,   
  
 @StatementType  Varchar(50) = NULL,      
 @TrackerId int = NULL,          
 @CreatedBy Varchar(50) = NULL,        
 @processid int = NULL,        
 @RoutingID Int = NULL, 
 @selectedData varchar(100) = NULL  
    
AS          
  BEGIN        

  BEGIN TRY
   
-- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements  
 SET NOCOUNT ON;  
    
 Declare @trgProcessId int;     
    -- This performs INSERT action against ContaminatedLandIndemnityForm table  
IF @StatementType = 'I'            
 BEGIN          
            INSERT INTO dbo.tblContaminatedLandIndemnityForm   
   (  
 ClientName  ,  
 MatterNumber  ,  
 CenterID  ,  
 PropertyAddress,  
 ExchangeDate,  
 CompletionDate ,  
 PropertyPrice  ,  
 PremiumIPAmount  ,  
 FeeEarner ,  
 CAOnly  ,  
 AttachFile,
 [ModifyingUser],
 [Action]
 )  
  
   Values  
   (  
 @ClientName  ,  
 @MatterNumber  ,  
 @CenterID  ,  
 @PropertyAddress,  
 @ExchangeDate,  
 @CompletionDate ,  
 @PropertyPrice  ,  
 @PremiumIPAmount  ,  
 @FeeEarner ,  
 @CAOnly  ,  
 @AttachFile,
 @CreatedBy,
 @StatementType 
  
 )  
  Declare @FormDataId int;          
 set @FormDataId = SCOPE_IDENTITY()          
        
 SELECT @trgProcessId=trgProcessId FROM tblRoutingDetails  
  WHERE TrackerId= @TrackerId AND srcprocessid = @processid AND RoutingID = @RoutingID        
    
   -- This is common across all the trackers and perofrms INSERT action against tbltrackersform table   
    EXEC usp_TrackersForm_Insert @TrackerId, @CreatedBy, @trgProcessId,  @FormDataId  
     
 END     
  
  
  SELECT @trgProcessId=trgProcessId FROM tblRoutingDetails WHERE TrackerId= @TrackerId AND srcprocessid =@ProcessId            
 AND RoutingID = @RoutingID     
  
  
  
--This performs UPDATE action against ContaminatedLandIndemnityForm table  
IF @StatementType = 'U'            
  BEGIN       
             
   UPDATE dbo.tblContaminatedLandIndemnityForm Set  
 ClientName = @ClientName ,  
 MatterNumber = @MatterNumber ,  
 CenterID = @CenterID ,  
 PropertyAddress = @PropertyAddress,  
 ExchangeDate = @ExchangeDate,  
 CompletionDate = @CompletionDate,  
 PropertyPrice = @PropertyPrice ,  
 PremiumIPAmount = @PremiumIPAmount ,  
 FeeEarner =@FeeEarner,  
 CAOnly = @CAOnly ,  
 AttachFile = @AttachFile,
 [ModifyingUser] = @CreatedBy,    
 [Action] = @StatementType 
 where Id = @Id       
          
   -- This is common across all the trackers and perofrms UPDATE action against tbltrackersform table   
     EXEC [dbo].[usp_TrackersForm_Update] @ID,@TrackerId, @ProcessId,@CreatedBy, @RoutingID, @selectedData,@StatementType  
    
END     
       --This performs DELETE/RE ACTIVATE action against ContaminatedLandIndemnityForm table  
IF (@StatementType = 'D' OR  @StatementType = 'R')          
  BEGIN         
  
     UPDATE dbo.tblContaminatedLandIndemnityForm   
   SET    
       [ModifyingUser] = @CreatedBy,    
       [Action] = @StatementType    
 WHERE Id IN (SELECT CAST(value AS INTEGER) FROM STRING_SPLIT(@selectedData, ','))           
    
   -- This is common across all the trackers and UPDATING Process id in tbltrackersform table based on Routing details  
    EXEC [dbo].[usp_TrackersForm_Update] @ID,@TrackerId, @ProcessId,@CreatedBy, @RoutingID, @selectedData,@StatementType  
       
  END    
  
   END TRY
BEGIN CATCH

        DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),
                @ErrorSeverity INT = ERROR_SEVERITY(),
                @ErrorNumber INT = ERROR_NUMBER();

		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);
 
END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[usp_CotrfReport]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[usp_CotrfReport]
@ReportType int,
@Fromdate date= null,
@Todate date= null
AS
BEGIN
if
@ReportType=1
begin
Select
MATAC_0."MT-CODE" As 'Matter Number',
MATAC_0."BAL-CLI" As 'Client Balance',
MATAC_0."BAL-UBD" As 'Unpaid Disbs',
MATAC_0."BAL-UPB" As 'Unpaid Bills'
From
PUB.MATAC MATAC_0
Where
((MATAC_0."BAL-CLI" >0) AND (MATAC_0."BAL-UBD" <>0) OR (MATAC_0."BAL-UPB" <>0))
order by MATAC_0."BAL-CLI"
end
else
BEGIN
Select
MATAC_0."MT-CODE" As 'Matter Number',
MATAC_0."BAL-CLI" As 'Client Balance',
MATAC_0."BAL-UBD" As 'Unpaid Disbs',
MATAC_0."BAL-UPB" As 'Unpaid Bills'
From
PUB.MATAC_History MATAC_0
Where
((MATAC_0."BAL-CLI" >0) AND (MATAC_0."BAL-UBD" <>0) OR (MATAC_0."BAL-UPB" <>0))
and
EffectiveDate between @Fromdate and @Todate 
--and 
--CurrentRecord = 'Y'and
--EndDate between @Fromdate and @Todate
EXCEPT
Select
MATAC_0."MT-CODE" As 'Matter Number',
MATAC_0."BAL-CLI" As 'Client Balance',
MATAC_0."BAL-UBD" As 'Unpaid Disbs',
MATAC_0."BAL-UPB" As 'Unpaid Bills'
From
PUB.MATAC_History MATAC_0 where 1=0

order by MATAC_0."BAL-CLI"
END
end
GO
/****** Object:  StoredProcedure [dbo].[usp_CreditCardRefund_Download]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
 -- Modified date: <16-04-2021> <Included Exception handling>
      
 --[dbo].[usp_CreditCardRefund_Download] 11,201,'view1'  
CREATE Procedure [dbo].[usp_CreditCardRefund_Download]            
@TrackerId int,          
@processId int,
@ViewName Varchar(100)
          
AS              
Begin         
 BEGIN TRY        
              
  select CC.ID, CC.ClientName, CC.MatterNumber, IB.InstructingBranch, CC.eDIFPaymentRef, CC.FurtherDetail,      
  CC.RefundAmount,Refund.ReasonForRefund, sts.Status      
               INTO #FilterResult
from tblcreditcardrefundform CC       
LEFT JOIN tblInstructingBranch IB on cc.InstructingBranchId = IB.ID      
LEFT JOIN tblstatus sts ON CC.StatusID = sts.ID      
LEFT JOIN tblReasonForRefund Refund ON CC.ReasonForRefundID = Refund.ID      
        

 where CC.ID in (select distinct FormDataId        
    FROM [dbo].[tblTrackerForms]          
    with (nolock) where TrackerId=@TrackerId and ProcessId=@processId  )         
 order by 1 asc  
 
  DECLARE @sql nvarchar(max);

SELECT  @sql = 'SELECT * FROM #FilterResult ' + dbo.FuncDynamicQueryResult(@TrackerId, @ViewName)

EXEC sp_executesql @sql

        
END TRY
BEGIN CATCH

        DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),
                @ErrorSeverity INT = ERROR_SEVERITY(),
                @ErrorNumber INT = ERROR_NUMBER();

		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);
 
END CATCH
DROP TABLE #FilterResult
      
End 
GO
/****** Object:  StoredProcedure [dbo].[usp_CreditCardRefundForm_AuditView]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


--Exec usp_CreditCardRefundForm_AuditView 1,null,null
CREATE PROCEDURE [dbo].[usp_CreditCardRefundForm_AuditView]
	-- Add the parameters for the stored procedure here
	@ID Int,  
	@FromDate datetime2 = '1900-01-01 00:00:00.0000000', 
	@ToDate datetime2	= '9999-12-31 23:59:59.9999999'
AS
BEGIN

  BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    
;WITH T
AS (
    SELECT		CC.ID
	            ,ClientName as ClientName , lag(ClientName) over(order by enddate asc) as Prev_ClientName
				,MatterNumber as MatterNumber , lag(MatterNumber) over(order by enddate asc) as Prev_MatterNumber
				,InstructingBranch as InstructingBranch , lag(InstructingBranch) over(order by enddate asc) as Prev_InstructingBranch
				,eDIFPaymentRef as eDIFPaymentRef , lag(eDIFPaymentRef) over(order by enddate asc) as Prev_eDIFPaymentRef
				,RefundAmount as RefundAmount , lag(RefundAmount) over(order by enddate asc) as Prev_RefundAmount
				,ReasonForRefund as ReasonForRefund , lag(ReasonForRefund) over(order by enddate asc) as Prev_ReasonForRefund
				,FurtherDetail as FurtherDetail , lag(FurtherDetail) over(order by enddate asc) as Prev_FurtherDetail
				,Status as Status , lag(Status) over(order by enddate asc) as Prev_Status
				,ModifyingUser as ModifyingUser , lag(ModifyingUser) over(order by enddate asc) as Prev_ModifyingUser
				,Action as Action , lag(Action) over(order by enddate asc) as Prev_Action
				,BeginDate
				,EndDate
				,ROW_Number() over(partition by  CC.ID Order by EndDate asc) as VersionNumber
    FROM dbo.tblCreditCardRefundForm FOR SYSTEM_TIME ALL AS CC       
LEFT JOIN tblInstructingBranch IB on cc.InstructingBranchId = IB.ID      
LEFT JOIN tblstatus sts ON CC.StatusID = sts.ID      
LEFT JOIN tblReasonForRefund Refund ON CC.ReasonForRefundID = Refund.ID
    WHERE CC.ID =@ID
)
SELECT T.BeginDate,
       T.EndDate,
       C.ColName as ColumnName,
	   t.VersionNumber,	   
       C.present_Value as CurrentValue,
       C.prev_value as PreviousValue,
	   T.ModifyingUser as ModifiedBy
FROM T
    CROSS APPLY
    (
        VALUES
		     ('Client name', Convert(varchar, T.ClientName) , Convert(varchar,T.Prev_ClientName))
			,('Matter number', Convert(varchar,T.MatterNumber) , Convert(varchar,T.Prev_MatterNumber))
			,('Instructing branch', Convert(varchar, T.InstructingBranch) , Convert(varchar,T.Prev_InstructingBranch))
			,('eDIF Payment Ref (if applicable)', Convert(varchar, T.eDIFPaymentRef) , Convert(varchar,T.Prev_eDIFPaymentRef))
			,('Refund amount', Convert(varchar, T.RefundAmount) , Convert(varchar,T.Prev_RefundAmount))
			,('Reason for refund', Convert(varchar, T.ReasonForRefund) , Convert(varchar,T.Prev_ReasonForRefund))
			,('Further detail', Convert(varchar, T.FurtherDetail) , Convert(varchar,T.Prev_FurtherDetail))
			,('Status', Convert(varchar, T.Status) , Convert(varchar,T.Prev_Status))
			,('ModifyingUser', Convert(varchar, T.ModifyingUser) , Convert(varchar,T.Prev_ModifyingUser))
			,('Action', Convert(varchar, T.Action) , Convert(varchar,T.Prev_Action))
			) AS C (ColName, present_Value, prev_value)
WHERE  EXISTS
(
    SELECT present_Value 
    EXCEPT 
    SELECT C.prev_value
)  and Convert(datetime,BeginDate) >= @FromDate and Convert(Datetime,BeginDate) <=@ToDate

END TRY
BEGIN CATCH

        DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),
                @ErrorSeverity INT = ERROR_SEVERITY(),
                @ErrorNumber INT = ERROR_NUMBER();

		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);
 
END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[usp_CreditCardRefundForm_InsertUpdate]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================  
-- Author:  <Author : Arun,Sundar>  
-- Create date: <10-04-2021>  
-- Modified date : <11-04-2021> <13-04-2021,Sundar> <Included Exception handling>
-- Description: <Description,The purpose of the Stored Procedure is to make CRUD operations on CreditCardRefundForm tables>  
-- Generic Info: <I - Insert , U- Update, D- Delete R-Re activate>    
-- =============================================  
-- Delete [spClientToClientTransfersForm] 0,'1356','25654',1000,'test1',1,'D',9,'csuman',201,0,'31,32,33'  
-- Reactivate  [spClientToClientTransfersForm] 0,'1356','25654',1000,'test1',1,'R',9,'csuman',601,9,'33'  
  
-- Insert [spClientToClientTransfersForm] 0,'1356','25654',1000,'test1',1,'I',9,'csuman',101,0,'31,32'  
-- Update [spClientToClientTransfersForm] 33,'5465','5645',311,'test65651',0,'U',9,'csuman',201,1,'31,32'  
  
  
  
CREATE PROCEDURE [dbo].[usp_CreditCardRefundForm_InsertUpdate]   
@ID Int= 0,  
@ClientName varchar(50)=NULL  ,  
@MatterNumber varchar(50)=NULL  ,  
@InstructingBranchID INT=NULL ,  
@eDIFPaymentRef varchar(20)=NULL ,  
@RefundAmount Money =NULL ,  
@ReasonForRefundID Int=NULL  ,  
@FurtherDetail varchar(200)=NULL ,  
@StatusID Int =NULL  ,  
    
 @StatementType  Varchar(50)=NULL,        
 @TrackerId int =NULL,          
 @CreatedBy Varchar(50)=NULL,        
 @processid int =NULL,        
 @RoutingID Int=NULL,  
 @selectedData varchar(100) = NULL    
AS  
BEGIN  
BEGIN TRY
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
  
  
 Declare @trgProcessId int;     
    -- This performs INSERT action against CreditCardRefundForm table  
IF @StatementType = 'I'            
 BEGIN         
         
   INSERT INTO   [dbo].[tblCreditCardRefundForm]  
(ClientName ,  
MatterNumber  ,  
InstructingBranchId  ,  
eDIFPaymentRef ,  
RefundAmount  ,  
ReasonForRefundID  ,  
FurtherDetail ,  
StatusID ,  
[Action],  
[ModifyingUser] )  
     
   Values    
 (@ClientName ,  
    @MatterNumber  ,  
    @InstructingBranchID  ,  
    @eDIFPaymentRef ,  
    @RefundAmount  ,  
    @ReasonForRefundID  ,  
    @FurtherDetail ,  
    @StatusID,  
    @StatementType,  
    @CreatedBy )    
    
    Declare @FormDataId int;          
 set @FormDataId = SCOPE_IDENTITY()          
        
 SELECT @trgProcessId=trgProcessId FROM tblRoutingDetails  
  WHERE TrackerId= @TrackerId AND srcprocessid = @processid AND RoutingID = @RoutingID        
    
    
   -- This is common across all the trackers and perofrms INSERT action against tbltrackersform table   
    EXEC usp_TrackersForm_Insert @TrackerId, @CreatedBy, @trgProcessId,  @FormDataId  
     
 END     
  
  
  SELECT @trgProcessId=trgProcessId FROM tblRoutingDetails WHERE TrackerId= @TrackerId AND   
  srcprocessid =@ProcessId            
 AND RoutingID = @RoutingID     
  
  
   
--This performs UPDATE action against CreditCardRefundForm table  
IF @StatementType = 'U'            
  BEGIN         
   
    UPDATE dbo.tblCreditCardRefundForm   
   SET    
        ClientName  = @ClientName,  
     MatterNumber = @MatterNumber ,  
     InstructingBranchId = @InstructingBranchID ,  
     eDIFPaymentRef = @eDIFPaymentRef,  
     RefundAmount = @RefundAmount ,  
     ReasonForRefundID = @ReasonForRefundID ,  
     FurtherDetail = @FurtherDetail ,  
     StatusID  = @StatusID ,    
        [ModifyingUser] = @CreatedBy,    
        [Action] = @StatementType    
 WHERE Id = @Id      
                  
    
 EXEC [dbo].[usp_TrackersForm_Update] @Id,@TrackerId, @ProcessId,@CreatedBy, @RoutingID, @selectedData, @StatementType     
          
       
  END     
    
--This performs DELETE/RE ACTIVATE action against CreditCardRefundForm table  
IF (@StatementType = 'D' OR  @StatementType = 'R')          
  BEGIN         
  
  
    UPDATE dbo.tblCreditCardRefundForm   
   SET    
       [ModifyingUser] = @CreatedBy,    
       [Action] = @StatementType    
 WHERE Id IN (SELECT CAST(value AS INTEGER) FROM STRING_SPLIT(@selectedData, ','))           
    
    -- This is common across all the trackers and UPDATING Process id in tbltrackersform table based on Routing details  
     EXEC [dbo].[usp_TrackersForm_Update] @Id,@TrackerId, @ProcessId,@CreatedBy, @RoutingID, @selectedData, @StatementType 
       
  END    
  END TRY
BEGIN CATCH

        DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),
                @ErrorSeverity INT = ERROR_SEVERITY(),
                @ErrorNumber INT = ERROR_NUMBER();

		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);
 
END CATCH
  
   
END  
GO
/****** Object:  StoredProcedure [dbo].[usp_dataretention]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[usp_dataretention]
as
	Begin
		begin transaction
		BEGIN TRY
			--3RdPartyComplaints
			delete from tbl3PartyComplaintsForm where BeginDate<=DATEADD(year, -6, GETDATE());
			--Complaint
			delete from tblComplaintsForm where BeginDate<=DATEADD(year, -6, GETDATE());
			--Out Of Office Hours Check
			delete from tblOutOfHoursIDCheckProcessForm where BeginDate<=DATEADD(year, -1, GETDATE());
			--SalesLead
			delete from tblSalesLeadForm where BeginDate<=DATEADD(year, -3, GETDATE());
			--Task Layer
			delete from tblTaskLawyerReportForm where BeginDate<=DATEADD(year, -1, GETDATE());

			COMMIT TRANSACTION
		END TRY
		
		BEGIN CATCH
		     ROLLBACK TRANSACTION
       END CATCH
	END
GO
/****** Object:  StoredProcedure [dbo].[usp_DeleteQueryBuilder]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
   
CREATE Procedure [dbo].[usp_DeleteQueryBuilder]    
@Trackerid int,
@ViewName varchar(100)

AS      
Begin      
 
 DELETE FROM [tblMultiViewQueryBulderDetails] where TrackerRefID=@Trackerid and ViewName=@ViewName

End    
    
GO
/****** Object:  StoredProcedure [dbo].[usp_DisbsReport]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[usp_DisbsReport]
@ReportType int,
@Fromdate date= null,
@Todate date= null
AS
BEGIN
if
@ReportType=1
BEGIN
Select
distinct MATDB_0."Notes" As 'Legal Team',
FEETR_0.NAME As 'Fee Earner',
MATDB_0."MT-CODE" As 'Matter Number',
MATDB_0.Description As 'Matter Description',
MATAC_0."BAL-CLI" As 'Client Balance',
MATAC_0."BAL-UBD" As 'Unpaid Disbs',
MATAC_0."BAL-UPB" As 'Unpaid Bills',
MATAC_0."BAL-CLI" + MATAC_0."BAL-UBD" + MATAC_0."BAL-UPB" As 'Balance'
into #over

From
PUB.FEETR FEETR_0, PUB.MATAC MATAC_0, PUB.MATDB MATDB_0, tblBridgeControlForm

Where
MATAC_0."MT-CODE"= MATDB_0."MT-CODE" and
FEETR_0."NAME" = MATDB_0."FEE-EARNER" AND 
(((MATAC_0."BAL-CLI") + (MATAC_0."BAL-UBD")+(MATAC_0."BAL-UPB")) <0)
order by FEETR_0.NAME
Select* into #Disbs from(select * from #over A join tblbridgecontrolform B
on A.[Matter Number] = B.MatterNumber
and (B.outstanding>=0))temp;
select* ,case
when ((Balance = -12 or Balance = -60 or Balance = -72 or Balance = -132) and ([Fee Earner] ='Cardiff - PC HMLR Payment Queries'))
OR ((Balance = -12 or Balance = -60 or Balance = -72 or Balance = -132) and ([Fee Earner] <>'Cardiff - PC HMLR Payment Queries'))then 'Fast Start Debt'
when (Balance <> -12 or Balance <> -60 or Balance <> -72 or Balance <> -132) and ([Fee Earner] ='Cardiff - PC HMLR Payment Queries') then 'HMLR Debt'
when (Balance <> -12 or Balance <> -60 or Balance <> -72 or Balance <> -132) and ([Fee Earner] <>'Cardiff - PC HMLR Payment Queries') then 'Other'
end as [Debt Type]
from #Disbs
end
else
begin
Select
distinct MATDB_0."Notes" As 'Legal Team',
FEETR_0.NAME As 'Fee Earner',
MATDB_0."MT-CODE" As 'Matter Number',
MATDB_0.Description As 'Matter Description',
MATAC_0."BAL-CLI" As 'Client Balance',
MATAC_0."BAL-UBD" As 'Unpaid Disbs',
MATAC_0."BAL-UPB" As 'Unpaid Bills',
MATAC_0."BAL-CLI" + MATAC_0."BAL-UBD" + MATAC_0."BAL-UPB" As 'Balance'
into #over1
From
PUB.FEETR_History FEETR_0, PUB.MATAC_History MATAC_0, PUB.MATDB_History MATDB_0, tblBridgecontrolform
where
(FEETR_0.[EffectiveDate] between @Fromdate and @Todate) and
(MATAC_0.[EffectiveDate] between @Fromdate and @Todate) and
(MATDB_0.[EffectiveDate] between @Fromdate and @Todate) and
(tblBridgecontrolform.[BeginDate] between @Fromdate and @Todate)and

MATAC_0."MT-CODE"= MATDB_0."MT-CODE" and
FEETR_0."NAME" = MATDB_0."FEE-EARNER" AND 
(((MATAC_0."BAL-CLI") + (MATAC_0."BAL-UBD") + (MATAC_0."BAL-UPB")) <0)
order by FEETR_0.NAME
Select* into #Disbs1 from(select * from #over1 A join tblbridgecontrolform B
on A.[Matter Number] = B.MatterNumber
and (B.outstanding>=0))temp
Except
Select
MATDB_0."Notes" As 'Legal Team',
FEETR_0.NAME As 'Fee Earner',
MATDB_0."MT-CODE" As 'Matter Number',
MATDB_0.Description As 'Matter Description',
MATAC_0."BAL-CLI" As 'Client Balance',
MATAC_0."BAL-UBD" As 'Unpaid Disbs',
MATAC_0."BAL-UPB" As 'Unpaid Bills',
MATAC_0."BAL-CLI" + MATAC_0."BAL-UBD" + MATAC_0."BAL-UPB" As 'Balance'


From
PUB.FEETR FEETR_0, PUB.MATAC MATAC_0, PUB.MATDB MATDB_0, tblBridgeControlForm
where 1=0
select* ,case
when ((Balance = -12 or Balance = -60 or Balance = -72 or Balance = -132) and ([Fee Earner] ='Cardiff - PC HMLR Payment Queries'))
OR ((Balance = -12 or Balance = -60 or Balance = -72 or Balance = -132) and ([Fee Earner] <>'Cardiff - PC HMLR Payment Queries'))then 'Fast Start Debt'
when (Balance <> -12 or Balance <> -60 or Balance <> -72 or Balance <> -132) and ([Fee Earner] ='Cardiff - PC HMLR Payment Queries') then 'HMLR Debt'
when (Balance <> -12 or Balance <> -60 or Balance <> -72 or Balance <> -132) and ([Fee Earner] <>'Cardiff - PC HMLR Payment Queries') then 'Other'
end as [Debt Type]
from #Disbs1
end
end

GO
/****** Object:  StoredProcedure [dbo].[usp_DisbsReportSummary]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[usp_DisbsReportSummary]
@ReportType int= null,
@Fromdate date= null,
@Todate date= null
AS
BEGIN
if
@ReportType=1
BEGIN
Select
distinct MATDB_0."Notes" As 'Category',
FEETR_0.NAME As 'Fee Earner',
MATDB_0."MT-CODE" As 'Matter Number',
MATDB_0.Description As 'Matter Description',
MATAC_0."BAL-CLI" As 'Client Balance',
MATAC_0."BAL-UBD" As 'Unpaid Disbs',
MATAC_0."BAL-UPB" As 'Unpaid Bills',
MATAC_0."BAL-CLI" + MATAC_0."BAL-UBD" + MATAC_0."BAL-UPB" As 'Balance',
MATAC_0."BAL-CLI" + MATAC_0."BAL-UBD" + MATAC_0."BAL-UPB" As 'OUTSTANDING'
into #over

From
PUB.FEETR FEETR_0, PUB.MATAC MATAC_0, PUB.MATDB MATDB_0, tblbridgecontrolform
Where
MATAC_0."MT-CODE"= MATDB_0."MT-CODE" and
FEETR_0."NAME" = MATDB_0."FEE-EARNER" AND 
(((MATAC_0."BAL-CLI") + (MATAC_0."BAL-UBD")+(MATAC_0."BAL-UPB")) <0)
order by FEETR_0.NAME
Select* into #Disbs from(select * from #over A join tblbridgecontrolform B
on A.[Matter Number] = B.MatterNumber
and (B.outstanding>=0))temp;

select* ,case
when ((Balance = -12 or Balance = -60 or Balance = -72 or Balance = -132) and ([Fee Earner] ='Cardiff - PC HMLR Payment Queries'))
OR ((Balance = -12 or Balance = -60 or Balance = -72 or Balance = -132) and ([Fee Earner] <>'Cardiff - PC HMLR Payment Queries'))then 'Fast Start Debt'
when (Balance <> -12 or Balance <> -60 or Balance <> -72 or Balance <> -132) and ([Fee Earner] ='Cardiff - PC HMLR Payment Queries') then 'HMLR Debt'
when (Balance <> -12 or Balance <> -60 or Balance <> -72 or Balance <> -132) and ([Fee Earner] <>'Cardiff - PC HMLR Payment Queries') then 'Other'
end as [Debt Type]
into #Temp1
from #over
select [Category],[Debt Type],
case 
when [Debt Type]='Fast Start Debt' then SUM([OUTSTANDING]) 
when [Debt Type]='HMLR Debt' then SUM([OUTSTANDING])
when [Debt Type]='Other' then SUM([OUTSTANDING])
else  0 end outstanding,
case 
when [Debt Type]='Fast Start Debt' then Count([Category]) 
when [Debt Type]='HMLR Debt' then Count([Category])
when [Debt Type]='Other' then Count([Category])
else  0 end Volume,
case 
when [Debt Type]='Fast Start Debt' then SUM([OUTSTANDING])/ Count([Category])
when [Debt Type]='HMLR Debt' then SUM([OUTSTANDING])/ Count([Category])
when [Debt Type]='Other' then SUM([OUTSTANDING])/ Count([Category])
else  0 end [Avg Volume of Debt]
from #Temp1
group by [Category],[Debt Type]
order  by [Category]
end
else
begin
Select
MATDB_0."Notes" As 'Category',
FEETR_0.NAME As 'Fee Earner',
MATDB_0."MT-CODE" As 'Matter Number',
MATDB_0.Description As 'Matter Description',
MATAC_0."BAL-CLI" As 'Client Balance',
MATAC_0."BAL-UBD" As 'Unpaid Disbs',
MATAC_0."BAL-UPB" As 'Unpaid Bills',
MATAC_0."BAL-CLI" + MATAC_0."BAL-UBD" + MATAC_0."BAL-UPB" As 'Balance',
MATAC_0."BAL-CLI" + MATAC_0."BAL-UBD" + MATAC_0."BAL-UPB" As 'OUTSTANDING'
into #over1
From
PUB.FEETR_History FEETR_0, PUB.MATAC_History MATAC_0, PUB.MATDB_History MATDB_0, tblBridgecontrolform
where
(FEETR_0.[EffectiveDate] between @Fromdate and @Todate) and
(MATAC_0.[EffectiveDate] between @Fromdate and @Todate) and
(MATDB_0.[EffectiveDate] between @Fromdate and @Todate) and
(tblBridgecontrolform.[BeginDate] between @Fromdate and @Todate)and

MATAC_0."MT-CODE"= MATDB_0."MT-CODE" and
FEETR_0."NAME" = MATDB_0."FEE-EARNER" AND 
(((MATAC_0."BAL-CLI") + (MATAC_0."BAL-UBD") + (MATAC_0."BAL-UPB")) <0)
order by FEETR_0.NAME
Select* into #Disbs1 from(select * from #over A join tblbridgecontrolform B
on A.[Matter Number] = B.MatterNumber
and (B.outstanding>=0))temp
except
Select
MATDB_0."Notes" As 'Category',
FEETR_0.NAME As 'Fee Earner',
MATDB_0."MT-CODE" As 'Matter Number',
MATDB_0.Description As 'Matter Description',
MATAC_0."BAL-CLI" As 'Client Balance',
MATAC_0."BAL-UBD" As 'Unpaid Disbs',
MATAC_0."BAL-UPB" As 'Unpaid Bills',
MATAC_0."BAL-CLI" + MATAC_0."BAL-UBD" + MATAC_0."BAL-UPB" As 'Balance',
MATAC_0."BAL-CLI" + MATAC_0."BAL-UBD" + MATAC_0."BAL-UPB" As 'OUTSTANDING'

From
PUB.FEETR_History FEETR_0, PUB.MATAC_History MATAC_0, PUB.MATDB_History MATDB_0, tblBridgecontrolform
where 1=0
select* ,case
when ((Balance = -12 or Balance = -60 or Balance = -72 or Balance = -132) and ([Fee Earner] ='Cardiff - PC HMLR Payment Queries'))
OR ((Balance = -12 or Balance = -60 or Balance = -72 or Balance = -132) and ([Fee Earner] <>'Cardiff - PC HMLR Payment Queries'))then 'Fast Start Debt'
when (Balance <> -12 or Balance <> -60 or Balance <> -72 or Balance <> -132) and ([Fee Earner] ='Cardiff - PC HMLR Payment Queries') then 'HMLR Debt'
when (Balance <> -12 or Balance <> -60 or Balance <> -72 or Balance <> -132) and ([Fee Earner] <>'Cardiff - PC HMLR Payment Queries') then 'Other'
end as [Debt Type]
into #Temp2
from #over1
select [Category],[Debt Type],
case 
when [Debt Type]='Fast Start Debt' then SUM([OUTSTANDING]) 
when [Debt Type]='HMLR Debt' then SUM([OUTSTANDING])
when [Debt Type]='Other' then SUM([OUTSTANDING])
else  0 end outstanding,
case 
when [Debt Type]='Fast Start Debt' then Count([Category]) 
when [Debt Type]='HMLR Debt' then Count([Category])
when [Debt Type]='Other' then Count([Category])
else  0 end Volume,
case 
when [Debt Type]='Fast Start Debt' then SUM([OUTSTANDING])/ Count([Category])
when [Debt Type]='HMLR Debt' then SUM([OUTSTANDING])/ Count([Category])
when [Debt Type]='Other' then SUM([OUTSTANDING])/ Count([Category])
else  0 end [Avg Volume of Debt]
from #Temp1
group by [Category],[Debt Type]
order  by [Category]
end
end
GO
/****** Object:  StoredProcedure [dbo].[usp_Dissatisfaction_Download]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
-- Modified date: <16-04-2021> <Included Exception handling>  
      
-- [usp_Dissatisfaction_Download] 4,201     
CREATE Procedure [dbo].[usp_Dissatisfaction_Download]      
@TrackerId int,    
@processId int,
@ViewName Varchar(100)        
    
AS        
Begin   
BEGIN TRY  
        
SELECT   Id = com.Id,  
    com.Receivedfrom,  
    objExec.ExecComplaint,  
    objSource.SourceName,  
    com.Datereceived,  
    com.MatterNumberID,  
       ABS = case when com.ABS = 1 then 'Yes' else 'No' end,   
  
    center.CenterName,  
    team.TeamName,  
    com.SubsidaryBranch,  
    reg.Region,  
    com.[AttributedTo],  
    com.[Keyareasofdissat],  
    UpdatesUD = case when com.UpdatesUD = 1 then 'Yes' else 'No' end,   
    PPW_PaperworkWentMissing = case when com.PPW_PaperworkWentMissing = 1 then 'Yes' else 'No' end,   
    TS_OCNotHappyWithTimescales = case when com.TS_OCNotHappyWithTimescales = 1 then 'Yes' else 'No' end,   
    ERR_ErrorsOnPaperwork = case when com.ERR_ErrorsOnPaperwork = 1 then 'Yes' else 'No' end,   
    CB_CallBacksnotReturned = case when com.CB_CallBacksnotReturned = 1 then 'Yes' else 'No' end,   
  
    LOC_OCPreferredalocalservice = case when com.LOC_OCPreferredalocalservice = 1 then 'Yes' else 'No' end,   
    WebsiteIssues = case when com.WebsiteIssues = 1 then 'Yes' else 'No' end,   
  
    INC_OCToldIncorrectThings = case when com.INC_OCToldIncorrectThings = 1 then 'Yes' else 'No' end,   
    PLS_HardToSpeakToPL = case when com.PLS_HardToSpeakToPL = 1 then 'Yes' else 'No' end,   
    EA_EASaidItWouldTakeLessTime = case when com.EA_EASaidItWouldTakeLessTime = 1 then 'Yes' else 'No' end,   
    COMP_ErrorAtCompletion = case when com.COMP_ErrorAtCompletion = 1 then 'Yes' else 'No' end,   
    POC_FewerPointsOfContact = case when com.POC_FewerPointsOfContact = 1 then 'Yes' else 'No' end,   
    CW_LongCallWaits = case when com.CW_LongCallWaits = 1 then 'Yes' else 'No' end,   
    INF_OCAskedForInformedAlreadyProvided = case when com.INF_OCAskedForInformedAlreadyProvided = 1 then 'Yes' else 'No' end,   
    RUD_OCWasSpokenToRudely = case when com.RUD_OCWasSpokenToRudely = 1 then 'Yes' else 'No' end,   
    Sdlt = case when com.Sdlt = 1 then 'Yes' else 'No' end,  
       ALLDelaysallocatingtoVFTeamPL=case when com.ALLDelaysallocatingtoVFTeamPL = 1 then 'Yes' else 'No' end,  
    objTran.TransactionStatus,  
    Hasnotebeenaddedtocriticaltoconfirmdissatisfaction = case when com.Hasnotebeenaddedtocriticaltoconfirmdissatisfaction = 1 then 'Yes' else 'No' end,   
    Ensureteammanagerisawareofdissat = case when com.Ensureteammanagerisawareofdissat = 1 then 'Yes' else 'No' end,  
       SendTCFexamplestoMelBeatty=case when com.SendTCFexamplestoMelBeatty = 1 then 'Yes' else 'No' end,  
    objCheck.CETCheck,  
    com.Resolution,  
    objsingle.SingleMulti,  
    com.Name,  
    com.AttachFile  
	
  INTO #FilterResult
  
from  [dbo].[tblDissatisfactionForm]  com   
  
 left join [dbo].[tblCenterName] center on com.CenterID = center.Id  
 left join [dbo].[tblTeamName] team on com.TeamID = team.ID  
 left join [dbo].[tblRegion] reg on com.RegionID = reg.Id  
 left join [dbo].[tblExeccomplaint] objExec on com.Execcomplaint = objExec.Id  
 left join [dbo].[tblCompliantsSource] objSource on com.Source = objSource.Id  
 left join [dbo].[tblTransactionStatus] objTran on com.Transactionstatus = objTran.Id  
 left join [dbo].[tblSingleMulti] objsingle on com.SingleMultiId = objsingle.Id  
  
 left join [dbo].[tblCETCheck] objCheck on com.CETCheck = objCheck.Id  
  
  
 where com.id in (select distinct FormDataId  
    FROM [dbo].[tblTrackerForms]    
    with (nolock) where TrackerId=@TrackerId and ProcessId=@processId  )   
 order by 1 asc  

 
 DECLARE @sql nvarchar(max);

SELECT  @sql = 'SELECT * FROM #FilterResult ' + dbo.FuncDynamicQueryResult(@TrackerId, @ViewName)

EXEC sp_executesql @sql
  
END TRY  
BEGIN CATCH  
  
        DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),  
                @ErrorSeverity INT = ERROR_SEVERITY(),  
                @ErrorNumber INT = ERROR_NUMBER();  
  
  RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);  
   
END CATCH  
DROP TABLE #FilterResult  
  
End      
      
GO
/****** Object:  StoredProcedure [dbo].[usp_DissatisfactionForm_AuditView]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


--Exec usp_DissatisfactionForm_AuditView 6,null,null
CREATE PROCEDURE [dbo].[usp_DissatisfactionForm_AuditView]
	-- Add the parameters for the stored procedure here
	@ID Int,  
	@FromDate datetime2 = '1900-01-01 00:00:00.0000000', 
	@ToDate datetime2	= '9999-12-31 23:59:59.9999999'
AS
BEGIN

  BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    
;WITH T
AS (
    SELECT		dis.ID
	            ,Receivedfrom as Receivedfrom , lag(Receivedfrom) over(order by enddate asc) as Prev_Receivedfrom
				,objExec.Execcomplaint as Execcomplaint , lag(objExec.Execcomplaint) over(order by enddate asc) as Prev_Execcomplaint
				,SourceName as Source , lag(SourceName) over(order by enddate asc) as Prev_Source
				,Datereceived as Datereceived , lag(Datereceived) over(order by enddate asc) as Prev_Datereceived
				,MatterNumberID as MatterNumberID , lag(MatterNumberID) over(order by enddate asc) as Prev_MatterNumberID
				,ABS as ABS , lag(ABS) over(order by enddate asc) as Prev_ABS
				,CenterName as Center , lag(CenterName) over(order by enddate asc) as Prev_Center
				,TeamName as Team , lag(TeamName) over(order by enddate asc) as Prev_Team
				,SubsidaryBranch as SubsidaryBranch , lag(SubsidaryBranch) over(order by enddate asc) as Prev_SubsidaryBranch
				,Region as Region , lag(Region) over(order by enddate asc) as Prev_Region
				,AttributedTo as AttributedTo , lag(AttributedTo) over(order by enddate asc) as Prev_AttributedTo
				,Keyareasofdissat as Keyareasofdissat , lag(Keyareasofdissat) over(order by enddate asc) as Prev_Keyareasofdissat
				,ISNULL(UpdatesUD ,0) as UpdatesUD ,ISNULL( lag(UpdatesUD) over(order by enddate asc) ,0) as Prev_UpdatesUD
				,ISNULL(PPW_PaperworkWentMissing ,0) as PPW_PaperworkWentMissing ,ISNULL( lag(PPW_PaperworkWentMissing) over(order by enddate asc) ,0) as Prev_PPW_PaperworkWentMissing
				,ISNULL(TS_OCNotHappyWithTimescales ,0) as TS_OCNotHappyWithTimescales ,ISNULL( lag(TS_OCNotHappyWithTimescales) over(order by enddate asc) ,0) as Prev_TS_OCNotHappyWithTimescales
				,ISNULL(ERR_ErrorsOnPaperwork ,0) as ERR_ErrorsOnPaperwork ,ISNULL( lag(ERR_ErrorsOnPaperwork) over(order by enddate asc) ,0) as Prev_ERR_ErrorsOnPaperwork
				,ISNULL(CB_CallBacksnotReturned ,0) as CB_CallBacksnotReturned ,ISNULL( lag(CB_CallBacksnotReturned) over(order by enddate asc) ,0) as Prev_CB_CallBacksnotReturned
				,ISNULL(LOC_OCPreferredalocalservice ,0) as LOC_OCPreferredalocalservice ,ISNULL( lag(LOC_OCPreferredalocalservice) over(order by enddate asc) ,0) as Prev_LOC_OCPreferredalocalservice
				,ISNULL(WebsiteIssues ,0) as WebsiteIssues ,ISNULL( lag(WebsiteIssues) over(order by enddate asc) ,0) as Prev_WebsiteIssues
				,ISNULL(INC_OCToldIncorrectThings ,0) as INC_OCToldIncorrectThings ,ISNULL( lag(INC_OCToldIncorrectThings) over(order by enddate asc) ,0) as Prev_INC_OCToldIncorrectThings
				,ISNULL(PLS_HardToSpeakToPL ,0) as PLS_HardToSpeakToPL ,ISNULL( lag(PLS_HardToSpeakToPL) over(order by enddate asc) ,0) as Prev_PLS_HardToSpeakToPL
				,ISNULL(EA_EASaidItWouldTakeLessTime ,0) as EA_EASaidItWouldTakeLessTime ,ISNULL( lag(EA_EASaidItWouldTakeLessTime) over(order by enddate asc) ,0) as Prev_EA_EASaidItWouldTakeLessTime
				,ISNULL(COMP_ErrorAtCompletion ,0) as COMP_ErrorAtCompletion ,ISNULL( lag(COMP_ErrorAtCompletion) over(order by enddate asc) ,0) as Prev_COMP_ErrorAtCompletion
				,ISNULL(POC_FewerPointsOfContact ,0) as POC_FewerPointsOfContact ,ISNULL( lag(POC_FewerPointsOfContact) over(order by enddate asc) ,0) as Prev_POC_FewerPointsOfContact
				,ISNULL(CW_LongCallWaits ,0) as CW_LongCallWaits ,ISNULL( lag(CW_LongCallWaits) over(order by enddate asc) ,0) as Prev_CW_LongCallWaits
				,ISNULL(INF_OCAskedForInformedAlreadyProvided ,0) as INF_OCAskedForInformedAlreadyProvided ,ISNULL( lag(INF_OCAskedForInformedAlreadyProvided) over(order by enddate asc) ,0) as Prev_INF_OCAskedForInformedAlreadyProvided
				,ISNULL(RUD_OCWasSpokenToRudely ,0) as RUD_OCWasSpokenToRudely ,ISNULL( lag(RUD_OCWasSpokenToRudely) over(order by enddate asc) ,0) as Prev_RUD_OCWasSpokenToRudely
				,ISNULL(Sdlt ,0) as Sdlt ,ISNULL( lag(Sdlt) over(order by enddate asc) ,0) as Prev_Sdlt
				,ISNULL(ALLDelaysallocatingtoVFTeamPL ,0) as ALLDelaysallocatingtoVFTeamPL ,ISNULL( lag(ALLDelaysallocatingtoVFTeamPL) over(order by enddate asc) ,0) as Prev_ALLDelaysallocatingtoVFTeamPL
				,objTran.Transactionstatus as Transactionstatus , lag(objTran.Transactionstatus) over(order by enddate asc) as Prev_Transactionstatus
				,ISNULL(Hasnotebeenaddedtocriticaltoconfirmdissatisfaction ,0) as Hasnotebeenaddedtocriticaltoconfirmdissatisfaction ,ISNULL( lag(Hasnotebeenaddedtocriticaltoconfirmdissatisfaction) over(order by enddate asc) ,0) as Prev_Hasnotebeenaddedtocriticaltoconfirmdissatisfaction
				,ISNULL(Ensureteammanagerisawareofdissat ,0) as Ensureteammanagerisawareofdissat ,ISNULL( lag(Ensureteammanagerisawareofdissat) over(order by enddate asc) ,0) as Prev_Ensureteammanagerisawareofdissat
				,ISNULL(SendTCFexamplestoMelBeatty ,0) as SendTCFexamplestoMelBeatty ,ISNULL( lag(SendTCFexamplestoMelBeatty) over(order by enddate asc) ,0) as Prev_SendTCFexamplestoMelBeatty
				,objCheck.CETCheck as CETCheck , lag(objCheck.CETCheck) over(order by enddate asc) as Prev_CETCheck
				,Resolution as Resolution , lag(Resolution) over(order by enddate asc) as Prev_Resolution
				,SingleMulti as SingleMulti , lag(SingleMulti) over(order by enddate asc) as Prev_SingleMulti
				,AttachFile as AttachFile , lag(AttachFile) over(order by enddate asc) as Prev_AttachFile
				,ModifyingUser as ModifyingUser , lag(ModifyingUser) over(order by enddate asc) as Prev_ModifyingUser
				,Action as Action , lag(Action) over(order by enddate asc) as Prev_Action
				,BeginDate
				,EndDate
				,ROW_Number() over(partition by  dis.Id Order by EndDate asc) as VersionNumber
    FROM dbo.tblDissatisfactionForm FOR SYSTEM_TIME ALL AS dis   
  
 left join [dbo].[tblCenterName] center on dis.CenterID = center.Id  
 left join [dbo].[tblTeamName] team on dis.TeamID = team.ID  
 left join [dbo].[tblRegion] reg on dis.RegionID = reg.Id  
 left join [dbo].[tblExeccomplaint] objExec on dis.Execcomplaint = objExec.Id  
 left join [dbo].[tblCompliantsSource] objSource on dis.Source = objSource.Id  
 left join [dbo].[tblTransactionStatus] objTran on dis.Transactionstatus = objTran.Id  
 left join [dbo].[tblSingleMulti] objsingle on dis.SingleMultiId = objsingle.Id  
  
 left join [dbo].[tblCETCheck] objCheck on dis.CETCheck = objCheck.Id 
    WHERE dis.id =@ID
)
SELECT T.BeginDate,
       T.EndDate,
       C.ColName as ColumnName,
	   t.VersionNumber,	   
       C.present_Value as CurrentValue,
       C.prev_value as PreviousValue,
	   T.ModifyingUser as ModifiedBy
FROM T
    CROSS APPLY
    (
        VALUES
		     ('Received From', Convert(varchar, T.Receivedfrom) , Convert(varchar,T.Prev_Receivedfrom))
			,('Exec complaint?', Convert(varchar,T.Execcomplaint) , Convert(varchar,T.Prev_Execcomplaint))
			,('Source', Convert(varchar, T.Source) , Convert(varchar,T.Prev_Source))
			,('Date received', Convert(varchar(10), T.Datereceived,103) , Convert(varchar(10),T.Prev_Datereceived,103))
			,('Customer Matter Number', Convert(varchar, T.MatterNumberID) , Convert(varchar,T.Prev_MatterNumberID))
			,('ABS', Convert(varchar, T.ABS) , Convert(varchar,T.Prev_ABS))
			,('Center', Convert(varchar, T.Center) , Convert(varchar,T.Prev_Center))
			,('Team', Convert(varchar, T.Team) , Convert(varchar,T.Prev_Team))
			,('Subsidiary / Branch', Convert(varchar, T.SubsidaryBranch) , Convert(varchar,T.Prev_SubsidaryBranch))
			,('Region', Convert(varchar, T.Region) , Convert(varchar,T.Prev_Region))
			,('Attributed to?', Convert(varchar, T.AttributedTo) , Convert(varchar,T.Prev_AttributedTo))
			,('Key areas of dissat', Convert(varchar, T.Keyareasofdissat) , Convert(varchar,T.Prev_Keyareasofdissat))
			,('UD-Updates', Convert(varchar, case when  T.UpdatesUD =1 then 'Yes' else 'No' end) , Convert(varchar, case when T.Prev_UpdatesUD =1 then 'Yes' else 'No' end))
			,('PPW-Paperwork went missing', Convert(varchar, case when  T.PPW_PaperworkWentMissing =1 then 'Yes' else 'No' end) , Convert(varchar, case when T.Prev_PPW_PaperworkWentMissing =1 then 'Yes' else 'No' end))
			,('TS-OC not happy with timescales', Convert(varchar, case when  T.TS_OCNotHappyWithTimescales =1 then 'Yes' else 'No' end) , Convert(varchar, case when T.Prev_TS_OCNotHappyWithTimescales =1 then 'Yes' else 'No' end))
			,('ERR-Errors on paperwork', Convert(varchar, case when  T.ERR_ErrorsOnPaperwork =1 then 'Yes' else 'No' end) , Convert(varchar, case when T.Prev_ERR_ErrorsOnPaperwork =1 then 'Yes' else 'No' end))
			,('CB-Call backs not returned', Convert(varchar, case when  T.CB_CallBacksnotReturned =1 then 'Yes' else 'No' end) , Convert(varchar, case when T.Prev_CB_CallBacksnotReturned =1 then 'Yes' else 'No' end))
			,('LOC-OC preferred a local service', Convert(varchar, case when  T.LOC_OCPreferredalocalservice =1 then 'Yes' else 'No' end) , Convert(varchar, case when T.Prev_LOC_OCPreferredalocalservice =1 then 'Yes' else 'No' end))
			,('WEB-Website issues', Convert(varchar, case when  T.WebsiteIssues =1 then 'Yes' else 'No' end) , Convert(varchar, case when T.Prev_WebsiteIssues =1 then 'Yes' else 'No' end))
			,('INC-OC told incorrect things', Convert(varchar, case when  T.INC_OCToldIncorrectThings =1 then 'Yes' else 'No' end) , Convert(varchar, case when T.Prev_INC_OCToldIncorrectThings =1 then 'Yes' else 'No' end))
			,('PLS-Hard to speak to PL', Convert(varchar, case when  T.PLS_HardToSpeakToPL =1 then 'Yes' else 'No' end) , Convert(varchar, case when T.Prev_PLS_HardToSpeakToPL =1 then 'Yes' else 'No' end))
			,('EA-EA said it would take less time', Convert(varchar, case when  T.EA_EASaidItWouldTakeLessTime =1 then 'Yes' else 'No' end) , Convert(varchar, case when T.Prev_EA_EASaidItWouldTakeLessTime =1 then 'Yes' else 'No' end))
			,('COMP-Error at completion', Convert(varchar, case when  T.COMP_ErrorAtCompletion =1 then 'Yes' else 'No' end) , Convert(varchar, case when T.Prev_COMP_ErrorAtCompletion =1 then 'Yes' else 'No' end))
			,('POC-Fewer points of contact', Convert(varchar, case when  T.POC_FewerPointsOfContact =1 then 'Yes' else 'No' end) , Convert(varchar, case when T.Prev_POC_FewerPointsOfContact =1 then 'Yes' else 'No' end))
			,('CW-Long call waits', Convert(varchar, case when  T.CW_LongCallWaits =1 then 'Yes' else 'No' end) , Convert(varchar, case when T.Prev_CW_LongCallWaits =1 then 'Yes' else 'No' end))
			,('INF-Asked for information already provided', Convert(varchar, case when  T.INF_OCAskedForInformedAlreadyProvided =1 then 'Yes' else 'No' end) , Convert(varchar, case when T.Prev_INF_OCAskedForInformedAlreadyProvided =1 then 'Yes' else 'No' end))
			,('RUD-Client was spoken to rudely', Convert(varchar, case when  T.RUD_OCWasSpokenToRudely =1 then 'Yes' else 'No' end) , Convert(varchar, case when T.Prev_RUD_OCWasSpokenToRudely =1 then 'Yes' else 'No' end))
			,('SDLT', Convert(varchar, case when  T.Sdlt =1 then 'Yes' else 'No' end) , Convert(varchar, case when T.Prev_Sdlt =1 then 'Yes' else 'No' end))
			,('ALL - Delays allocating to VF/Team/PL', Convert(varchar, case when  T.ALLDelaysallocatingtoVFTeamPL =1 then 'Yes' else 'No' end) , Convert(varchar, case when T.Prev_ALLDelaysallocatingtoVFTeamPL =1 then 'Yes' else 'No' end))
			,('Transaction status', Convert(varchar, T.Transactionstatus) , Convert(varchar,T.Prev_Transactionstatus))
			,('Has note been added to critical to confirm dissatisfaction?', Convert(varchar, case when  T.Hasnotebeenaddedtocriticaltoconfirmdissatisfaction =1 then 'Yes' else 'No' end) , Convert(varchar, case when T.Prev_Hasnotebeenaddedtocriticaltoconfirmdissatisfaction =1 then 'Yes' else 'No' end))
			,('Ensure team manager is aware of dissat', Convert(varchar, case when  T.Ensureteammanagerisawareofdissat =1 then 'Yes' else 'No' end) , Convert(varchar, case when T.Prev_Ensureteammanagerisawareofdissat =1 then 'Yes' else 'No' end))
			,('Send TCF examples to Mel Beatty', Convert(varchar, case when T.SendTCFexamplestoMelBeatty=1 then 'Yes' else 'No' end) , Convert(varchar,case when T.Prev_SendTCFexamplestoMelBeatty=1 then 'Yes' else 'No' end))

			--,('UD-Updates', Convert(varchar, T.UpdatesUD) , Convert(varchar,T.Prev_UpdatesUD))
			--,('PPW-Paperwork went missing', Convert(varchar, T.PPW_PaperworkWentMissing) , Convert(varchar,T.Prev_PPW_PaperworkWentMissing))
			--,('TS-OC not happy with timescales', Convert(varchar, T.TS_OCNotHappyWithTimescales) , Convert(varchar,T.Prev_TS_OCNotHappyWithTimescales))
			--,('ERR-Errors on paperwork', Convert(varchar, T.ERR_ErrorsOnPaperwork) , Convert(varchar,T.Prev_ERR_ErrorsOnPaperwork))
			--,('CB-Call backs not returned', Convert(varchar, T.CB_CallBacksnotReturned) , Convert(varchar,T.Prev_CB_CallBacksnotReturned))
			--,('LOC-OC preferred a local service', Convert(varchar, T.LOC_OCPreferredalocalservice) , Convert(varchar,T.Prev_LOC_OCPreferredalocalservice))
			--,('WEB-Website issues', Convert(varchar, T.WebsiteIssues) , Convert(varchar,T.Prev_WebsiteIssues))
			--,('INC-OC told incorrect things', Convert(varchar, T.INC_OCToldIncorrectThings) , Convert(varchar,T.Prev_INC_OCToldIncorrectThings))
			--,('PLS-Hard to speak to PL', Convert(varchar, T.PLS_HardToSpeakToPL) , Convert(varchar,T.Prev_PLS_HardToSpeakToPL))
			--,('EA-EA said it would take less time', Convert(varchar, T.EA_EASaidItWouldTakeLessTime) , Convert(varchar,T.Prev_EA_EASaidItWouldTakeLessTime))
			--,('COMP-Error at completion', Convert(varchar, T.COMP_ErrorAtCompletion) , Convert(varchar,T.Prev_COMP_ErrorAtCompletion))
			--,('POC-Fewer points of contact', Convert(varchar, T.POC_FewerPointsOfContact) , Convert(varchar,T.Prev_POC_FewerPointsOfContact))
			--,('CW-Long call waits', Convert(varchar, T.CW_LongCallWaits) , Convert(varchar,T.Prev_CW_LongCallWaits))
			--,('INF-Asked for information already provided', Convert(varchar, T.INF_OCAskedForInformedAlreadyProvided) , Convert(varchar,T.Prev_INF_OCAskedForInformedAlreadyProvided))
			--,('RUD-Client was spoken to rudely', Convert(varchar, T.RUD_OCWasSpokenToRudely) , Convert(varchar,T.Prev_RUD_OCWasSpokenToRudely))
			--,('SDLT', Convert(varchar, T.Sdlt) , Convert(varchar,T.Prev_Sdlt))
			--,('ALL - Delays allocating to VF/Team/PL', Convert(varchar, T.ALLDelaysallocatingtoVFTeamPL) , Convert(varchar,T.Prev_ALLDelaysallocatingtoVFTeamPL))
			--,('Has note been added to critical to confirm dissatisfaction?', Convert(varchar, T.Hasnotebeenaddedtocriticaltoconfirmdissatisfaction) , Convert(varchar,T.Prev_Hasnotebeenaddedtocriticaltoconfirmdissatisfaction))
			--,('Ensure team manager is aware of dissat', Convert(varchar, T.Ensureteammanagerisawareofdissat) , Convert(varchar,T.Prev_Ensureteammanagerisawareofdissat))
			--,('Send TCF examples to Mel Beatty', Convert(varchar, T.SendTCFexamplestoMelBeatty) , Convert(varchar,T.Prev_SendTCFexamplestoMelBeatty))
			,('CET Check', Convert(varchar, T.CETCheck) , Convert(varchar,T.Prev_CETCheck))
			,('Resolution', Convert(varchar, T.Resolution) , Convert(varchar,T.Prev_Resolution))
			,('Single / Multi', Convert(varchar, T.SingleMulti) , Convert(varchar,T.Prev_SingleMulti))
			,('AttachFile', Convert(varchar, T.AttachFile) , Convert(varchar,T.Prev_AttachFile))
			,('ModifyingUser', Convert(varchar, T.ModifyingUser) , Convert(varchar,T.Prev_ModifyingUser))
			,('Action', Convert(varchar, T.Action) , Convert(varchar,T.Prev_Action))
			) AS C (ColName, present_Value, prev_value)
WHERE  EXISTS
(
    SELECT present_Value 
    EXCEPT 
    SELECT C.prev_value
)  and Convert(datetime,BeginDate) >= @FromDate and Convert(Datetime,BeginDate) <=@ToDate

END TRY
BEGIN CATCH

        DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),
                @ErrorSeverity INT = ERROR_SEVERITY(),
                @ErrorNumber INT = ERROR_NUMBER();

		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);
 
END CATCH
END

GO
/****** Object:  StoredProcedure [dbo].[usp_DissatisfactionForm_InsertUpdate]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author : kumari>
-- Create date: <10-04-2021>
-- Modified date : <11-04-2021> <13-04-2021,Sundar> <Included Exception handling>
-- Description:	<Description,The purpose of the Stored Procedure is to make CRUD operations on DissatisfactionForm tables>
-- Generic Info: <I - Insert , U- Update, D- Delete R-Re activate>  
-- =============================================

--Exec [usp_DissatisfactionForm_InsertUpdate] @id=0,@Receivedfrom='kumari',@Execcomplaint=1,@Source=1,@Datereceived='2021/04/12',@MatterNumberID='25654',@ABS=0,  @CenterID=1,
--@TeamID=1,@SubsidaryBranch='test',@RegionID=1,@AttributedTo='test',@AttachFile='test',@Keyareasofdissat='test',@SingleMultiId=1,@CETCheck=1,@Hasnotebeenaddedtocriticaltoconfirmdissatisfaction=1,@Ensureteammanagerisawareofdissat=1,@StatementType='I',@TrackerId=4,@CreatedBy='kumaric',@processid=101,@RoutingID=0
--Exec [usp_DissatisfactionForm_InsertUpdate] @id=8, @Receivedfrom='kumari',@Execcomplaint=1,@Source=1,@Datereceived='2021/04/12',@MatterNumberID='25654',@ABS=0,  @CenterID=1,
--@TeamID=1,@SubsidaryBranch='test',@RegionID=1,@AttributedTo='test',@AttachFile='test',@Keyareasofdissat='test',@SingleMultiId=1,@CETCheck=1,@Hasnotebeenaddedtocriticaltoconfirmdissatisfaction=1,@Ensureteammanagerisawareofdissat=1,@StatementType='U',@TrackerId=4,@CreatedBy='kumaric',@processid=201,@RoutingID=1
--Exec [usp_DissatisfactionForm_InsertUpdate] @id=6,@Receivedfrom='kumari',@Execcomplaint=1,@Source=1,@Datereceived='2021/04/12',@MatterNumberID='25654',@ABS=0,  @CenterID=1,
--@TeamID=1,@SubsidaryBranch='test',@RegionID=1,@AttributedTo='test',@AttachFile='test',@Keyareasofdissat='test',@SingleMultiId=1,@CETCheck=1,@Hasnotebeenaddedtocriticaltoconfirmdissatisfaction=1,@Ensureteammanagerisawareofdissat=1,@StatementType='D',@TrackerId=4,@CreatedBy='kumaric',@processid=201,@RoutingID=0,@selectedData ='8'  
--Exec [usp_DissatisfactionForm_InsertUpdate] @id=6,@Receivedfrom='kumari',@Execcomplaint=1,@Source=1,@Datereceived='2021/04/12',@MatterNumberID='25654',@ABS=0,  @CenterID=1,
--@TeamID=1,@SubsidaryBranch='test',@RegionID=1,@AttributedTo='test',@AttachFile='test',@Keyareasofdissat='test',@SingleMultiId=1,@CETCheck=1,@Hasnotebeenaddedtocriticaltoconfirmdissatisfaction=1,@Ensureteammanagerisawareofdissat=1,@StatementType='R',@TrackerId=4,@CreatedBy='kumaric',@processid=601,@RoutingID=9,@selectedData ='8'  

-- Delete [spDissatisfactionForm] 0,'1356','25654',1000,'test1',1,'D',9,'csuman',201,0,'31,32,33'
-- Reactivate  [spDissatisfactionForm] 0,'1356','25654',1000,'test1',1,'R',9,'csuman',601,9,'33'




CREATE PROCEDURE [dbo].[usp_DissatisfactionForm_InsertUpdate] 
	-- Add the parameters for the stored procedure here
 @ID int = 0,  
 @Receivedfrom  varchar(50)=null,    
 @Execcomplaint  int  =null,    
 @Source  int  =null,    
 @Datereceived   datetime =null ,    
 @MatterNumberID   varchar(100)=null ,    
 @ABS   bit =null ,    
 @CenterID   int  =null,    
 @TeamID   int  =null,    
 @SubsidaryBranch   varchar(50) =null,    
 @RegionID   int  =null,    
 @AttributedTo   varchar(50)=null ,    
 @Keyareasofdissat   varchar(max)=null ,    
 @UpdatesUD   bit=null,    
 @PPW_PaperworkWentMissing   bit=null  ,    
 @TS_OCNotHappyWithTimescales   bit =null ,    
 @ERR_ErrorsOnPaperwork   bit=null  ,    
 @CB_CallBacksnotReturned   bit  =null,    
 @LOC_OCPreferredalocalservice   bit =null ,    
 @WebsiteIssues   bit =null ,    
 @INC_OCToldIncorrectThings   bit =null ,    
 @PLS_HardToSpeakToPL   bit =null ,    
 @EA_EASaidItWouldTakeLessTime   bit  =null,    
 @COMP_ErrorAtCompletion   bit =null ,    
 @POC_FewerPointsOfContact   bit =null ,    
 @CW_LongCallWaits   bit  =null,    
 @INF_OCAskedForInformedAlreadyProvided   bit =null ,    
 @RUD_OCWasSpokenToRudely   bit =null ,    
 @Sdlt   bit =null ,    
 @ALLDelaysallocatingtoVFTeamPL   bit =null ,    
 @Transactionstatus   int  =null,    
 @Hasnotebeenaddedtocriticaltoconfirmdissatisfaction   bit=null ,    
 @Ensureteammanagerisawareofdissat   bit=null ,    
 @SendTCFexamplestoMelBeatty   bit =null ,    
 @CETCheck   int  =null,    
 @Resolution   varchar (200) =null,    
 @SingleMultiId   int =null ,    
 @Name   varchar(100)=null,    
 @AttachFile   varchar(max)=null,  
  
 @StatementType  Varchar(50)=NULL,      
 @TrackerId int =NULL,        
 @CreatedBy NVarchar(256)=NULL,      
 @processid int =NULL,      
 @RoutingID Int=NULL,
 @selectedData varchar(100) = NULL  
AS
BEGIN
BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements
	SET NOCOUNT ON;


	Declare @trgProcessId int;   
    -- This performs INSERT action against ClientToClientTransfersForm table
IF @StatementType = 'I'          
 BEGIN       
       
   INSERT INTO [dbo].[tblDissatisfactionForm]    
           ([Receivedfrom]    
           ,[Execcomplaint]    
           ,[Source]    
           ,[Datereceived]    
           ,[MatterNumberID]    
           ,[ABS]    
           ,[CenterID]    
           ,[TeamID]    
           ,[SubsidaryBranch]    
           ,[RegionID]    
           ,[AttributedTo]    
           ,[Keyareasofdissat]    
           ,[UpdatesUD]    
           ,[PPW_PaperworkWentMissing]    
           ,[TS_OCNotHappyWithTimescales]    
           ,[ERR_ErrorsOnPaperwork]    
           ,[CB_CallBacksnotReturned]    
           ,[LOC_OCPreferredalocalservice]    
           ,[WebsiteIssues]    
           ,[INC_OCToldIncorrectThings]    
           ,[PLS_HardToSpeakToPL]    
           ,[EA_EASaidItWouldTakeLessTime]    
           ,[COMP_ErrorAtCompletion]    
           ,[POC_FewerPointsOfContact]    
           ,[CW_LongCallWaits]    
           ,[INF_OCAskedForInformedAlreadyProvided]    
           ,[RUD_OCWasSpokenToRudely]    
           ,[Sdlt]    
           ,[ALLDelaysallocatingtoVFTeamPL]    
           ,[Transactionstatus]    
           ,[Hasnotebeenaddedtocriticaltoconfirmdissatisfaction]    
           ,[Ensureteammanagerisawareofdissat]    
           ,[SendTCFexamplestoMelBeatty]    
           ,[CETCheck]    
           ,[Resolution]    
           ,[SingleMultiId]    
           ,[Name]    
          ,AttachFile
   , ModifyingUser 
	,[Action]) 
     VALUES(    
  @Receivedfrom,    
  @Execcomplaint,    
  @Source,    
  @Datereceived,    
  @MatterNumberID,    
  @ABS,    
  @CenterID,    
  @TeamID,    
  @SubsidaryBranch,    
  @RegionID,    
  @AttributedTo,    
  @Keyareasofdissat,    
  @UpdatesUD,    
  @PPW_PaperworkWentMissing,    
  @TS_OCNotHappyWithTimescales,    
  @ERR_ErrorsOnPaperwork,    
  @CB_CallBacksnotReturned,    
  @LOC_OCPreferredalocalservice,    
  @WebsiteIssues,    
  @INC_OCToldIncorrectThings,    
  @PLS_HardToSpeakToPL,    
  @EA_EASaidItWouldTakeLessTime,    
  @COMP_ErrorAtCompletion,    
  @POC_FewerPointsOfContact,    
  @CW_LongCallWaits,    
  @INF_OCAskedForInformedAlreadyProvided,    
  @RUD_OCWasSpokenToRudely,    
  @Sdlt,    
  @ALLDelaysallocatingtoVFTeamPL,    
  @Transactionstatus,    
  @Hasnotebeenaddedtocriticaltoconfirmdissatisfaction,    
  @Ensureteammanagerisawareofdissat,    
  @SendTCFexamplestoMelBeatty,    
  @CETCheck,    
  @Resolution,    
  @SingleMultiId,    
  @Name,    
 @AttachFile,
	@CreatedBy,
	@StatementType)  
    Declare @FormDataId int;        
	set @FormDataId = SCOPE_IDENTITY()        
      
	SELECT @trgProcessId=trgProcessId FROM tblRoutingDetails
		WHERE TrackerId= @TrackerId AND srcprocessid = @processid AND RoutingID = @RoutingID      
  
   -- This is common across all the trackers and perofrms INSERT action against tbltrackersform table 
    EXEC usp_TrackersForm_Insert @TrackerId, @CreatedBy, @trgProcessId,  @FormDataId
   
 END   


  SELECT @trgProcessId=trgProcessId FROM tblRoutingDetails WHERE TrackerId= @TrackerId AND srcprocessid =@ProcessId          
 AND RoutingID = @RoutingID   



--This performs UPDATE action against ClientToClientTransfersForm table
IF @StatementType = 'U'          
  BEGIN       
	
   UPDATE [dbo].[tblDissatisfactionForm]
   SET [Receivedfrom] = @Receivedfrom
      ,[Execcomplaint] = @Execcomplaint
      ,[Source] = @Source
      ,[Datereceived] = @Datereceived
      ,[MatterNumberID] = @MatterNumberID
      ,[ABS] = @ABS
      ,[CenterID] = @CenterID
      ,[TeamID] = @TeamID
      ,[SubsidaryBranch] = @SubsidaryBranch
      ,[RegionID] = @RegionID
      ,[AttributedTo] = @AttributedTo
      ,[Keyareasofdissat] = @Keyareasofdissat
	  ,UpdatesUD = @UpdatesUD,    
	   PPW_PaperworkWentMissing = @PPW_PaperworkWentMissing,    
	   TS_OCNotHappyWithTimescales = @TS_OCNotHappyWithTimescales,    
	   ERR_ErrorsOnPaperwork = @ERR_ErrorsOnPaperwork,    
	   CB_CallBacksnotReturned = @CB_CallBacksnotReturned,    
	   LOC_OCPreferredalocalservice = @LOC_OCPreferredalocalservice,    
	   WebsiteIssues = @WebsiteIssues,    
	   INC_OCToldIncorrectThings = @INC_OCToldIncorrectThings,    
	   PLS_HardToSpeakToPL = @PLS_HardToSpeakToPL,    
	   EA_EASaidItWouldTakeLessTime = @EA_EASaidItWouldTakeLessTime,    
	   COMP_ErrorAtCompletion= @COMP_ErrorAtCompletion,    
	   POC_FewerPointsOfContact = @POC_FewerPointsOfContact,    
	   CW_LongCallWaits = @CW_LongCallWaits,    
	   INF_OCAskedForInformedAlreadyProvided = @INF_OCAskedForInformedAlreadyProvided,    
	   RUD_OCWasSpokenToRudely = @RUD_OCWasSpokenToRudely,    
	   Sdlt = @Sdlt	
      ,[ALLDelaysallocatingtoVFTeamPL] = @ALLDelaysallocatingtoVFTeamPL
      ,[Transactionstatus] = @Transactionstatus
      ,[Hasnotebeenaddedtocriticaltoconfirmdissatisfaction] = @Hasnotebeenaddedtocriticaltoconfirmdissatisfaction 
      ,[Ensureteammanagerisawareofdissat] = @Ensureteammanagerisawareofdissat
      ,[SendTCFexamplestoMelBeatty] = @SendTCFexamplestoMelBeatty
      ,[CETCheck] = @CETCheck
      ,[Resolution] = @Resolution
      ,[SingleMultiId] = @SingleMultiId
      ,[Name] = @Name
      ,AttachFile = @AttachFile  
	 , ModifyingUser = @CreatedBy
	,[Action] = @StatementType
where Id = @Id    
    
  
	EXEC [dbo].[usp_TrackersForm_Update] @ID,@TrackerId, @ProcessId,@CreatedBy, @RoutingID, @selectedData,@StatementType   
           
        
     
  END   
  
--This performs DELETE/RE ACTIVATE action against ClientToClientTransfersForm table
IF (@StatementType = 'D' OR  @StatementType = 'R')        
  BEGIN       


    UPDATE dbo.[tblDissatisfactionForm] 
			SET  
				   [ModifyingUser] = @CreatedBy,  
				   [Action] = @StatementType  
	WHERE Id IN (SELECT CAST(value AS INTEGER) FROM STRING_SPLIT(@selectedData, ','))         
  
   -- This is common across all the trackers and UPDATING Process id in tbltrackersform table based on Routing details
   	EXEC [dbo].[usp_TrackersForm_Update] @id, @TrackerId, @ProcessId,@CreatedBy, @RoutingID, @selectedData,@StatementType
     
  END  

  END TRY
BEGIN CATCH

        DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),
                @ErrorSeverity INT = ERROR_SEVERITY(),
                @ErrorNumber INT = ERROR_NUMBER();

		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);
 
END CATCH
	
END
GO
/****** Object:  StoredProcedure [dbo].[usp_DynamicViewConfiguration_Update]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
-- [sp_updateConfiguration] 4,'Review','1,2,3,4','1_2,2_1,3_4,4_3'    
CREATE Procedure [dbo].[usp_DynamicViewConfiguration_Update]    
@Trackerid int,
@ViewName varchar(50),
@SelectedColumnData varchar(100),
@SelectedOrderwithId varchar(100)

AS      
Begin      
 

 UPDATE [dbo].[tblDynamicViewConfiguration] SET IsSelected=0,isOrdered=0 WHERE TrackerRefId=@Trackerid and ViewName=@ViewName

 UPDATE [dbo].[tblDynamicViewConfiguration]  SET IsSelected=1 WHERE Id IN(SELECT CAST(value AS INTEGER) FROM STRING_SPLIT(@SelectedColumnData, ','))

 SELECT  PARSENAME(REPLACE(value, '_', '.'), 2) AS SelectedIdValue,PARSENAME(REPLACE(value, '_', '.'), 1) AS orderedValue
				INTO #selectDataTable from (select value 
											from 
											STRING_SPLIT(@SelectedOrderwithId,',')
											) as cl
	
  UPDATE [dbo].[tblDynamicViewConfiguration]  SET isOrdered=sel.orderedValue
		 
				from [tblDynamicViewConfiguration] join #selectDataTable as sel on sel.SelectedIdValue=[tblDynamicViewConfiguration].Id

	
  DROP TABLE #selectDataTable	

      
End    
    
GO
/****** Object:  StoredProcedure [dbo].[usp_ForfeitOfLease_Download]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Modified date: <16-04-2021> <Included Exception handling>
--[usp_ForfeitOfLease_Download]   13 , 201,'view1'
CREATE Procedure [dbo].[usp_ForfeitOfLease_Download]             
@TrackerId int,            
@processId int,
@ViewName Varchar(100)                
            
AS                
Begin           
 BEGIN TRY      
 Select  
 ID = FEIT.ID ,  
 ClientName ,  
 MatterNumber ,  
 CENTER.CenterName as Center ,  
 PropertyAddress ,  
 ExchangeDate ,  
 CompletionDate ,  
 PropertyPrice,  
 PremiumIPAmount ,  
 FeeEarner ,  
 CAOnly = case when FEIT.CAOnly = 1 then 'Yes' else 'No' end,  
 AttachFile   
 INTO #FilterResult
 From [dbo].[tblForfeitOfLeaseForm] FEIT  
 Left Join tblcentername CENTER on FEIT.CenterID = CENTER.ID  
  
 where FEIT.id in (select distinct FormDataId  
    FROM [dbo].[tblTrackerForms]    
    with (nolock) where TrackerId=@TrackerId and ProcessId=@processId  )   
 order by 1 asc  
  
   DECLARE @sql nvarchar(max);

SELECT  @sql = 'SELECT * FROM #FilterResult ' + dbo.FuncDynamicQueryResult(@TrackerId, @ViewName)

EXEC sp_executesql @sql


END TRY
BEGIN CATCH

        DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),
                @ErrorSeverity INT = ERROR_SEVERITY(),
                @ErrorNumber INT = ERROR_NUMBER();

		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);
 
END CATCH
DROP TABLE #FilterResult

End      
      
GO
/****** Object:  StoredProcedure [dbo].[usp_ForfeitOfLeaseForm_AuditView]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ajay George Kalathil
-- Create date: 6th May 2021
-- Description:	This SP will be used to get the Audit Trial Version View for the front end system
-- =============================================


--Exec usp_ForfeitOfLeaseForm_AuditView 1,null,null
CREATE PROCEDURE [dbo].[usp_ForfeitOfLeaseForm_AuditView]
	-- Add the parameters for the stored procedure here
	@ID Int,  
	@FromDate datetime2 = '1900-01-01 00:00:00.0000000', 
	@ToDate datetime2	= '9999-12-31 23:59:59.9999999'
AS
BEGIN

  BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    
;WITH T
AS (
    SELECT		FEIT.ID	
	            ,ClientName as ClientName , lag(ClientName) over(order by enddate asc) as Prev_ClientName
				,MatterNumber as MatterNumber , lag(MatterNumber) over(order by enddate asc) as Prev_MatterNumber				
				,CENTER.CenterName as Center , lag(CENTER.CenterName) over(order by enddate asc) as Prev_Center				
				,PropertyAddress as PropertyAddress , lag(PropertyAddress) over(order by enddate asc) as Prev_PropertyAddress
				,ExchangeDate as ExchangeDate , lag(ExchangeDate) over(order by enddate asc) as Prev_ExchangeDate
				,CompletionDate as CompletionDate , lag(CompletionDate) over(order by enddate asc) as Prev_CompletionDate
				,PropertyPrice as PropertyPrice , lag(PropertyPrice) over(order by enddate asc) as Prev_PropertyPrice
				,PremiumIPAmount as PremiumIPAmount , lag(PremiumIPAmount) over(order by enddate asc) as Prev_PremiumIPAmount
				,FeeEarner as FeeEarner , lag(FeeEarner) over(order by enddate asc) as Prev_FeeEarner				
				,ISNULL(CAOnly,0) as CAOnly , ISNULL(lag(CAOnly) over(order by enddate asc),0) as Prev_CAOnly				
				,AttachFile as AttachFile , lag(AttachFile) over(order by enddate asc) as Prev_AttachFile
				,ModifyingUser as ModifyingUser , lag(ModifyingUser) over(order by enddate asc) as Prev_ModifyingUser
				,Action as Action , lag(Action) over(order by enddate asc) as Prev_Action
				,BeginDate
				,EndDate
				,ROW_Number() over(partition by  FEIT.Id Order by EndDate asc) as VersionNumber
    FROM dbo.tblForfeitOfLeaseForm FOR SYSTEM_TIME ALL AS FEIT	  
 Left Join tblcentername CENTER on FEIT.CenterID = CENTER.ID  
    WHERE FEIT.id =@ID
)
SELECT T.BeginDate,
       T.EndDate,
       C.ColName as ColumnName,
	   t.VersionNumber,	   
       C.present_Value as CurrentValue,
       C.prev_value as PreviousValue,
	   T.ModifyingUser as ModifiedBy
FROM T
    CROSS APPLY
    (
        VALUES	
		     ('Client Name', Convert(varchar, T.ClientName) , Convert(varchar,T.Prev_ClientName))		
			,('Matter Number', Convert(varchar, T.MatterNumber) , Convert(varchar,T.Prev_MatterNumber))			
			,('Centre', Convert(varchar, T.Center) , Convert(varchar,T.Prev_Center))
			,('Property Address', Convert(varchar, T.PropertyAddress) , Convert(varchar,T.Prev_PropertyAddress))
			,('Exchange Date', Convert(varchar(10), T.ExchangeDate,103) , Convert(varchar(10),T.Prev_ExchangeDate,103))			
			,('Completion Date', Convert(varchar(10), T.CompletionDate,103) , Convert(varchar(10),T.Prev_CompletionDate,103))
			,('Property Price', Convert(varchar, T.PropertyPrice) , Convert(varchar,T.Prev_PropertyPrice))
			,('Premium IP Amount', Convert(varchar, T.PremiumIPAmount) , Convert(varchar,T.Prev_PremiumIPAmount))			
			,('FeeEarner', Convert(varchar, T.FeeEarner) , Convert(varchar,T.Prev_FeeEarner))
			,('CAOnly', Convert(varchar,case when T.CAOnly=1 then 'Yes' else 'No' end) , Convert(varchar,case when T.Prev_CAOnly=1 then 'Yes' else 'No' end))					
			,('Attach File', Convert(varchar, T.AttachFile) , Convert(varchar,T.Prev_AttachFile))
			,('ModifyingUser', Convert(varchar, T.ModifyingUser) , Convert(varchar,T.Prev_ModifyingUser))
			,('Action', Convert(varchar, T.Action) , Convert(varchar,T.Prev_Action))
			) AS C (ColName, present_Value, prev_value)
WHERE  EXISTS
(
    SELECT present_Value 
    EXCEPT 
    SELECT C.prev_value
)  and Convert(datetime,BeginDate) >= @FromDate and Convert(Datetime,BeginDate) <=@ToDate

END TRY
BEGIN CATCH

        DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),
                @ErrorSeverity INT = ERROR_SEVERITY(),
                @ErrorNumber INT = ERROR_NUMBER();

		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);
 
END CATCH
END

GO
/****** Object:  StoredProcedure [dbo].[usp_ForfeitOfLeaseForm_InsertUpdate]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
-- =============================================  
-- Author:  <Author : Sivanathan>  
-- Create date: <12-04-2021>  
-- Modified date : <12-04-2021> <13-04-2021,Sundar> <Included Exception handling>  
-- Description: <Description,The purpose of the Stored Procedure is to make CRUD operations on Unallocated Funds Form tables>  
-- Generic Info: <I - Insert , U- Update, D- Delete R-Re activate>    
-- =============================================  
        
CREATE PROCEDURE [dbo].[usp_ForfeitOfLeaseForm_InsertUpdate]         
 -- Add the parameters for the stored procedure here        
 @ID int = 0,          
 @ClientName varchar(50) =NULL,      
 @MatterNumber varchar(50) =NULL,      
 @CenterID int =NULL,      
 @PropertyAddress varchar(max) =NULL,      
 @ExchangeDate datetime =NULL,      
 @CompletionDate datetime =NULL,      
 @PropertyPrice money =NULL,      
 @PremiumIPAmount money =NULL,      
 @FeeEarner varchar(max) =NULL,      
 @CAOnly bit =NULL,      
 @AttachFile varchar(max) =NULL, 
 
 @StatementType  Varchar(50)=NULL,              
 @TrackerId int =NULL,                
 @CreatedBy Varchar(256)=NULL,           
 @processid int =NULL,              
 @RoutingID Int=NULL,        
 @selectedData varchar(100) = NULL          
AS        
BEGIN        
 
 -- SET NOCOUNT ON added to prevent extra result sets from        
 -- interfering with SELECT statements        
 SET NOCOUNT ON;        
        
        
 Declare @trgProcessId int;           
    -- This performs INSERT action against tblForfeitOfLeaseForm table        
IF @StatementType = 'I'                  
 BEGIN               
               
  INSERT INTO dbo.tblForfeitOfLeaseForm       
   (      
 ClientName  ,      
 MatterNumber  ,      
 CenterID  ,      
 PropertyAddress,      
 ExchangeDate,      
 CompletionDate ,      
 PropertyPrice  ,      
 PremiumIPAmount  ,      
 FeeEarner ,      
 CAOnly  ,      
 AttachFile,      
  [Action],      
 ModifyingUser       
 )      
      
   Values      
   (      
 @ClientName  ,      
 @MatterNumber  ,      
 @CenterID  ,      
 @PropertyAddress,      
 @ExchangeDate,      
 @CompletionDate ,      
 @PropertyPrice  ,      
 @PremiumIPAmount  ,      
 @FeeEarner ,      
 @CAOnly  ,      
 @AttachFile,      
 @StatementType,      
 @CreatedBy      
 )       
          
    Declare @FormDataId int;            
  set @FormDataId = SCOPE_IDENTITY()            
          
 select @trgProcessId=trgProcessId From tblRoutingDetails       
 where TrackerId= @TrackerId and srcprocessid =@processid          
 And RoutingID = @RoutingID                 
          
   -- This is common across all the trackers and perofrms INSERT action against tbltrackersform table         
    EXEC usp_TrackersForm_Insert @TrackerId, @CreatedBy, @trgProcessId,  @FormDataId        
           
 END           
        
        
  SELECT @trgProcessId=trgProcessId FROM tblRoutingDetails WHERE TrackerId= @TrackerId AND srcprocessid =@ProcessId                  
 AND RoutingID = @RoutingID           
        
        
        
--This performs UPDATE action against ClientToClientTransfersForm table        
IF @StatementType = 'U'                  
  BEGIN               
         
    UPDATE dbo.tblForfeitOfLeaseForm Set      
 ClientName = @ClientName ,      
 MatterNumber = @MatterNumber ,      
 CenterID = @CenterID ,      
 PropertyAddress = @PropertyAddress,      
 ExchangeDate = @ExchangeDate,      
 CompletionDate = @CompletionDate,      
 PropertyPrice = @PropertyPrice ,      
 PremiumIPAmount = @PremiumIPAmount ,      
 FeeEarner =@FeeEarner,      
 CAOnly = @CAOnly ,      
 AttachFile = @AttachFile,      
 [Action]=@StatementType,      
 ModifyingUser=@CreatedBy      
 where Id = @Id        
          
     EXEC [dbo].[usp_TrackersForm_Update] @Id,@TrackerId, @ProcessId,@CreatedBy, @RoutingID, @selectedData,@StatementType      
             
  END           
          
--This performs DELETE/RE ACTIVATE action against ClientToClientTransfersForm table        
IF (@StatementType = 'D' OR  @StatementType = 'R')           
  BEGIN               
        
        
    UPDATE dbo.tblForfeitOfLeaseForm         
   SET          
       [ModifyingUser] = @CreatedBy,          
       [Action] = @StatementType          
 WHERE Id IN (SELECT CAST(value AS INTEGER) FROM STRING_SPLIT(@selectedData, ','))                 
          
   -- This is common across all the trackers and UPDATING Process id in tbltrackersform table based on Routing details        
    EXEC [dbo].[usp_TrackersForm_Update] @Id,@TrackerId, @ProcessId,@CreatedBy, @RoutingID, @selectedData,@StatementType        
             
  END          
END 
GO
/****** Object:  StoredProcedure [dbo].[usp_GetDetailsByNotes]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[usp_GetDetailsByNotes]
@ITEMNOTE VARCHAR(250)=NULL,
@ReportType int= null,
@Fromdate datetime= null,
@Todate datetime= null
AS
BEGIN--
if
@ReportType=1
begin
IF (@ITEMNOTE LIKE '%Cardiff%' or @ITEMNOTE LIKE '%Manchester%' or @ITEMNOTE IS NULL)
BEGIN
Select
distinct MATDB_0.Notes As 'Legal Team',
FEETR_0.NAME As 'Fee Earner',
MATDB_0."MT-CODE" As 'Matter Number',
MATDB_0.Description As 'Matter Description',
MATAC_0."BAL-CLI" As 'Client Balance',
MATDB_0."NO-BILLS" As 'No of Bills',
CASE WHEN [Notes]Like'%Cardiff%' then 'Cardiff'
WHEN [Notes]Like'%Manchester%' then 'Manchester'
else 'OTHER'
end as [Legal Team1]
--INTO #OVER
From
PUB.FEETR FEETR_0, PUB.MATAC MATAC_0, PUB.MATDB MATDB_0
Where
MATAC_0."MT-CODE"= MATDB_0."MT-CODE" AND
FEETR_0.NAME = MATDB_0."FEE-EARNER" AND 
MATAC_0."BAL-CLI">5000 AND (MATDB_0.NOTES LIKE '%' + @ITEMNOTE + '%' or @ITEMNOTE is NULL)
END
ELSE
BEGIN
Select
distinct MATDB_0.Notes As 'Legal Team',
FEETR_0.NAME As 'Fee Earner',
MATDB_0."MT-CODE" As 'Matter Number',
MATDB_0.Description As 'Matter Description',
MATAC_0."BAL-CLI" As 'Client Balance',
MATDB_0."NO-BILLS" As 'No of Bills',
CASE WHEN [Notes]Like'%Cardiff%' then 'Cardiff'
WHEN [Notes]Like'%Manchester%' then 'Manchester'
else 'OTHER'
end as [Legal Team1]
--INTO #OVER
From
PUB.FEETR FEETR_0, PUB.MATAC MATAC_0, PUB.MATDB MATDB_0
Where
MATAC_0."MT-CODE"= MATDB_0."MT-CODE" AND
FEETR_0.NAME = MATDB_0."FEE-EARNER" AND 
MATAC_0."BAL-CLI">5000 AND MATDB_0.NOTES NOT LIKE '%Cardiff%' and MATDB_0.NOTES NOT LIKE '%Manchester%'
END
END
else
begin
IF (@ITEMNOTE LIKE '%Cardiff%' or @ITEMNOTE LIKE '%Manchester%' or @ITEMNOTE IS NULL)
BEGIN
Select
distinct MATDB_0.Notes As 'Legal Team',
FEETR_0.NAME As 'Fee Earner',
MATDB_0."MT-CODE" As 'Matter Number',
MATDB_0.Description As 'Matter Description',
MATAC_0."BAL-CLI" As 'Client Balance',
MATDB_0."NO-BILLS" As 'No of Bills',
CASE WHEN [Notes]Like'%Cardiff%' then 'Cardiff'
WHEN [Notes]Like'%Manchester%' then 'Manchester'
else 'OTHER'
end as [Legal Team1]
From
PUB.FEETR_History FEETR_0, PUB.MATAC_History MATAC_0, PUB.MATDB_History MATDB_0
Where
(FEETR_0.[EffectiveDate] between @Fromdate and @Todate) and
(MATAC_0.[EffectiveDate] between @Fromdate and @Todate) and
(MATDB_0.[EffectiveDate] between @Fromdate and @Todate) and
MATAC_0."MT-CODE"= MATDB_0."MT-CODE" AND
FEETR_0.NAME = MATDB_0."FEE-EARNER" AND 
MATAC_0."BAL-CLI">5000 AND MATDB_0.NOTES NOT LIKE '%Cardiff%' and MATDB_0.NOTES NOT LIKE '%Manchester%'
end
ELSE
BEGIN
Select
distinct MATDB_0.Notes As 'Legal Team',
FEETR_0.NAME As 'Fee Earner',
MATDB_0."MT-CODE" As 'Matter Number',
MATDB_0.Description As 'Matter Description',
MATAC_0."BAL-CLI" As 'Client Balance',
MATDB_0."NO-BILLS" As 'No of Bills',
CASE WHEN [Notes]Like'%Cardiff%' then 'Cardiff'
WHEN [Notes]Like'%Manchester%' then 'Manchester'
else 'OTHER'
end as [Legal Team1]
--INTO #OVER
From
PUB.FEETR_History FEETR_0, PUB.MATAC_History MATAC_0, PUB.MATDB_History MATDB_0
Where
(FEETR_0.[EffectiveDate] between @Fromdate and @Todate) and
(MATAC_0.[EffectiveDate] between @Fromdate and @Todate) and
(MATDB_0.[EffectiveDate] between @Fromdate and @Todate) and
MATAC_0."MT-CODE"= MATDB_0."MT-CODE" AND
FEETR_0.NAME = MATDB_0."FEE-EARNER" AND 
MATAC_0."BAL-CLI">5000 AND MATDB_0.NOTES NOT LIKE '%Cardiff%' and MATDB_0.NOTES NOT LIKE '%Manchester%'
Except
Select
MATDB_0.Notes As 'Legal Team',
FEETR_0.NAME As 'Fee Earner',
MATDB_0."MT-CODE" As 'Matter Number',
MATDB_0.Description As 'Matter Description',
MATAC_0."BAL-CLI" As 'Client Balance',
MATDB_0."NO-BILLS" As 'No of Bills',
CASE WHEN [Notes]Like'%Cardiff%' then 'Cardiff'
WHEN [Notes]Like'%Manchester%' then 'Manchester'
else 'OTHER'
end as [Legal Team1]
From
PUB.FEETR_History FEETR_0, PUB.MATAC_History MATAC_0, PUB.MATDB_History MATDB_0
Where 1=0
--drop table #over
end
end
END--pROCEDURE END
--SELECT* FROM #OVER
--SELECT [LEGAL TEAM],SUM([CLIENT BALANCE])[VALUES], COUNT([matter number]) [NO Of Files] FROM #OVER
--GROUP BY [LEGAL TEAM]
--drop table #over
GO
/****** Object:  StoredProcedure [dbo].[usp_GetKeyTaskLawyerReport_excel]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
    
CREATE PROCEDURE [dbo].[usp_GetKeyTaskLawyerReport_excel](@STARTDATE NVARCHAR(MAX)='',@ENDDATE NVARCHAR(MAX)='')      
AS      
BEGIN      
      
DECLARE @STARTTIME DATETIME      
DECLARE @ENDTIME DATETIME      
BEGIN TRY         
IF RTRIM(LTRIM(@STARTDATE))='' OR RTRIM(LTRIM(@ENDDATE))=''      
BEGIN      
  SELECT           
       KTL.[KTL_Mentor] as [KTL/Mentor] --       
   ,Team.TeamName    --       
   ,EFE.[Enter_FEInitials] as [Enter FE initials]    --      
   ,LG.LawyerGrade     --      
   ,TASK.Task          --       
      ,[StartTime]      --      
      ,[EndTime]          --       
   ,DATEDIFF(minute,[StartTime],[EndTime]) as [Time Taken] --      
   ,(DATEDIFF(minute,[StartTime],[EndTime])/60) as [Hour] --      
   ,LEFT(RTRIM(LTRIM(DATENAME(month,[StartTime]))),3) as [Month] --      
   ,RTRIM(LTRIM(DATENAME(YYYY,[StartTime]))) as [Year] --      
   ,'QTR'+RTRIM(LTRIM(DATENAME(QUARTER,[StartTime]))) as [Quarter] --      
   ,[Notes_Comments]    as [Notes/Comments] --            
      ,[ModifyingUser]    as [Modified By]--      
      ,[MatterNumber]    as [Matter Number]--      
   ,'' as Month2  --      
      ,[Admin]    as [admin] --      
   ,[Annotated] = CASE WHEN TLR.Annotated = 1 then 'True' else 'False' END    --      
      ,[Completed as Part of Holiday Cover] = CASE WHEN [HolidayCoverage] = 1 then 'True' else 'False' END    --      
      ,[Additional Fees Billed] = CASE WHEN TLR.AdditionalFee = 1 then 'True' else 'False' END    --      
      ,[ReasonForAdditionalFee]   as [Reason for Additional Fee] --      
      ,[FeeAmount]    as [Fee Amount (excl. VAT)] --      
      ,[VATDue]    as [VAT Due]--      
  FROM [tblTaskLawyerReportForm] TLR          
  LEFT JOIN [dbo].[tblCenterName] Center ON TLR.[CenterId]= Center.ID          
  LEFT JOIN [dbo].[tblKTL_Mentor] KTL ON TLR.[KTL_MentorID]= KTL.ID          
  LEFT JOIN [dbo].[tblTaskLawyerTeam] TEAM ON TLR.[TeamId] = TEAM.ID          
  LEFT JOIN [dbo].[tblEnter_FEInitials] EFE ON TLR.Enter_FEInitialsID = EFE.ID          
  LEFT JOIN [dbo].[TBLLawyerGrade] LG ON TLR.[LawyerGradeID] = LG.ID          
  LEFT JOIN [dbo].[TBLTASK] TASK ON TLR.TASKID = TASK.ID          
  WHERE TLR.[ACTION]<>'D'   
     
  RETURN      
END      
      
SET @STARTTIME=CAST(@STARTDATE as datetime)      
SET @ENDTIME=CAST(@ENDDATE as datetime)      
      
      
PRINT @STARTTIME      
PRINT @ENDTIME      
      
SELECT           
       KTL.[KTL_Mentor] as [KTL/Mentor] --       
   ,Team.TeamName    --       
   ,EFE.[Enter_FEInitials] as [Enter FE initials]    --      
   ,LG.LawyerGrade     --      
   ,TASK.Task          --       
      ,[StartTime]      --      
      ,[EndTime]          --       
   ,DATEDIFF(minute,[StartTime],[EndTime]) as [Time Taken] --      
   ,(DATEDIFF(minute,[StartTime],[EndTime])/60) as [Hour] --      
   ,LEFT(RTRIM(LTRIM(DATENAME(month,[StartTime]))),3) as [Month] --      
   ,RTRIM(LTRIM(DATENAME(YYYY,[StartTime]))) as [Year] --      
   ,'QTR'+RTRIM(LTRIM(DATENAME(QUARTER,[StartTime]))) as [Quarter] --      
   ,[Notes_Comments]    as [Notes/Comments] --            
      ,[ModifyingUser]    as [Modified By]--      
      ,[MatterNumber]    as [Matter Number]--      
   ,'' as Month2  --      
      ,[Admin]    as [admin] --      
   ,[Annotated] = CASE WHEN TLR.Annotated = 1 then 'True' else 'False' END    --      
      ,[Completed as Part of Holiday Cover] = CASE WHEN [HolidayCoverage] = 1 then 'True' else 'False' END    --      
      ,[Additional Fees Billed] = CASE WHEN TLR.AdditionalFee = 1 then 'True' else 'False' END    --      
      ,[ReasonForAdditionalFee]   as [Reason for Additional Fee] --      
      ,[FeeAmount]    as [Fee Amount (excl. VAT)] --      
      ,[VATDue]    as [VAT Due]--      
  FROM [tblTaskLawyerReportForm] TLR          
  LEFT JOIN [dbo].[tblCenterName] Center ON TLR.[CenterId]= Center.ID          
  LEFT JOIN [dbo].[tblKTL_Mentor] KTL ON TLR.[KTL_MentorID]= KTL.ID          
  LEFT JOIN [dbo].[tblTaskLawyerTeam] TEAM ON TLR.[TeamId] = TEAM.ID          
  LEFT JOIN [dbo].[tblEnter_FEInitials] EFE ON TLR.Enter_FEInitialsID = EFE.ID          
  LEFT JOIN [dbo].[TBLLawyerGrade] LG ON TLR.[LawyerGradeID] = LG.ID          
  LEFT JOIN [dbo].[TBLTASK] TASK ON TLR.TASKID = TASK.ID          
  WHERE [StartTime] >= @STARTTIME AND [StartTime] <=@ENDTIME   
    AND TLR.[ACTION]<>'D'      
      
END TRY              
BEGIN CATCH              
              
        DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),              
                @ErrorSeverity INT = ERROR_SEVERITY(),              
                @ErrorNumber INT = ERROR_NUMBER();              
              
  RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);              
               
END CATCH           
END      
GO
/****** Object:  StoredProcedure [dbo].[usp_HighValueIndemnity_Download]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[usp_HighValueIndemnity_Download]                
@TrackerId int,              
@processId int,  
@ViewName Varchar(100)  
              
AS                  
Begin             
 BEGIN TRY        
 Select    
 ID = HVI.ID ,    
 ClientName ,    
 MatterNumber ,    
 Center =CENTER.CenterName,    
 PropertyAddress ,    
 ExchangeDate ,    
 CompletionDate ,    
 PropertyPrice,    
 PremiumIPAmount ,    
 FeeEarner ,    
 CAOnly = case when HVI.CAOnly = 1 then 'Yes' else 'No' end,    
 Notes,    
 AttachFile   INTO #FilterResult  
 From [dbo].[tblHighValueIndemnityForm] HVI    
 Left Join [dbo].[tblCenterName] CENTER on HVI.CenterID = CENTER.Id    
    
 where HVI.id in (select distinct FormDataId    
    FROM [dbo].[tblTrackerForms]      
    with (nolock) where TrackerId=@TrackerId and ProcessId=@processId  )     
 order by 1 asc    
    
   DECLARE @sql nvarchar(max);  
  
SELECT  @sql = 'SELECT * FROM #FilterResult ' + dbo.FuncDynamicQueryResult(@TrackerId, @ViewName)  
  
EXEC sp_executesql @sql  
    
 END TRY    
Begin Catch    
Select     
  Error_Number() as errorNumber,    
  Error_Message() as errorMessage;    
  End Catch    
  DROP TABLE #FilterResult
End        
        
GO
/****** Object:  StoredProcedure [dbo].[usp_HighValueIndemnityForm_AuditView]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ajay George Kalathil
-- Create date: 6th May 2021
-- Description:	This SP will be used to get the Audit Trial Version View for the front end system
-- =============================================


--Exec usp_HighValueIndemnityForm_AuditView 10,null,null
CREATE PROCEDURE [dbo].[usp_HighValueIndemnityForm_AuditView]
	-- Add the parameters for the stored procedure here
	@ID Int,  
	@FromDate datetime2 = '1900-01-01 00:00:00.0000000', 
	@ToDate datetime2	= '9999-12-31 23:59:59.9999999'
AS
BEGIN

  BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    
;WITH T
AS (
    SELECT		HI.ID				
				,MatterNumber as MatterNumber , lag(MatterNumber) over(order by enddate asc) as Prev_MatterNumber
				,ClientName as ClientName , lag(ClientName) over(order by enddate asc) as Prev_ClientName
				,C.CenterName as Center,lag(C.CenterName) over(order by enddate) as Prev_Center
				--,CenterID as CenterID , lag(CenterID) over(order by enddate asc) as Prev_CenterID				
				,PropertyAddress as PropertyAddress , lag(PropertyAddress) over(order by enddate asc) as Prev_PropertyAddress
				,ExchangeDate as ExchangeDate , lag(ExchangeDate) over(order by enddate asc) as Prev_ExchangeDate
				,CompletionDate as CompletionDate , lag(CompletionDate) over(order by enddate asc) as Prev_CompletionDate
				,PropertyPrice as PropertyPrice , lag(PropertyPrice) over(order by enddate asc) as Prev_PropertyPrice
				,PremiumIPAmount as PremiumIPAmount , lag(PremiumIPAmount) over(order by enddate asc) as Prev_PremiumIPAmount
				,FeeEarner as FeeEarner , lag(FeeEarner) over(order by enddate asc) as Prev_FeeEarner				
				,ISNULL(CAOnly,0) as CAOnly , ISNULL(lag(CAOnly) over(order by enddate asc),0) as Prev_CAOnly
				,Notes as Notes , lag(Notes) over(order by enddate asc) as Prev_Notes
				,AttachFile as AttachFile , lag(AttachFile) over(order by enddate asc) as Prev_AttachFile
				,ModifyingUser as ModifyingUser , lag(ModifyingUser) over(order by enddate asc) as Prev_ModifyingUser
				,Action as Action , lag(Action) over(order by enddate asc) as Prev_Action
				,BeginDate
				,EndDate
				,ROW_Number() over(partition by  HI.Id Order by EndDate asc) as VersionNumber
    FROM dbo.tblHighValueIndemnityForm FOR SYSTEM_TIME ALL as HI
	 Left Join [dbo].[tblCenterName] C on HI.CenterID = C.Id  

    WHERE HI.ID =@ID
)
SELECT T.BeginDate,
       T.EndDate,
       C.ColName as ColumnName,
	   t.VersionNumber,	   
       C.present_Value as CurrentValue,
       C.prev_value as PreviousValue,
	   T.ModifyingUser as ModifiedBy
FROM T
    CROSS APPLY
    (
        VALUES			
			 ('Matter Number', Convert(varchar, T.MatterNumber) , Convert(varchar,T.Prev_MatterNumber))
			,('Client Name', Convert(varchar, T.ClientName) , Convert(varchar,T.Prev_ClientName))
			,('Center', Convert(varchar, T.Center) , Convert(varchar,T.Prev_Center))
			,('Property Address', Convert(varchar, T.PropertyAddress) , Convert(varchar,T.Prev_PropertyAddress))
			,('Exchange Date', Convert(varchar(10), T.ExchangeDate,103) , Convert(varchar(10),T.Prev_ExchangeDate,103))			
			,('Completion Date', Convert(varchar(10), T.CompletionDate,103) , Convert(varchar(10),T.Prev_CompletionDate,103))
			,('Property Price', Convert(varchar, T.PropertyPrice) , Convert(varchar,T.Prev_PropertyPrice))
			,('Premium IP Amount', Convert(varchar, T.PremiumIPAmount) , Convert(varchar,T.Prev_PremiumIPAmount))			
			,('FeeEarner', Convert(varchar, T.FeeEarner) , Convert(varchar,T.Prev_FeeEarner))
			,('CAOnly', Convert(varchar, case when T.CAOnly = 1 then 'Yes' else 'No' end) , Convert(varchar,case when T.Prev_CAOnly = 1 then 'Yes' else 'No' end))
			,('Notes', Convert(varchar, T.Notes) , Convert(varchar,T.Prev_Notes))			
			,('Attach File', Convert(varchar, T.AttachFile) , Convert(varchar,T.Prev_AttachFile))
			,('ModifyingUser', Convert(varchar, T.ModifyingUser) , Convert(varchar,T.Prev_ModifyingUser))
			,('Action', Convert(varchar, T.Action) , Convert(varchar,T.Prev_Action))
			) AS C (ColName, present_Value, prev_value)
WHERE  EXISTS
(
    SELECT present_Value 
    EXCEPT 
    SELECT C.prev_value
)  and Convert(datetime,BeginDate) >= @FromDate and Convert(Datetime,BeginDate) <=@ToDate

END TRY
BEGIN CATCH

        DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),
                @ErrorSeverity INT = ERROR_SEVERITY(),
                @ErrorNumber INT = ERROR_NUMBER();

		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);
 
END CATCH
END

GO
/****** Object:  StoredProcedure [dbo].[usp_HighValueIndemnityForm_InsertUpdate]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
  
    
-- =============================================    
-- Author:  <Author : Arun,Sundar>    
-- Create date: <10-04-2021>    
-- Modified date : <11-04-2021> <13-04-2021,Sundar> <Included Exception handling>
-- Description: <Description,The purpose of the Stored Procedure is to make CRUD operations on HighValueIndemnityForm tables>    
-- Generic Info: <I - Insert , U- Update, D- Delete R-Re activate>      
-- =============================================    
    
    
CREATE PROCEDURE [dbo].[usp_HighValueIndemnityForm_InsertUpdate]     
(    
 @ID int =0,      
 @MatterNumber varchar(50) =NULL ,      
 @ClientName varchar(50) =NULL ,      
 @CenterID int =NULL,      
 @PropertyAddress varchar(max) =NULL,      
 @ExchangeDate datetime =NULL,      
 @CompletionDate datetime =NULL ,      
 @PropertyPrice money =NULL ,      
 @PremiumIPAmount money =NULL,      
 @FeeEarner varchar(50) =NULL,      
 @CAOnly bit =NULL,      
 @Notes Varchar(max) =NULL,      
 @AttachFile varchar(max) =NULL ,      
      
 @StatementType  Varchar(50)=NULL,          
 @TrackerId int =NULL,            
 @CreatedBy Varchar(256)=NULL,          
 @processid int =NULL,          
 @RoutingID Int=NULL,    
 @selectedData varchar(100) = NULL )     
    
AS    
BEGIN    
BEGIN TRY
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements    
 SET NOCOUNT ON;    
    
    
 Declare @trgProcessId int;       
    -- This performs INSERT action against HighValueIndemnityForm table    
IF @StatementType = 'I'              
 BEGIN           
           
  INSERT INTO dbo.tblHighValueIndemnityForm       
   (      
MatterNumber,       
ClientName  ,      
CenterID  ,      
PropertyAddress,      
ExchangeDate,      
CompletionDate ,      
PropertyPrice  ,      
PremiumIPAmount  ,      
FeeEarner ,      
CAOnly  ,      
Notes,      
AttachFile,    
[Action],    
[ModifyingUser] )    
       
Values      
( @MatterNumber,    
 @ClientName  ,      
 @CenterID  ,      
 @PropertyAddress,      
 @ExchangeDate,      
 @CompletionDate ,      
 @PropertyPrice  ,      
 @PremiumIPAmount  ,      
 @FeeEarner ,      
 @CAOnly  ,      
 @Notes,      
 @AttachFile ,    
@StatementType,    
@CreatedBy)      
      
    Declare @FormDataId int;            
 set @FormDataId = SCOPE_IDENTITY()            
          
 SELECT @trgProcessId=trgProcessId FROM tblRoutingDetails    
  WHERE TrackerId= @TrackerId AND srcprocessid = @processid AND RoutingID = @RoutingID          
      
   -- This is common across all the trackers and perofrms INSERT action against tbltrackersform table     
    EXEC usp_TrackersForm_Insert @TrackerId, @CreatedBy, @trgProcessId,  @FormDataId    
       
 END       
    
    
  SELECT @trgProcessId=trgProcessId FROM tblRoutingDetails WHERE TrackerId= @TrackerId AND     
  srcprocessid =@ProcessId              
 AND RoutingID = @RoutingID       
    
    
     
--This performs UPDATE action against HighValueIndemnityForm table    
IF @StatementType = 'U'              
  BEGIN           
     
UPDATE dbo.tblHighValueIndemnityForm Set    
ClientName = @ClientName ,    
MatterNumber = @MatterNumber ,    
CenterID = @CenterID ,    
PropertyAddress = @PropertyAddress,    
ExchangeDate = @ExchangeDate,    
CompletionDate = @CompletionDate,    
PropertyPrice = @PropertyPrice ,    
PremiumIPAmount = @PremiumIPAmount ,    
FeeEarner =@FeeEarner,    
CAOnly = @CAOnly ,    
Notes = @Notes,    
AttachFile = @AttachFile ,      
[ModifyingUser] = @CreatedBy,      
[Action] = @StatementType      
WHERE Id = @Id        
                    
    EXEC [dbo].[usp_TrackersForm_Update] @Id,@TrackerId, @ProcessId,@CreatedBy, @RoutingID, @selectedData,@StatementType    

         
  END       
      
--This performs DELETE/RE ACTIVATE action against HighValueIndemnityForm table    
IF (@StatementType = 'D' OR  @StatementType = 'R')            
  BEGIN           
    
    
    UPDATE [dbo].[tblHighValueIndemnityForm]      
   SET      
      [ModifyingUser] = @CreatedBy,      
       [Action] = @StatementType      
 WHERE Id IN (SELECT CAST(value AS INTEGER) FROM STRING_SPLIT(@selectedData, ','))             
      
     -- This is common across all the trackers and UPDATING Process id in tbltrackersform table based on Routing details    
    EXEC [dbo].[usp_TrackersForm_Update] @Id,@TrackerId, @ProcessId,@CreatedBy, @RoutingID, @selectedData,@StatementType    
         
  END      
END TRY
BEGIN CATCH

        DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),
                @ErrorSeverity INT = ERROR_SEVERITY(),
                @ErrorNumber INT = ERROR_NUMBER();

		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);
 
END CATCH 
    
     
END 
GO
/****** Object:  StoredProcedure [dbo].[usp_IndemnityPolicies_Download]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Modified date: <16-04-2021> <Included Exception handling>  
        
 --[dbo].[sp_GetAlltblIndemnityPoliciesDownload] 15,201    
CREATE Procedure [dbo].[usp_IndemnityPolicies_Download]              
@TrackerId int,            
@processId int,  
@ViewName Varchar(100)  
                                
            
AS                
Begin           
 BEGIN TRY          
                
  select Indemnitypolicy.ID, Indemnitypolicy.MatterNumber,Center= center.CenterName, Indemnitypolicy.Address, Indemnitypolicy.Premium,        
  Indemnitypolicy.QuoteNumber,Status=sts.Status, Indemnitypolicy.CompletionDate         
      INTO #FilterResult  
from tblIndemnityPoliciesForm Indemnitypolicy         
LEFT JOIN [dbo].[tblCenterName] center on Indemnitypolicy.CenterID = center.Id  
LEFT JOIN [dbo].[tblIndemnityPoliciesStatus] sts ON Indemnitypolicy.StatusID = sts.ID        
          
          
 where Indemnitypolicy.ID in (select distinct FormDataId          
    FROM [dbo].[tblTrackerForms]            
    with (nolock) where TrackerId=@TrackerId and ProcessId=@processId  )           
 order by 1 asc          
  
  DECLARE @sql nvarchar(max);  
  
SELECT  @sql = 'SELECT * FROM #FilterResult ' + dbo.FuncDynamicQueryResult(@TrackerId, @ViewName)  
  
EXEC sp_executesql @sql  
  
   
END TRY  
BEGIN CATCH  
  
        DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),  
                @ErrorSeverity INT = ERROR_SEVERITY(),  
                @ErrorNumber INT = ERROR_NUMBER();  
  
  RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);  
   
END CATCH        
DROP TABLE #FilterResult
End   
GO
/****** Object:  StoredProcedure [dbo].[usp_IndemnityPoliciesForm_AuditView]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


--Exec usp_IndemnityPoliciesForm_AuditView 9,null,null
CREATE PROCEDURE [dbo].[usp_IndemnityPoliciesForm_AuditView]
	-- Add the parameters for the stored procedure here
	@ID Int,  
	@FromDate datetime2 = '1900-01-01 00:00:00.0000000', 
	@ToDate datetime2	= '9999-12-31 23:59:59.9999999'
AS
BEGIN

  BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    
;WITH T
AS (
    SELECT		Indemnitypolicy.ID
	            ,MatterNumber as MatterNumber , lag(MatterNumber) over(order by enddate asc) as Prev_MatterNumber
				,C.[CenterName] as Center,lag(C.[CenterName]) over(order by enddate) as Prev_Center
				--,CenterID as CenterID , lag(CenterID) over(order by enddate asc) as Prev_CenterID
				,Address as Address , lag(Address) over(order by enddate asc) as Prev_Address
				,Premium as Premium , lag(Premium) over(order by enddate asc) as Prev_Premium
				,QuoteNumber as QuoteNumber , lag(QuoteNumber) over(order by enddate asc) as Prev_QuoteNumber
				,sts.Status as IPStatus , lag(sts.Status) over(order by enddate asc) as Prev_IPStatus
				--,StatusId as StatusId , lag(StatusId) over(order by enddate asc) as Prev_StatusId
				,CompletionDate as CompletionDate , lag(CompletionDate) over(order by enddate asc) as Prev_CompletionDate
				,ModifyingUser as ModifyingUser , lag(ModifyingUser) over(order by enddate asc) as Prev_ModifyingUser
				,Action as Action , lag(Action) over(order by enddate asc) as Prev_Action
				,BeginDate
				,EndDate
				,ROW_Number() over(partition by  Indemnitypolicy.Id Order by EndDate asc) as VersionNumber
    FROM dbo.tblIndemnityPoliciesForm FOR SYSTEM_TIME ALL AS Indemnitypolicy
	LEFT JOIN [dbo].[tblCenterName] c on Indemnitypolicy.CenterID = c.Id
LEFT JOIN [dbo].[tblIndemnityPoliciesStatus] sts ON Indemnitypolicy.StatusID = sts.ID    
    WHERE Indemnitypolicy.id =@ID
)
SELECT T.BeginDate,
       T.EndDate,
       C.ColName as ColumnName,
	   t.VersionNumber,	   
       C.present_Value as CurrentValue,
       C.prev_value as PreviousValue,
	   T.ModifyingUser as ModifiedBy
FROM T
    CROSS APPLY
    (
        VALUES
			('Matter number', Convert(varchar, T.MatterNumber) , Convert(varchar,T.Prev_MatterNumber))
			,('Centre', Convert(varchar, T.Center) , Convert(varchar,T.Prev_Center))
			,('Address', Convert(varchar, T.Address) , Convert(varchar,T.Prev_Address))
			,('Premium', Convert(varchar, T.Premium) , Convert(varchar,T.Prev_Premium))
			,('Quote Number', Convert(varchar, T.QuoteNumber) , Convert(varchar,T.Prev_QuoteNumber))
			,('Status', Convert(varchar, T.IPStatus) , Convert(varchar,T.Prev_IPStatus))
			,('Completion Date', Convert(varchar(10), T.CompletionDate,103) , Convert(varchar(10),T.Prev_CompletionDate,103))
			,('ModifyingUser', Convert(varchar, T.ModifyingUser) , Convert(varchar,T.Prev_ModifyingUser))
			,('Action', Convert(varchar, T.Action) , Convert(varchar,T.Prev_Action))
			) AS C (ColName, present_Value, prev_value)
WHERE  EXISTS
(
    SELECT present_Value 
    EXCEPT 
    SELECT C.prev_value
)  and Convert(datetime,BeginDate) >= @FromDate and Convert(Datetime,BeginDate) <=@ToDate

END TRY
BEGIN CATCH

        DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),
                @ErrorSeverity INT = ERROR_SEVERITY(),
                @ErrorNumber INT = ERROR_NUMBER();

		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);
 
END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[usp_IndemnityPoliciesForm_InsertUpdate]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author : Sundar>
-- Create date: <12-04-2021>
-- Modified date: <13-04-2021> <Included Exception handling>
-- Description:	<Description,The purpose of the Stored Procedure is to make CRUD operations on IndemnityPoliciesForm tables>
-- Generic Info: <I - Insert , U- Update, D- Delete R-Re activate>  
-- =============================================

--INSERT
--EXEC [dbo].[usp_IndemnityPoliciesForm_InsertUpdate] 0,'11111',1,'address test',100.00,'test',1,'2021-04-12 00:00:00.000','I',15,'Sundar',101,0
--update
--EXEC [dbo].[usp_IndemnityPoliciesForm_InsertUpdate] 4,'22222',1,'address update test',1000.00,'test',1,'2021-04-12 00:00:00.000','U',15,'Sundar',201,1
--DELETE
--EXEC [dbo].[usp_IndemnityPoliciesForm_InsertUpdate] 0,'12345',1,'address1 update',1100.00,'test',1,'2021-04-12 00:00:00.000','D',15,'Sundar',201,0,4
--Re-Activate
--EXEC [dbo].[usp_IndemnityPoliciesForm_InsertUpdate] 0,'12345',1,'address1 update',1100.00,'test',1,'2021-04-12 00:00:00.000','R',15,'Sundar',601,9,4

CREATE PROCEDURE [dbo].[usp_IndemnityPoliciesForm_InsertUpdate] (
-- Add the parameters for the stored procedure here
	@ID int =0,
	@MatterNumber varchar(20) = NULL, 
	@CenterID int = NULL, 
	@Address varchar(max) = NULL, 
	@Premium  int = NULL, 
	@QuoteNumber varchar(50) = NULL, 
	@StatusID  int = NULL, 
	@CompletionDate datetime = NULL,  
	
	@StatementType  Varchar(1)=NULL,      
    @TrackerId int =NULL,        
    @CreatedBy Varchar(50)=NULL,      
    @processid int =NULL,      
    @RoutingID Int=NULL,
    @selectedData varchar(100) = NULL  
)

AS          
  BEGIN        
  BEGIN TRY
-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements
	SET NOCOUNT ON;


	Declare @trgProcessId int;   
    -- This performs INSERT action against IndemnityPoliciesForm table
IF @StatementType = 'I'    

BEGIN    
INSERT INTO [tblIndemnityPoliciesForm]
( 
	[MatterNumber]
    ,[CenterID]
    ,[Address]
    ,[Premium]
    ,[QuoteNumber]
    ,[StatusID]
    ,[CompletionDate]
	,[Action]
    ,[ModifyingUser] 
	)
Values

( 
    @MatterNumber, 
	@CenterID, 
	@Address, 
	@Premium, 
	@QuoteNumber, 
	@StatusID, 
	@CompletionDate,
    @StatementType,
    @CreatedBy 
  )
  
	Declare @FormDataId int;        
	set @FormDataId = SCOPE_IDENTITY()        
      
	SELECT @trgProcessId=trgProcessId FROM tblRoutingDetails
		WHERE TrackerId= @TrackerId AND srcprocessid = @processid AND RoutingID = @RoutingID      
  
   -- This is common across all the trackers and perofrms INSERT action against tbltrackersform table 
    EXEC usp_TrackersForm_Insert @TrackerId, @CreatedBy, @trgProcessId,  @FormDataId
   
 END   

  SELECT @trgProcessId=trgProcessId FROM tblRoutingDetails WHERE TrackerId= @TrackerId AND srcprocessid =@ProcessId          
 AND RoutingID = @RoutingID   
  

--This performs UPDATE action against IndemnityPoliciesForm table
IF @StatementType = 'U'          
  BEGIN     
   
   UPDATE [tblIndemnityPoliciesForm]
	SET
	  [MatterNumber] = @MatterNumber
      ,[CenterID] = @CenterID
      ,[Address] = @Address
      ,[Premium] = @Premium
      ,[QuoteNumber] = @QuoteNumber
      ,[StatusID] = @StatusID
      ,[CompletionDate] = @CompletionDate
	  ,[ModifyingUser] = @CreatedBy    
      ,[Action] = @StatementType 
where ID = @Id

   -- This is common across all the trackers and perofrms UPDATE action against tbltrackersform table 
   		EXEC [dbo].[usp_TrackersForm_Update] @ID,@TrackerId, @ProcessId,@CreatedBy, @RoutingID, @selectedData,@StatementType
    
  END   
  --This performs DELETE/RE ACTIVATE action against IndemnityPoliciesForm table
IF (@StatementType = 'D' OR  @StatementType = 'R')        
  BEGIN       
   UPDATE dbo.tblIndemnityPoliciesForm 
			SET  
				   [ModifyingUser] = @CreatedBy,  
				   [Action] = @StatementType  
	WHERE Id IN (SELECT CAST(value AS INTEGER) FROM STRING_SPLIT(@selectedData, ','))         
  
   -- This is common across all the trackers and UPDATING Process id in tbltrackersform table based on Routing details
   	EXEC [dbo].[usp_TrackersForm_Update] @ID,@TrackerId, @ProcessId,@CreatedBy, @RoutingID, @selectedData,@StatementType
     
  END  
  
END TRY
BEGIN CATCH

        DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),
                @ErrorSeverity INT = ERROR_SEVERITY(),
                @ErrorNumber INT = ERROR_NUMBER();

		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);
 
END CATCH

   	
END
GO
/****** Object:  StoredProcedure [dbo].[usp_InterestPayableOC_Download]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Modified date: <16-04-2021> <Included Exception handling>

--[sp_GetAllInterestPayableDownload] 4,201
CREATE Procedure [dbo].[usp_InterestPayableOC_Download]    
@TrackerId int,  
@processId int,
@ViewName Varchar(100)  
AS      
Begin 
BEGIN TRY

SELECT Id = Interest.Id
      ,MatterNumber
      ,center.CenterName
	  ,team.TeamName
      ,DateFundsRevd
      ,PaymentDate
      ,FundsHeld
      ,ReasonForInterestPayment
      ,Posted = CASE WHEN Interest.Posted = 1 then 'Yes' else 'No' END
	  ,NumberOfDatesFundsHeld
	  ,Formula_25
	  INTO #FilterResult
FROM dbo.tblInterestPayableOCForm as Interest
left join [dbo].[tblCenterName] center on Interest.CenterID = center.Id
left join [dbo].[tblTeamName] team on Interest.TeamID = team.ID

where Interest.id in (select distinct FormDataId
				FROM [dbo].[tblTrackerForms]  
				with (nolock) where TrackerId=@TrackerId and ProcessId=@processId  ) 
order by 1 asc
 DECLARE @sql nvarchar(max);

SELECT  @sql = 'SELECT * FROM #FilterResult ' + dbo.FuncDynamicQueryResult(@TrackerId, @ViewName)

EXEC sp_executesql @sql


END TRY
BEGIN CATCH

        DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),
                @ErrorSeverity INT = ERROR_SEVERITY(),
                @ErrorNumber INT = ERROR_NUMBER();

		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);
 
END CATCH
DROP TABLE #FilterResult

End    
    
GO
/****** Object:  StoredProcedure [dbo].[usp_InterestPayableOCForm_AuditView]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
  
  
--Exec usp_InterestPayableOCForm_AuditView 1,null,null  
CREATE PROCEDURE [dbo].[usp_InterestPayableOCForm_AuditView]  
 -- Add the parameters for the stored procedure here  
 @ID Int,    
 @FromDate datetime2 = '1900-01-01 00:00:00.0000000',   
 @ToDate datetime2 = '9999-12-31 23:59:59.9999999'  
AS  
BEGIN  
  
  BEGIN TRY  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
  
      
;WITH T  
AS (  
    SELECT  Interest.ID  
             ,MatterNumber as MatterNumber , lag(MatterNumber) over(order by enddate asc) as Prev_MatterNumber  
    ,CenterName as Center , lag(CenterName) over(order by enddate asc) as Prev_Center  
    ,TeamName as Team , lag(TeamName) over(order by enddate asc) as Prev_Team  
    ,DateFundsRevd as DateFundsRevd , lag(DateFundsRevd) over(order by enddate asc) as Prev_DateFundsRevd  
    ,PaymentDate as PaymentDate , lag(PaymentDate) over(order by enddate asc) as Prev_PaymentDate  
    ,FundsHeld as FundsHeld , lag(FundsHeld) over(order by enddate asc) as Prev_FundsHeld  
    ,ReasonForInterestPayment as ReasonForInterestPayment, lag(ReasonForInterestPayment) over(order by enddate asc) as Prev_ReasonForInterestPayment  
    ,ISNULL(Posted,0) as Posted , ISNULL(lag(Posted) over(order by enddate asc),0) as Prev_Posted  
    ,ModifyingUser as ModifyingUser , lag(ModifyingUser) over(order by enddate asc) as Prev_ModifyingUser  
    ,Action as Action , lag(Action) over(order by enddate asc) as Prev_Action  
    ,BeginDate  
    ,EndDate  
    ,ROW_Number() over(partition by  Interest.Id Order by EndDate asc) as VersionNumber  
    FROM dbo.tblInterestPayableOCForm FOR SYSTEM_TIME ALL AS Interest  
LEFT JOIN [dbo].[tblCenterName] center on Interest.CenterID = center.Id  
LEFT JOIN [dbo].[tblTeamName] team on Interest.TeamID = team.ID  
    WHERE Interest.id =@ID  
)  
SELECT T.BeginDate,  
       T.EndDate,  
       C.ColName as ColumnName,  
    t.VersionNumber,      
       C.present_Value as CurrentValue,  
       C.prev_value as PreviousValue,  
    T.ModifyingUser as ModifiedBy  
FROM T  
    CROSS APPLY  
    (  
        VALUES  
      ('Matter Number', Convert(varchar, T.MatterNumber) , Convert(varchar,T.Prev_MatterNumber))  
   ,('Centre', Convert(varchar, T.Center) , Convert(varchar,T.Prev_Center))  
   ,('Team', Convert(varchar, T.Team) , Convert(varchar,T.Prev_Team))  
   ,('Date funds received', Convert(varchar(10), T.DateFundsRevd,103) , Convert(varchar(10),T.Prev_DateFundsRevd,103))  
   ,('Payment Date', Convert(varchar(10), T.PaymentDate,103) , Convert(varchar(10),T.Prev_PaymentDate,103))  
   ,('Funds held', Convert(varchar, T.FundsHeld) , Convert(varchar,T.Prev_FundsHeld))  
   ,('Reason for interest payment', Convert(varchar, T.ReasonForInterestPayment) , Convert(varchar,T.Prev_ReasonForInterestPayment))  
   ,('Posted(CA Only)', Convert(varchar,case when T.Posted=1 then 'Yes' else 'No' end) , Convert(varchar,case when T.Prev_Posted=1 then 'Yes' else 'No' end))  
   ,('ModifyingUser', Convert(varchar, T.ModifyingUser) , Convert(varchar,T.Prev_ModifyingUser))  
   ,('Action', Convert(varchar, T.Action) , Convert(varchar,T.Prev_Action))  
   ) AS C (ColName, present_Value, prev_value)  
WHERE  EXISTS  
(  
    SELECT present_Value   
    EXCEPT   
    SELECT C.prev_value  
)  and Convert(datetime,BeginDate) >= @FromDate and Convert(Datetime,BeginDate) <=@ToDate  
  
END TRY  
BEGIN CATCH  
  
        DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),  
                @ErrorSeverity INT = ERROR_SEVERITY(),  
                @ErrorNumber INT = ERROR_NUMBER();  
  
  RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);  
   
END CATCH  
END  
GO
/****** Object:  StoredProcedure [dbo].[usp_InterestPayableOCForm_InsertUpdate]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


  
  
    
-- =============================================    
-- Author:  <Author : Arun,Sundar>    
-- Create date: <10-04-2021>    
-- Modified date : <11-04-2021><13-04-2021,Sundar> <Included Exception handling>
-- Description: <Description,The purpose of the Stored Procedure is to make CRUD operations on InterestPayableOCForm tables>    
-- Generic Info: <I - Insert , U- Update, D- Delete R-Re activate>      
-- =============================================    
    
    
CREATE PROCEDURE [dbo].[usp_InterestPayableOCForm_InsertUpdate]     
(    
@ID int =0,    
@MatterNumber Varchar(50)=NULL,    
@CenterID Int =NULL,    
@TeamID Int =NULL,    
@DateFundsRevd Datetime=NULL,    
@PaymentDate Datetime=NULL,    
@FundsHeld Int=NULL,    
@ReasonForInterestPayment Varchar(Max)=NULL,    
@Posted Bit=NULL,    
@NumberOfDatesFundsHeld Int=NULL,    
@Formula_25 Decimal(18,2)=NULL,    
      
 @StatementType  Varchar(50)=NULL,          
 @TrackerId int =NULL,            
 @CreatedBy Varchar(256)=NULL,          
 @processid int =NULL,          
 @RoutingID Int=NULL,    
 @selectedData varchar(100) = NULL )     
    
AS    
BEGIN    
BEGIN TRY
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
    
    
 Declare @trgProcessId int;       
    -- This performs INSERT action against InterestPayableOCForm table    
IF @StatementType = 'I'              
 BEGIN           
           
   INSERT INTO   [dbo].[tblInterestPayableOCForm]    
(MatterNumber,    
CenterID ,    
TeamID ,    
DateFundsRevd ,    
PaymentDate ,    
FundsHeld ,    
ReasonForInterestPayment,    
Posted,    
NumberOfDatesFundsHeld,    
Formula_25 ,    
[Action],    
[ModifyingUser] )    
       
Values      
(@MatterNumber,    
@CenterID ,    
@TeamID ,    
@DateFundsRevd ,    
@PaymentDate ,    
@FundsHeld ,    
@ReasonForInterestPayment,    
@Posted,    
@NumberOfDatesFundsHeld,    
@Formula_25,    
@StatementType,    
@CreatedBy)      
      
    Declare @FormDataId int;            
 set @FormDataId = SCOPE_IDENTITY()            
          
 SELECT @trgProcessId=trgProcessId FROM tblRoutingDetails    
  WHERE TrackerId= @TrackerId AND srcprocessid = @processid AND RoutingID = @RoutingID          
         
   -- This is common across all the trackers and perofrms INSERT action against tbltrackersform table     
    EXEC usp_TrackersForm_Insert @TrackerId, @CreatedBy, @trgProcessId,  @FormDataId    
       
    
 END       
    
    
  SELECT @trgProcessId=trgProcessId FROM tblRoutingDetails WHERE TrackerId= @TrackerId AND     
  srcprocessid =@ProcessId              
 AND RoutingID = @RoutingID       
    
     
--This performs UPDATE action against InterestPayableOCForm table    
IF @StatementType = 'U'              
  BEGIN           
     
    UPDATE [dbo].[tblInterestPayableOCForm]     
   SET      
        MatterNumber =@MatterNumber,    
     CenterID = @CenterID ,    
     TeamID =@TeamID ,    
     DateFundsRevd = @DateFundsRevd ,    
     PaymentDate = @PaymentDate ,    
     FundsHeld = @FundsHeld,    
     ReasonForInterestPayment = @ReasonForInterestPayment,    
     Posted = @Posted,    
     NumberOfDatesFundsHeld=@NumberOfDatesFundsHeld,    
     Formula_25=@Formula_25,      
        [ModifyingUser] = @CreatedBy,      
        [Action] = @StatementType      
 WHERE Id = @Id        
                    
 EXEC [dbo].[usp_TrackersForm_Update] @Id,@TrackerId, @ProcessId,@CreatedBy, @RoutingID, @selectedData,@StatementType   
          
            
         
  END       
    
--     
--This performs DELETE/RE ACTIVATE action against InterestPayableOCForm table    
IF (@StatementType = 'D' OR  @StatementType = 'R')            
  BEGIN           
    
    
    UPDATE [dbo].[tblInterestPayableOCForm]      
   SET      
       [ModifyingUser] = @CreatedBy,      
       [Action] = @StatementType      
 WHERE Id IN (SELECT CAST(value AS INTEGER) FROM STRING_SPLIT(@selectedData, ','))             
      
  -- This is common across all the trackers and UPDATING Process id in tbltrackersform table based on Routing details    
    EXEC [dbo].[usp_TrackersForm_Update]  @Id,@TrackerId, @ProcessId,@CreatedBy, @RoutingID, @selectedData,@StatementType    
           
  END      
END TRY
BEGIN CATCH

        DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),
                @ErrorSeverity INT = ERROR_SEVERITY(),
                @ErrorNumber INT = ERROR_NUMBER();

		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);
 
END CATCH
    
     
END 
GO
/****** Object:  StoredProcedure [dbo].[usp_Load_FEETRHistory]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_Load_FEETRHistory]
AS
BEGIN
BEGIN TRY
DECLARE @CURRDT DATE = CONVERT(DATE,GETDATE());
SET NOCOUNT ON;
INSERT INTO [PUB].[FEETR_History]
( --Table and columns in which to insert the data
       [FEE-EARNER]
      ,[NAME]
      ,[EXTENSION]
      ,[FESTATUS]
      ,[WEIGHTING]  
      ,[AVAILABILITY]
      ,[COST-RATE]
      ,[CHARGE-RATE]
      ,[EXPENSE-RATE]
      ,[STD-DAY-START]
      ,[STD-DAY-HRS]
      ,[SALARY-ACT]
      ,[SALARY-NOT]
      ,[UPLIFT]
      ,[SALARY-REL]
      ,[EXP-PC]
      ,[EXP-PS]
      ,[EXP-TOT]
      ,[HOURS]
      ,[TARGET]
      ,[CURR]
      ,[DEPARTMENT]
      ,[DAY-TARGET]
      ,[EQUITY-PR]
      ,[ROLLNO]
      ,[CURRENCY-CODE]
      ,[EMAIL]
      ,[MOD-DATE]
      ,[MOD-TIME]
      ,[MOD-USER]
      ,[USER-ID]
      ,[NOT-FOR-PROFIT-COSTS]
      ,[ANALY-STATUS]
      ,[FEE-CREDIT-FE]
      ,[EffectiveDate]
      ,[EndDate]
     )
-- Select the rows/columns to insert that are output from this merge statement 
-- In this example, the rows to be inserted are the rows that have changed (UPDATE).


select    
 [FEE-EARNER]
      ,[NAME]
      ,[EXTENSION]
      ,[FESTATUS]
      ,[WEIGHTING]  
      ,[AVAILABILITY]
      ,[COST-RATE]
      ,[CHARGE-RATE]
      ,[EXPENSE-RATE]
      ,[STD-DAY-START]
      ,[STD-DAY-HRS]
      ,[SALARY-ACT]
      ,[SALARY-NOT]
      ,[UPLIFT]
      ,[SALARY-REL]
      ,[EXP-PC]
      ,[EXP-PS]
      ,[EXP-TOT]
      ,[HOURS]
      ,[TARGET]
      ,[CURR]
      ,[DEPARTMENT]
      ,[DAY-TARGET]
      ,[EQUITY-PR]
      ,[ROLLNO]
      ,[CURRENCY-CODE]
      ,[EMAIL]
      ,[MOD-DATE]
      ,[MOD-TIME]
      ,[MOD-USER]
      ,[USER-ID]
      ,[NOT-FOR-PROFIT-COSTS]
      ,[ANALY-STATUS]
      ,[FEE-CREDIT-FE]
      ,[EffectiveDate]
      ,[EndDate]
	  
from 
(
 
 -- This is the beginning of the merge statement.
  -- The target must be defined, in this example it is our slowly changing
  -- dimension table
  MERGE into [PUB].[FEETR_History] AS target
  -- The source must be defined with the USING clause
  USING 
  (
    -- The source is made up of the attribute columns from the staging table.
    SELECT 
   [FEE-EARNER]
      ,[NAME]
      ,[EXTENSION]
      ,[FESTATUS]
      ,[WEIGHTING]  
      ,[AVAILABILITY]
      ,[COST-RATE]
      ,[CHARGE-RATE]
      ,[EXPENSE-RATE]
      ,[STD-DAY-START]
      ,[STD-DAY-HRS]
      ,[SALARY-ACT]
      ,[SALARY-NOT]
      ,[UPLIFT]
      ,[SALARY-REL]
      ,[EXP-PC]
      ,[EXP-PS]
      ,[EXP-TOT]
      ,[HOURS]
      ,[TARGET]
      ,[CURR]
      ,[DEPARTMENT]
      ,[DAY-TARGET]
      ,[EQUITY-PR]
      ,[ROLLNO]
      ,[CURRENCY-CODE]
      ,[EMAIL]
      ,[MOD-DATE]
      ,[MOD-TIME]
      ,[MOD-USER]
      ,[USER-ID]
      ,[NOT-FOR-PROFIT-COSTS]
      ,[ANALY-STATUS]
      ,[FEE-CREDIT-FE]
    from [PUB].[FEETR] 
  ) AS source 
  ( 
    [FEE-EARNER]
      ,[NAME]
      ,[EXTENSION]
      ,[FESTATUS]
      ,[WEIGHTING]  
      ,[AVAILABILITY]
      ,[COST-RATE]
      ,[CHARGE-RATE]
      ,[EXPENSE-RATE]
      ,[STD-DAY-START]
      ,[STD-DAY-HRS]
      ,[SALARY-ACT]
      ,[SALARY-NOT]
      ,[UPLIFT]
      ,[SALARY-REL]
      ,[EXP-PC]
      ,[EXP-PS]
      ,[EXP-TOT]
      ,[HOURS]
      ,[TARGET]
      ,[CURR]
      ,[DEPARTMENT]
      ,[DAY-TARGET]
      ,[EQUITY-PR]
      ,[ROLLNO]
      ,[CURRENCY-CODE]
      ,[EMAIL]
      ,[MOD-DATE]
      ,[MOD-TIME]
      ,[MOD-USER]
      ,[USER-ID]
      ,[NOT-FOR-PROFIT-COSTS]
      ,[ANALY-STATUS]
      ,[FEE-CREDIT-FE]
  ) ON --We are matching on the [FEE-EARNER] in the target table and the source table.
  (
    target.[FEE-EARNER] = source.[FEE-EARNER]
  )
  -- If the ID's match, then the record has changed;
  -- therefore, update the existing record in the target, end dating the record 
  -- and set the CurrentRecord flag to N
  WHEN MATCHED and target.CurrentRecord='Y'
  THEN 
  UPDATE SET 
    --EndDate=getdate()-1, 
    CurrentRecord='N', 
    LastUpdated=getdate(),
	EndDate= 
	(CASE WHEN [EffectiveDate] = @CURRDT THEN @CURRDT
	      WHEN [EffectiveDate] <> @CURRDT THEN GETDATE()-1
		  END
	)
    
  -- If the ID's do not match, then the record is new;
  -- therefore, insert the new record into the target using the values from the source.
  WHEN NOT MATCHED THEN  
  INSERT 
  (
    [FEE-EARNER]
      ,[NAME]
      ,[EXTENSION]
      ,[FESTATUS]
      ,[WEIGHTING]  
      ,[AVAILABILITY]
      ,[COST-RATE]
      ,[CHARGE-RATE]
      ,[EXPENSE-RATE]
      ,[STD-DAY-START]
      ,[STD-DAY-HRS]
      ,[SALARY-ACT]
      ,[SALARY-NOT]
      ,[UPLIFT]
      ,[SALARY-REL]
      ,[EXP-PC]
      ,[EXP-PS]
      ,[EXP-TOT]
      ,[HOURS]
      ,[TARGET]
      ,[CURR]
      ,[DEPARTMENT]
      ,[DAY-TARGET]
      ,[EQUITY-PR]
      ,[ROLLNO]
      ,[CURRENCY-CODE]
      ,[EMAIL]
      ,[MOD-DATE]
      ,[MOD-TIME]
      ,[MOD-USER]
      ,[USER-ID]
      ,[NOT-FOR-PROFIT-COSTS]
      ,[ANALY-STATUS]
      ,[FEE-CREDIT-FE]
  )
  VALUES 
  (
       Source.[FEE-EARNER]
      ,Source.[NAME]
      ,Source.[EXTENSION]
      ,Source.[FESTATUS]
      ,Source.[WEIGHTING]  
      ,Source.[AVAILABILITY]
      ,Source.[COST-RATE]
      ,Source.[CHARGE-RATE]
      ,Source.[EXPENSE-RATE]
      ,Source.[STD-DAY-START]
      ,Source.[STD-DAY-HRS]
      ,Source.[SALARY-ACT]
      ,Source.[SALARY-NOT]
      ,Source.[UPLIFT]
      ,Source.[SALARY-REL]
      ,Source.[EXP-PC]
      ,Source.[EXP-PS]
      ,Source.[EXP-TOT]
      ,Source.[HOURS]
      ,Source.[TARGET]
      ,Source.[CURR]
      ,Source.[DEPARTMENT]
      ,Source.[DAY-TARGET]
      ,Source.[EQUITY-PR]
      ,Source.[ROLLNO]
      ,Source.[CURRENCY-CODE]
      ,Source.[EMAIL]
      ,Source.[MOD-DATE]
      ,Source.[MOD-TIME]
      ,Source.[MOD-USER]
      ,Source.[USER-ID]
      ,Source.[NOT-FOR-PROFIT-COSTS]
      ,Source.[ANALY-STATUS]
      ,Source.[FEE-CREDIT-FE]
  )
  OUTPUT $action,
    
       Source.[FEE-EARNER]
      ,Source.[NAME]
      ,Source.[EXTENSION]
      ,Source.[FESTATUS]
      ,Source.[WEIGHTING]  
      ,Source.[AVAILABILITY]
      ,Source.[COST-RATE]
      ,Source.[CHARGE-RATE]
      ,Source.[EXPENSE-RATE]
      ,Source.[STD-DAY-START]
      ,Source.[STD-DAY-HRS]
      ,Source.[SALARY-ACT]
      ,Source.[SALARY-NOT]
      ,Source.[UPLIFT]
      ,Source.[SALARY-REL]
      ,Source.[EXP-PC]
      ,Source.[EXP-PS]
      ,Source.[EXP-TOT]
      ,Source.[HOURS]
      ,Source.[TARGET]
      ,Source.[CURR]
      ,Source.[DEPARTMENT]
      ,Source.[DAY-TARGET]
      ,Source.[EQUITY-PR]
      ,Source.[ROLLNO]
      ,Source.[CURRENCY-CODE]
      ,Source.[EMAIL]
      ,Source.[MOD-DATE]
      ,Source.[MOD-TIME]
      ,Source.[MOD-USER]
      ,Source.[USER-ID]
      ,Source.[NOT-FOR-PROFIT-COSTS]
      ,Source.[ANALY-STATUS]
      ,Source.[FEE-CREDIT-FE],
    getdate(),
    '12/31/9999' 
) -- the end of the merge statement
--The changes output below are the records that have changed and will need
--to be inserted into the slowly changing dimension.
as changes
(
        action, 
       [FEE-EARNER]
      ,[NAME]
      ,[EXTENSION]
      ,[FESTATUS]
      ,[WEIGHTING]  
      ,[AVAILABILITY]
      ,[COST-RATE]
      ,[CHARGE-RATE]
      ,[EXPENSE-RATE]
      ,[STD-DAY-START]
      ,[STD-DAY-HRS]
      ,[SALARY-ACT]
      ,[SALARY-NOT]
      ,[UPLIFT]
      ,[SALARY-REL]
      ,[EXP-PC]
      ,[EXP-PS]
      ,[EXP-TOT]
      ,[HOURS]
      ,[TARGET]
      ,[CURR]
      ,[DEPARTMENT]
      ,[DAY-TARGET]
      ,[EQUITY-PR]
      ,[ROLLNO]
      ,[CURRENCY-CODE]
      ,[EMAIL]
      ,[MOD-DATE]
      ,[MOD-TIME]
      ,[MOD-USER]
      ,[USER-ID]
      ,[NOT-FOR-PROFIT-COSTS]
      ,[ANALY-STATUS]
      ,[FEE-CREDIT-FE]
      ,[EffectiveDate]
      ,[EndDate]
)
where action='UPDATE';

declare @InsCnt as int = 0; 
--declare @Effdt as date = convert(date,getdate());
select @InsCnt = count(*) from [PUB].[FEETR_History] 
--where effectivedate = @Effdt ;
where effectivedate = @CURRDT and CurrentRecord = 'Y' ;


DROP TABLE IF EXISTS #maxlstupd
select max(lastupdated) as maxlstupddt into #maxlstupd 
from [PUB].[FEETR_History]
where effectivedate >= @CURRDT and Enddate <> '9999-12-31' and CurrentRecord = 'N' ;

declare @UpdCnt as int = 0;
--declare @Enddt as date = convert(date,getdate()-1);
select @UpdCnt = count(*) from [PUB].[FEETR_History] 
--where Enddate = @Enddt and CurrentRecord = 'N';
where (effectivedate >= @CURRDT and Enddate <> '9999-12-31' and CurrentRecord = 'N')
and lastupdated in (SELECT maxlstupddt FROM #maxlstupd);

INSERT INTO [dbo].[ETLHistoryAuditLog]
([DESCRIPTION],[TOTALRECORD],[RECORDSINSERTED],[RECORDSUPDATED])
VALUES('FEETR_History',@InsCnt,@InsCnt,@UpdCnt);

END TRY
BEGIN CATCH
  DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),  
                @ErrorSeverity INT = ERROR_SEVERITY(),  
                @ErrorNumber INT = ERROR_NUMBER();  
  
  RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);

END CATCH
DROP TABLE IF EXISTS #maxlstupd;
END



GO
/****** Object:  StoredProcedure [dbo].[usp_Load_MATACHistory]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_Load_MATACHistory]
AS

BEGIN
BEGIN TRY
SET NOCOUNT ON;
DECLARE @CURRDT DATE = CONVERT(DATE,GETDATE());
INSERT INTO [PUB].[MATAC_History]
( --Table and columns in which to insert the data
       [MT-CODE]
      ,[BANK-OFFICE]
      ,[BANK-CLIENT]
      ,[BANK-DEPOSIT]
      ,[BAL-UC]
      ,[BAL-UPB]
      ,[BAL-UBD]
      ,[BAL-CLI]
      ,[BAL-DEP]
      ,[BAL-UPC]
      ,[D-LIMIT]
      ,[DATE-LAST-POST]
      ,[CL-CODE]
      ,[TOT-DISBS]
      ,[MOD-DATE]
      ,[MOD-TIME]
      ,[MOD-USER]
      ,[EffectiveDate]
      ,[EndDate]  
  )
-- Select the rows/columns to insert that are output from this merge statement 
-- In this example, the rows to be inserted are the rows that have changed (UPDATE).
select    
       [MT-CODE]
      ,[BANK-OFFICE]
      ,[BANK-CLIENT]
      ,[BANK-DEPOSIT]
      ,[BAL-UC]
      ,[BAL-UPB]
      ,[BAL-UBD]
      ,[BAL-CLI]
      ,[BAL-DEP]
      ,[BAL-UPC]
      ,[D-LIMIT]
      ,[DATE-LAST-POST]
      ,[CL-CODE]
      ,[TOT-DISBS]
      ,[MOD-DATE]
      ,[MOD-TIME]
      ,[MOD-USER]
      ,[EffectiveDate]
      ,[EndDate] 
from 
(
 -- This is the beginning of the merge statement.
  -- The target must be defined, in this example it is our slowly changing
  -- dimension table
  MERGE into [PUB].[MATAC_History] AS target
  -- The source must be defined with the USING clause
  USING 
  (
    -- The source is made up of the attribute columns from the staging table.
    SELECT 
       [MT-CODE]
      ,[BANK-OFFICE]
      ,[BANK-CLIENT]
      ,[BANK-DEPOSIT]
      ,[BAL-UC]
      ,[BAL-UPB]
      ,[BAL-UBD]
      ,[BAL-CLI]
      ,[BAL-DEP]
      ,[BAL-UPC]
      ,[D-LIMIT]
      ,[DATE-LAST-POST]
      ,[CL-CODE]
      ,[TOT-DISBS]
      ,[MOD-DATE]
      ,[MOD-TIME]
      ,[MOD-USER]
    from [PUB].[MATAC] 
  ) AS source 
  ( 
       [MT-CODE]
      ,[BANK-OFFICE]
      ,[BANK-CLIENT]
      ,[BANK-DEPOSIT]
      ,[BAL-UC]
      ,[BAL-UPB]
      ,[BAL-UBD]
      ,[BAL-CLI]
      ,[BAL-DEP]
      ,[BAL-UPC]
      ,[D-LIMIT]
      ,[DATE-LAST-POST]
      ,[CL-CODE]
      ,[TOT-DISBS]
      ,[MOD-DATE]
      ,[MOD-TIME]
      ,[MOD-USER]
  ) ON --We are matching on the [MT-CODE] in the target table and the source table.
  (
    target.[MT-CODE] = source.[MT-CODE]
  )
  -- If the [MT-CODE]'s match, then the record has changed;
  -- therefore, update the existing record in the target, end dating the record 
  -- and set the CurrentRecord flag to N
  WHEN MATCHED and target.CurrentRecord='Y'
  THEN 
  UPDATE SET 
   	CurrentRecord='N', 
    LastUpdated=getdate(),
	EndDate= 
	(CASE WHEN [EffectiveDate] = @CURRDT THEN @CURRDT
	      WHEN [EffectiveDate] <> @CURRDT THEN GETDATE()-1
		  END
	)
    
    
  -- If the [MT-CODE]'s do not match, then the record is new;
  -- therefore, insert the new record into the target using the values from the source.
  WHEN NOT MATCHED THEN  
  INSERT 
  (
       [MT-CODE]
      ,[BANK-OFFICE]
      ,[BANK-CLIENT]
      ,[BANK-DEPOSIT]
      ,[BAL-UC]
      ,[BAL-UPB]
      ,[BAL-UBD]
      ,[BAL-CLI]
      ,[BAL-DEP]
      ,[BAL-UPC]
      ,[D-LIMIT]
      ,[DATE-LAST-POST]
      ,[CL-CODE]
      ,[TOT-DISBS]
      ,[MOD-DATE]
      ,[MOD-TIME]
      ,[MOD-USER]
  )
  VALUES 
  (
       Source.[MT-CODE]
      ,Source.[BANK-OFFICE]
      ,Source.[BANK-CLIENT]
      ,Source.[BANK-DEPOSIT]
      ,Source.[BAL-UC]
      ,Source.[BAL-UPB]
      ,Source.[BAL-UBD]
      ,Source.[BAL-CLI]
      ,Source.[BAL-DEP]
      ,Source.[BAL-UPC]
      ,Source.[D-LIMIT]
      ,Source.[DATE-LAST-POST]
      ,Source.[CL-CODE]
      ,Source.[TOT-DISBS]
      ,Source.[MOD-DATE]
      ,Source.[MOD-TIME]
      ,Source.[MOD-USER]
  )
  OUTPUT $action,
 
       Source.[MT-CODE]
      ,Source.[BANK-OFFICE]
      ,Source.[BANK-CLIENT]
      ,Source.[BANK-DEPOSIT]
      ,Source.[BAL-UC]
      ,Source.[BAL-UPB]
      ,Source.[BAL-UBD]
      ,Source.[BAL-CLI]
      ,Source.[BAL-DEP]
      ,Source.[BAL-UPC]
      ,Source.[D-LIMIT]
      ,Source.[DATE-LAST-POST]
      ,Source.[CL-CODE]
      ,Source.[TOT-DISBS]
      ,Source.[MOD-DATE]
      ,Source.[MOD-TIME]
      ,Source.[MOD-USER],
      getdate(),
      '12/31/9999' 
) -- the end of the merge statement
--The changes output below are the records that have changed and will need
--to be inserted into the slowly changing dimension.
as changes
(
       action, 
       [MT-CODE]
      ,[BANK-OFFICE]
      ,[BANK-CLIENT]
      ,[BANK-DEPOSIT]
      ,[BAL-UC]
      ,[BAL-UPB]
      ,[BAL-UBD]
      ,[BAL-CLI]
      ,[BAL-DEP]
      ,[BAL-UPC]
      ,[D-LIMIT]
      ,[DATE-LAST-POST]
      ,[CL-CODE]
      ,[TOT-DISBS]
      ,[MOD-DATE]
      ,[MOD-TIME]
      ,[MOD-USER]
      ,[EffectiveDate]
      ,[EndDate] 
)
where action='UPDATE';

declare @InsCnt as int = 0; 
select @InsCnt = count(*) from [PUB].[MATAC_History]
where effectivedate = @CURRDT and CurrentRecord = 'Y' ;

DROP TABLE IF EXISTS #maxlstupd
select max(lastupdated) as maxlstupddt into #maxlstupd 
from [PUB].[MATAC_History] 
where effectivedate >= @CURRDT and Enddate <> '9999-12-31' and CurrentRecord = 'N' ;

declare @UpdCnt as int = 0;
select @UpdCnt = count(*) from [PUB].[MATAC_History] 
where (effectivedate >= @CURRDT and Enddate <> '9999-12-31' and CurrentRecord = 'N')
and lastupdated in (SELECT maxlstupddt FROM #maxlstupd);

INSERT INTO [dbo].[ETLHistoryAuditLog]
([DESCRIPTION],[TOTALRECORD],[RECORDSINSERTED],[RECORDSUPDATED])
VALUES('MATAC_History',@InsCnt,@InsCnt,@UpdCnt);

END TRY
BEGIN CATCH
  DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),  
                @ErrorSeverity INT = ERROR_SEVERITY(),  
                @ErrorNumber INT = ERROR_NUMBER();  
  
  RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);
END CATCH
DROP TABLE IF EXISTS #maxlstupd;
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Load_MATDBHistory]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_Load_MATDBHistory]
AS
BEGIN
BEGIN TRY
SET NOCOUNT ON;
DECLARE @CURRDT DATE = CONVERT(DATE,GETDATE());
INSERT INTO [PUB].[MATDB_History]
( --Table and columns in which to insert the data
       [BRANCH]
      ,[MT-CODE]
      ,[CL-CODE]
      ,[MT-TYPE]
      ,[DESCRIPTION]
      ,[FEE-EARNER]
      ,[DATE-OPENED]
      ,[ST-CODE]
      ,[SUPERVISOR]
      ,[ESTIMATE]
      ,[DATE-ESTIMATE]
      ,[DATE-REVIEW1]
      ,[DATE-REVIEW2]
      ,[REFERENCE]
      ,[NOTES]
      ,[REMINDERS]
      ,[DATE-CLOSED]
      ,[RETENTION]
      ,[HOLD-FLAG]
      ,[NO-BILLS]
      ,[BILLD-ANTI-FLAG]
      ,[REGISTER]
      ,[ARCHIVED-FLAG]
      ,[INT-SCAN]
      ,[POST_LOCK]
      ,[OTHER-PARTY]
      ,[LOCATION]
      ,[MOD-DATE]
      ,[YRPR-OPENED]
      ,[MOD-TIME]
      ,[RISK-LEVEL]
      ,[MOD-USER]
      ,[INTRO-CODE]
      ,[MAT-GRP]
      ,[ARCHIVE-ID]
      ,[INTEREST-GROUP]
      ,[DESTRUCTION-DATE]
      ,[PERMITTED-USERS]
      ,[CSB-SCHEME]
      ,[MKT-ACTIVITY]
      ,[ONVERTED-ID]
      ,[ANALY-STATUS]
      ,[DATE-REDACTED]
      ,[REDACTED-BY]
      ,[NEVER-REDACT]
      ,[ACCESS-GROUP-OVERRIDE]
      ,[EffectiveDate]
      ,[EndDate]  
  )
-- Select the rows/columns to insert that are output from this merge statement 
-- In this example, the rows to be inserted are the rows that have changed (UPDATE).
select    
       [BRANCH]
      ,[MT-CODE]
      ,[CL-CODE]
      ,[MT-TYPE]
      ,[DESCRIPTION]
      ,[FEE-EARNER]
      ,[DATE-OPENED]
      ,[ST-CODE]
      ,[SUPERVISOR]
      ,[ESTIMATE]
      ,[DATE-ESTIMATE]
      ,[DATE-REVIEW1]
      ,[DATE-REVIEW2]
      ,[REFERENCE]
      ,[NOTES]
      ,[REMINDERS]
      ,[DATE-CLOSED]
      ,[RETENTION]
      ,[HOLD-FLAG]
      ,[NO-BILLS]
      ,[BILLD-ANTI-FLAG]
      ,[REGISTER]
      ,[ARCHIVED-FLAG]
      ,[INT-SCAN]
      ,[POST_LOCK]
      ,[OTHER-PARTY]
      ,[LOCATION]
      ,[MOD-DATE]
      ,[YRPR-OPENED]
      ,[MOD-TIME]
      ,[RISK-LEVEL]
      ,[MOD-USER]
      ,[INTRO-CODE]
      ,[MAT-GRP]
      ,[ARCHIVE-ID]
      ,[INTEREST-GROUP]
      ,[DESTRUCTION-DATE]
      ,[PERMITTED-USERS]
      ,[CSB-SCHEME]
      ,[MKT-ACTIVITY]
      ,[ONVERTED-ID]
      ,[ANALY-STATUS]
      ,[DATE-REDACTED]
      ,[REDACTED-BY]
      ,[NEVER-REDACT]
      ,[ACCESS-GROUP-OVERRIDE]
      ,[EffectiveDate]
      ,[EndDate] 
from 
(
 -- This is the beginning of the merge statement.
  -- The target must be defined, in this example it is our slowly changing
  -- dimension table
  MERGE into [PUB].[MATDB_History] AS target
  -- The source must be defined with the USING clause
  USING 
  (
    -- The source is made up of the attribute columns from the staging table.
    SELECT 
       [BRANCH]
      ,[MT-CODE]
      ,[CL-CODE]
      ,[MT-TYPE]
      ,[DESCRIPTION]
      ,[FEE-EARNER]
      ,[DATE-OPENED]
      ,[ST-CODE]
      ,[SUPERVISOR]
      ,[ESTIMATE]
      ,[DATE-ESTIMATE]
      ,[DATE-REVIEW1]
      ,[DATE-REVIEW2]
      ,[REFERENCE]
      ,[NOTES]
      ,[REMINDERS]
      ,[DATE-CLOSED]
      ,[RETENTION]
      ,[HOLD-FLAG]
      ,[NO-BILLS]
      ,[BILLD-ANTI-FLAG]
      ,[REGISTER]
      ,[ARCHIVED-FLAG]
      ,[INT-SCAN]
      ,[POST_LOCK]
      ,[OTHER-PARTY]
      ,[LOCATION]
      ,[MOD-DATE]
      ,[YRPR-OPENED]
      ,[MOD-TIME]
      ,[RISK-LEVEL]
      ,[MOD-USER]
      ,[INTRO-CODE]
      ,[MAT-GRP]
      ,[ARCHIVE-ID]
      ,[INTEREST-GROUP]
      ,[DESTRUCTION-DATE]
      ,[PERMITTED-USERS]
      ,[CSB-SCHEME]
      ,[MKT-ACTIVITY]
      ,[ONVERTED-ID]
      ,[ANALY-STATUS]
      ,[DATE-REDACTED]
      ,[REDACTED-BY]
      ,[NEVER-REDACT]
      ,[ACCESS-GROUP-OVERRIDE]
    from  [PUB].[MATDB]
  ) AS source 
  ( 
       [BRANCH]
      ,[MT-CODE]
      ,[CL-CODE]
      ,[MT-TYPE]
      ,[DESCRIPTION]
      ,[FEE-EARNER]
      ,[DATE-OPENED]
      ,[ST-CODE]
      ,[SUPERVISOR]
      ,[ESTIMATE]
      ,[DATE-ESTIMATE]
      ,[DATE-REVIEW1]
      ,[DATE-REVIEW2]
      ,[REFERENCE]
      ,[NOTES]
      ,[REMINDERS]
      ,[DATE-CLOSED]
      ,[RETENTION]
      ,[HOLD-FLAG]
      ,[NO-BILLS]
      ,[BILLD-ANTI-FLAG]
      ,[REGISTER]
      ,[ARCHIVED-FLAG]
      ,[INT-SCAN]
      ,[POST_LOCK]
      ,[OTHER-PARTY]
      ,[LOCATION]
      ,[MOD-DATE]
      ,[YRPR-OPENED]
      ,[MOD-TIME]
      ,[RISK-LEVEL]
      ,[MOD-USER]
      ,[INTRO-CODE]
      ,[MAT-GRP]
      ,[ARCHIVE-ID]
      ,[INTEREST-GROUP]
      ,[DESTRUCTION-DATE]
      ,[PERMITTED-USERS]
      ,[CSB-SCHEME]
      ,[MKT-ACTIVITY]
      ,[ONVERTED-ID]
      ,[ANALY-STATUS]
      ,[DATE-REDACTED]
      ,[REDACTED-BY]
      ,[NEVER-REDACT]
      ,[ACCESS-GROUP-OVERRIDE]
  ) ON --We are matching on the [MT-CODE] in the target table and the source table.
  (
    target.[MT-CODE] = source.[MT-CODE]
  )
  -- If the [MT-CODE]'s match, then the record has changed;
  -- therefore, update the existing record in the target, end dating the record 
  -- and set the CurrentRecord flag to N
  WHEN MATCHED and target.CurrentRecord='Y'
  THEN 
  UPDATE SET 
  	CurrentRecord='N', 
    LastUpdated=getdate(),
	EndDate= 
	(CASE WHEN [EffectiveDate] = @CURRDT THEN @CURRDT
	      WHEN [EffectiveDate] <> @CURRDT THEN GETDATE()-1
		  END
	)
    
  -- If the [MT-CODE]'s do not match, then the record is new;
  -- therefore, insert the new record into the target using the values from the source.
  WHEN NOT MATCHED THEN  
  INSERT 
  (
       [BRANCH]
      ,[MT-CODE]
      ,[CL-CODE]
      ,[MT-TYPE]
      ,[DESCRIPTION]
      ,[FEE-EARNER]
      ,[DATE-OPENED]
      ,[ST-CODE]
      ,[SUPERVISOR]
      ,[ESTIMATE]
      ,[DATE-ESTIMATE]
      ,[DATE-REVIEW1]
      ,[DATE-REVIEW2]
      ,[REFERENCE]
      ,[NOTES]
      ,[REMINDERS]
      ,[DATE-CLOSED]
      ,[RETENTION]
      ,[HOLD-FLAG]
      ,[NO-BILLS]
      ,[BILLD-ANTI-FLAG]
      ,[REGISTER]
      ,[ARCHIVED-FLAG]
      ,[INT-SCAN]
      ,[POST_LOCK]
      ,[OTHER-PARTY]
      ,[LOCATION]
      ,[MOD-DATE]
      ,[YRPR-OPENED]
      ,[MOD-TIME]
      ,[RISK-LEVEL]
      ,[MOD-USER]
      ,[INTRO-CODE]
      ,[MAT-GRP]
      ,[ARCHIVE-ID]
      ,[INTEREST-GROUP]
      ,[DESTRUCTION-DATE]
      ,[PERMITTED-USERS]
      ,[CSB-SCHEME]
      ,[MKT-ACTIVITY]
      ,[ONVERTED-ID]
      ,[ANALY-STATUS]
      ,[DATE-REDACTED]
      ,[REDACTED-BY]
      ,[NEVER-REDACT]
      ,[ACCESS-GROUP-OVERRIDE]
  )
  VALUES 
  (
       Source.[BRANCH]
      ,Source.[MT-CODE]
      ,Source.[CL-CODE]
      ,Source.[MT-TYPE]
      ,Source.[DESCRIPTION]
      ,Source.[FEE-EARNER]
      ,Source.[DATE-OPENED]
      ,Source.[ST-CODE]
      ,Source.[SUPERVISOR]
      ,Source.[ESTIMATE]
      ,Source.[DATE-ESTIMATE]
      ,Source.[DATE-REVIEW1]
      ,Source.[DATE-REVIEW2]
      ,Source.[REFERENCE]
      ,Source.[NOTES]
      ,Source.[REMINDERS]
      ,Source.[DATE-CLOSED]
      ,Source.[RETENTION]
      ,Source.[HOLD-FLAG]
      ,Source.[NO-BILLS]
      ,Source.[BILLD-ANTI-FLAG]
      ,Source.[REGISTER]
      ,Source.[ARCHIVED-FLAG]
      ,Source.[INT-SCAN]
      ,Source.[POST_LOCK]
      ,Source.[OTHER-PARTY]
      ,Source.[LOCATION]
      ,Source.[MOD-DATE]
      ,Source.[YRPR-OPENED]
      ,Source.[MOD-TIME]
      ,Source.[RISK-LEVEL]
      ,Source.[MOD-USER]
      ,Source.[INTRO-CODE]
      ,Source.[MAT-GRP]
      ,Source.[ARCHIVE-ID]
      ,Source.[INTEREST-GROUP]
      ,Source.[DESTRUCTION-DATE]
      ,Source.[PERMITTED-USERS]
      ,Source.[CSB-SCHEME]
      ,Source.[MKT-ACTIVITY]
      ,Source.[ONVERTED-ID]
      ,Source.[ANALY-STATUS]
      ,Source.[DATE-REDACTED]
      ,Source.[REDACTED-BY]
      ,Source.[NEVER-REDACT]
      ,Source.[ACCESS-GROUP-OVERRIDE]
  )
  OUTPUT $action,
      
       Source.[BRANCH]
      ,Source.[MT-CODE]
      ,Source.[CL-CODE]
      ,Source.[MT-TYPE]
      ,Source.[DESCRIPTION]
      ,Source.[FEE-EARNER]
      ,Source.[DATE-OPENED]
      ,Source.[ST-CODE]
      ,Source.[SUPERVISOR]
      ,Source.[ESTIMATE]
      ,Source.[DATE-ESTIMATE]
      ,Source.[DATE-REVIEW1]
      ,Source.[DATE-REVIEW2]
      ,Source.[REFERENCE]
      ,Source.[NOTES]
      ,Source.[REMINDERS]
      ,Source.[DATE-CLOSED]
      ,Source.[RETENTION]
      ,Source.[HOLD-FLAG]
      ,Source.[NO-BILLS]
      ,Source.[BILLD-ANTI-FLAG]
      ,Source.[REGISTER]
      ,Source.[ARCHIVED-FLAG]
      ,Source.[INT-SCAN]
      ,Source.[POST_LOCK]
      ,Source.[OTHER-PARTY]
      ,Source.[LOCATION]
      ,Source.[MOD-DATE]
      ,Source.[YRPR-OPENED]
      ,Source.[MOD-TIME]
      ,Source.[RISK-LEVEL]
      ,Source.[MOD-USER]
      ,Source.[INTRO-CODE]
      ,Source.[MAT-GRP]
      ,Source.[ARCHIVE-ID]
      ,Source.[INTEREST-GROUP]
      ,Source.[DESTRUCTION-DATE]
      ,Source.[PERMITTED-USERS]
      ,Source.[CSB-SCHEME]
      ,Source.[MKT-ACTIVITY]
      ,Source.[ONVERTED-ID]
      ,Source.[ANALY-STATUS]
      ,Source.[DATE-REDACTED]
      ,Source.[REDACTED-BY]
      ,Source.[NEVER-REDACT]
      ,Source.[ACCESS-GROUP-OVERRIDE],
      getdate(),
      '12/31/9999' 
) -- the end of the merge statement
--The changes output below are the records that have changed and will need
--to be inserted into the slowly changing dimension.
as changes
(
       action, 
       [BRANCH]
      ,[MT-CODE]
      ,[CL-CODE]
      ,[MT-TYPE]
      ,[DESCRIPTION]
      ,[FEE-EARNER]
      ,[DATE-OPENED]
      ,[ST-CODE]
      ,[SUPERVISOR]
      ,[ESTIMATE]
      ,[DATE-ESTIMATE]
      ,[DATE-REVIEW1]
      ,[DATE-REVIEW2]
      ,[REFERENCE]
      ,[NOTES]
      ,[REMINDERS]
      ,[DATE-CLOSED]
      ,[RETENTION]
      ,[HOLD-FLAG]
      ,[NO-BILLS]
      ,[BILLD-ANTI-FLAG]
      ,[REGISTER]
      ,[ARCHIVED-FLAG]
      ,[INT-SCAN]
      ,[POST_LOCK]
      ,[OTHER-PARTY]
      ,[LOCATION]
      ,[MOD-DATE]
      ,[YRPR-OPENED]
      ,[MOD-TIME]
      ,[RISK-LEVEL]
      ,[MOD-USER]
      ,[INTRO-CODE]
      ,[MAT-GRP]
      ,[ARCHIVE-ID]
      ,[INTEREST-GROUP]
      ,[DESTRUCTION-DATE]
      ,[PERMITTED-USERS]
      ,[CSB-SCHEME]
      ,[MKT-ACTIVITY]
      ,[ONVERTED-ID]
      ,[ANALY-STATUS]
      ,[DATE-REDACTED]
      ,[REDACTED-BY]
      ,[NEVER-REDACT]
      ,[ACCESS-GROUP-OVERRIDE]
      ,[EffectiveDate]
      ,[EndDate]  
)
where action='UPDATE';

declare @InsCnt as int = 0; 
select @InsCnt = count(*) from [PUB].[MATDB_History]
where effectivedate = @CURRDT and CurrentRecord = 'Y' ;

DROP TABLE IF EXISTS #maxlstupd
select max(lastupdated) as maxlstupddt into #maxlstupd 
from [PUB].[MATDB_History] 
where effectivedate >= @CURRDT and Enddate <> '9999-12-31' and CurrentRecord = 'N' ;

declare @UpdCnt as int = 0;
select @UpdCnt = count(*) from [PUB].[MATDB_History]
where (effectivedate >= @CURRDT and Enddate <> '9999-12-31' and CurrentRecord = 'N')
and lastupdated in (SELECT maxlstupddt FROM #maxlstupd);

INSERT INTO [dbo].[ETLHistoryAuditLog]
([DESCRIPTION],[TOTALRECORD],[RECORDSINSERTED],[RECORDSUPDATED])
VALUES('MATDB_History',@InsCnt,@InsCnt,@UpdCnt);

END TRY
BEGIN CATCH
  DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),  
                @ErrorSeverity INT = ERROR_SEVERITY(),  
                @ErrorNumber INT = ERROR_NUMBER();  
  
  RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);

END CATCH
DROP TABLE IF EXISTS #maxlstupd;
END



GO
/****** Object:  StoredProcedure [dbo].[usp_MultiViewconfiguration]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
-- [usp_MultiViewconfiguration] 4,'Review1','1_2,2_1,3_4,4_3'    
CREATE Procedure [dbo].[usp_MultiViewconfiguration]    
@Trackerid int,
@ViewName varchar(100),
@UpdateViewName varchar(100),
@SelectedOrderwithId varchar(500),
@IsActive bit=null,
@StatementType varchar(10),
@TransactionDate  datetime,
@User varchar(100)

AS      
Begin      
 
  SELECT  [TrackerId]=@Trackerid,ViewName=@UpdateViewName,IsSelected=1, PARSENAME(REPLACE(value, '_', '.'), 2) AS ColumnRefId,PARSENAME(REPLACE(value, '_', '.'), 1) AS orderedValue
				INTO #selectDataTable from (select value 
											from 
											STRING_SPLIT(@SelectedOrderwithId,',')
											) as cl

 IF(@StatementType = 'CREATE')
 BEGIN



  IF(@IsActive = 1)
	BEGIN
		UPDATE [tblMultiViewConfiguration] SET IsActive=0,UpdatedBy=@User,UpdatedDate=@TransactionDate WHERE TrackerId=@Trackerid
	END
 
	DECLARE @ViewsCount INT
	
	SELECT @ViewsCount=COUNT(*) FROM [tblMultiViewConfiguration] WHERE TrackerId=@Trackerid
	
	IF(@ViewsCount = 0)
	BEGIN
			SET @IsActive=1
	END


  INSERT INTO [tblMultiViewConfiguration] 
(ViewName,
 TrackerId,
 IsSelected,
 IsOrdered,
 ColumnRefId,
 IsActive,
 CreateBy,
CreateDate,
TransactionStatus
 )
SELECT ViewName,
	   TrackerId,
	   IsSelected,
	   orderedValue,
	   ColumnRefId,
	   @IsActive,
	   @User,
	   @TransactionDate,
	   @StatementType
	FROM 
 #selectDataTable 


 END
 ELSE IF(@StatementType = 'DELETE')
 BEGIN
		DELETE FROM [tblMultiViewConfiguration] WHERE TrackerId = @Trackerid AND ViewName=@ViewName
		
		   DECLARE @NumberofviewCount INT
		   SELECT @NumberofviewCount=COUNT(DISTINCT(VIEWNAME)) from [tblMultiViewConfiguration] WHERE TrackerId=@Trackerid
	
			IF(@NumberofviewCount = 1)
			BEGIN
				UPDATE [tblMultiViewConfiguration] SET IsActive=1,UpdatedBy=@User,UpdatedDate=@TransactionDate WHERE TrackerId=@Trackerid
			END


 END
 ELSE IF(@StatementType = 'UPDATE')
 BEGIN

		 IF(@IsActive = 1)
		   BEGIN
				UPDATE [tblMultiViewConfiguration] SET IsActive=0,UpdatedBy=@User,UpdatedDate=@TransactionDate,TransactionStatus=@StatementType WHERE TrackerId=@Trackerid
		   END


		   DECLARE @ViewCount INT
		   SELECT @ViewCount=COUNT(DISTINCT(VIEWNAME)) from [tblMultiViewConfiguration] WHERE TrackerId=@Trackerid
	
			IF(@ViewCount = 1)
			BEGIN
					SET @IsActive=1
			END

		    UPDATE [dbo].[tblMultiViewConfiguration]  SET isOrdered=sel.orderedValue,ViewName=@UpdateViewName,IsActive=@IsActive
														,UpdatedBy=@User,UpdatedDate=@TransactionDate,TransactionStatus=@StatementType
				from [tblMultiViewConfiguration] join #selectDataTable as sel on sel.ColumnRefId=[tblMultiViewConfiguration].ColumnRefId
				where sel.TrackerId=tblMultiViewConfiguration.TrackerId AND tblMultiViewConfiguration.ViewName=@ViewName

			DELETE FROM [tblMultiViewConfiguration] WHERE TrackerId=@Trackerid AND ViewName=@UpdateViewName AND 
					ColumnRefId NOT IN ( SELECT ColumnRefId FROM #selectDataTable)

			 INSERT INTO [tblMultiViewConfiguration] 
			(ViewName,
			 TrackerId,
			 IsSelected,
			 IsOrdered,
			 ColumnRefId,
			 IsActive,
			 UpdatedBy,
			 UpdatedDate,
			 TransactionStatus
			 )
			SELECT ViewName,
				   TrackerId,
				   IsSelected,
				   orderedValue,
				   ColumnRefId,
				   @IsActive,
				   @User,
				   @TransactionDate,
				   @StatementType
				FROM 
			 #selectDataTable WHERE ColumnRefId NOT IN (select ColumnRefId from [tblMultiViewConfiguration] WHERE TrackerId=@Trackerid AND ViewName=@UpdateViewName)

			 UPDATE [tblMultiViewQueryBulderDetails] set ViewName=@UpdateViewName where TrackerRefID=@Trackerid AND ViewName=@ViewName

 END


DROP TABLE #selectDataTable


End    
    
GO
/****** Object:  StoredProcedure [dbo].[usp_OOHEmailConfig_Update]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_OOHEmailConfig_Update]   
@ID Int,
@MailSubject Varchar(100),
@MailBody Varchar(Max)

AS  
BEGIN  
BEGIN TRY

UPDATE dbo.tblOOHEmailConfig   
   SET    MailSubject=@MailSubject,
		  MailBody=@MailBody
		  Where Id = @Id 


    
  END TRY
BEGIN CATCH

        DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),
                @ErrorSeverity INT = ERROR_SEVERITY(),
                @ErrorNumber INT = ERROR_NUMBER();

		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);
 
END CATCH
  
   
END  
GO
/****** Object:  StoredProcedure [dbo].[usp_OutOfHoursIDCheckProcess_Download]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Modified date: <16-04-2021> <Included Exception handling>

--[usp_OutOfHoursIDCheckProcess_Download] 4,201
CREATE Procedure [dbo].[usp_OutOfHoursIDCheckProcess_Download]    
@TrackerId int,  
@processId int,
@ViewName Varchar(100)      
  
AS      
Begin 
BEGIN TRY



SELECT [ID] = OOH.ID
      ,[Datetimetocall]
      ,[MatterNumber]
      ,[PersonNeedingAMLCheck]
      ,[BestNumberToCall]
      ,[AdditionalInstructions]
      ,[PropertyLawyerEmailAddress]
      ,[EmailAddressYours]
      ,[AMLCallSuccessfull]  = CASE WHEN OOH.AMLCallSuccessfull = 1 then 'Yes' else 'No' END
      ,[Consultant_Other]
      ,[OOHNotes]
      ,[AttachFile]
	   INTO #FilterResult
  FROM [tblOutOfHoursIDCheckProcessForm] OOH
  LEFT JOIN [dbo].[tblOOHConsultant] Consultant ON OOH.ConsultantID = Consultant.ID

  where OOH.ID in (select distinct FormDataId
				FROM [dbo].[tblTrackerForms]  
				with (nolock) where TrackerId=@TrackerId and ProcessId=@processId  ) 
order by 1 asc

 DECLARE @sql nvarchar(max);

SELECT  @sql = 'SELECT * FROM #FilterResult ' + dbo.FuncDynamicQueryResult(@TrackerId, @ViewName)

EXEC sp_executesql @sql


END TRY
BEGIN CATCH

        DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),
                @ErrorSeverity INT = ERROR_SEVERITY(),
                @ErrorNumber INT = ERROR_NUMBER();

		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);
 
END CATCH
DROP TABLE #FilterResult

End    
    
GO
/****** Object:  StoredProcedure [dbo].[usp_OutOfHoursIDCheckProcessForm_AuditView]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ajay George Kalathil
-- Create date: 6th May 2021
-- Description:	This SP will be used to get the Audit Trial Version View for the front end system
-- =============================================


--Exec usp_OutOfHoursIDCheckProcessForm_AuditView 17,null,null
CREATE PROCEDURE [dbo].[usp_OutOfHoursIDCheckProcessForm_AuditView]
	-- Add the parameters for the stored procedure here
	@ID Int,  
	@FromDate datetime2 = '1900-01-01 00:00:00.0000000', 
	@ToDate datetime2	= '9999-12-31 23:59:59.9999999'
AS
BEGIN

  BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    
;WITH T
AS (
    SELECT		OOH.ID	
	            ,Datetimetocall as Datetimetocall , lag(Datetimetocall) over(order by enddate asc) as Prev_Datetimetocall
				,MatterNumber as MatterNumber , lag(MatterNumber) over(order by enddate asc) as Prev_MatterNumber
				,PersonNeedingAMLCheck as PersonNeedingAMLCheck , lag(PersonNeedingAMLCheck) over(order by enddate asc) as Prev_PersonNeedingAMLCheck
				,BestNumberToCall as BestNumberToCall , lag(BestNumberToCall) over(order by enddate asc) as Prev_BestNumberToCall
				,AdditionalInstructions as AdditionalInstructions, lag(AdditionalInstructions) over(order by enddate asc) as Prev_AdditionalInstructions
				,PropertyLawyerEmailAddress as PropertyLawyerEmailAddress , lag(PropertyLawyerEmailAddress) over(order by enddate asc) as Prev_PropertyLawyerEmailAddress
				,EmailAddressYours as EmailAddressYours , lag(EmailAddressYours) over(order by enddate asc) as Prev_EmailAddressYours
				,ISNULL(AMLCallSuccessfull,0) as AMLCallSuccessfull ,ISNULL(lag(AMLCallSuccessfull) over(order by enddate asc),0) as Prev_AMLCallSuccessfull
				,Consultant as Consultant , lag(Consultant) over(order by enddate asc) as Prev_Consultant
				,Consultant_Other as Consultant_Other , lag(Consultant_Other) over(order by enddate asc) as Prev_Consultant_Other				
				,OOHNotes as OOHNotes , lag(OOHNotes) over(order by enddate asc) as Prev_OOHNotes				
				,AttachFile as AttachFile , lag(AttachFile) over(order by enddate asc) as Prev_AttachFile				
				,ModifyingUser as ModifyingUser , lag(ModifyingUser) over(order by enddate asc) as Prev_ModifyingUser
				,Action as Action , lag(Action) over(order by enddate asc) as Prev_Action
				,BeginDate
				,EndDate
				,ROW_Number() over(partition by  OOH.Id Order by EndDate asc) as VersionNumber
    FROM dbo.tblOutOfHoursIDCheckProcessForm FOR SYSTEM_TIME ALL AS OOH
  LEFT JOIN [dbo].[tblOOHConsultant] Consultant ON OOH.ConsultantID = Consultant.ID
    WHERE OOH.ID =@ID
)
SELECT T.BeginDate,
       T.EndDate,
       C.ColName as ColumnName,
	   t.VersionNumber,	   
       C.present_Value as CurrentValue,
       C.prev_value as PreviousValue,
	   T.ModifyingUser as ModifiedBy
FROM T
    CROSS APPLY
    (
        VALUES		    
			 ('Date time to call', Convert(varchar(10), T.Datetimetocall,103) , Convert(varchar(10),T.Prev_Datetimetocall,103))
			,('Matter Number', Convert(varchar, T.MatterNumber) , Convert(varchar,T.Prev_MatterNumber))
			,('Person Needing AML Check', Convert(varchar, T.PersonNeedingAMLCheck) , Convert(varchar,T.Prev_PersonNeedingAMLCheck))
			,('Best Number To Call', Convert(varchar, T.BestNumberToCall) , Convert(varchar,T.Prev_BestNumberToCall))	
			,('Additional Instructions', Convert(varchar, T.AdditionalInstructions) , Convert(varchar,T.Prev_AdditionalInstructions))
			,('Property Lawyer Email Address', Convert(varchar, T.PropertyLawyerEmailAddress) , Convert(varchar,T.Prev_PropertyLawyerEmailAddress))
			,('Email Address Yours', Convert(varchar, T.EmailAddressYours) , Convert(varchar,T.EmailAddressYours))			
			,('AML Call Successfull', Convert(varchar,case when T.AMLCallSuccessfull=1 then 'Yes' else 'No' end) , Convert(varchar,case when T.Prev_AMLCallSuccessfull=1 then 'Yes' else 'No' end))
			,('Consultant', Convert(varchar, T.Consultant_Other) , Convert(varchar,T.Prev_Consultant_Other))
			--,('Consultant Other', Convert(varchar, T.Consultant_Other) , Convert(varchar,T.Prev_Consultant_Other))			
			,('OOH Notes', Convert(varchar, T.OOHNotes) , Convert(varchar,T.Prev_OOHNotes))
			,('Attach File', Convert(varchar, T.AttachFile) , Convert(varchar,T.Prev_AttachFile)) 						
			,('ModifyingUser', Convert(varchar, T.ModifyingUser) , Convert(varchar,T.Prev_ModifyingUser))
			,('Action', Convert(varchar, T.Action) , Convert(varchar,T.Prev_Action))
			) AS C (ColName, present_Value, prev_value)
WHERE  EXISTS
(
    SELECT present_Value 
    EXCEPT 
    SELECT C.prev_value
)  and Convert(datetime,BeginDate) >= @FromDate and Convert(Datetime,BeginDate) <=@ToDate

END TRY
BEGIN CATCH

        DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),
                @ErrorSeverity INT = ERROR_SEVERITY(),
                @ErrorNumber INT = ERROR_NUMBER();

		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);
 
END CATCH
END

GO
/****** Object:  StoredProcedure [dbo].[usp_OutOfHoursIDCheckProcessForm_InsertUpdate]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_OutOfHoursIDCheckProcessForm_InsertUpdate]    
 -- Add the parameters for the stored procedure here  
@ID Int= 0,  
@Datetimetocall Datetime = Null,
@MatterNumber Varchar(20) = Null,
@PersonNeedingAMLCheck Varchar(50) = Null,
@BestNumberToCall Varchar(50) = Null,
@AdditionalInstructions Varchar(Max) = Null,
@PropertyLawyerEmailAddress Varchar(50) = Null,
@EmailAddressYours Varchar(50) = Null,
@AMLCallSuccessfull Bit = Null,
@ConsultantID Int = Null,
@Consultant_Other Varchar(50) = Null,
@OOHNotes Varchar(Max) = Null,
@AttachFile Varchar(Max) = Null,   
    
 @StatementType  Varchar(1)=NULL,        
 @TrackerId int =NULL,          
 @CreatedBy Varchar(256)=NULL,        
 @processid int =NULL,        
 @RoutingID Int=NULL,  
 @selectedData varchar(100) = NULL    
       
AS            
  BEGIN        
  BEGIN TRY
-- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements  
 SET NOCOUNT ON;  
  
  
 Declare @trgProcessId int;     
    -- This performs INSERT action against OutOfHoursIDCheckProcessForm table  
IF @StatementType = 'I'            
 BEGIN      
 INSERT INTO [dbo].[tblOutOfHoursIDCheckProcessForm]  
           (Datetimetocall,
			MatterNumber ,
			PersonNeedingAMLCheck ,
			BestNumberToCall ,
			AdditionalInstructions ,
			PropertyLawyerEmailAddress ,
			EmailAddressYours ,
			AMLCallSuccessfull,
			ConsultantID ,
			Consultant_Other ,
			OOHNotes ,
			AttachFile ,  
            [Action] , 
            [ModifyingUser]   
     )  
     VALUES  
   (    
			@Datetimetocall,
			@MatterNumber ,
			@PersonNeedingAMLCheck ,
			@BestNumberToCall ,
			@AdditionalInstructions ,
			@PropertyLawyerEmailAddress ,
			@EmailAddressYours ,
			@AMLCallSuccessfull,
			@ConsultantID ,
			@Consultant_Other ,
			@OOHNotes ,
			@AttachFile ,  
            @StatementType,  
            @CreatedBy   
 )    
Declare @FormDataId int;          
 set @FormDataId = SCOPE_IDENTITY()          
        
 SELECT @trgProcessId=trgProcessId FROM tblRoutingDetails  
  WHERE TrackerId= @TrackerId AND srcprocessid = @processid AND RoutingID = @RoutingID        
    
   -- This is common across all the trackers and perofrms INSERT action against tbltrackersform table   
    EXEC usp_TrackersForm_Insert @TrackerId, @CreatedBy, @trgProcessId,  @FormDataId  
     
 END     
  
  
  SELECT @trgProcessId=trgProcessId FROM tblRoutingDetails WHERE TrackerId= @TrackerId AND srcprocessid =@ProcessId            
 AND RoutingID = @RoutingID     
  
  
  
--This performs UPDATE action against OutOfHoursIDCheckProcessForm table  
IF @StatementType = 'U'            
  BEGIN       
         
 UPDATE [dbo].[tblOutOfHoursIDCheckProcessForm]  
SET Datetimetocall = @Datetimetocall,
	MatterNumber = @MatterNumber ,
	PersonNeedingAMLCheck = @PersonNeedingAMLCheck,
	BestNumberToCall = @BestNumberToCall,
	AdditionalInstructions = @AdditionalInstructions,
	PropertyLawyerEmailAddress = @PropertyLawyerEmailAddress,
	EmailAddressYours  = @EmailAddressYours,
	AMLCallSuccessfull =@AMLCallSuccessfull,
	ConsultantID  = @ConsultantID,
	Consultant_Other = @Consultant_Other,
	OOHNotes  = @OOHNotes,
	AttachFile = @AttachFile ,   
	[ModifyingUser] = @CreatedBy,    
    [Action] = @StatementType  
 WHERE ID=@ID  
        
   -- This is common across all the trackers and perofrms UPDATE action against tbltrackersform table   
     EXEC [dbo].[usp_TrackersForm_Update] @ID,@TrackerId, @ProcessId,@CreatedBy, @RoutingID, @selectedData,@StatementType  
      
       
  END     
    
--This performs DELETE/RE ACTIVATE action against OutOfHoursIDCheckProcessForm table  
IF (@StatementType = 'D' OR  @StatementType = 'R')          
  BEGIN         
  
  
    UPDATE dbo.tblOutOfHoursIDCheckProcessForm   
   SET    
       [ModifyingUser] = @CreatedBy,    
       [Action] = @StatementType    
 WHERE Id IN (SELECT CAST(value AS INTEGER) FROM STRING_SPLIT(@selectedData, ','))           
    
   -- This is common across all the trackers and UPDATING Process id in tbltrackersform table based on Routing details  
    EXEC [dbo].[usp_TrackersForm_Update] @ID,@TrackerId, @ProcessId,@CreatedBy, @RoutingID, @selectedData,@StatementType  
       
  END    
  
END TRY
BEGIN CATCH

        DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),
                @ErrorSeverity INT = ERROR_SEVERITY(),
                @ErrorNumber INT = ERROR_NUMBER();

		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);
 
END CATCH
  
   
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Over5KReport]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[usp_Over5KReport]
@ReportType int,
@Fromdate date= null,
@Todate date= null
AS
BEGIN
if
@ReportType=1
BEGIN
Select
distinct MATDB_0.Notes As 'Legal Team',
FEETR_0.NAME As 'Fee Earner',
MATDB_0."MT-CODE" As 'Matter Number',
MATDB_0.Description As 'Matter Description',
MATAC_0."BAL-CLI" As 'Client Balance'
INTO #OVER
From
PUB.FEETR FEETR_0, PUB.MATAC MATAC_0, PUB.MATDB MATDB_0

Where
MATAC_0."MT-CODE"= MATDB_0."MT-CODE" AND
FEETR_0.NAME = MATDB_0."FEE-EARNER" AND 
MATAC_0."BAL-CLI">5000 --AND MATDB_0.NOTES Like '%Cardiff%'

SELECT CASE
WHEN [Legal Team] LIKE '%Cardiff%' THEN 'Cardiff'
WHEN [Legal Team] LIKE '%Manchester%' THEN 'Manchester'
ELSE  'OTHER'
END
[LEGAL TEAM],SUM([CLIENT BALANCE])[VALUES], COUNT([Matter Number]) [NO Of Files] FROM #OVER
GROUP BY CASE
WHEN [Legal Team] LIKE '%Cardiff%' THEN 'Cardiff'
WHEN [Legal Team] LIKE '%Manchester%' THEN 'Manchester'
ELSE 'OTHER'
END
drop table #OVER
end
else
begin
Select
distinct MATDB_0.Notes As 'Legal Team',
FEETR_0.NAME As 'Fee Earner',
MATDB_0."MT-CODE" As 'Matter Number',
MATDB_0.Description As 'Matter Description',
MATAC_0."BAL-CLI" As 'Client Balance'
INTO #OVER1
From
PUB.FEETR_History FEETR_0, PUB.MATAC_History MATAC_0, PUB.MATDB_History MATDB_0

Where
MATAC_0."MT-CODE"= MATDB_0."MT-CODE" AND
FEETR_0.NAME = MATDB_0."FEE-EARNER" AND 
MATAC_0."BAL-CLI">5000 --AND MATDB_0.NOTES Like '%Cardiff%'
EXCEPT
Select
MATDB_0.Notes As 'Legal Team',
FEETR_0.NAME As 'Fee Earner',
MATDB_0."MT-CODE" As 'Matter Number',
MATDB_0.Description As 'Matter Description',
MATAC_0."BAL-CLI" As 'Client Balance'
--INTO #OVER
From
PUB.FEETR_History FEETR_0, PUB.MATAC_History MATAC_0, PUB.MATDB_History MATDB_0
Where 1=0

SELECT CASE
WHEN [Legal Team] LIKE '%Cardiff%' THEN 'Cardiff'
WHEN [Legal Team] LIKE '%Manchester%' THEN 'Manchester'
ELSE  'OTHER'
END
[LEGAL TEAM],SUM([CLIENT BALANCE])[VALUES], COUNT([matter number]) [NO Of Files] FROM #OVER1
GROUP BY CASE
WHEN [Legal Team] LIKE '%Cardiff%' THEN 'Cardiff'
WHEN [Legal Team] LIKE '%Manchester%' THEN 'Manchester'
ELSE 'OTHER'
END
--drop table #OVER1

drop table #OVER1
end
end
GO
/****** Object:  StoredProcedure [dbo].[usp_Panel_Download]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
/****** Script for SelectTopNRows command from SSMS  ******/    
    
-- Modified date: <16-04-2021> <Included Exception handling>  
    
-- [usp_Panel_Download] 23,201       
CREATE Procedure [dbo].[usp_Panel_Download]        
@TrackerId int,      
@processId int,  
@ViewName Varchar(100)  
      
AS          
Begin     
BEGIN TRY    
    
SELECT ID = Panel.ID    
      ,tblTypeOfPanel.TypeOfPanel   
   ,Reason.ReasonForPanel  
      ,ClientName    
      ,[Address]    
      ,TMID    
      ,Details    
      ,PanelFrim_FundsGoing    
      ,PanelFrim_FundsComing    
      ,tblAwaitingFunds.AwaitingFunds    
      ,Amount    
      ,ClientAccounts_complete = case when Panel.ClientAccounts_complete = 1 then 'Yes' else 'No' end    
      ,ActionrequiredByPanel = case when Panel.ActionrequiredByPanel = 1 then 'Yes' else 'No' end    
      ,PanelTeamComplete = case when Panel.PanelTeamComplete = 1 then 'Yes' else 'No' end   
   ,[PanelFirmAllocated_Appointed]  
   ,[AttachFile]  
   ,[ModifyingUser]  
      ,[Action]  
      ,[BeginDate]  
      ,[EndDate]  
  INTO #FilterResult  
  FROM tblPanelForm AS Panel    
  Left Join tblTypeOfPanel on Panel.TypeOfPanelID = tblTypeOfPanel.ID    
  Left Join [tblReasonForPanel] Reason on Panel.ReasonForPanelID = Reason.ID   
  Left Join tblAwaitingFunds on Panel.AwaitingFundsId = tblAwaitingFunds.ID    
    
  where Panel.id in (select distinct FormDataId    
    FROM tblTrackerForms      
    with (nolock) where TrackerId=@TrackerId and ProcessId=@processId  )     
 order by 1 asc    
  
  DECLARE @sql nvarchar(max);  
  
SELECT  @sql = 'SELECT * FROM #FilterResult ' + dbo.FuncDynamicQueryResult(@TrackerId, @ViewName)  
  
EXEC sp_executesql @sql  
     
END TRY  
BEGIN CATCH  
  
        DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),  
                @ErrorSeverity INT = ERROR_SEVERITY(),  
                @ErrorNumber INT = ERROR_NUMBER();  
  
  RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);  
   
END CATCH  
  DROP TABLE #FilterResult
End        
        
GO
/****** Object:  StoredProcedure [dbo].[usp_PanelForm_AuditView]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--Exec usp_PanelForm_AuditView 1,null,null
CREATE PROCEDURE [dbo].[usp_PanelForm_AuditView]
	-- Add the parameters for the stored procedure here
	@ID Int,  
	@FromDate datetime2 = '1900-01-01 00:00:00.0000000', 
	@ToDate datetime2	= '9999-12-31 23:59:59.9999999'
AS
BEGIN

  BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    
;WITH T
AS (
    SELECT		Panel.ID
				,TypeOfPanel as TypeOfPanel , lag(TypeOfPanel) over(order by enddate asc) as Prev_TypeOfPanel
				,ReasonForPanel as ReasonForPanel , lag(ReasonForPanel) over(order by enddate asc) as Prev_ReasonForPanel
				,ClientName as ClientName , lag(ClientName) over(order by enddate asc) as Prev_ClientName
				,Address as Address , lag(Address) over(order by enddate asc) as Prev_Address
				,TMID as TMID , lag(TMID) over(order by enddate asc) as Prev_TMID
				,Details as Details , lag(Details) over(order by enddate asc) as Prev_Details 
				,PanelFrim_FundsGoing as PanelFrim_FundsGoing , lag(PanelFrim_FundsGoing) over(order by enddate asc) as Prev_PanelFrim_FundsGoing
				,PanelFrim_FundsComing as PanelFrim_FundsComing , lag(PanelFrim_FundsComing) over(order by enddate asc) as Prev_PanelFrim_FundsComing
				,AwaitingFunds as AwaitingFunds , lag(AwaitingFunds) over(order by enddate asc) as Prev_AwaitingFunds
				,Amount as Amount , lag(Amount) over(order by enddate asc) as Prev_Amount
				,ISNULL(ClientAccounts_complete,0) as ClientAccounts_complete , ISNULL(lag(ClientAccounts_complete) over(order by enddate asc),0) as Prev_ClientAccounts_complete
				,ISNULL(ActionrequiredByPanel,0) as ActionrequiredByPanel ,ISNULL( lag(ActionrequiredByPanel) over(order by enddate asc),0) as Prev_ActionrequiredByPanel
				,ISNULL(PanelTeamComplete,0) as PanelTeamComplete , ISNULL(lag(PanelTeamComplete) over(order by enddate asc),0) as Prev_PanelTeamComplete
				,PanelFirmAllocated_Appointed as PanelFirmAllocated_Appointed , lag(PanelFirmAllocated_Appointed) over(order by enddate asc) as Prev_PanelFirmAllocated_Appointed
				,AttachFile as AttachFile , lag(AttachFile) over(order by enddate asc) as Prev_AttachFile
				,ModifyingUser as ModifyingUser , lag(ModifyingUser) over(order by enddate asc) as Prev_ModifyingUser
				,Action as Action , lag(Action) over(order by enddate asc) as Prev_Action
				,BeginDate
				,EndDate
				,ROW_Number() over(partition by  Panel.Id Order by EndDate asc) as VersionNumber
    FROM dbo.tblPanelForm FOR SYSTEM_TIME ALL AS Panel  
  Left Join tblTypeOfPanel on Panel.TypeOfPanelID = tblTypeOfPanel.ID  
  Left Join [tblReasonForPanel] Reason on Panel.ReasonForPanelID = Reason.ID 
  Left Join tblAwaitingFunds on Panel.AwaitingFundsId = tblAwaitingFunds.ID 
    WHERE Panel.id =@ID
)
SELECT T.BeginDate,
       T.EndDate,
       C.ColName as ColumnName,
	   t.VersionNumber,	   
       C.present_Value as CurrentValue,
       C.prev_value as PreviousValue,
	   T.ModifyingUser as ModifiedBy
FROM T
    CROSS APPLY
    (
        VALUES
		
			('Type Of Panel', Convert(varchar, T.TypeOfPanel) , Convert(varchar,T.Prev_TypeOfPanel))
			,('Reason For Panel', Convert(varchar, T.ReasonForPanel) , Convert(varchar,T.Prev_ReasonForPanel))
			,('Client Name', Convert(varchar, T.ClientName) , Convert(varchar,T.Prev_ClientName))
			,('Address', Convert(varchar, T.Address) , Convert(varchar,T.Prev_Address))
			,('TMID/eDIF No', Convert(varchar, T.TMID) , Convert(varchar,T.Prev_TMID))
			,('Details', Convert(varchar, T.Details) , Convert(varchar,T.Prev_Details))
			,('Panel Firm (Funds going to)', Convert(varchar, T.PanelFrim_FundsGoing) , Convert(varchar,T.Prev_PanelFrim_FundsGoing))
			,('Panel Firm (Funds going from)', Convert(varchar, T.PanelFrim_FundsComing) , Convert(varchar,T.Prev_PanelFrim_FundsComing))
			,('Awaiting funds?', Convert(varchar, T.AwaitingFunds) , Convert(varchar,T.Prev_AwaitingFunds))
			,('Amount', Convert(varchar, T.Amount) , Convert(varchar,T.Prev_Amount))
			,('Client accounts - complete', Convert(varchar, case when T.ClientAccounts_complete=1 then 'Yes' else 'No' end) , Convert(varchar,case when T.Prev_ClientAccounts_complete=1 then 'Yes' else 'No' end))
			,('Action required by panel', Convert(varchar, case when T.ActionrequiredByPanel=1 then 'Yes' else 'No' end) , Convert(varchar,case when T.Prev_ActionrequiredByPanel=1 then 'Yes' else 'No' end))
			,('Panel team complete', Convert(varchar, case when T.PanelTeamComplete=1 then 'Yes' else 'No' end) , Convert(varchar,case when T.Prev_PanelTeamComplete=1 then 'Yes' else 'No' end))
			,('Panel firm allocated/appointed', Convert(varchar, T.PanelFirmAllocated_Appointed) , Convert(varchar,T.Prev_PanelFirmAllocated_Appointed))
			,('Attach File', Convert(varchar, T.AttachFile) , Convert(varchar,T.Prev_AttachFile))
			,('ModifyingUser', Convert(varchar, T.ModifyingUser) , Convert(varchar,T.Prev_ModifyingUser))
			,('Action', Convert(varchar, T.Action) , Convert(varchar,T.Prev_Action))
			) AS C (ColName, present_Value, prev_value)
WHERE  EXISTS
(
    SELECT present_Value 
    EXCEPT 
    SELECT C.prev_value
)  and Convert(datetime,BeginDate) >= @FromDate and Convert(Datetime,BeginDate) <=@ToDate

END TRY
BEGIN CATCH

        DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),
                @ErrorSeverity INT = ERROR_SEVERITY(),
                @ErrorNumber INT = ERROR_NUMBER();

		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);
 
END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[usp_PanelForm_InsertUpdate]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
-- =============================================  
-- Author:  <Author : CKusume>  
-- Create date: <12-04-2021>  
-- Modified date : <12-04-2021><13-04-2021,Sundar> <Included Exception handling>  
-- Description: <Description,The purpose of the Stored Procedure is to make CRUD operations on Panel Form tables>  
-- Generic Info: <I - Insert , U- Update, D- Delete R-Re activate>    
-- =============================================  
 -- [usp_PanelForm_InsertUpdate] 0,1,1,'abc','abc','abc','abc','abc','abc',1,1234,1,1,1,'abc','abc','I',23,'Arun',201,1,1
 --[usp_PanelForm_InsertUpdate] 1,1,1,'abc','abc','abcd','abc','abc','abc',1,1234,1,1,1,'abc','abc','U',23,'Arun',201,1,1
 --[usp_PanelForm_InsertUpdate] 0,1,1,'abc','abc','abc','abc','abc','abc',1,1234,1,1,1,'abc','abc','D',23,'Arun',201,1,1
 -- [usp_PanelForm_InsertUpdate] 0,1,1,'abc','abc','abc','abc','abc','abc',1,1234,1,1,1,'abc','abc','R',23,'Arun',201,1,1

  --select * from tblPanelForm

  --SELECT * FROM [dbo].[tblPanelForm] FOR SYSTEM_TIME ALL WHERE ID = 1

--SELECT * FROM [dbo].[tblPanelForm]

CREATE PROCEDURE [dbo].[usp_PanelForm_InsertUpdate]     
 -- Add the parameters for the stored procedure here    
 @ID int = 0,      
 @TypeOfPanelID int=NULL,
 @ReasonForPanelID Int NULL,
 @ClientName varchar(200)=NULL,      
 @Address varchar(200)=NULL,      
 @TMID varchar(200)=NULL,      
 @Details varchar(Max)=NULL,  
 @PanelFrim_FundsGoing varchar(200) =NULL ,  
 @PanelFrim_FundsComing varchar(200)=NULL,  
 @AwaitingFundsID int=NULL,  
 @Amount int=NULL,  
 @ClientAccounts_complete bit=NULL,  
 @ActionrequiredByPanel bit=NULL,  
 @PanelTeamComplete bit=NULL,
@PanelFirmAllocated_Appointed [varchar](100) NULL,
@AttachFile Varchar(Max) Null,

      
 @StatementType  Varchar(50)=NULL,          
 @TrackerId int =NULL,            
 @CreatedBy Varchar(50)=NULL,          
 @processid int =NULL,          
 @RoutingID Int=NULL,    
 @selectedData varchar(100) = NULL      
AS    
BEGIN    
BEGIN TRY
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements    
 SET NOCOUNT ON;    
    
    
 Declare @trgProcessId int;       
    -- This performs INSERT action against PanelForm table    
IF @StatementType = 'I'              
 BEGIN           
           
   INSERT INTO dbo.tblPanelForm       
(TypeOfPanelID  ,
ReasonForPanelID ,
ClientName  ,      
Address ,      
TMID  ,      
Details,  
PanelFrim_FundsGoing,  
PanelFrim_FundsComing,  
AwaitingFundsID,  
Amount,  
ClientAccounts_complete,  
ActionrequiredByPanel,  
PanelTeamComplete, 
PanelFirmAllocated_Appointed,
AttachFile,

[Action],    
[ModifyingUser],
BeginDateLocalTime
       
)      
      
   Values      
(@TypeOfPanelID, 
@ReasonForPanelID,      
@ClientName  ,      
@Address  ,      
@TMID  ,      
@Details,    
@PanelFrim_FundsGoing,  
@PanelFrim_FundsComing,  
@AwaitingFundsID,  
@Amount,  
@ClientAccounts_complete,  
@ActionrequiredByPanel,  
@PanelTeamComplete,
@PanelFirmAllocated_Appointed,
@AttachFile,

@StatementType,    
@CreatedBy,
GETDATE())      
      
    Declare @FormDataId int;            
 set @FormDataId = SCOPE_IDENTITY()            
          
 SELECT @trgProcessId=trgProcessId FROM tblRoutingDetails    
  WHERE TrackerId= @TrackerId AND srcprocessid = @processid AND RoutingID = @RoutingID          
      
   -- This is common across all the trackers and perofrms INSERT action against tbltrackersform table     
    EXEC usp_TrackersForm_Insert @TrackerId, @CreatedBy, @trgProcessId,  @FormDataId    
       
 END       
    
    
  SELECT @trgProcessId=trgProcessId FROM tblRoutingDetails WHERE TrackerId= @TrackerId AND srcprocessid =@ProcessId              
 AND RoutingID = @RoutingID       
    
    
    
--This performs UPDATE action against PanelForm table    
IF @StatementType = 'U'              
  BEGIN           
     
    UPDATE dbo.tblPanelForm     
   SET      
[TypeOfPanelID] = @TypeOfPanelID ,
ReasonForPanelID= @ReasonForPanelID,
ClientName = @ClientName ,      
Address  = @Address,  
TMID  = @TMID,  
Details = @Details ,  
PanelFrim_FundsGoing = @PanelFrim_FundsGoing,  
PanelFrim_FundsComing = @PanelFrim_FundsComing,  
AwaitingFundsID = @AwaitingFundsID,  
Amount = @Amount,    
ClientAccounts_complete = @ClientAccounts_complete,  
ActionrequiredByPanel = @ActionrequiredByPanel,  
PanelTeamComplete = @PanelTeamComplete,
PanelFirmAllocated_Appointed = @PanelFirmAllocated_Appointed,
AttachFile = @AttachFile,

[ModifyingUser] = @CreatedBy,      
[Action] = @StatementType,
BeginDateLocalTime=GETDATE()
WHERE Id = @Id        
                    
      
 EXEC [dbo].[usp_TrackersForm_Update] @ID,@TrackerId, @ProcessId,@CreatedBy, @RoutingID, @selectedData,@StatementType         
            
         
  END       
      
--This performs DELETE/RE ACTIVATE action against PanelForm table    
IF (@StatementType = 'D' OR  @StatementType = 'R')            
  BEGIN           
    
    
    UPDATE dbo.tblPanelForm     
   SET      
       [ModifyingUser] = @CreatedBy,      
       [Action] = @StatementType      
 WHERE Id IN (SELECT CAST(value AS INTEGER) FROM STRING_SPLIT(@selectedData, ','))             
      
   -- This is common across all the trackers and UPDATING Process id in tbltrackersform table based on Routing details    
    EXEC [dbo].[usp_TrackersForm_Update] @ID,@TrackerId, @ProcessId,@CreatedBy, @RoutingID, @selectedData,@StatementType    
         
  END      
    
END TRY
BEGIN CATCH

        DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),
                @ErrorSeverity INT = ERROR_SEVERITY(),
                @ErrorNumber INT = ERROR_NUMBER();

		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);
 
END CATCH
     
END
GO
/****** Object:  StoredProcedure [dbo].[usp_queryBuilderImport]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- [usp_CompletionDiaryImport] 
CREATE Procedure [dbo].[usp_queryBuilderImport]              
@queryBuilderType [tblQueryBuilderMockImport] readonly
--,
--@trackerid  int,
--@viewname varchar(100)
            
AS                
Begin           




DELETE FROM [tblMultiViewQueryBulderDetails] WHERE TrackerRefID in (SELECT top 1 TrackerRefID FROM @queryBuilderType) AND ViewName=(SELECT top 1 ViewName FROM @queryBuilderType)

INSERT INTO [dbo].[tblMultiViewQueryBulderDetails](
TrackerRefID,
ViewName,
ColumnRefID,
OperatorRefID,
Condition,
SearchName,
IsOrdered
)
SELECT 
	TrackerRefID,
ViewName,
ColumnRefID,
OperatorRefID,
Condition,
SearchName,
SortValue
	FROM @queryBuilderType 

INSERT INTO tblquerybuilderlog
VALUES((SELECT top 1 TrackerRefID FROM @queryBuilderType),(SELECT top 1 ViewName FROM @queryBuilderType),dbo.FuncDynamicQueryResult((SELECT top 1 TrackerRefID FROM @queryBuilderType),(SELECT top 1 ViewName FROM @queryBuilderType)),getdate())


End      
      
GO
/****** Object:  StoredProcedure [dbo].[usp_SalesLead_Download]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Modified date: <16-04-2021> <Included Exception handling>  
    
-- [sp_GetAllSalesLeadDownload] 21,201     
CREATE Procedure [dbo].[usp_SalesLead_Download]        
@TrackerId int,      
@processId int,  
@ViewName Varchar(100)  
      
AS          
Begin     
BEGIN TRY    
    
SELECT ID = SALES.ID    
      ,tblConsultant.Consultant    
      ,LeadReceived    
      ,tblSource.Sourcename    
      ,Area=s1.Area          
      ,tblBrand.Brand    
      ,tblSalesLeadBranch.Branch    
      ,Referrer=Referrer_Other    
      ,Referrer_Other    
      ,tblTransactionType.TransactionType    
      ,CustomerName    
      ,CustomerMobile    
      ,CustomerEmail    
      ,PropertyPrice    
      ,NextFollowUpDue    
      ,tblLeadStatus.LeadStatus    
      ,tblPendingReason.PendingReason    
      ,tblDeclineReason.DeclineReason    
      ,tblNotValidReason.NotValidreason    
      ,Notes    
      ,PC =  case when Sales.PC = 1 then 'Yes' else 'No' end    
      ,InstructionDate    
      ,eDif_Or_TMID    
      ,TotalFeeQuoted    
      ,Discount  
   ,Revenue  
      ,AttachFile   INTO #FilterResult  
  FROM dbo.tblSalesLeadForm As SALES    
  LEFT JOIN tblConsultant on SALES.ConsultantId = tblConsultant.ID    
  LEFT JOIN tblSource on SALES.SourceID = tblSource.ID    
  LEFT JOIN tblArea s1 on SALES.AreaID = s1.ID    
  LEFT JOIN tblBrand on SALES.BrandId = tblBrand.ID    
  LEFT JOIN tblSalesLeadBranch on SALES.BranchId = tblSalesLeadBranch.ID    
  LEFT JOIN tblReferrer on SALES.ReferrerId = tblReferrer.ID    
  LEFT JOIN tblTransactionType on SALES.TransactionId = tblTransactionType.ID    
  LEFT JOIN tblLeadStatus on SALES.LeadStatusId = tblLeadStatus.ID    
    
  LEFT JOIN tblPendingReason on SALES.PendingReasonId = tblPendingReason.ID    
  LEFT JOIN tblDeclineReason on SALES.DeclineReasonId = tblDeclineReason.ID    
  LEFT JOIN tblNotValidReason on SALES.NotValidreasonId = tblNotValidReason.ID    
     
 where SALES.ID in (select distinct FormDataId    
    FROM [dbo].[tblTrackerForms]      
    with (nolock) where TrackerId=@TrackerId and ProcessId=@processId  )     
 order by 1 asc    
  
  DECLARE @sql nvarchar(max);  
  
SELECT  @sql = 'SELECT * FROM #FilterResult ' + dbo.FuncDynamicQueryResult(@TrackerId, @ViewName)  
  
EXEC sp_executesql @sql  
  
    
END TRY  
BEGIN CATCH  
  
        DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),  
                @ErrorSeverity INT = ERROR_SEVERITY(),  
                @ErrorNumber INT = ERROR_NUMBER();  
  
  RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);  
   
END CATCH  

DROP TABLE #FilterResult
End        
GO
/****** Object:  StoredProcedure [dbo].[usp_SalesLeadForm_AuditView]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================  
-- Author:  Ajay George Kalathil  
-- Create date: 6th May 2021  
-- Description: This SP will be used to get the Audit Trial Version View for the front end system  
-- =============================================  
  
  
--Exec usp_SalesLeadForm_AuditView 4,null,null  
CREATE PROCEDURE [dbo].[usp_SalesLeadForm_AuditView]  
 -- Add the parameters for the stored procedure here  
 @ID Int,    
 @FromDate datetime2 = '1900-01-01 00:00:00.0000000',   
 @ToDate datetime2 = '9999-12-31 23:59:59.9999999'  
AS  
BEGIN  
  
  BEGIN TRY  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
  
      
;WITH T  
AS (  
    SELECT  SALES.ID                     
    ,Consultant as Consultant , lag(Consultant) over(order by enddate asc) as Prev_Consultant     
    ,LeadReceived as LeadReceived , lag(LeadReceived) over(order by enddate asc) as Prev_LeadReceived  
    ,SourceName as Source , lag(SourceName) over(order by enddate asc) as Prev_Source  
    ,Area as Area , lag(Area) over(order by enddate asc) as Prev_Area  
    ,Brand as Brand , lag(Brand) over(order by enddate asc) as Prev_Brand  
    ,Branch as Branch , lag(Branch) over(order by enddate asc) as Prev_Branch  
    ,Referrer as Referrer , lag(Referrer) over(order by enddate asc) as Prev_Referrer  
    ,Referrer_Other as Referrer_Other , lag(Referrer_Other) over(order by enddate asc) as Prev_Referrer_Other  
    ,TransactionType as TransactionType , lag(TransactionType) over(order by enddate asc) as Prev_TransactionType  
    ,CustomerName as CustomerName , lag(CustomerName) over(order by enddate asc) as Prev_CustomerName  
    ,CustomerMobile as CustomerMobile , lag(CustomerMobile) over(order by enddate asc) as Prev_CustomerMobile  
    ,CustomerEmail as CustomerEmail , lag(CustomerEmail) over(order by enddate asc) as Prev_CustomerEmail  
    ,PropertyPrice as PropertyPrice , lag(PropertyPrice) over(order by enddate asc) as Prev_PropertyPrice   
    ,NextFollowUpDue as NextFollowUpDue , lag(NextFollowUpDue) over(order by enddate asc) as Prev_NextFollowUpDue  
    ,LeadStatus as LeadStatus , lag(LeadStatus) over(order by enddate asc) as Prev_LeadStatus  
    ,PendingReason as PendingReason , lag(PendingReason) over(order by enddate asc) as Prev_PendingReason  
    ,DeclineReason as DeclineReason , lag(DeclineReason) over(order by enddate asc) as Prev_DeclineReason  
    ,NotValidReason as NotValidReason , lag(NotValidReason) over(order by enddate asc) as Prev_NotValidReason  
    ,Notes as Notes , lag(Notes) over(order by enddate asc) as Prev_Notes  
    ,ISNULL(PC,0) as PC , ISNULL(lag(PC) over(order by enddate asc),0) as Prev_PC  
    ,InstructionDate as InstructionDate , lag(InstructionDate) over(order by enddate asc) as Prev_InstructionDate  
    ,eDif_Or_TMID as eDif_Or_TMID , lag(eDif_Or_TMID) over(order by enddate asc) as Prev_eDif_Or_TMID  
    ,TotalFeeQuoted as TotalFeeQuoted , lag(TotalFeeQuoted) over(order by enddate asc) as Prev_TotalFeeQuoted  
    ,Discount as Discount , lag(Discount) over(order by enddate asc) as Prev_Discount  
    ,Revenue as Revenue , lag(Revenue) over(order by enddate asc) as Prev_Revenue  
    ,AttachFile as AttachFile , lag(AttachFile) over(order by enddate asc) as Prev_AttachFile         
    ,ModifyingUser as ModifyingUser , lag(ModifyingUser) over(order by enddate asc) as Prev_ModifyingUser  
    ,Action as Action , lag(Action) over(order by enddate asc) as Prev_Action  
    ,BeginDate  
    ,EndDate  
    ,ROW_Number() over(partition by  SALES.Id Order by EndDate asc) as VersionNumber  
    FROM dbo.tblSalesLeadForm FOR SYSTEM_TIME ALL AS SALES    
  LEFT JOIN tblConsultant on SALES.ConsultantId = tblConsultant.ID    
  LEFT JOIN tblSource on SALES.SourceID = tblSource.ID    
  LEFT JOIN tblArea s1 on SALES.AreaID = s1.ID    
  LEFT JOIN tblBrand on SALES.BrandId = tblBrand.ID    
  LEFT JOIN tblSalesLeadBranch on SALES.BranchId = tblSalesLeadBranch.ID    
  LEFT JOIN tblReferrer on SALES.ReferrerId = tblReferrer.ID    
  LEFT JOIN tblTransactionType on SALES.TransactionId = tblTransactionType.ID    
  LEFT JOIN tblLeadStatus on SALES.LeadStatusId = tblLeadStatus.ID    
    
  LEFT JOIN tblPendingReason on SALES.PendingReasonId = tblPendingReason.ID    
  LEFT JOIN tblDeclineReason on SALES.DeclineReasonId = tblDeclineReason.ID    
  LEFT JOIN tblNotValidReason on SALES.NotValidreasonId = tblNotValidReason.ID  
    WHERE SALES.id =@ID  
)  
SELECT T.BeginDate,  
       T.EndDate,  
       C.ColName as ColumnName,  
    t.VersionNumber,      
       C.present_Value as CurrentValue,  
       C.prev_value as PreviousValue,  
    T.ModifyingUser as ModifiedBy  
FROM T  
    CROSS APPLY  
    (  
        VALUES              
    ('Consultant', Convert(varchar, T.Consultant) , Convert(varchar,T.Prev_Consultant))    
   ,('Lead Received', Convert(varchar(10), T.LeadReceived,103) , Convert(varchar(10),T.Prev_LeadReceived,103))   
   ,('Source ', Convert(varchar, T.Source) , Convert(varchar,T.Prev_Source))  
   ,('Area ', Convert(varchar, T.Area) , Convert(varchar,T.Prev_Area))  
   ,('Brand ', Convert(varchar, T.Brand) , Convert(varchar,T.Prev_Brand))  
   ,('Branch', Convert(varchar, T.Branch) , Convert(varchar,T.Prev_Branch))  
   ,('Referrer', Convert(varchar, T.Referrer) , Convert(varchar,T.Prev_Referrer))  
   ,('Referrer Other', Convert(varchar, T.Referrer_Other) , Convert(varchar,T.Prev_Referrer_Other))  
   ,('Transaction ', Convert(varchar, T.TransactionType) , Convert(varchar,T.Prev_TransactionType))  
   ,('Customer Name', Convert(varchar, T.CustomerName) , Convert(varchar,T.Prev_CustomerName))  
   ,('Customer Mobile', Convert(varchar, T.CustomerMobile) , Convert(varchar,T.Prev_CustomerMobile))  
   ,('Customer Email', Convert(varchar, T.CustomerEmail) , Convert(varchar,T.Prev_CustomerEmail))  
   ,('Property Price', Convert(varchar, T.PropertyPrice) , Convert(varchar,T.PropertyPrice))  
   ,('Next Follow Up Due', Convert(varchar(10), T.NextFollowUpDue,103) , Convert(varchar(10),T.Prev_NextFollowUpDue,103))  
   ,('Lead Status  ', Convert(varchar, T.LeadStatus) , Convert(varchar,T.Prev_LeadStatus))  
   ,('Pending Reason  ', Convert(varchar, T.PendingReason) , Convert(varchar,T.Prev_PendingReason))  
   ,('Decline Reason ', Convert(varchar, T.DeclineReason) , Convert(varchar,T.Prev_DeclineReason))  
   ,('Not Valid reason ', Convert(varchar, T.NotValidReason) , Convert(varchar,T.Prev_NotValidReason))  
   ,('Notes ', Convert(varchar, T.Notes ) , Convert(varchar,T.Prev_Notes))  
   ,('PC', Convert(varchar,case when T.PC=1 then 'Yes' else 'No' end) , Convert(varchar,case when T.Prev_PC=1 then 'Yes' else 'No' end))  
   ,('Instruction Date ', Convert(varchar(10), T.InstructionDate ,103) , Convert(varchar(10),T.Prev_InstructionDate ,103))   
   ,('eDif_Or_TMID', Convert(varchar, T.eDif_Or_TMID) , Convert(varchar,T.Prev_eDif_Or_TMID))  
   ,('Total Fee Quoted', Convert(varchar, T.TotalFeeQuoted) , Convert(varchar,T.Prev_TotalFeeQuoted))  
   ,('Discount', Convert(varchar, T.Discount) , Convert(varchar,T.Discount))  
   ,('Revenue', Convert(varchar, T.Revenue) , Convert(varchar,T.Revenue))  
   ,('Attach File', Convert(varchar, T.AttachFile) , Convert(varchar,T.Prev_AttachFile))      
   ,('ModifyingUser', Convert(varchar, T.ModifyingUser) , Convert(varchar,T.Prev_ModifyingUser))  
   ,('Action', Convert(varchar, T.Action) , Convert(varchar,T.Prev_Action))  
   ) AS C (ColName, present_Value, prev_value)  
WHERE  EXISTS  
(  
    SELECT present_Value   
    EXCEPT   
    SELECT C.prev_value  
)  and Convert(datetime,BeginDate) >= @FromDate and Convert(Datetime,BeginDate) <=@ToDate  
  
END TRY  
BEGIN CATCH  
  
        DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),  
                @ErrorSeverity INT = ERROR_SEVERITY(),  
                @ErrorNumber INT = ERROR_NUMBER();  
  
  RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);  
   
END CATCH  
END  
  
GO
/****** Object:  StoredProcedure [dbo].[usp_SalesLeadForm_InsertUpdate]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


  
-- =============================================  
-- Author:  <Author : Phani>  
-- Create date: <12-04-2021>  
-- Modified date : <13-04-2021>  <13-04-2021,Sundar> <Included Exception handling>
-- Description: <Description,The purpose of the Stored Procedure is to make CRUD operations on   SalesLeadForm tables>  
-- Generic Info: <I - Insert , U- Update, D- Delete R-Re activate>    
-- =============================================  

--TrackerId =21


--insert exec [dbo].[usp_SalesLeadForm_InsertUpdate] 0,1,'2021-03-31 14:06:01.827',1,1,1,1,1,'Test',1,'TestName',916094012,'Test@g.com',9089,'2021-04-13 14:06:01.827',1,1,1,1,'Test_Notes',1,'2021-04-13 14:06:01.827','Test','Test',6767,3434,'TestFile','I',21,'Phani',101,0
 --update exec [dbo].[usp_SalesLeadForm_InsertUpdate] 3,1,'2021-03-31 14:06:01.827',1,1,1,1,1,'Test',1,'TestNameUpdate',916094012,'Test@g.com',9089,'2021-04-13 14:06:01.827',1,1,1,1,'Test_Notes_Updated',1,'2021-04-13 14:06:01.827','TestUpdate','TestUpdate',6767,3434,'TestFile','U',21,'Phani',201,1
 --DELETE exec [dbo].[usp_SalesLeadForm_InsertUpdate] 0,1,'2021-03-31 14:06:01.827',1,1,1,1,1,'Test',1,'TestNameDelete',916094012,'Test@g.com',9089,'2021-04-13 14:06:01.827',1,1,1,1,'Test_Notes_Updated',1,'2021-04-13 14:06:01.827','TestDelete','TestDelete',6767,3434,'TestFile','D',21,'Phani',201,0,3
 --Re-activate exec [dbo].[usp_SalesLeadForm_InsertUpdate] 0,1,'2021-03-31 14:06:01.827',1,1,1,1,1,'Test',1,'TestNameReActive',916094012,'Test@g.com',9089,'2021-04-13 14:06:01.827',1,1,1,1,'Test_Notes_ReActive',1,'2021-04-13 14:06:01.827','TestReActive','TestReActive',6767,3434,'TestFile','R',21,'Phani',601,9,3
 --select * from [dbo].[tblSalesLeadForm] for system_time all
CREATE PROCEDURE [dbo].[usp_SalesLeadForm_InsertUpdate]   
(  
 @ID int =0,    
 @ConsultantId int =NULL ,   
 @LeadReceived datetime=NULL ,    
 @SourceID int =NULL,    
 @AreaID int=NULL,    
 @BrandId int=NULL,    
 @BranchId int=NULL ,    
 @ReferrerId int =NULL ,    
 @Referrer_Other varchar(50) =NULL,    
 @TransactionId int =NULL,    
 @CustomerName varchar(50) =NULL,    
 @CustomerMobile int =NULL,       
 @CustomerEmail Varchar(50) =NULL, 
 @PropertyPrice money=NULL,        
 @NextFollowUpDue datetime =NULL,         
 @LeadStatusId int=NULL,       
 @PendingReasonId int =NULL,      
 @DeclineReasonId Int=NULL,  
 @NotValidreasonId int=NULL,
 @Notes varchar(max)=NULL,
 @PC bit=NULL, 
 @InstructionDate datetime=NULL,
 @eDif_Or_TMID varchar(50)=NULL, 
 @TotalFeeQuoted varchar(50)=NULL, 
 @Discount money=NULL, 
 @Revenue money=NULL,
 @AttachFile varchar(max)=NULL, 


 @StatementType  Varchar(5)=NULL,    
 @TrackerId int =NULL,          
 @CreatedBy nvarchar(128)=NULL,        
 @processid int =NULL,        
 @RoutingID Int=NULL,  
 @selectedData varchar(100) = NULL




)   
  
AS  
BEGIN  
BEGIN TRY
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements  
 SET NOCOUNT ON;  
  
  
 Declare @trgProcessId int;     
    -- This performs INSERT action against StopChequeForm table  
IF @StatementType = 'I'            
 BEGIN         
         
  INSERT INTO [dbo].[tblSalesLeadForm]    
   (    
[ConsultantId],     
[LeadReceived]  ,    
[SourceID] ,  
[AreaID],
[BrandId],
[BranchId],
[ReferrerId],
[Referrer_Other],
[TransactionId],
[CustomerName],
[CustomerMobile],
[CustomerEmail],
[PropertyPrice],
[NextFollowUpDue],
[LeadStatusId],
[PendingReasonId],
[DeclineReasonId],
[NotValidreasonId],
[Notes],
[PC],
[InstructionDate],
[eDif_Or_TMID],
[TotalFeeQuoted],
[Discount],
[Revenue],
[AttachFile],
[ModifyingUser],
[Action]
 )  
     
Values    
( @ConsultantId ,
  @LeadReceived,
  @SourceID,
  @AreaID,@BrandId,@BranchId,@ReferrerId,@Referrer_Other,@TransactionId,@CustomerName,@CustomerMobile,
   @CustomerEmail,@PropertyPrice,@NextFollowUpDue,@LeadStatusId,@PendingReasonId,@DeclineReasonId,@NotValidreasonId,
    @Notes,@PC,@InstructionDate,@eDif_Or_TMID,@TotalFeeQuoted,@Discount,@Revenue,@AttachFile, @CreatedBy,@StatementType)



    Declare @FormDataId int;          
 set @FormDataId = SCOPE_IDENTITY()          
        
 SELECT @trgProcessId=trgProcessId FROM tblRoutingDetails  
  WHERE TrackerId= @TrackerId AND srcprocessid = @processid AND RoutingID = @RoutingID        
    
   -- This is common across all the trackers and perofrms INSERT action against tbltrackersform table   
    EXEC usp_TrackersForm_Insert @TrackerId, @CreatedBy, @trgProcessId,  @FormDataId  
     
 END     
  
  
  SELECT @trgProcessId=trgProcessId FROM tblRoutingDetails WHERE TrackerId= @TrackerId AND   
  srcprocessid =@ProcessId            
 AND RoutingID = @RoutingID     
  
  
   
--This performs UPDATE action against StopChequeForm table  
IF @StatementType = 'U'            
  BEGIN         
   
  


UPDATE   [dbo].[tblSalesLeadForm] Set  
 [ConsultantId]=    @ConsultantId , 
[LeadReceived]  = @LeadReceived,
[SourceID] =  @SourceID,
[AreaID]= @AreaID,
[BrandId]=@BrandId,
[BranchId]=@BranchId,
[ReferrerId]=@ReferrerId,
[Referrer_Other]=@Referrer_Other,
[TransactionId]=@TransactionId,
[CustomerName]=@CustomerName,
[CustomerMobile]=@CustomerMobile,
[CustomerEmail]=@CustomerEmail,
[PropertyPrice]=@PropertyPrice,
[NextFollowUpDue]=@NextFollowUpDue,
[LeadStatusId]=@LeadStatusId,
[PendingReasonId]=@PendingReasonId,
[DeclineReasonId]=@DeclineReasonId,
[NotValidreasonId]=@NotValidreasonId,
[Notes]=@Notes,
[PC]=@PC,
[InstructionDate]=@InstructionDate,
[eDif_Or_TMID]=@eDif_Or_TMID,
[TotalFeeQuoted]=@TotalFeeQuoted,
[Discount]=@Discount,
[Revenue]=@Revenue,
[AttachFile]=@AttachFile,
[ModifyingUser]=@CreatedBy,
[Action]=@StatementType
   
WHERE Id = @Id      
               
			   
			    -- This is common across all the trackers and perofrms UPDATE action against tbltrackersform table 
   		EXEC [dbo].[usp_TrackersForm_Update] @ID,@TrackerId, @ProcessId,@CreatedBy,
		@RoutingID, @selectedData,@StatementType

    
 --UPDATE tblTrackerForms   
 --           SET  ProcessId = @trgProcessId,              
 --    CreationDate = getdate(),            
 --    CreatedBy= @CreatedBy,            
 --    FormDataTable= 'tblTrackerForms'            
 --    WHERE FormDataId= @ID  AND TrackerId= @TrackerId        
          
       
  END     
    
--This performs DELETE/RE ACTIVATE action against StopChequeForm table  
IF (@StatementType = 'D' OR  @StatementType = 'R')          
  BEGIN         
  
  
    UPDATE   [dbo].[tblSalesLeadForm]   
   SET    
       [ModifyingUser] = @CreatedBy,    
       [Action] = @StatementType    
 WHERE Id IN (SELECT CAST(value AS INTEGER) FROM STRING_SPLIT(@selectedData, ','))           
    
     -- This is common across all the trackers and UPDATING Process id in tbltrackersform table based on Routing details  
    EXEC [dbo].[usp_TrackersForm_Update] @Id,@TrackerId, @ProcessId,@CreatedBy, @RoutingID, @selectedData ,@StatementType 
       
  END    
END TRY
BEGIN CATCH

        DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),
                @ErrorSeverity INT = ERROR_SEVERITY(),
                @ErrorNumber INT = ERROR_NUMBER();

		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);
 
END CATCH
  
   
END  
GO
/****** Object:  StoredProcedure [dbo].[usp_StopCheque_Download]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/****** Script for SelectTopNRows command from SSMS  ******/


CREATE Procedure [dbo].[usp_StopCheque_Download]            
@TrackerId int,          
@processId int,
@ViewName Varchar(100)              
          
AS              
Begin         
 BEGIN TRY

SELECT ID = SC.ID 
      ,MatterNumber
      ,ClientName
      ,Center.Center
      ,Payee
      ,IssuedDate
      ,Amount
      ,ReasonForStoppingCheque
      ,AdvisePayeeChequeStopped
      ,AssignedTo
      ,STS.[Status]
      ,ChqNo
	  INTO #FilterResult
  FROM tblStopChequeForm SC
  LEFT JOIN dbo.tblStopChequeCenter Center on SC.CenterID = Center.ID
  LEFT JOIN dbo.tblStopChequeStatus STS on SC.StatusId = STS.ID

  where SC.id in (select distinct FormDataId
				FROM [dbo].[tblTrackerForms]  
				with (nolock) where TrackerId=@TrackerId and ProcessId=@processId  ) 
	order by 1 asc

	 DECLARE @sql nvarchar(max);

SELECT  @sql = 'SELECT * FROM #FilterResult ' + dbo.FuncDynamicQueryResult(@TrackerId, @ViewName)

EXEC sp_executesql @sql

     

	END TRY
Begin Catch
Select 
		Error_Number() as errorNumber,
		Error_Message() as errorMessage;
		End Catch

DROP TABLE #FilterResult

End    
    
GO
/****** Object:  StoredProcedure [dbo].[usp_StopChequeForm_AuditView]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ajay George Kalathil
-- Create date: 6th May 2021
-- Description:	This SP will be used to get the Audit Trial Version View for the front end system
-- =============================================


--Exec usp_StopChequeForm_AuditView 7,null,null
CREATE PROCEDURE [dbo].[usp_StopChequeForm_AuditView]
	-- Add the parameters for the stored procedure here
	@ID Int,  
	@FromDate datetime2 = '1900-01-01 00:00:00.0000000', 
	@ToDate datetime2	= '9999-12-31 23:59:59.9999999'
AS
BEGIN

  BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    
;WITH T
AS (
    SELECT		SC.ID
	            ,MatterNumber as MatterNumber , lag(MatterNumber) over(order by enddate asc) as Prev_MatterNumber
	            ,ClientName as ClientName , lag(ClientName) over(order by enddate asc) as Prev_ClientName								
				,Center as Center , lag(Center) over(order by enddate asc) as Prev_Center			
				,Payee as Payee , lag(Payee) over(order by enddate asc) as Prev_Payee
				,IssuedDate as IssuedDate , lag(IssuedDate) over(order by enddate asc) as Prev_IssuedDate				
				,Amount as Amount , lag(Amount) over(order by enddate asc) as Prev_Amount				
				,ReasonForStoppingCheque as ReasonForStoppingCheque , lag(ReasonForStoppingCheque) over(order by enddate asc) as Prev_ReasonForStoppingCheque
				,AdvisePayeeChequeStopped as AdvisePayeeChequeStopped , lag(AdvisePayeeChequeStopped) over(order by enddate asc) as Prev_AdvisePayeeChequeStopped
				,AssignedTo as AssignedTo , lag(AssignedTo) over(order by enddate asc) as Prev_AssignedTo
				,STS.STATUS as Status , lag(STS.Status) over(order by enddate asc) as Prev_Status
				,ChqNo as ChqNo , lag(ChqNo) over(order by enddate asc) as Prev_ChqNo				
				,ModifyingUser as ModifyingUser , lag(ModifyingUser) over(order by enddate asc) as Prev_ModifyingUser
				,Action as Action , lag(Action) over(order by enddate asc) as Prev_Action
				,BeginDate
				,EndDate
				,ROW_Number() over(partition by  SC.Id Order by EndDate asc) as VersionNumber
    FROM dbo.tblStopChequeForm FOR SYSTEM_TIME ALL AS SC
  LEFT JOIN dbo.tblStopChequeCenter Center on SC.CenterID = Center.ID
  LEFT JOIN dbo.tblStopChequeStatus STS on SC.StatusId = STS.ID
    WHERE SC.id =@ID
)
SELECT T.BeginDate,
       T.EndDate,
       C.ColName as ColumnName,
	   t.VersionNumber,	   
       C.present_Value as CurrentValue,
       C.prev_value as PreviousValue,
	   T.ModifyingUser as ModifiedBy
FROM T
    CROSS APPLY
    (
        VALUES	
		     ('Matter Number', Convert(varchar, T.MatterNumber) , Convert(varchar,T.Prev_MatterNumber)) 
		    ,('Client Name', Convert(varchar, T.ClientName) , Convert(varchar,T.Prev_ClientName))						
			,('Center', Convert(varchar, T.Center) , Convert(varchar,T.Prev_Center))
			,('Payee', Convert(varchar, T.Payee) , Convert(varchar,T.Prev_Payee))
			,('Issued Date', Convert(varchar(10), T.IssuedDate,103) , Convert(varchar(10),T.Prev_IssuedDate,103))			
			,('Amount', Convert(varchar, T.Amount) , Convert(varchar,T.Amount))						
			,('Reason For Stopping Cheque', Convert(varchar, T.ReasonForStoppingCheque) , Convert(varchar,T.Prev_ReasonForStoppingCheque))
			,('Advise Payee Cheque Stopped', Convert(varchar, T.AdvisePayeeChequeStopped) , Convert(varchar,T.Prev_AdvisePayeeChequeStopped))
			,('Assigned To', Convert(varchar, T.AssignedTo) , Convert(varchar,T.Prev_AssignedTo))
			,('Status ', Convert(varchar, T.Status) , Convert(varchar,T.Prev_Status))
			,('Chq No', Convert(varchar, T.ChqNo) , Convert(varchar,T.Prev_ChqNo))				
			,('ModifyingUser', Convert(varchar, T.ModifyingUser) , Convert(varchar,T.Prev_ModifyingUser))
			,('Action', Convert(varchar, T.Action) , Convert(varchar,T.Prev_Action))
			) AS C (ColName, present_Value, prev_value)
WHERE  EXISTS
(
    SELECT present_Value 
    EXCEPT 
    SELECT C.prev_value
)  and Convert(datetime,BeginDate) >= @FromDate and Convert(Datetime,BeginDate) <=@ToDate

END TRY
BEGIN CATCH

        DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),
                @ErrorSeverity INT = ERROR_SEVERITY(),
                @ErrorNumber INT = ERROR_NUMBER();

		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);
 
END CATCH
END

GO
/****** Object:  StoredProcedure [dbo].[usp_StopChequeForm_InsertUpdate]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


  
-- =============================================  
-- Author:  <Author : Phani>  
-- Create date: <12-04-2021>  
-- Modified date : <13-04-2021> <13-04-2021,Sundar> <Included Exception handling>
-- Description: <Description,The purpose of the Stored Procedure is to make CRUD operations on StopChequeForm tables>  
-- Generic Info: <I - Insert , U- Update, D- Delete R-Re activate>    
-- =============================================  
  
  --insert exec [dbo].[usp_StopChequeForm_InsertUpdate] 0,'Test','Test',1,'Test','2021-03-31 14:06:01.827',100,'Test','Test','Test',1,'CQTest','I',19,'Phani',101,0
 -- update exec [dbo].[usp_StopChequeForm_InsertUpdate] 5,'Test','Test',1,'Test','2021-03-31 14:06:01.827',100,'Test','Test','Test',1,'CQTest','U',19,'Phani',201,1
 -- DELETE exec [dbo].[usp_StopChequeForm_InsertUpdate] 0,'Test','Test',1,'Test','2021-03-31 14:06:01.827',100,'Test','Test','Test',1,'CQTest','D',19,'Phani',201,0,5
 -- Re-activate exec [dbo].[usp_StopChequeForm_InsertUpdate] 0,'Test','Test',1,'Test','2021-03-31 14:06:01.827',100,'Test','Test','Test',1,'CQTest','R',19,'Phani',601,9,5
 
 
 --select * from [dbo].[tblStopChequeForm]  for system_time all where id = 5
 -- select * from [dbo].[StopChequeFormHistory] where id = 5
  --Tracker Id =19
CREATE PROCEDURE [dbo].[usp_StopChequeForm_InsertUpdate]   
(  
 @ID int =0,    
 @MatterNumber varchar(50) =NULL ,    
 @ClientName varchar(50) =NULL ,    
 @CenterID int =NULL,    
 @Payee varchar(50) =NULL,    
 @IssuedDate datetime=NULL,    
 @Amount money=NULL ,    
 @ReasonForStoppingCheque varchar(max) =NULL ,    
 @AdvisePayeeChequeStopped varchar(max) =NULL,    
 @AssignedTo varchar(50) =NULL,    
 @StatusId int =NULL,    
 @ChqNo Varchar(20) =NULL,       
    
 @StatementType  Varchar(5)=NULL,        
 @TrackerId int =NULL,          
 @CreatedBy nvarchar(128)=NULL,        
 @processid int =NULL,        
 @RoutingID Int=NULL,  
 @selectedData varchar(100) = NULL )   
  
AS  
BEGIN  
BEGIN TRY
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements  
 SET NOCOUNT ON;  
  
  
 Declare @trgProcessId int;     
    -- This performs INSERT action against StopChequeForm table  
IF @StatementType = 'I'            
 BEGIN         
         
  INSERT INTO [dbo].[tblStopChequeForm]     
            ([MatterNumber],     
				[ClientName]  ,    
				[CenterID]  ,    
				[Payee],    
				[IssuedDate],    
				[Amount] ,    
				[ReasonForStoppingCheque] ,    
				[AdvisePayeeChequeStopped] ,    
				[AssignedTo],    
				[StatusId],    
				[ChqNo],     
				[Action],  
				[ModifyingUser] 
				)  
     
              Values    
			(   @MatterNumber,  
				@ClientName  ,    
				@CenterID  ,    
			    @Payee,    
				@IssuedDate,    
				@Amount ,    
				@ReasonForStoppingCheque  ,    
				@AdvisePayeeChequeStopped  ,    
				@AssignedTo ,    
				@StatusId  ,    
				@ChqNo,      
				@StatementType,  
				@CreatedBy)    
    
    Declare @FormDataId int;          
 set @FormDataId = SCOPE_IDENTITY()          
        
 SELECT @trgProcessId=trgProcessId FROM tblRoutingDetails  
  WHERE TrackerId= @TrackerId AND srcprocessid = @processid AND RoutingID = @RoutingID        
    
   -- This is common across all the trackers and perofrms INSERT action against tbltrackersform table   
    EXEC usp_TrackersForm_Insert @TrackerId, @CreatedBy, @trgProcessId,  @FormDataId  
     
 END     
  
  
  SELECT @trgProcessId=trgProcessId FROM tblRoutingDetails WHERE TrackerId= @TrackerId AND   
  srcprocessid =@ProcessId            
 AND RoutingID = @RoutingID     
  
  
   
--This performs UPDATE action against StopChequeForm table  
IF @StatementType = 'U'            
  BEGIN         
   
  


UPDATE [dbo].[tblStopChequeForm] Set  
 [MatterNumber]=   @MatterNumber , 
[ClientName]  =  @ClientName , 
[CenterID]  =   @CenterID  ,
[Payee]=    @Payee,
[IssuedDate]=    @IssuedDate,
[Amount] =    @Amount,
[ReasonForStoppingCheque] = @ReasonForStoppingCheque,   
[AdvisePayeeChequeStopped] =   @AdvisePayeeChequeStopped  ,
[AssignedTo]= @AssignedTo  , 
[StatusId]=@StatusId    ,
[ChqNo]= @ChqNo  ,  
[Action]= @StatementType, 
[ModifyingUser]=@CreatedBy
   
WHERE Id = @Id      
     
	    
	  -- This is common across all the trackers and perofrms UPDATE action against tbltrackersform table 
   		EXEC [dbo].[usp_TrackersForm_Update] @ID,@TrackerId, @ProcessId,@CreatedBy,
		@RoutingID, @selectedData,@StatementType
    
 --UPDATE tblTrackerForms   
 --           SET  ProcessId = @trgProcessId,              
 --    CreationDate = getdate(),            
 --    CreatedBy= @CreatedBy,            
 --    FormDataTable= 'tblTrackerForms'            
 --    WHERE FormDataId= @ID  AND TrackerId= @TrackerId        
          
       
  END     
    
--This performs DELETE/RE ACTIVATE action against StopChequeForm table  
IF (@StatementType = 'D' OR  @StatementType = 'R')          
  BEGIN         
  
  
    UPDATE [dbo].[tblStopChequeForm]    
   SET    
       [ModifyingUser] = @CreatedBy,    
       [Action] = @StatementType    
 WHERE Id IN (SELECT CAST(value AS INTEGER) FROM STRING_SPLIT(@selectedData, ','))           
    
     -- This is common across all the trackers and UPDATING Process id in tbltrackersform table based on Routing details  
    EXEC [dbo].[usp_TrackersForm_Update] @ID, @TrackerId, @ProcessId,@CreatedBy, @RoutingID, @selectedData,
	@StatementType  
       
  END    
  
END TRY
BEGIN CATCH

        DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),
                @ErrorSeverity INT = ERROR_SEVERITY(),
                @ErrorNumber INT = ERROR_NUMBER();

		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);
 
END CATCH
   
END  
GO
/****** Object:  StoredProcedure [dbo].[usp_TaskLawyerReport_Download]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/****** Script for SelectTopNRows command from SSMS  ******/  
  
--EXEC usp_TaskLawyerReport_Download 24,201  
  
CREATE Procedure [dbo].[usp_TaskLawyerReport_Download]                    
@TrackerId int,                  
@processId int,  
@ViewName Varchar(100)  
                  
AS                      
Begin                 
BEGIN TRY            
      
  
SELECT   
    [ID] = TLR.ID  
      ,Center.CenterName  
      ,KTL.[KTL_Mentor]  
      ,[Overtime] = CASE WHEN TLR.Overtime = 1 then 'Yes' else 'No' END   
      ,[StartTime]  
      ,Team.TeamName  
      ,[ABS]  = CASE WHEN TLR.[ABS] = 1 then 'Yes' else 'No' END   
      ,EFE.[Enter_FEInitials]  
      ,LG.LawyerGrade  
      ,[MatterNumber]  
      ,TASK.Task  
      ,[EndTime]  
      ,[Notes_Comments]  
      ,[Annotated] = CASE WHEN TLR.Annotated = 1 then 'Yes' else 'No' END  
      ,[Admin]  
      ,[HolidayCoverage]  
      ,[AdditionalFee] = CASE WHEN TLR.AdditionalFee = 1 then 'Yes' else 'No' END  
      ,[ReasonForAdditionalFee]  
      ,[FeeAmount]  
      ,[VATDue]  
      ,[ModifyingUser]  
      ,[Action]  
      ,[BeginDate]  
      ,[EndDate]  INTO #FilterResult  
  FROM [tblTaskLawyerReportForm] TLR  
  LEFT JOIN [dbo].[tblCenterName] Center ON TLR.[CenterId]= Center.ID  
  LEFT JOIN [dbo].[tblKTL_Mentor] KTL ON TLR.[KTL_MentorID]= KTL.ID  
  LEFT JOIN [dbo].[tblTaskLawyerTeam] TEAM ON TLR.[TeamId] = TEAM.ID  
  LEFT JOIN [dbo].[tblEnter_FEInitials] EFE ON TLR.Enter_FEInitialsID = EFE.ID  
  LEFT JOIN [dbo].[TBLLawyerGrade] LG ON TLR.[LawyerGradeID] = LG.ID  
  LEFT JOIN [dbo].[TBLTASK] TASK ON TLR.TASKID = TASK.ID  
  
  where TLR.ID in (select distinct FormDataId        
    FROM [dbo].[tblTrackerForms]          
    with (nolock) where TrackerId=@TrackerId and ProcessId=@processId  )         
 order by 1 asc       
   
  DECLARE @sql nvarchar(max);  
  
SELECT  @sql = 'SELECT * FROM #FilterResult ' + dbo.FuncDynamicQueryResult(@TrackerId, @ViewName)  
  
EXEC sp_executesql @sql  
  
        
        
END TRY      
BEGIN CATCH      
      
        DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),      
                @ErrorSeverity INT = ERROR_SEVERITY(),      
                @ErrorNumber INT = ERROR_NUMBER();      
      
  RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);      
       
END CATCH   
DROP TABLE #FilterResult
End            

GO
/****** Object:  StoredProcedure [dbo].[usp_TaskLawyerReportForm_AuditView]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ajay George Kalathil
-- Create date: 6th May 2021
-- Description:	This SP will be used to get the Audit Trial Version View for the front end system
-- =============================================


--Exec usp_TaskLawyerReportForm_AuditView 1,null,null
CREATE PROCEDURE [dbo].[usp_TaskLawyerReportForm_AuditView]
	-- Add the parameters for the stored procedure here
	@ID Int,  
	@FromDate datetime2 = '1900-01-01 00:00:00.0000000', 
	@ToDate datetime2	= '9999-12-31 23:59:59.9999999'
AS
BEGIN

  BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    
;WITH T
AS (
    SELECT		TLR.ID		           
				,CenterName as Center , lag(CenterName) over(order by enddate asc) as Prev_Center
				,KTL_Mentor as KTL_Mentor , lag(KTL_Mentor) over(order by enddate asc) as Prev_KTL_Mentor
				,ISNULL(Overtime,0) as Overtime , ISNULL(lag(Overtime) over(order by enddate asc),0) as Prev_Overtime
				,StartTime as StartTime , lag(StartTime) over(order by enddate asc) as Prev_StartTime
				,TeamName as Team , lag(TeamName) over(order by enddate asc) as Prev_Team
				,ISNULL(ABS,0) as ABS , ISNULL(lag(ABS) over(order by enddate asc),0) as Prev_ABS
				,Enter_FEInitials as Enter_FEInitials , lag(Enter_FEInitials) over(order by enddate asc) as Prev_Enter_FEInitials
				,LawyerGrade as LawyerGrade , lag(LawyerGrade) over(order by enddate asc) as Prev_LawyerGrade
				,MatterNumber as MatterNumber , lag(MatterNumber) over(order by enddate asc) as Prev_MatterNumber
				,Task as Task , lag(Task) over(order by enddate asc) as Prev_Task				
				,EndTime as EndTime , lag(EndTime) over(order by enddate asc) as Prev_EndTime				
				,Notes_Comments as Notes_Comments , lag(Notes_Comments) over(order by enddate asc) as Prev_Notes_Comments
				,ISNULL(Annotated,0) as Annotated ,ISNULL( lag(Annotated) over(order by enddate asc),0) as Prev_Annotated
				,Admin as Admin , lag(Admin) over(order by enddate asc) as Prev_Admin
				,ISNULL(HolidayCoverage,0) as HolidayCoverage , ISNULL(lag(HolidayCoverage) over(order by enddate asc),0) as Prev_HolidayCoverage
				,ISNULL(AdditionalFee,0) as AdditionalFee , ISNULL(lag(AdditionalFee) over(order by enddate asc),0) as Prev_AdditionalFee				
				,ReasonForAdditionalFee as ReasonForAdditionalFee , lag(ReasonForAdditionalFee) over(order by enddate asc) as Prev_ReasonForAdditionalFee
				,FeeAmount as FeeAmount , lag(FeeAmount) over(order by enddate asc) as Prev_FeeAmount
				,VATDue as VATDue , lag(VATDue) over(order by enddate asc) as Prev_VATDue
				,ModifyingUser as ModifyingUser , lag(ModifyingUser) over(order by enddate asc) as Prev_ModifyingUser
				,Action as Action , lag(Action) over(order by enddate asc) as Prev_Action
				,BeginDate
				,EndDate
				,ROW_Number() over(partition by  TLR.Id Order by EndDate asc) as VersionNumber
    FROM dbo.tblTaskLawyerReportForm FOR SYSTEM_TIME ALL AS TLR
  LEFT JOIN [dbo].[tblCenterName] Center ON	TLR.[CenterId]= Center.Id
  LEFT JOIN [dbo].[tblKTL_Mentor] KTL ON TLR.[KTL_MentorID]= KTL.ID
  LEFT JOIN [dbo].[tblTaskLawyerTeam] TEAM ON TLR.[TeamId] = TEAM.ID
  LEFT JOIN [dbo].[tblEnter_FEInitials] EFE ON TLR.Enter_FEInitialsID = EFE.ID
  LEFT JOIN [dbo].[TBLLawyerGrade] LG ON TLR.[LawyerGradeID] = LG.ID
  LEFT JOIN [dbo].[TBLTASK] TASK ON TLR.TASKID = TASK.ID
    WHERE TLR.ID =@ID
)
SELECT T.BeginDate,
       T.EndDate,
       C.ColName as ColumnName,
	   t.VersionNumber,	   
       C.present_Value as CurrentValue,
       C.prev_value as PreviousValue,
	   T.ModifyingUser as ModifiedBy
FROM T
    CROSS APPLY
    (
        VALUES
		     ('Center', Convert(varchar, T.Center) , Convert(varchar,T.Prev_Center))
		    ,('KTL Mentor', Convert(varchar, T.KTL_Mentor) , Convert(varchar,T.Prev_KTL_Mentor))
			,('Overtime', Convert(varchar, case when T.Overtime=1 then 'Yes' else 'No' end) , Convert(varchar,case when T.Prev_Overtime=1 then 'Yes' else 'No' end))
			,('Start Time', Convert(varchar(10), T.StartTime,103) , Convert(varchar(10),T.Prev_StartTime,103))
			,('Team', Convert(varchar, T.Team) , Convert(varchar,T.Prev_Team))
			,('ABS', Convert(varchar,case when T.ABS=1 then 'Yes' else 'No' end ) , Convert(varchar,case when T.Prev_ABS=1 then 'Yes' else 'No' end))
			,('Enter FEInitials', Convert(varchar, T.Enter_FEInitials) , Convert(varchar,T.Prev_Enter_FEInitials))	
			,('Lawyer Grade', Convert(varchar, T.LawyerGrade) , Convert(varchar,T.Prev_LawyerGrade))
			,('MatterNumber', Convert(varchar, T.MatterNumber) , Convert(varchar,T.Prev_MatterNumber))
			,('Task', Convert(varchar, T.Task) , Convert(varchar,T.Task))
			,('End Time', Convert(varchar(10), T.EndTime,103) , Convert(varchar(10),T.Prev_EndTime,103))
			,('Notes Comments', Convert(varchar, T.Notes_Comments) , Convert(varchar,T.Notes_Comments))
			,('Annotated', Convert(varchar,case when T.Annotated=1 then 'Yes' else 'No' end) , Convert(varchar,case when T.Prev_Annotated=1 then 'Yes' else 'No' end))
			,('Admin', Convert(varchar, T.Admin) , Convert(varchar,T.Admin))			
			,('Holiday Coverage', Convert(varchar,case when T.HolidayCoverage=1 then 'Yes' else 'No' end ) , Convert(varchar,case when T.Prev_HolidayCoverage=1 then 'Yes' else 'No' end))
			,('Additional Fee', Convert(varchar,case when T.AdditionalFee=1 then 'Yes' else 'No' end) , Convert(varchar,case when T.Prev_AdditionalFee=1 then 'Yes' else 'No' end))					
			,('Reason For Additional Fee', Convert(varchar, T.ReasonForAdditionalFee) , Convert(varchar,T.Prev_ReasonForAdditionalFee))
			,('Fee Amount', Convert(varchar, T.FeeAmount) , Convert(varchar,T.Prev_FeeAmount))
			,('VAT Due', Convert(varchar, T.VATDue) , Convert(varchar,T.Prev_VATDue))			
			,('ModifyingUser', Convert(varchar, T.ModifyingUser) , Convert(varchar,T.Prev_ModifyingUser))
			,('Action', Convert(varchar, T.Action) , Convert(varchar,T.Prev_Action))
			) AS C (ColName, present_Value, prev_value)
WHERE  EXISTS
(
    SELECT present_Value 
    EXCEPT 
    SELECT C.prev_value
)  and Convert(datetime,BeginDate) >= @FromDate and Convert(Datetime,BeginDate) <=@ToDate

END TRY
BEGIN CATCH

        DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),
                @ErrorSeverity INT = ERROR_SEVERITY(),
                @ErrorNumber INT = ERROR_NUMBER();

		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);
 
END CATCH
END

GO
/****** Object:  StoredProcedure [dbo].[usp_TaskLawyerReportForm_InsertUpdate]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_TaskLawyerReportForm_InsertUpdate]    
 -- Add the parameters for the stored procedure here  
@ID INT = 0,
@CenterId Int  =Null,
@KTL_MentorID Int  =Null,
@Overtime Bit  =Null,
@StartTime Datetime  =Null,
@TeamId Int  =Null,
@ABS Bit = Null,
@Enter_FEInitialsID Int  =Null,
@LawyerGradeID Int  =Null,
@MatterNumber Varchar(20)  =Null,
@TaskID Int  =Null,
@EndTime Datetime  =Null,
@Notes_Comments Varchar(Max) =Null,
@Annotated Bit =Null,
@Admin Varchar(50) =Null,
@HolidayCoverage Bit =Null,
@AdditionalFee Bit =Null,
@ReasonForAdditionalFee Varchar(100) =Null,
@FeeAmount Money =Null,
@VATDue Money =Null,

@StatementType  Varchar(50)=NULL,        
@TrackerId int =NULL,          
@CreatedBy Varchar(256)=NULL,       
@ProcessId INT=NULL,            
@RoutingID Int=NULL,  
@selectedData varchar(100) = NULL      
AS  
BEGIN  
BEGIN TRY
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements  
 SET NOCOUNT ON;  
  
  
 Declare @trgProcessId int;     
    -- This performs INSERT action against tblCallCoachingLog table  
IF @StatementType = 'I'            
 BEGIN         
         
            INSERT INTO [dbo].[tblTaskLawyerReportForm]  
           (CenterId ,
			KTL_MentorID ,
			Overtime ,
			StartTime ,
			TeamId ,
			[ABS] ,
			Enter_FEInitialsID ,
			LawyerGradeID,
			MatterNumber ,
			TaskID ,
			EndTime  ,
			Notes_Comments ,
			Annotated ,
			[Admin] ,
			HolidayCoverage ,
			AdditionalFee ,
			ReasonForAdditionalFee ,
			FeeAmount ,
			VATDue ,   
            [Action] , 
            [ModifyingUser]   
     )  
     VALUES  
   (    
			@CenterId ,
			@KTL_MentorID ,
			@Overtime ,
			@StartTime ,
			@TeamId ,
			@ABS ,
			@Enter_FEInitialsID ,
			@LawyerGradeID,
			@MatterNumber ,
			@TaskID ,
			@EndTime  ,
			@Notes_Comments ,
			@Annotated ,
			@Admin ,
			@HolidayCoverage ,
			@AdditionalFee ,
			@ReasonForAdditionalFee ,
			@FeeAmount ,
			@VATDue ,   
            @StatementType,  
            @CreatedBy   
 )
 
  Declare @FormDataId int;          
 set @FormDataId = SCOPE_IDENTITY()          
        
 SELECT @trgProcessId=trgProcessId FROM tblRoutingDetails  
  WHERE TrackerId= @TrackerId AND srcprocessid = @processid AND RoutingID = @RoutingID        
    
   -- This is common across all the trackers and perofrms INSERT action against tbltrackersform table   
    EXEC usp_TrackersForm_Insert @TrackerId, @CreatedBy, @trgProcessId,  @FormDataId    
     
 END     
  
  
--This performs UPDATE action against tblCallCoachingLog table  
IF @StatementType = 'U'            
  BEGIN         

  UPDATE [dbo].[tblTaskLawyerReportForm]  
SET			CenterId =                   @CenterId,
			KTL_MentorID =				 @KTL_MentorID,
			Overtime =					 @Overtime,
			StartTime =					 @StartTime,
			TeamId =					 @TeamId,
			[ABS] =						 @ABS,
			Enter_FEInitialsID =		 @Enter_FEInitialsID,
			LawyerGradeID=				 @LawyerGradeID,
			MatterNumber =				 @MatterNumber,
			TaskID =					 @TaskID,
			EndTime  =					 @EndTime,
			Notes_Comments =			 @Notes_Comments,
			Annotated =					 @Annotated,
			[Admin] =					 @Admin,
			HolidayCoverage =			 @HolidayCoverage,
			AdditionalFee =				 @AdditionalFee,
			ReasonForAdditionalFee =	 @ReasonForAdditionalFee,
			FeeAmount =					 @FeeAmount,
			VATDue =   					 @VATDue,
            [Action] = 					 @StatementType,
            [ModifyingUser] =			 @CreatedBy
			WHERE ID=@ID  

select @trgProcessId=trgProcessId From tblRoutingDetails   
where TrackerId= @TrackerId and srcprocessid =@ProcessId          
And RoutingID = @RoutingID          
  
EXEC [dbo].[usp_TrackersForm_Update] @ID,@TrackerId, @ProcessId,@CreatedBy, @RoutingID, @selectedData,@StatementType         
          
       
END     
    
--This performs DELETE/RE ACTIVATE action against tblCallCoachingLog table  
IF (@StatementType = 'D' OR  @StatementType = 'R')          
BEGIN      

  UPDATE dbo.tblTaskLawyerReportForm   
   SET    
       [ModifyingUser] = @CreatedBy,    
       [Action] = @StatementType    
 WHERE Id IN (SELECT CAST(value AS INTEGER) FROM STRING_SPLIT(@selectedData, ','))           
    
   -- This is common across all the trackers and UPDATING Process id in tbltrackersform table based on Routing details  
    EXEC [dbo].[usp_TrackersForm_Update] @ID,@TrackerId, @ProcessId,@CreatedBy, @RoutingID, @selectedData,@StatementType  
       
  END    
  
END TRY
BEGIN CATCH

        DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),
                @ErrorSeverity INT = ERROR_SEVERITY(),
                @ErrorNumber INT = ERROR_NUMBER();

		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);
 
END CATCH
  
   
END
GO
/****** Object:  StoredProcedure [dbo].[usp_tblCompletionDiary_Download]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Script for SelectTopNRows command from SSMS  ******/




CREATE Procedure [dbo].[usp_tblCompletionDiary_Download]                  
@TrackerId int,                
@processId int                    
                
AS                    
Begin               
BEGIN TRY          
    


SELECT  [ID] = CDH.ID
      ,[MatterNumber]
      ,Team.TeamName
      ,[ClientSurname]
      ,MT.MatterType
      ,[ExpectedCompletionDate]
      ,[PackReceived]
      ,[PackChecked]
      ,[CANotes_Amendments]
      ,[VFImportedNotes]
      ,[CanGo]
      ,[SentForAuthorisation]
      ,[Sent]
      ,[LegallyCompleted]
      ,[SLRBilled] =   CASE WHEN CDH.SLRBilled = 1 then 'Yes' else 'No' END 
      ,[Healthcheck] =   CASE WHEN CDH.Healthcheck = 1 then 'Yes' else 'No' END 
      ,[ModifiedBy]
      ,[Action]
      ,[BeginDate]
      ,[EndDate]
  FROM [dbo].[tblCompletionDiary] CDH
  LEFT Join [dbo].[tblTeamName] Team on CDH.TeamID = Team.ID
  LEFT JOIN [dbo].[tblMatterType] MT on CDH.MatterTypeID = MT.ID

  where CDH.ID in (select distinct FormDataId      
    FROM [dbo].[tblTrackerForms]        
    with (nolock) where TrackerId=@TrackerId and ProcessId=@processId  )       
 order by 1 asc      
      
      
END TRY    
BEGIN CATCH    
    
        DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),    
                @ErrorSeverity INT = ERROR_SEVERITY(),    
                @ErrorNumber INT = ERROR_NUMBER();    
    
  RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);    
     
END CATCH    
End          
GO
/****** Object:  StoredProcedure [dbo].[usp_tblUser_InsertUpdate]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






CREATE PROCEDURE [dbo].[usp_tblUser_InsertUpdate]     
 -- Add the parameters for the stored procedure here    
 @ID int = 0,   
 @userId nvarchar(100),
 @RoleId int ,
 @action nvarchar(10)
 AS
 BEGIN
 IF @action ='Add'
 BEGIN
    INSERT INTO tblUser(DomainId,RoleId)
	VALUES(@userId,@RoleId)
 END
 ELSE
 BEGIN
    UPDATE tblUser SET DomainId=@userId,RoleId=@roleid where Id=@Id
 END

 END
GO
/****** Object:  StoredProcedure [dbo].[usp_TitleAllocation_AuditView]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


--Exec [usp_TitleAllocation_AuditView] 8,null,null
CREATE PROCEDURE [dbo].[usp_TitleAllocation_AuditView]
	-- Add the parameters for the stored procedure here
	@ID Int,  
	@FromDate datetime2 = '1900-01-01 00:00:00.0000000', 
	@ToDate datetime2	= '9999-12-31 23:59:59.9999999'
AS
BEGIN

  BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    
;WITH T
AS (
    SELECT		TAF.ID	
	            ,MatterNumber as MatterNumber , lag(MatterNumber) over(order by enddate asc) as Prev_MatterNumber
				,Status as Status , lag(Status) over(order by enddate asc) as Prev_Status
				,CenterName as CenterName , lag(CenterName) over(order by enddate asc) as Prev_CenterName
				,TitleCheckerName as TitleCheckerName , lag(TitleCheckerName) over(order by enddate asc) as Prev_TitleCheckerName
				,CreationDateByWNS as CreationDateByWNS , lag(CreationDateByWNS) over(order by enddate asc) as Prev_CreationDateByWNS
				,AssignedTo as AssignedTo , lag(AssignedTo) over(order by enddate asc) as Prev_AssignedTo				
				,Message as Message , lag(Message) over(order by enddate asc) as Prev_Message				
				,MentorName as MentorName , lag(MentorName) over(order by enddate asc) as Prev_MentorName
				,DateAllocatedToMentor as DateAllocatedToMentor , lag(DateAllocatedToMentor) over(order by enddate asc) as Prev_DateAllocatedToMentor
				,MentorInitialCheckComplete as MentorInitialCheckComplete , lag(MentorInitialCheckComplete) over(order by enddate asc) as Prev_MentorInitialCheckComplete
				,FirstTitleAmendmentsCompleted as FirstTitleAmendmentsCompleted , lag(FirstTitleAmendmentsCompleted) over(order by enddate asc) as Prev_FirstTitleAmendmentsCompleted
				,ISNULL(OffshoreQCDone,0) as OffshoreQCDone , ISNULL(lag(OffshoreQCDone) over(order by enddate asc),0) as Prev_OffshoreQCDone
				,FinalNumberRevisionsNeeded	  as FinalNumberRevisionsNeeded , lag(FinalNumberRevisionsNeeded) over(order by enddate asc) as Prev_FinalNumberRevisionsNeeded
				,NumberOfErrors	  as NumberOfErrors , lag(NumberOfErrors) over(order by enddate asc) as Prev_NumberOfErrors
				,FatalityOfError  as FatalityOfError , lag(FatalityOfError) over(order by enddate asc) as Prev_FatalityOfError
				,DateCompleted	  as DateCompleted , lag(DateCompleted) over(order by enddate asc) as Prev_DateCompleted
				,ISNULL(ABSMatter,0) as ABSMatter , ISNULL(lag(ABSMatter) over(order by enddate asc),0) as Prev_ABSMatter
				,CategoryOfTitle  as CategoryOfTitle , lag(CategoryOfTitle) over(order by enddate asc) as Prev_CategoryOfTitle
				,Feedback		  as Feedback , lag(Feedback) over(order by enddate asc) as Prev_Feedback
				,MentorSupport	as MentorSupport , lag(MentorSupport) over(order by enddate asc) as Prev_MentorSupport
				,ISNULL(ErrorThemes,0) as ErrorThemes ,ISNULL( lag(ErrorThemes) over(order by enddate asc),0) as Prev_ErrorThemes
				,AuditOrSignOff	as AuditOrSignOff , lag(AuditOrSignOff) over(order by enddate asc) as Prev_AuditOrSignOff
				,AttachFile as AttachFile , lag(AttachFile) over(order by enddate asc) as Prev_AttachFile
				,ErrorThemes1 as ErrorThemes1 , lag(ErrorThemes1) over(order by enddate asc) as Prev_ErrorThemes1
				,ErrorThemes2 as ErrorThemes2 , lag(ErrorThemes2) over(order by enddate asc) as Prev_ErrorThemes2
				,ErrorThemes3 as ErrorThemes3 , lag(ErrorThemes3) over(order by enddate asc) as Prev_ErrorThemes3
				,ErrorThemes4 as ErrorThemes4 , lag(ErrorThemes4) over(order by enddate asc) as Prev_ErrorThemes4
				,ErrorThemes5 as ErrorThemes5 , lag(ErrorThemes5) over(order by enddate asc) as Prev_ErrorThemes5
				,ErrorThemes6 as ErrorThemes6 , lag(ErrorThemes6) over(order by enddate asc) as Prev_ErrorThemes6
				,ErrorThemes7 as ErrorThemes7 , lag(ErrorThemes7) over(order by enddate asc) as Prev_ErrorThemes7

				,ModifyingUser as ModifyingUser , lag(ModifyingUser) over(order by enddate asc) as Prev_ModifyingUser
				,Action as Action , lag(Action) over(order by enddate asc) as Prev_Action
				,BeginDate
				,EndDate
				,ROW_Number() over(partition by  TAF.Id Order by EndDate asc) as VersionNumber
    FROM dbo.tblTitleAllocationForm  FOR SYSTEM_TIME ALL AS TAF  
 Left Join [dbo].tblTitleAllocationCentre CENTER on TAF.CenterID = CENTER.Id
 Left Join [dbo].tblTitleAllocationStatus TAS on TAF.StatusID = TAS.Id
 Left Join [dbo].tblTitleChecker TAC on TAF.TitleCheckerID = TAC.Id  
 Left Join [dbo].tblTitleAllocationMentor TAM on TAF.MentorId = TAM.Id
 Left Join [dbo].tblFatalityOfError FOE on TAF.FatalityOfErrorId = FOE.Id
 Left Join [dbo].tblCategoryOfTitle TACOT on TAF.CategoryOfTitleId = TACOT.Id 
 Left Join [dbo].tblAuditOrSignOff AOS on TAF.AuditOrSignOffId = AOS.Id
 Left Join [dbo].tblmentorsupport MS on TAF.MentorSupportId = MS.Id

    WHERE TAF.id =@ID
)
SELECT T.BeginDate,
       T.EndDate,
       C.ColName as ColumnName,
	   t.VersionNumber,	   
       C.present_Value as CurrentValue,
       C.prev_value as PreviousValue,
	   T.ModifyingUser as ModifiedBy
FROM T
    CROSS APPLY
    (
        VALUES
		     ('Matter Number', Convert(varchar, T.MatterNumber) , Convert(varchar,T.Prev_MatterNumber))		   		
		     ,('Status', Convert(varchar, T.Status) , Convert(varchar,T.Prev_Status))	
		     ,('Centre', Convert(varchar, T.CenterName) , Convert(varchar,T.Prev_CenterName))		   					
		     ,('Title Checker', Convert(varchar, T.TitleCheckerName) , Convert(varchar,T.Prev_TitleCheckerName))		   								
			,('Creation date by WNS', Convert(varchar(10), T.CreationDateByWNS,103) , Convert(varchar(10),T.Prev_CreationDateByWNS,103))
		     ,('Assigned to', Convert(varchar, T.AssignedTo) , Convert(varchar,T.Prev_AssignedTo))		   					
		     ,('Message', Convert(varchar, T.Message) , Convert(varchar,T.Prev_Message))		   					
		      ,('Mentor', Convert(varchar, T.MentorName) , Convert(varchar,T.Prev_MentorName))	
			,('Date Allocated to Mentor', Convert(varchar(10), T.DateAllocatedToMentor,103) , Convert(varchar(10),T.Prev_DateAllocatedToMentor,103))
			,('Mentor initial check complete', Convert(varchar(10), T.MentorInitialCheckComplete,103) , Convert(varchar(10),T.Prev_MentorInitialCheckComplete,103))
			,('1st Title amendments completed', Convert(varchar(10), T.FirstTitleAmendmentsCompleted,103) , Convert(varchar(10),T.Prev_FirstTitleAmendmentsCompleted,103))
			,('Offshore QC done', Convert(varchar, case when  T.OffshoreQCDone =1 then 'Yes' else 'No' end) , Convert(varchar, case when T.Prev_OffshoreQCDone=1 then 'Yes' else 'No' end))				 
		    ,('Final Number Revisions needed', Convert(varchar, T.FinalNumberRevisionsNeeded) , Convert(varchar,T.Prev_FinalNumberRevisionsNeeded))	
		    ,('Number of errors', Convert(varchar, T.NumberOfErrors) , Convert(varchar,T.Prev_NumberOfErrors))		   								 
		    ,('Fatality of error', Convert(varchar, T.FatalityOfError) , Convert(varchar,T.Prev_FatalityOfError))		
		    ,('Date Completed', Convert(varchar(10), T.DateCompleted,103) , Convert(varchar(10),T.Prev_DateCompleted,103))		
		    ,('ABS matter', Convert(varchar, case when  T.ABSMatter =1 then 'Yes' else 'No' end) , Convert(varchar, case when T.ABSMatter=1 then 'Yes' else 'No' end))					     
		    ,('Category of Title', Convert(varchar, T.CategoryOfTitle) , Convert(varchar,T.Prev_CategoryOfTitle))	
		    ,('Feedback', Convert(varchar, T.Feedback) , Convert(varchar,T.Prev_Feedback))	
		    ,('Mentor Support', Convert(varchar, T.MentorSupport) , Convert(varchar,T.Prev_MentorSupport))	
		    ,('Error Themes',Convert(varchar, case when  T.ErrorThemes =1 then 'Yes' else 'No' end) , Convert(varchar, case when T.ErrorThemes=1 then 'Yes' else 'No' end))	 
			,('Audit or Sign Off', Convert(varchar, T.AuditOrSignOff) , Convert(varchar,T.Prev_AuditOrSignOff))			
			,('Attach File', Convert(varchar, T.AttachFile) , Convert(varchar,T.Prev_AttachFile))
			,('Title', Convert(varchar, case when  T.ErrorThemes1 =1 then 'Yes' else 'No' end) , Convert(varchar, case when T.Prev_ErrorThemes1=1 then 'Yes' else 'No' end))
			,('Rights', Convert(varchar, case when  T.ErrorThemes2 =1 then 'Yes' else 'No' end) , Convert(varchar, case when T.Prev_ErrorThemes2=1 then 'Yes' else 'No' end))
			,('Plans', Convert(varchar, case when  T.ErrorThemes3 =1 then 'Yes' else 'No' end) , Convert(varchar, case when T.Prev_ErrorThemes3=1 then 'Yes' else 'No' end))
			,('Contract', Convert(varchar, case when  T.ErrorThemes4 =1 then 'Yes' else 'No' end) , Convert(varchar, case when T.Prev_ErrorThemes4=1 then 'Yes' else 'No' end))
			,('Transfer', Convert(varchar, case when  T.ErrorThemes5 =1 then 'Yes' else 'No' end) , Convert(varchar, case when T.Prev_ErrorThemes5=1 then 'Yes' else 'No' end))
			,('AEs', Convert(varchar, case when  T.ErrorThemes6 =1 then 'Yes' else 'No' end) , Convert(varchar, case when T.Prev_ErrorThemes6=1 then 'Yes' else 'No' end))
			,('Admin', Convert(varchar, case when  T.ErrorThemes7 =1 then 'Yes' else 'No' end) , Convert(varchar, case when T.Prev_ErrorThemes7=1 then 'Yes' else 'No' end))
			,('ModifyingUser', Convert(varchar, T.ModifyingUser) , Convert(varchar,T.Prev_ModifyingUser))
			,('Action', Convert(varchar, T.Action) , Convert(varchar,T.Prev_Action))
			) AS C (ColName, present_Value, prev_value)
WHERE  EXISTS
(
    SELECT present_Value 
    EXCEPT 
    SELECT C.prev_value
)  and Convert(datetime,BeginDate) >= @FromDate and Convert(Datetime,BeginDate) <=@ToDate

END TRY
BEGIN CATCH

        DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),
                @ErrorSeverity INT = ERROR_SEVERITY(),
                @ErrorNumber INT = ERROR_NUMBER();

		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);
 
END CATCH
END

GO
/****** Object:  StoredProcedure [dbo].[usp_TitleAllocationForm_InsertUpdate]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_TitleAllocationForm_InsertUpdate]    
 -- Add the parameters for the stored procedure here  
 @ID Int= 0,  
@MatterNumber  varchar(20) =NULL,
@StatusID   int  =NULL,
@CenterID   int   =NULL,
@TitleCheckerID   int   =NULL,
@CreationDateByWNS   datetime   =NULL,
@AssignedTo   varchar (50)  =NULL,
@Message   nvarchar (max) =NULL,
@MentorId   int   =NULL,
@DateAllocatedToMentor   datetime  =NULL,
@MentorInitialCheckComplete   datetime  =NULL,
@FirstTitleAmendmentsCompleted   datetime  =NULL,
@OffshoreQCDone   bit  =NULL,
@FinalNumberRevisionsNeeded   int   =NULL,
@NumberOfErrors   int   =NULL,
@FatalityOfErrorId   int   =NULL,
@DateCompleted   datetime  =NULL,
@ABSMatter   bit  =NULL,
@CategoryOfTitleId   int  =NULL,
@Feedback   nvarchar(max) =NULL,
@MentorSupportId   int =NULL,
@ErrorThemes   bit  =NULL,
@AuditOrSignOffId   int  =NULL,
@AttachFile   varchar(max) =NULL,
@ModifyingUser   nvarchar(128) =NULL,
@Action   varchar(1) =NULL,   

 @ErrorThemes1 bit = null,        
 @ErrorThemes2 bit = null,        
 @ErrorThemes3 bit = null,        
 @ErrorThemes4 bit = null,        
 @ErrorThemes5 bit = null,        
 @ErrorThemes6 bit = null,        
 @ErrorThemes7 bit = null,        

    
 @StatementType  Varchar(1)=NULL,        
 @TrackerId int =NULL,          
 @CreatedBy Varchar(256)=NULL,        
 @processid int =NULL,        
 @RoutingID Int=NULL,  
 @selectedData varchar(100) = NULL    
       
AS            
  BEGIN        
  BEGIN TRY
-- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements  
 SET NOCOUNT ON;  
  
  
 Declare @trgProcessId int;     
    -- This performs INSERT action against BridgeControlForm table  
IF @StatementType = 'I'            
 BEGIN      
 INSERT INTO [dbo].tblTitleAllocationForm  
  (			[MatterNumber]
           ,[StatusID]
           ,[CenterID]
           ,[TitleCheckerID]
           ,[CreationDateByWNS]
           ,[AssignedTo]
           ,[Message]
           ,[MentorId]
           ,[DateAllocatedToMentor]
           ,[MentorInitialCheckComplete]
           ,[FirstTitleAmendmentsCompleted]
           ,[OffshoreQCDone]
           ,[FinalNumberRevisionsNeeded]
           ,[NumberOfErrors]
           ,[FatalityOfErrorId]
           ,[DateCompleted]
           ,[ABSMatter]
           ,[CategoryOfTitleId]
           ,[Feedback]
           ,[ErrorThemes]
           ,[AuditOrSignOffId]
           ,[AttachFile]
           ,[Action]  
           ,[ModifyingUser]
		   ,MentorSupportId
		   ,ErrorThemes1
		   ,ErrorThemes2
		   ,ErrorThemes3
		   ,ErrorThemes4
		   ,ErrorThemes5
		   ,ErrorThemes6
		   ,ErrorThemes7
		   
     )  
     VALUES  
   (    
			@MatterNumber 
           ,@StatusID 
           ,@CenterID 
           ,@TitleCheckerID 
           ,@CreationDateByWNS 
           ,@AssignedTo 
           ,@Message 
           ,@MentorId 
           ,@DateAllocatedToMentor 
           ,@MentorInitialCheckComplete 
           ,@FirstTitleAmendmentsCompleted 
           ,@OffshoreQCDone 
           ,@FinalNumberRevisionsNeeded 
           ,@NumberOfErrors 
           ,@FatalityOfErrorId 
           ,@DateCompleted 
           ,@ABSMatter 
           ,@CategoryOfTitleId 
           ,@Feedback 
           ,@ErrorThemes 
           ,@AuditOrSignOffId 
           ,@AttachFile     
           ,@StatementType  
           ,@CreatedBy
		   ,@MentorSupportId
		   ,@ErrorThemes1
		,@ErrorThemes2
		,@ErrorThemes3
		,@ErrorThemes4
		,@ErrorThemes5
		,@ErrorThemes6
		,@ErrorThemes7
 )    
Declare @FormDataId int;          
 set @FormDataId = SCOPE_IDENTITY()      
 
 INSERT INTO tblErrorThemes(   
   
ErrorThemes1,
ErrorThemes2,
ErrorThemes3,
ErrorThemes4,
ErrorThemes5,
ErrorThemes6,
ErrorThemes7,
TitleAllocationRefId)  
 Values(   
 
  @ErrorThemes1,
 @ErrorThemes2,
 @ErrorThemes3,
 @ErrorThemes4,
 @ErrorThemes5,
 @ErrorThemes6,
 @ErrorThemes7,
 @FormDataId )  



        
 SELECT @trgProcessId=trgProcessId FROM tblRoutingDetails  
  WHERE TrackerId= @TrackerId AND srcprocessid = @processid AND RoutingID = @RoutingID        
    
   -- This is common across all the trackers and perofrms INSERT action against tbltrackersform table   
    EXEC usp_TrackersForm_Insert @TrackerId, @CreatedBy, @trgProcessId,  @FormDataId  
     
 END     
  
  
  SELECT @trgProcessId=trgProcessId FROM tblRoutingDetails WHERE TrackerId= @TrackerId AND srcprocessid =@ProcessId            
 AND RoutingID = @RoutingID     
  
  
  
--This performs UPDATE action against BridgeControlForm table  
IF @StatementType = 'U'            
  BEGIN       
         
 UPDATE [dbo].tblTitleAllocationForm  
   SET		[MatterNumber]=@MatterNumber 
           ,[StatusID]=@StatusID 
           ,[CenterID]=@CenterID 
           ,[TitleCheckerID]=@TitleCheckerID 
           ,[CreationDateByWNS]=@CreationDateByWNS 
           ,[AssignedTo]=@AssignedTo 
           ,[Message]=@Message 
           ,[MentorId]=@MentorId 
           ,[DateAllocatedToMentor]=@DateAllocatedToMentor 
           ,[MentorInitialCheckComplete]=@MentorInitialCheckComplete 
           ,[FirstTitleAmendmentsCompleted]=@FirstTitleAmendmentsCompleted 
           ,[OffshoreQCDone]=@OffshoreQCDone 
           ,[FinalNumberRevisionsNeeded]=@FinalNumberRevisionsNeeded 
           ,[NumberOfErrors]=@NumberOfErrors 
           ,[FatalityOfErrorId]=@FatalityOfErrorId 
           ,[DateCompleted]=@DateCompleted 
           ,[ABSMatter]=@ABSMatter 
           ,[CategoryOfTitleId]=@CategoryOfTitleId 
           ,[Feedback]=@Feedback 
           ,[ErrorThemes]=@ErrorThemes 
           ,[AuditOrSignOffId]=@AuditOrSignOffId 
           ,[AttachFile]=@AttachFile     
           ,[Action]  =@StatementType  
           ,[ModifyingUser]=@CreatedBy
		   ,MentorSupportId=@MentorSupportId
		   ,ErrorThemes1=@ErrorThemes1,
			ErrorThemes2=@ErrorThemes2,
			ErrorThemes3=@ErrorThemes3,
			ErrorThemes4=@ErrorThemes4,
			ErrorThemes5=@ErrorThemes5,
			ErrorThemes6=@ErrorThemes6,
			ErrorThemes7=@ErrorThemes7

 WHERE ID=@ID  

 UPDATE [dbo].tblErrorThemes  
   SET		ErrorThemes1=@ErrorThemes1,
			ErrorThemes2=@ErrorThemes2,
			ErrorThemes3=@ErrorThemes3,
			ErrorThemes4=@ErrorThemes4,
			ErrorThemes5=@ErrorThemes5,
			ErrorThemes6=@ErrorThemes6,
			ErrorThemes7=@ErrorThemes7
 WHERE TitleAllocationRefId=@ID  
        
   -- This is common across all the trackers and perofrms UPDATE action against tbltrackersform table   
     EXEC [dbo].[usp_TrackersForm_Update] @ID,@TrackerId, @ProcessId,@CreatedBy, @RoutingID, @selectedData,@StatementType  
      
       
  END     
    
--This performs DELETE/RE ACTIVATE action against BridgeControlForm table  
IF (@StatementType = 'D' OR  @StatementType = 'R')          
  BEGIN         
  
  
    UPDATE tblTitleAllocationForm   
   SET    
       [ModifyingUser] = @CreatedBy,    
       [Action] = @StatementType    
 WHERE Id IN (SELECT CAST(value AS INTEGER) FROM STRING_SPLIT(@selectedData, ','))           
    
   -- This is common across all the trackers and UPDATING Process id in tbltrackersform table based on Routing details  
    EXEC [dbo].[usp_TrackersForm_Update] @ID,@TrackerId, @ProcessId,@CreatedBy, @RoutingID, @selectedData,@StatementType  
       
  END    
  
END TRY
BEGIN CATCH

        DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),
                @ErrorSeverity INT = ERROR_SEVERITY(),
                @ErrorNumber INT = ERROR_NUMBER();

		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);
 
END CATCH
  
   
END
GO
/****** Object:  StoredProcedure [dbo].[usp_TrackerForms_InsertUpdate]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[usp_TrackerForms_InsertUpdate]
(@FormID      INTEGER ,  
@TrackerId    INTEGER ,  
@ProcessId     INTEGER ,
@CreationDate  datetime ,  
@CreatedBy     NVARCHAR(50) ,  
@FormDataTable NVARCHAR(50) ,
@FormDataId     Varchar(50) , 
@StatementType  Varchar(50) )                                     
AS  
  BEGIN  
      IF @StatementType = 'INSERT'  
        BEGIN  
            INSERT INTO tblTrackerForms  
                        (FormId,  
                         TrackerId,  
                         ProcessId,  
                         CreationDate,  
                         CreatedBy,
						 FormDataTable,
						 FormDataId)  
            VALUES     ( @FormID,  
                         @TrackerId,  
                         @ProcessId,  
                         @CreationDate,  
                         @CreatedBy,
						 @FormDataTable,
						 @FormDataId)  
END
IF @StatementType = 'UPDATE'  
        BEGIN  
            UPDATE tblTrackerForms  
            SET      
                   TrackerId = @TrackerId,  
                   ProcessId = @ProcessId,  
                   CreationDate = @CreationDate,
				   CreatedBy= @CreatedBy,
				   FormDataTable= @FormDataTable,
				   FormDataId= @FormDataId
            WHERE  FormId = @FormID
        END 
		END
GO
/****** Object:  StoredProcedure [dbo].[usp_TrackersForm_Insert]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Shivaiah>
-- Create date: <Create Date,10-04-2021>
-- Description:	<Description,Inserting or updating routing details in tbltrackrsform table>
-- =============================================
CREATE PROCEDURE [dbo].[usp_TrackersForm_Insert] 
	-- Add the parameters for the stored procedure here	       
 @TrackerId int,        
 @CreatedBy Varchar(50),      
 @processid int,      
 @FormDataId int

AS
BEGIN
	
	SET NOCOUNT ON;


	   INSERT INTO tblTrackerForms          
                        (         
                         TrackerId,          
                         ProcessId,          
                         CreationDate,          
                         CreatedBy,        
                         FormDataTable,        
                         FormDataId)          
            VALUES     (          
                         @TrackerId,          
                         @processid,          
                         GETDATE(),          
                         @CreatedBy,        
                        'tblTrackerForms',        
                         @FormDataId)    

END
GO
/****** Object:  StoredProcedure [dbo].[usp_TrackersForm_Update]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--  [spUpdatetblTrackersForm]   1,201,'csuman',0,'11,12'
CREATE PROCEDURE [dbo].[usp_TrackersForm_Update]        
(  
 @ID int = 0,
 @TrackerId INT,        
 @ProcessId INT,        
 @CreatedBy Varchar(50),           
 @RoutingID Int,
 @selectedData varchar(100),
 @StatementType varchar(1) = NULL
      
)        
AS          
  BEGIN   
  BEGIN TRY
  
 
 Declare @trgProcessId int          
           
  select @trgProcessId=trgProcessId From tblRoutingDetails where TrackerId= @TrackerId and srcprocessid =@ProcessId          
And RoutingID = @RoutingID          
                 
  IF (@StatementType = 'D' OR  @StatementType = 'R')        
  BEGIN 
             
          
UPDATE tblTrackerForms  SET                
ProcessId = @trgProcessId,            
CreationDate = getdate(),          
CreatedBy= @CreatedBy,          
FormDataTable= 'tblTrackerForms'          
WHERE FormDataId IN (SELECT CAST(value AS INTEGER) FROM STRING_SPLIT(@selectedData, ','))  AND TrackerId= @TrackerId      
 
END 
  
   IF @StatementType = 'U'          
		 BEGIN 

			 UPDATE tblTrackerForms 
	           SET  ProcessId = @trgProcessId,            
					CreationDate = getdate(),          
					CreatedBy= @CreatedBy,          
					FormDataTable= 'tblTrackerForms'          
					WHERE FormDataId= @ID  AND TrackerId= @TrackerId      
        
	  
		 END   
	
END TRY
BEGIN CATCH

        DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),
                @ErrorSeverity INT = ERROR_SEVERITY(),
                @ErrorNumber INT = ERROR_NUMBER();

		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);
 
END CATCH


  END
GO
/****** Object:  StoredProcedure [dbo].[usp_UnallocatedFunds_Download]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

 -- Modified date: <16-04-2021> <Included Exception handling>
      
 
CREATE Procedure [dbo].[usp_UnallocatedFunds_Download]            
@TrackerId int,          
@processId int,
@ViewName Varchar(100)              
          
AS              
Begin         
 BEGIN TRY        
              
  select UAF.ID,UAF.[Date],UAF.Amount,UPM.PaymentMethod,UAFUA.UnallocatedNo,
  UAF.PaymentDetails,UAF.CANotes,UAF.MatterNumber,UAF.UnallocatedMoniesVF2,
   sts.[Status]      
      INTO #FilterResult
from tblUnallocatedFundsForm UAF       
LEFT JOIN [dbo].[tblUnallocatedFundsPaymentMethod] UPM on UAF.PaymentmethodID = UPM.ID      
LEFT JOIN [dbo].[tblUnallocatedFundsStatus] sts ON UAF.StatusID = sts.ID      
LEFT JOIN [dbo].[tblUnallocatedFundsUnallocatedNo] UAFUA ON UAF.UnallocatedNoID = UAFUA.ID      
        
        
 where UAF.ID in (select distinct FormDataId        
    FROM [dbo].[tblTrackerForms]          
    with (nolock) where TrackerId=@TrackerId and ProcessId=@processId  )         
 order by 1 asc        
        
		 DECLARE @sql nvarchar(max);

SELECT  @sql = 'SELECT * FROM #FilterResult ' + dbo.FuncDynamicQueryResult(@TrackerId, @ViewName)

EXEC sp_executesql @sql

      

END TRY
BEGIN CATCH

        DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),
                @ErrorSeverity INT = ERROR_SEVERITY(),
                @ErrorNumber INT = ERROR_NUMBER();

		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);
 
END CATCH
DROP TABLE #FilterResult

     
End 
GO
/****** Object:  StoredProcedure [dbo].[usp_UnallocatedFundsForm_AuditView]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ajay George Kalathil
-- Create date: 6th May 2021
-- Description:	This SP will be used to get the Audit Trial Version View for the front end system
-- =============================================


--Exec usp_UnallocatedFundsForm_AuditView 4,null,null
CREATE PROCEDURE [dbo].[usp_UnallocatedFundsForm_AuditView]
	-- Add the parameters for the stored procedure here
	@ID Int,  
	@FromDate datetime2 = '1900-01-01 00:00:00.0000000', 
	@ToDate datetime2	= '9999-12-31 23:59:59.9999999'
AS
BEGIN

  BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    
;WITH T
AS (
    SELECT		UAF.ID	           							
				,Date as Date, lag(Date) over(order by enddate asc) as Prev_Date
				,Amount as Amount , lag(Amount) over(order by enddate asc) as Prev_Amount				
				,PaymentMethod as PaymentMethod , lag(PaymentMethod) over(order by enddate asc) as Prev_PaymentMethod			
				,PaymentDetails as PaymentDetails , lag(PaymentDetails) over(order by enddate asc) as Prev_PaymentDetails
				,CANotes as CANotes , lag(CANotes) over(order by enddate asc) as Prev_CANotes
				,MatterNumber as MatterNumber , lag(MatterNumber) over(order by enddate asc) as Prev_MatterNumber
				,sts.Status as Status , lag(sts.Status) over(order by enddate asc) as Prev_Status	
				,UnallocatedNo as UnallocatedNo , lag(UnallocatedNo) over(order by enddate asc) as Prev_UnallocatedNo				
				,UnallocatedMoniesVF2 as UnallocatedMoniesVF2 , lag(UnallocatedMoniesVF2) over(order by enddate asc) as Prev_UnallocatedMoniesVF2										
				,ModifyingUser as ModifyingUser , lag(ModifyingUser) over(order by enddate asc) as Prev_ModifyingUser
				,Action as Action , lag(Action) over(order by enddate asc) as Prev_Action
				,BeginDate
				,EndDate
				,ROW_Number() over(partition by  UAF.Id Order by EndDate asc) as VersionNumber
    FROM dbo.tblUnallocatedFundsForm FOR SYSTEM_TIME ALL AS UAF       
LEFT JOIN [dbo].[tblUnallocatedFundsPaymentMethod] UPM on UAF.PaymentmethodID = UPM.ID      
LEFT JOIN [dbo].[tblUnallocatedFundsStatus] sts ON UAF.StatusID = sts.ID      
LEFT JOIN [dbo].[tblUnallocatedFundsUnallocatedNo] UAFUA ON UAF.UnallocatedNoID = UAFUA.ID 
    WHERE UAF.id =@ID
)
SELECT T.BeginDate,
       T.EndDate,
       C.ColName as ColumnName,
	   t.VersionNumber,	   
       C.present_Value as CurrentValue,
       C.prev_value as PreviousValue,
	   T.ModifyingUser as ModifiedBy
FROM T
    CROSS APPLY
    (
        VALUES				
			 ('Date', Convert(varchar(10), T.Date,103) , Convert(varchar(10),T.Prev_Date,103))
			,('Amount', Convert(varchar, T.Amount) , Convert(varchar,T.Amount))
			,('Payment Method ', Convert(varchar, T.PaymentMethod) , Convert(varchar,T.Prev_PaymentMethod))			
			,('Payment Details', Convert(varchar, T.PaymentDetails) , Convert(varchar,T.Prev_PaymentDetails))
			,('CANotes', Convert(varchar, T.CANotes) , Convert(varchar,T.Prev_CANotes))
			,('Matter Number', Convert(varchar, T.MatterNumber) , Convert(varchar,T.Prev_MatterNumber))			
			,('Status ', Convert(varchar, T.Status) , Convert(varchar,T.Prev_Status))
			,('Unallocated No ', Convert(varchar, T.UnallocatedNo) , Convert(varchar,T.Prev_UnallocatedNo))			
			,('Unallocated Monies VF2 ', Convert(varchar, T.UnallocatedMoniesVF2 ) , Convert(varchar,T.Prev_UnallocatedMoniesVF2))						
			,('ModifyingUser', Convert(varchar, T.ModifyingUser) , Convert(varchar,T.Prev_ModifyingUser))
			,('Action', Convert(varchar, T.Action) , Convert(varchar,T.Prev_Action))
			) AS C (ColName, present_Value, prev_value)
WHERE  EXISTS
(
    SELECT present_Value 
    EXCEPT 
    SELECT C.prev_value
)  and Convert(datetime,BeginDate) >= @FromDate and Convert(Datetime,BeginDate) <=@ToDate

END TRY
BEGIN CATCH

        DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),
                @ErrorSeverity INT = ERROR_SEVERITY(),
                @ErrorNumber INT = ERROR_NUMBER();

		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);
 
END CATCH
END

GO
/****** Object:  StoredProcedure [dbo].[usp_UnallocatedFundsForm_InsertUpdate]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
  
-- =============================================  
-- Author:  <Author : chaitanya>  
-- Create date: <12-04-2021>  
-- Modified date : <12-04-2021> <13-04-2021,Sundar> <Included Exception handling>  
-- Description: <Description,The purpose of the Stored Procedure is to make CRUD operations on Unallocated Funds Form tables>  
-- Generic Info: <I - Insert , U- Update, D- Delete R-Re activate>    
-- =============================================  
  
--Exec [usp_usp_UnallocatedFundsForm_InsertUpdate] @id=0,@Date='2021/04/12 00:00:00',@Amount=500.00,@PaymentMethodID=1,@PaymentDetails='Test Payment',@CANotes='Test Ca Notes',@MatterNumber=123456,  @StatusID=3,  
--@UnallocatedNoID=2,@UnallocatedMoniesVF2='Test Monies',@StatementType='I',@TrackerId=20,@CreatedBy='CKusume',@processid=101,@RoutingID=0  
--Exec [usp_usp_UnallocatedFundsForm_InsertUpdate] @id=15,@Date='2021/04/12 00:00:00',@Amount=5000.00,@PaymentMethodID=1,@PaymentDetails='Test Payments',@CANotes='Test Ca Notes',@MatterNumber=123456,  @StatusID=2,  
--@UnallocatedNoID=3,@UnallocatedMoniesVF2='Test Monies',@StatementType='U',@TrackerId=20,@CreatedBy='CKusume',@processid=201,@RoutingID=1  
  
CREATE PROCEDURE [dbo].[usp_UnallocatedFundsForm_InsertUpdate]     
 -- Add the parameters for the stored procedure here    
 @ID int = 0,      
 @Date datetime=NULL,      
 @Amount money=NULL,      
 @PaymentMethodID int=NULL,      
 @PaymentDetails varchar(200)=NULL,      
 @CANotes varchar(Max)=NULL,  
 @MatterNumber varchar(50) =NULL ,  
 @StatusID int=NULL,  
 @UnallocatedNoID int=NULL,  
 @UnallocatedMoniesVF2 varchar(50)=NULL,  
      
 @StatementType  Varchar(50)=NULL,          
 @TrackerId int =NULL,            
 @CreatedBy Varchar(50)=NULL,          
 @processid int =NULL,          
 @RoutingID Int=NULL,    
 @selectedData varchar(100) = NULL      
AS    
BEGIN    
BEGIN TRY  
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements    
 SET NOCOUNT ON;    
    
    
 Declare @trgProcessId int;       
    -- This performs INSERT action against Unallocated Funds Form table    
IF @StatementType = 'I'              
 BEGIN           
           
   INSERT INTO dbo.tblUnallocatedFundsForm       
   ([Date]  ,      
   Amount  ,      
   PaymentMethodID ,      
   PaymentDetails  ,      
   CANotes,  
   MatterNumber,  
   StatusID,  
   UnallocatedNoID,  
   UnallocatedMoniesVF2,  
   [Action],    
   [ModifyingUser]     
       
)      
      
   Values      
   (@Date  ,      
    @Amount  ,      
    @PaymentMethodID  ,      
    @PaymentDetails  ,      
    @CANotes,    
 @MatterNumber,  
 @StatusID,  
 @UnallocatedNoID,  
 @UnallocatedMoniesVF2,  
    @StatementType,    
    @CreatedBy )      
      
    Declare @FormDataId int;            
 set @FormDataId = SCOPE_IDENTITY()            
          
 SELECT @trgProcessId=trgProcessId FROM tblRoutingDetails    
  WHERE TrackerId= @TrackerId AND srcprocessid = @processid AND RoutingID = @RoutingID          
      
   -- This is common across all the trackers and perofrms INSERT action against tbltrackersform table     
    EXEC usp_TrackersForm_Insert @TrackerId, @CreatedBy, @trgProcessId,  @FormDataId    
       
 END       
    
    
  SELECT @trgProcessId=trgProcessId FROM tblRoutingDetails WHERE TrackerId= @TrackerId AND srcprocessid =@ProcessId              
 AND RoutingID = @RoutingID       
    
    
    
--This performs UPDATE action against Unallocated Funds Form table    
IF @StatementType = 'U'              
  BEGIN           
     
    UPDATE dbo.tblUnallocatedFundsForm     
   SET      
       [Date] = @Date ,      
    Amount = @Amount ,      
       PaymentMethodID  = @PaymentMethodID,  
    PaymentDetails  = @PaymentDetails,  
       CANotes = @CANotes ,  
    MatterNumber = @MatterNumber,  
    StatusID = @StatusID,  
    UnallocatedNoID = @UnallocatedNoID,  
       UnallocatedMoniesVF2 = @UnallocatedMoniesVF2,      
       [ModifyingUser] = @CreatedBy,      
       [Action] = @StatementType      
 WHERE Id = @Id        
                    
      
 EXEC [dbo].[usp_TrackersForm_Update] @ID,@TrackerId, @ProcessId,@CreatedBy, @RoutingID, @selectedData,@StatementType         
            
         
  END       
      
--This performs DELETE/RE ACTIVATE action against Unallocated Funds Form table    
IF (@StatementType = 'D' OR  @StatementType = 'R')            
  BEGIN           
    
    
    UPDATE dbo.tblUnallocatedFundsForm     
   SET      
       [ModifyingUser] = @CreatedBy,      
       [Action] = @StatementType      
 WHERE Id IN (SELECT CAST(value AS INTEGER) FROM STRING_SPLIT(@selectedData, ','))             
      
   -- This is common across all the trackers and UPDATING Process id in tbltrackersform table based on Routing details    
    EXEC [dbo].[usp_TrackersForm_Update] @Id,@TrackerId, @ProcessId,@CreatedBy, @RoutingID, @selectedData,@StatementType   
         
  END      
END TRY  
BEGIN CATCH  
  
        DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),  
                @ErrorSeverity INT = ERROR_SEVERITY(),  
                @ErrorNumber INT = ERROR_NUMBER();  
  
  RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);  
   
END CATCH   
       
END  
GO
/****** Object:  StoredProcedure [dbo].[usp_UnAllocatedReport]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[usp_UnAllocatedReport]--@DATE DATETIME,@USERID INT
@ReportType int,
@Fromdate date= null,
@Todate date= null
AS
BEGIN
if
@ReportType=1
begin
Select
MATAC_0."BAL-UC" ,
MATAC_0."MT-CODE"
From
PUB.MATAC MATAC_0
Where
(MATAC_0."BAL-UC">0)
end
else
begin
Select
MATAC_0."BAL-UC" ,
MATAC_0."MT-CODE"
From
PUB.MATAC_History MATAC_0
Where
(MATAC_0."BAL-UC">0) and
EffectiveDate between @Fromdate and @Todate 
--and 
--CurrentRecord = 'Y'and
--EndDate between @Fromdate and @Todate
EXCEPT
Select
MATAC_0."BAL-UC" ,
MATAC_0."MT-CODE"
From
PUB.MATAC_History MATAC_0 where 1=0
END
end
GO
/****** Object:  StoredProcedure [dbo].[usp_updateConfiguration]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
-- [sp_updateConfiguration] 4,'Review','1,2,3,4','1_2,2_1,3_4,4_3'    
CREATE Procedure [dbo].[usp_updateConfiguration]    
@Trackerid int,
@ViewName varchar(50),
@SelectedColumnData nvarchar(max),
@SelectedOrderwithId nvarchar(max)

AS      
Begin      
 

 UPDATE [dbo].[tblDynamicViewConfiguration] SET IsSelected=0,isOrdered=0 WHERE TrackerRefId=@Trackerid and ViewName=@ViewName

 UPDATE [dbo].[tblDynamicViewConfiguration]  SET IsSelected=1 WHERE Id IN(SELECT CAST(value AS INTEGER) FROM STRING_SPLIT(@SelectedColumnData, ','))

 SELECT  PARSENAME(REPLACE(value, '_', '.'), 2) AS SelectedIdValue,PARSENAME(REPLACE(value, '_', '.'), 1) AS orderedValue
				INTO #selectDataTable from (select value 
											from 
											STRING_SPLIT(@SelectedOrderwithId,',')
											) as cl
	
  UPDATE [dbo].[tblDynamicViewConfiguration]  SET isOrdered=sel.orderedValue
		 
				from [tblDynamicViewConfiguration] join #selectDataTable as sel on sel.SelectedIdValue=[tblDynamicViewConfiguration].Id

	
  DROP TABLE #selectDataTable	

      
End    
    
GO
/****** Object:  StoredProcedure [dbo].[usp_Users_Download]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
-- Modified date: <16-04-2021> <Included Exception handling>    
-- [usp_Users_Download]   
Create Procedure [dbo].[usp_Users_Download]    


  
AS      
Begin 
BEGIN TRY
      
SELECT   
        
		u.DomainId,

		R.RoleName,
		u.Id

from  [dbo].tblUser  u 

	inner join tblRole R on R.RoleId=U.RoleId
	

END TRY
BEGIN CATCH

        DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),
                @ErrorSeverity INT = ERROR_SEVERITY(),
                @ErrorNumber INT = ERROR_NUMBER();

		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);
 
END CATCH
End    
    
GO
/****** Object:  StoredProcedure [dbo].[usp_WriteOffsForm_AuditView]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ajay George Kalathil
-- Create date: 6th May 2021
-- Description:	This SP will be used to get the Audit Trial Version View for the front end system
-- =============================================


--Exec usp_WriteOffsForm_AuditView 7,null,null
CREATE PROCEDURE [dbo].[usp_WriteOffsForm_AuditView]
	-- Add the parameters for the stored procedure here
	@ID Int,  
	@FromDate datetime2 = '1900-01-01 00:00:00.0000000', 
	@ToDate datetime2	= '9999-12-31 23:59:59.9999999'
AS
BEGIN

  BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    
;WITH T
AS (
    SELECT		WOF.ID	
	            ,PositionOfRequester as PositionOfRequester , lag(PositionOfRequester) over(order by enddate asc) as Prev_PositionOfRequester
				,CentreWriteOffAgainst as CentreWriteOffAgainst , lag(CentreWriteOffAgainst) over(order by enddate asc) as Prev_CentreWriteOffAgainst
				,TeamWriteOffAgainst as TeamWriteOffAgainst , lag(TeamWriteOffAgainst) over(order by enddate asc) as Prev_TeamWriteOffAgainst
				,FeeEarnersWriteOffAgainst as FeeEarnersWriteOffAgainst , lag(FeeEarnersWriteOffAgainst) over(order by enddate asc) as Prev_FeeEarnersWriteOffAgainst
				,MatterNumber as MatterNumber , lag(MatterNumber) over(order by enddate asc) as Prev_MatterNumber
				,ClientName as ClientName , lag(ClientName) over(order by enddate asc) as Prev_ClientName				
				,CompletionDate as CompletionDate , lag(CompletionDate) over(order by enddate asc) as Prev_CompletionDate				
				,Amount as Amount , lag(Amount) over(order by enddate asc) as Prev_Amount
				,ReasonforWriteOff as ReasonforWriteOff , lag(ReasonforWriteOff) over(order by enddate asc) as Prev_ReasonforWriteOff
				,Narrative as Narrative , lag(Narrative) over(order by enddate asc) as Prev_Narrative
				,Authoriser as Authoriser , lag(Authoriser) over(order by enddate asc) as Prev_Authoriser
				,WOS.Status as Status , lag(WOS.Status) over(order by enddate asc) as Prev_Status			
				,AttachFile as AttachFile , lag(AttachFile) over(order by enddate asc) as Prev_AttachFile
				,ModifyingUser as ModifyingUser , lag(ModifyingUser) over(order by enddate asc) as Prev_ModifyingUser
				,Action as Action , lag(Action) over(order by enddate asc) as Prev_Action
				,BeginDate
				,EndDate
				,ROW_Number() over(partition by  WOF.Id Order by EndDate asc) as VersionNumber
    FROM dbo.tblWriteOffsForm FOR SYSTEM_TIME ALL AS WOF  
 Left Join [dbo].[tblCentreWriteOffAgainst] CWOFA on CWOFA.ID = WOF.[CentreWriteOffAgainstID]
 Left Join [dbo].[tblTeamWriteOffAgainst] TWOFA on TWOFA.ID = WOF.[TeamWriteOffAgainstID]
 Left Join [dbo].[tblReasonforWriteOff] RFWO on RFWO.ID = WOF. [ReasonforWriteOffID]
 Left Join [dbo].[tblWriteOffsStatus] WOS on WOS.ID = WOF.STATUSID
    WHERE WOF.id =@ID
)
SELECT T.BeginDate,
       T.EndDate,
       C.ColName as ColumnName,
	   t.VersionNumber,	   
       C.present_Value as CurrentValue,
       C.prev_value as PreviousValue,
	   T.ModifyingUser as ModifiedBy
FROM T
    CROSS APPLY
    (
        VALUES
		     ('Position Of Requester', Convert(varchar, T.PositionOfRequester) , Convert(varchar,T.Prev_PositionOfRequester))
		    ,('Centre Write Off Against', Convert(varchar, T.CentreWriteOffAgainst) , Convert(varchar,T.Prev_CentreWriteOffAgainst))
			,('Team Write Off Against', Convert(varchar, T.TeamWriteOffAgainst) , Convert(varchar,T.Prev_TeamWriteOffAgainst))
			,('FeeEarners Write Off Against', Convert(varchar, T.FeeEarnersWriteOffAgainst) , Convert(varchar,T.Prev_FeeEarnersWriteOffAgainst))
			,('Matter Number', Convert(varchar, T.MatterNumber) , Convert(varchar,T.Prev_MatterNumber))
			,('Client Name', Convert(varchar, T.ClientName) , Convert(varchar,T.Prev_ClientName))						
			,('Completion Date', Convert(varchar(10), T.CompletionDate,103) , Convert(varchar(10),T.Prev_CompletionDate,103))
			,('Amount', Convert(varchar, T.Amount) , Convert(varchar,T.Amount))
			,('Reason for Write Off', Convert(varchar, T.ReasonforWriteOff) , Convert(varchar,T.Prev_ReasonforWriteOff))
			,('Narrative', Convert(varchar, T.Narrative) , Convert(varchar,T.Prev_Narrative))			
			,('Authoriser', Convert(varchar, T.Authoriser) , Convert(varchar,T.Prev_Authoriser))
			,('Status', Convert(varchar, T.Status) , Convert(varchar,T.Prev_Status))					
			,('Attach File', Convert(varchar, T.AttachFile) , Convert(varchar,T.Prev_AttachFile))
			,('ModifyingUser', Convert(varchar, T.ModifyingUser) , Convert(varchar,T.Prev_ModifyingUser))
			,('Action', Convert(varchar, T.Action) , Convert(varchar,T.Prev_Action))
			) AS C (ColName, present_Value, prev_value)
WHERE  EXISTS
(
    SELECT present_Value 
    EXCEPT 
    SELECT C.prev_value
)  and Convert(datetime,BeginDate) >= @FromDate and Convert(Datetime,BeginDate) <=@ToDate

END TRY
BEGIN CATCH

        DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),
                @ErrorSeverity INT = ERROR_SEVERITY(),
                @ErrorNumber INT = ERROR_NUMBER();

		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);
 
END CATCH
END

GO
/****** Object:  StoredProcedure [dbo].[usp_WriteOffsForm_Download]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Modified date: <16-04-2021> <Included Exception handling>
-- [usp_WriteOffsForm_Download] 18,201,'view1'
CREATE Procedure [dbo].[usp_WriteOffsForm_Download]              
@TrackerId int,            
@processId int,
@ViewName Varchar(100)                
            
AS                
Begin           
 BEGIN TRY      
 Select  
 ID = WOF.ID ,
 
 PositionOfRequester,
	CWOFA.CentreWriteOffAgainst,
	TWOFA.TeamWriteOffAgainst ,
	FeeEarnersWriteOffAgainst,
	MatterNumber,
    ClientName,
    CompletionDate,
	Amount,
	ReasonforWriteOffID=RFWO.ReasonforWriteOff,
	Narrative,
	Authoriser,
	WOS.[Status],
	AttachFile
	into #FilterResult
 From [dbo].[tblWriteOffsForm] WOF   
 Left Join [dbo].[tblCentreWriteOffAgainst] CWOFA on CWOFA.ID = WOF.[CentreWriteOffAgainstID]
 Left Join [dbo].[tblTeamWriteOffAgainst] TWOFA on TWOFA.ID = WOF.[TeamWriteOffAgainstID]
 Left Join [dbo].[tblReasonforWriteOff] RFWO on RFWO.ID = WOF. [ReasonforWriteOffID]
 Left Join [dbo].[tblWriteOffsStatus] WOS on WOS.ID = WOF.STATUSID
 
 where WOF.id in (select distinct FormDataId  
    FROM [dbo].[tblTrackerForms]    
    with (nolock) where TrackerId=@TrackerId and ProcessId=@processId  )   
 order by 1 asc  

  DECLARE @sql nvarchar(max);

SELECT  @sql = 'SELECT * FROM #FilterResult ' + dbo.FuncDynamicQueryResult(@TrackerId, @ViewName)

EXEC sp_executesql @sql
  

END TRY
BEGIN CATCH

        DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),
                @ErrorSeverity INT = ERROR_SEVERITY(),
                @ErrorNumber INT = ERROR_NUMBER();

		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);
 
END CATCH
DROP TABLE #FilterResult
  
End      
      
GO
/****** Object:  StoredProcedure [dbo].[usp_WriteOffsForm_InsertUpdate]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
  
    
-- =============================================    
-- Author:  <Author : Arun,Sundar>    
-- Create date: <10-04-2021>    
-- Modified date : <11-04-2021> <13-04-2021,Sundar> <Included Exception handling>    
-- Description: <Description,The purpose of the Stored Procedure is to make CRUD operations on WriteOffsForm tables>    
-- Generic Info: <I - Insert , U- Update, D- Delete R-Re activate>      
-- =============================================    
    
CREATE PROCEDURE [dbo].[usp_WriteOffsForm_InsertUpdate]     
(    
@ID int =0,      
@PositionOfRequester [varchar](50) =NULL,      
@CentreWriteOffAgainstID [int] =NULL,      
@TeamWriteOffAgainstID [int] =NULL ,      
@FeeEarnersWriteOffAgainst [varchar](100) =NULL ,      
@MatterNumber [varchar](50) =NULL ,      
@ClientName [varchar](50) =NULL ,      
@CompletionDate [datetime] =NULL ,      
@Amount [money] =NULL ,      
@ReasonforWriteOffID [int]  =NULL,      
@Narrative [varchar] (MAX) =NULL ,      
@Authoriser [varchar] (50) =NULL ,      
@StatusID INT =NULL,      
@AttachFile [varchar](max) =NULL,     
      
 @StatementType  Varchar(50)=NULL,          
 @TrackerId int =NULL,            
 @CreatedBy Varchar(256)=NULL,          
 @processid int =NULL,          
 @RoutingID Int=NULL,    
 @selectedData varchar(100) = NULL )     
    
AS    
BEGIN    
BEGIN TRY
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements    
 SET NOCOUNT ON;    
    
    
 Declare @trgProcessId int;       
    -- This performs INSERT action against WriteOffsForm table    
IF @StatementType = 'I'              
 BEGIN           
           
   INSERT INTO   [dbo].[tblWriteOffsForm]    
(PositionOfRequester,      
CentreWriteOffAgainstID,      
TeamWriteOffAgainstID ,      
FeeEarnersWriteOffAgainst,      
MatterNumber,      
ClientName,      
CompletionDate,      
Amount,      
ReasonforWriteOffID,      
Narrative,      
Authoriser,      
Statusid,      
AttachFile,    
[Action],    
[ModifyingUser] )    
       
Values      
(@PositionOfRequester,      
@CentreWriteOffAgainstID,      
@TeamWriteOffAgainstID,      
@FeeEarnersWriteOffAgainst,      
@MatterNumber,      
@ClientName,      
@CompletionDate,      
@Amount,      
@ReasonforWriteOffID,      
@Narrative,      
@Authoriser,      
@StatusID,      
@AttachFile ,    
@StatementType,    
@CreatedBy)      
      
    Declare @FormDataId int;            
 set @FormDataId = SCOPE_IDENTITY()            
          
 SELECT @trgProcessId=trgProcessId FROM tblRoutingDetails    
  WHERE TrackerId= @TrackerId AND srcprocessid = @processid AND RoutingID = @RoutingID          
      
   -- This is common across all the trackers and perofrms INSERT action against tbltrackersform table     
    EXEC usp_TrackersForm_Insert @TrackerId, @CreatedBy, @trgProcessId,  @FormDataId    
       
 END       
    
    
  SELECT @trgProcessId=trgProcessId FROM tblRoutingDetails WHERE TrackerId= @TrackerId AND     
  srcprocessid =@ProcessId              
 AND RoutingID = @RoutingID       
    
    
     
--This performs UPDATE action against WriteOffsForm table    
IF @StatementType = 'U'              
  BEGIN           
     
    UPDATE [dbo].[tblWriteOffsForm]     
   SET      
        PositionOfRequester = @PositionOfRequester,      
     CentreWriteOffAgainstID = @CentreWriteOffAgainstID,      
     TeamWriteOffAgainstID = @TeamWriteOffAgainstID,      
     FeeEarnersWriteOffAgainst = @FeeEarnersWriteOffAgainst,      
     MatterNumber = @MatterNumber,      
     ClientName = @ClientName,      
     CompletionDate = @CompletionDate,      
     Amount = @Amount,      
     ReasonforWriteOffID = @ReasonforWriteOffID,      
     Narrative = @Narrative,      
     Authoriser = @Authoriser,      
     StatusID = @StatusID,      
     AttachFile = @AttachFile ,      
        [ModifyingUser] = @CreatedBy,      
        [Action] = @StatementType      
 WHERE Id = @Id        
                    
      
EXEC [dbo].[usp_TrackersForm_Update] @Id,@TrackerId, @ProcessId,@CreatedBy, @RoutingID, @selectedData,@StatementType          
            
         
  END       
    
--This performs DELETE/RE ACTIVATE action against WriteOffsForm table    
IF (@StatementType = 'D' OR  @StatementType = 'R')            
BEGIN           
    
    
    UPDATE [dbo].[tblWriteOffsForm]      
   SET      
       [ModifyingUser] = @CreatedBy,      
       [Action] = @StatementType      
 WHERE Id IN (SELECT CAST(value AS INTEGER) FROM STRING_SPLIT(@selectedData, ','))             
      
   -- This is common across all the trackers and UPDATING Process id in tbltrackersform table based on Routing details    
    EXEC [dbo].[usp_TrackersForm_Update] @Id,@TrackerId, @ProcessId,@CreatedBy, @RoutingID, @selectedData,@StatementType    
         
  END      
END TRY
BEGIN CATCH

        DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),
                @ErrorSeverity INT = ERROR_SEVERITY(),
                @ErrorNumber INT = ERROR_NUMBER();

		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);
 
END CATCH 
    
     
END 
GO
/****** Object:  StoredProcedure [dbo].[uspTitleAllocationDownload]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Modified date: <13-04-2021,Sundar> <Included Exception handling>
-- [uspTitleAllocationDownload] 31,201,null
CREATE Procedure [dbo].[uspTitleAllocationDownload]              
@TrackerId int,            
@processId int,
@ViewName varchar(100)=null                
            
AS                
Begin           
 BEGIN TRY      
 Select  
		TAF.ID
		,MatterNumber
		,TAS.Status
		,center=CENTER.CenterName
		,TAC.TitleCheckerName
		,CreationDateByWNS
		,AssignedTo
		,Message
		,TAM.MentorName
		,DateAllocatedToMentor
		,MentorInitialCheckComplete
		,FirstTitleAmendmentsCompleted
		,OffshoreQCDone = case when TAF.OffshoreQCDone = 1 then 'Yes' else 'No' end  
		,FinalNumberRevisionsNeeded
		,NumberOfErrors
		,FOE.FatalityOfError
		,DateCompleted
		,ABSMatter= case when TAF.ABSMatter = 1 then 'Yes' else 'No' end  
		,TACOT.CategoryOfTitle
		,Feedback
		,ErrorThemes=case when TAF.ErrorThemes = 1 then 'Yes' else 'No' end 
		,AOS.AuditOrSignOff
		,AttachFile 
		,ErrorThemes1= case when error.ErrorThemes1 = 1 then 'Yes' else 'No' end  
		,ErrorThemes2= case when error.ErrorThemes2 = 1 then 'Yes' else 'No' end  
		,ErrorThemes3= case when error.ErrorThemes3 = 1 then 'Yes' else 'No' end  
		,ErrorThemes4= case when error.ErrorThemes4 = 1 then 'Yes' else 'No' end  
		,ErrorThemes5= case when error.ErrorThemes5 = 1 then 'Yes' else 'No' end  
		,ErrorThemes6= case when error.ErrorThemes6 = 1 then 'Yes' else 'No' end  
		,ErrorThemes7= case when error.ErrorThemes7 = 1 then 'Yes' else 'No' end
		,MentorSupport
		   			  
	  INTO #FilterResult
 From [dbo].[tblTitleAllocationForm] TAF  

 Left Join [dbo].tblTitleAllocationCentre CENTER on TAF.CenterID = CENTER.Id
 Left Join [dbo].tblTitleAllocationStatus TAS on TAF.StatusID = TAS.Id
 Left Join [dbo].tblTitleChecker TAC on TAF.TitleCheckerID = TAC.Id  
 Left Join [dbo].tblTitleAllocationMentor TAM on TAF.MentorId = TAM.Id
 Left Join [dbo].tblFatalityOfError FOE on TAF.FatalityOfErrorId = FOE.Id
 Left Join [dbo].tblCategoryOfTitle TACOT on TAF.CategoryOfTitleId = TACOT.Id 
 Left Join [dbo].tblAuditOrSignOff AOS on TAF.AuditOrSignOffId = AOS.Id 
 Left Join [dbo].tblerrorthemes error on TAF.ID = error.TitleAllocationRefId
 Left Join [dbo].tblMentorSupport MS on TAF.MentorSupportId = MS.ID


  
 where TAF.ID in (select distinct FormDataId  
    FROM [dbo].[tblTrackerForms]    
    with (nolock) where TrackerId=@TrackerId and ProcessId=@processId  )   
 order by 1 asc  
  
  DECLARE @sql nvarchar(max);

SELECT  @sql = 'SELECT * FROM #FilterResult ' + dbo.FuncDynamicQueryResult(@TrackerId, @ViewName)

EXEC sp_executesql @sql
  
END TRY
BEGIN CATCH

        DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),
                @ErrorSeverity INT = ERROR_SEVERITY(),
                @ErrorNumber INT = ERROR_NUMBER();

		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);
 
END CATCH
DROP TABLE #FilterResult

End      
      
GO
/****** Object:  StoredProcedure [dbo].[uspUpdateCompliantsResolvedStatus]    Script Date: 07/10/2021 13:24:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
    
     
CREATE Procedure [dbo].[uspUpdateCompliantsResolvedStatus]        
@SelectedColumnData varchar(100),    
@Compliantstatus  bit ,   
@CreatedBy Varchar(256) 
AS          
Begin          
    
 UPDATE  [dbo].[tblComplaintsForm] SET ComplaintClosed=@Compliantstatus, [Action] = 'U', ModifyingUser = @CreatedBy WHERE Id IN(SELECT CAST(value AS INTEGER) FROM STRING_SPLIT(@SelectedColumnData, ','))    
    
End 
GO
USE [master]
GO
ALTER DATABASE [CCSQA] SET  READ_WRITE 
GO
