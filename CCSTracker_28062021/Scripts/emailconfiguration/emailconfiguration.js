﻿
$(document).ready(function () {
    var end = $('#endDate').val();

    var rec = $('#Rec').val();

    if (rec == "6") {
        $("#weekly").hide();
        $("#monthly").show();
        $('.monthly').prop('checked', true);

    }

    

    if (end == "") {
        $("#stopSchedule").attr("checked", false);
        $("#endDate").attr("disabled", "disabled");
    }
    else {
        $("#stopSchedule").attr("checked", true);
        $("#endDate").removeAttr("disabled");
    }


    var freqvalue = "";
    freqvalue = $('input[name="RecurrsionType"]:checked').val();
    if (freqvalue == "4") {
        $("#weekly").show();
        $("#monthly").hide();

    }
    else if (freqvalue == "5") {
        $("#weekly").hide();
        $("#monthly").show();
    }
   

    $('input[name="RecurrsionType"]').click(function () {
        freqvalue = $(this).val();
        if (freqvalue == "4") {
            $("#weekly").show();
            $("#monthly").hide();

        }
        else if (freqvalue == "5") {
            $("#weekly").hide();
            $("#monthly").show();
        }
    });
    
    $("#stopSchedule").change(function () {
        var stop = $('input[name="stopSchedule"]:checked').val();
        if (stop == undefined) {
            $("#endDate").removeAttr("required");
            $("#endDate").attr("disabled", "disabled");
            $("#endDate").val('yyyy-MM-dd');
        }
        else {
            $("#endDate").removeAttr("disabled");
            $("#endDate").attr("required", "required");
        }
    });

    $('#endDate,#startDate').change(function () {
        var check = IsEndDateLessThanStartDate();
        if (check == "false")
            return false;
    });

    //CLICK Function
    $("#btnSubmit").click(function () {
        
        var monthlyChecked = $('.weekly').is(':checked');
        var weeklyChecked = $('.monthly').is(':checked');

        if ($('#To').val().trim() != '' && $('#Body').val().trim() != '' && $('#Subject').val().trim() != '') {
            if (monthlyChecked != true && weeklyChecked != true) {
                ShowMessage('Please select any recurrence type i.e Weekly or Monthly');
                return false;
            }
        }
        

        //Check the recurrence type and based on that perform validations
        if (freqvalue == "4") {
            $('input:checkbox.weeklyclassname').each(function () {
                var sThisVal = (this.checked ? $(this).val() : "");
            });

            if ($('#To').val().trim() != '' && $('#Body').val().trim() != '' && $('#Subject').val().trim() != '') {
                if ($('.weeklyclassname:checked').length == 0) {
                    ShowMessage('Please select any weekly checkbox');
                    return false;
                }
            }
        }

        if (freqvalue == "5") {
            debugger;
            if ($('#To').val().trim() != '' && $('#Body').val().trim() != '' && $('#Subject').val().trim() != '') {
                var selectedLanguage = new Array();
                var checkedMonth = $('.monthclassname:checked').length;

                if (checkedMonth > 0) {
                    $('.monthclassname:checked').each(function () {
                        selectedLanguage.push(this.id);
                    });
                }

                
                    if ($('.monthclassname:checked').length == 0) {
                        ShowMessage('Please select any month');
                        $(".common").prop("disabled", true);
                        return false;
                    }

                    if ($('.monthclassname:checked').length > 0) {
                    $(".common").prop("disabled", false);
                    //return false;
                    }

                    var getReturnValue = isRangeAscending();
                    if (getReturnValue == "true") {
                        var getValue = CalendarDaysMonthValidation();
                        if (getValue == "false")
                            return false;

                    }
                    else {
                        return false;
                    }
                

                var check = IsEndDateLessThanStartDate();
                if (check == "false")
                    return false;

            }
        }
        

    });

    $('#January').change(function () {
        if ($(this).is(":checked")) {
            $(".common").prop("disabled", false);
        }
    });

    $('#February').change(function () {
        if ($(this).is(":checked")) {
            $(".common").prop("disabled", false);
        }
    });

    $('#March').change(function () {
        if ($(this).is(":checked")) {
            $(".common").prop("disabled", false);
        }
    });

    $('#April').change(function () {
        if ($(this).is(":checked")) {
            $(".common").prop("disabled", false);
        }
    });

    $('#May').change(function () {
        if ($(this).is(":checked")) {
            $(".common").prop("disabled", false);
        }
    });

    $('#June').change(function () {
        if ($(this).is(":checked")) {
            $(".common").prop("disabled", false);
        }
    });

    $('#July').change(function () {
        if ($(this).is(":checked")) {
            $(".common").prop("disabled", false);
            var passTheMOnthValPart = $(this).attr("Id");
        }
    });

    $('#August').change(function () {
        if ($(this).is(":checked")) {
            $(".common").prop("disabled", false);
            var passTheMOnthValPart = $(this).attr("Id");
        }
    });

    $('#September').change(function () {
        if ($(this).is(":checked")) {
            $(".common").prop("disabled", false);
            var passTheMOnthValPart = $(this).attr("Id");
        }
    });

    $('#October').change(function () {
        if ($(this).is(":checked")) {
            $(".common").prop("disabled", false);
        }
    });

    $('#November').change(function () {
        if ($(this).is(":checked")) {
            $(".common").prop("disabled", false);
        }
    });

    $('#December').change(function () {
        if ($(this).is(":checked")) {
            $(".common").prop("disabled", false);
        }
    });

});


function IsAllMonthSelected(alertMessage) {
    if (ifTrue("#January") || ifTrue("#March") || ifTrue("#July") || ifTrue("#August") || ifTrue("#October") || ifTrue("#December")) {
        return true;
    }
    else if (ifTrue("#April") || ifTrue("#June") || ifTrue("#September") || ifTrue("#November")) {
        return true
    }
    else if (ifTrue("#February")) {
        return true;
    }
    ShowMessage(alertMessage);
    return false;
}

function IsEndDateLessThanStartDate() {
    var startDate = new Date($('#startDate').val());
    var endDate = new Date($('#endDate').val());
    if (endDate <= startDate) {
        ShowMessage("End Date should be always greater than Start Date");
        return "false";
    }
    else {
        return "true";
    }
}
function IsWeekSelected(alertMessage) {

    if (ifTrue("#Sunday") || ifTrue("#Monday") || ifTrue("#Tuesday") || ifTrue("#Wednesday") || ifTrue("#Thursday") || ifTrue("#Friday") || ifTrue("#Saturday") || ifTrue("#Sunday")) {
        return true;
    }
    ShowMessage(alertMessage);
    return false;
}

class MonthDayResult {
    errorMsg = ""; isError = false;
}

function ifTrue(monthVal) {
    var queryVal = $(monthVal).prop('checked');
    if (queryVal == "True" || queryVal == "true" || queryVal == true) {
        return true;
    }
}
// program to check leap year
function checkLeapYear(year) {
    //leap year
    //three conditions to find out the leap year
    if (year % 4 == 0 && year % 100 != 0) {

        return true;
    }
    else if (year % 400 == 0 && year % 100 == 0 && year % 4 == 0) {

        return true;
    }
    else if (year % 4 != 0) {

        return false;
    }
    else if (year % 400 != 0 || year % 100 == 0) {

        return false;
    }

}

function isAscending() {
    
    var ondaysText = $("#ondaystext").val();

    if (ondaysText != '' || ondaysText != undefined) {
        var replaceString = ondaysText.replace('-', ',');
        var ondaysArray = replaceString.split(",");

        var arrLength = ondaysArray.length;
       
        if (ondaysArray[arrLength - 1] != "") {
            for (var i = 0; i < ondaysArray.length; i++) {
                if (Number(ondaysArray[i]) > Number(ondaysArray[i + 1])) {
                    ShowMessage('Calendar Days are not in Ascending Order');
                    return false;
                    break;
                }
            }
            return true;
        }
        
    }

    
}
function CalendarDaysMonthValidation() {
    var jan = $('#January').is(':checked');
    var feb = $('#February').is(':checked');
    var mar = $('#March').is(':checked');
    var april = $('#April').is(':checked');
    var may = $('#May').is(':checked');
    var june = $('#June').is(':checked');
    var july = $('#July').is(':checked');
    var aug = $('#August').is(':checked');
    var sept = $('#September').is(':checked');
    var oct = $('#October').is(':checked');
    var nov = $('#November').is(':checked');
    var dec = $('#December').is(':checked');
    var ondaysText = $("#ondaystext").val();
    var date = new Date($('#startDate').val());
    if (ondaysText.includes(',') || ondaysText.includes('-')) {
        if (ondaysText != "" || ondaysText != undefined || ondaysText.length != 0) {
            var replaceString = ondaysText.replace('-', ',');
            var ondaysArray = replaceString.split(",");
        }
        if (feb == true) {
            if (ondaysArray != "" || ondaysArray != undefined || ondaysArray.length != 0) {
                for (var i = 0; i < ondaysArray.length; i++) {
                    if (ondaysArray[i] > 28 && !checkLeapYear(date.getFullYear())) {

                        ShowMessage("More than 28 is not allowed since feb is selected and year selected is not a leap year");
                        return "false";
                        break;
                    }
                    else if (ondaysArray[i] >= 30 && checkLeapYear(date.getFullYear())) {
                        ShowMessage("More than 29 is not allowed since feb is selected and year selected is leap year");
                        return "false";
                        break;
                    }
                }
            }
            var date = new Date($('#startDate').val());
        }

        else if (april == true || june == true || sept == true || nov == true) {
            if (ondaysArray != "" || ondaysArray != undefined || ondaysArray.length != 0) {
                for (var i = 0; i < ondaysArray.length; i++) {
                    if (ondaysArray[i] > 30) {
                        ShowMessage("More than 30 is not allowed since month with 30 days is selected");
                        return "false";
                        break;
                    }
                }
            }

        }

        else if (jan == true || mar == true || may == true || july == true || aug == true || oct == true || dec == true) {
            if (ondaysArray != "" || ondaysArray != undefined || ondaysArray.length != 0) {

                for (var i = 0; i < ondaysArray.length; i++) {
                    if (ondaysArray[i] > 31) {
                        ShowMessage("More than 31 is not allowed since month with 31 days is selected");
                        return "false";
                        break;
                    }
                }
            }
        }
    }
    else {
        if (feb == true) {
            if (Number(ondaysText) > 28 && !checkLeapYear(date.getFullYear())) {
                ShowMessage("More than 28 is not allowed since feb is selected and year selected is not a leap year");
                return "false";
            }
            else if (Number(ondaysText) >= 30 && checkLeapYear(date.getFullYear())) {
                ShowMessage("More than 29 is not allowed since feb is selected and year selected is leap year");
                return "false";
            }
        }
        else if (april == true || june == true || sept == true || nov == true) {
            if (Number(ondaysText) > 30) {
                ShowMessage("More than 30 is not allowed since month with 30 days is selected");
           
                return "false";

            }
        }

        else if (jan == true || mar == true || may == true || july == true || aug == true || oct == true || dec == true) {

            if (Number(ondaysText) > 31) {
                ShowMessage("More than 31 is not allowed since month with 31 days is selected");
                return "false";

            }
        }

    }
    return "true";
}


function isRangeAscending() {
    // alert('range');
    //$("#btnSubmit").attr("disabled", false);
   // alert('Inside Range');
    var ondaysText = $("#ondaystext").val();
    
    
        //var arrLength = ondaysText.length;
        for (var i = 0; i < ondaysText.length - 1; i++) {
            if (ondaysText[i] == '-') {
                if (ondaysText[i - 1] > ondaysText[i + 1]) {
                    ShowMessage('Range is not in Ascending order');
                    //$("#btnSubmit").attr("disabled", true);
                    return "false";
                    //break;
                }
                if (ondaysText[i - 1] < ondaysText[i + 1]) {
                    if (ondaysText[i + 2] == '-') {
                        ShowMessage('Improper Format');
                        //$("#btnSubmit").attr("disabled", true);
                        return "false";
                        //break;
                    }
                }


            }

    }
    return "true"; 
}




