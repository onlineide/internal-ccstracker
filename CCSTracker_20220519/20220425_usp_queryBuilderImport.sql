USE [CCSQA]
GO
/****** Object:  StoredProcedure [dbo].[usp_queryBuilderImport]    Script Date: 25/04/2022 12:41:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- [usp_CompletionDiaryImport] 
ALTER Procedure [dbo].[usp_queryBuilderImport]              
@queryBuilderType [tblQueryBuilderMockImport] readonly
--,
--@trackerid  int,
--@viewname varchar(100)
            
AS                
Begin           




DELETE FROM [tblMultiViewQueryBulderDetails] WHERE TrackerRefID in (SELECT top 1 TrackerRefID FROM @queryBuilderType) AND ViewName=(SELECT top 1 ViewName FROM @queryBuilderType)

INSERT INTO [dbo].[tblMultiViewQueryBulderDetails](
TrackerRefID,
ViewName,
ColumnRefID,
OperatorRefID,
Condition,
SearchName,
IsOrdered
)
SELECT 
	TrackerRefID,
ViewName,
ColumnRefID,
OperatorRefID,
Condition,
SearchName,
SortValue
	FROM @queryBuilderType 

	DECLARE @queryString Nvarchar(max)
	SELECT @queryString= dbo.FuncDynamicQueryResult((SELECT top 1 TrackerRefID FROM @queryBuilderType),(SELECT top 1 ViewName FROM @queryBuilderType))
	IF @queryString IS NOT NULL
	BEGIN
		INSERT INTO tblquerybuilderlog
		VALUES((SELECT top 1 TrackerRefID FROM @queryBuilderType),(SELECT top 1 ViewName FROM @queryBuilderType),@queryString,getdate())
    END

End      
      
