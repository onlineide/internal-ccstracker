USE [CCSQA]
GO
/****** Object:  StoredProcedure [dbo].[usp_CardPaymentRequestsForm_AuditView]    Script Date: 28/04/2022 15:13:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


--Exec usp_CardPaymentRequestsForm_AuditView 2,null,null
ALTER PROCEDURE [dbo].[usp_CardPaymentRequestsForm_AuditView]
	-- Add the parameters for the stored procedure here
	@ID Int,  
	@FromDate datetime2 = '1900-01-01 00:00:00.0000000', 
	@ToDate datetime2	= '9999-12-31 23:59:59.9999999'
AS
BEGIN

  BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    
;WITH T
AS (
    SELECT		ID
				,MatterID as MatterID , lag(MatterID) over(order by enddate asc) as Prev_MatterID
				,ClientName as ClientName , lag(ClientName) over(order by enddate asc) as Prev_ClientName
				,ContactNumber as ContactNumber , lag(ContactNumber) over(order by enddate asc) as Prev_ContactNumber
				,DateTimeToCall as DateTimeToCall , lag(DateTimeToCall) over(order by enddate asc) as Prev_DateTimeToCall
				,Amount as Amount , lag(Amount) over(order by enddate asc) as Prev_Amount
				,PaymentReason as PaymentReason , lag(PaymentReason) over(order by enddate asc) as Prev_PaymentReason
				,PaymentTaken as PaymentTaken , lag(PaymentTaken) over(order by enddate asc) as Prev_PaymentTaken
				,ISNULL(UnableToContact,0) as UnableToContact , ISNULL(lag(UnableToContact) over(order by enddate asc),0) as Prev_UnableToContact
				,CANotes as CANotes , lag(CANotes) over(order by enddate asc) as Prev_CANotes
				,RequesterName as RequesterName , lag(RequesterName) over(order by enddate asc) as Prev_RequesterName
				,ClientIsExpectingCall as ClientIsExpectingCall , lag(ClientIsExpectingCall) over(order by enddate asc) as Prev_ClientIsExpectingCall
				,AttachedFile as AttachedFile , lag(AttachedFile) over(order by enddate asc) as Prev_AttachedFile
				,ModifyingUser as ModifyingUser , lag(ModifyingUser) over(order by enddate asc) as Prev_ModifyingUser
				,Action as Action , lag(Action) over(order by enddate asc) as Prev_Action
				,BeginDate
				,EndDate
				,ROW_Number() over(partition by  Id Order by EndDate asc) as VersionNumber
    FROM dbo.tblCardPaymentRequestForm FOR SYSTEM_TIME ALL
    WHERE id =@ID
)
SELECT T.BeginDate,
       T.EndDate,
       C.ColName as ColumnName,
	   t.VersionNumber,	   
       C.present_Value as CurrentValue,
       C.prev_value as PreviousValue,
	   T.ModifyingUser as ModifiedBy
FROM T
    CROSS APPLY
    (
        VALUES
			('Matter number', Convert(varchar,T.MatterID) , Convert(varchar,T.Prev_MatterID))
			,('Client Name(Full Name)', Convert(varchar, T.ClientName) , Convert(varchar,T.Prev_ClientName))
			,('Contact Numbers', Convert(varchar, T.ContactNumber) , Convert(varchar,T.Prev_ContactNumber))
			,('Date & Time To Call', Convert(varchar(10), T.DateTimeToCall,103)+' '+ SUBSTRING( convert(varchar, T.DateTimeToCall,108),1,5) , Convert(varchar(10),T.Prev_DateTimeToCall,103)+' '+ SUBSTRING( convert(varchar, T.Prev_DateTimeToCall,108),1,5))
			,('Amount', Convert(varchar, T.Amount) , Convert(varchar,T.Prev_Amount))
			,('Reason for Payment', Convert(varchar, T.PaymentReason) , Convert(varchar,T.Prev_PaymentReason))
			,('Payment Taken', Convert(varchar, T.PaymentTaken) , Convert(varchar,T.Prev_PaymentTaken))
			,('(CA only) Unable to contact', Convert(varchar, case when T.UnableToContact =1 then 'Yes' else 'No' end) , Convert(varchar,case when T.Prev_UnableToContact =1 then 'Yes' else 'No' end))
			,('CA (Only) Notes', Convert(varchar, T.CANotes) , Convert(varchar,T.Prev_CANotes))
			,('Requestor Name', Convert(varchar, T.RequesterName) , Convert(varchar,T.Prev_RequesterName))
			,('I confirm that the client is expecting call', Convert(varchar,case when T.ClientIsExpectingCall=1 then 'Yes' else 'No' end) , Convert(varchar,case when T.Prev_ClientIsExpectingCall=1 then 'Yes' else 'No' end))
			,('Attach a file:', Convert(varchar, T.AttachedFile) , Convert(varchar,T.Prev_AttachedFile))
			,('ModifyingUser', Convert(varchar, T.ModifyingUser) , Convert(varchar,T.Prev_ModifyingUser))
			,('Action', Convert(varchar, T.Action) , Convert(varchar,T.Prev_Action))
			) AS C (ColName, present_Value, prev_value)
WHERE  EXISTS
(
    SELECT present_Value 
    EXCEPT 
    SELECT C.prev_value
)  and Convert(datetime,BeginDate) >= @FromDate and Convert(Datetime,BeginDate) <=@ToDate

END TRY
BEGIN CATCH

        DECLARE @ErrorMessage varchar(4000) = ERROR_MESSAGE(),
                @ErrorSeverity INT = ERROR_SEVERITY(),
                @ErrorNumber INT = ERROR_NUMBER();

		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorNumber);
 
END CATCH
END
